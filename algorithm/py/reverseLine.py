def reverseLine(filename):
    with open(filename, 'r') as text:
        for line in text:
            temp = line.split()
            for word in temp[::-1]:
                print(word, end=' ')
            print()

file = 'algorithm/temp.txt'
reverseLine(file)
