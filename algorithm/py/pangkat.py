def pangkat(n, m):
    if m <= 0:
        return 1
    else:
        return n * pangkat(n, m-1)

print(pangkat(3, 3))