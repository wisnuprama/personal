def fib(rng):
    a, b, x = 0, 1, 1
    print(a, end=' ')
    for fib in range(rng):
        a, b = b, x
        x = a + b
        print(a, end=' ')
    print()

def fib2(rng):
    counter = 0
    a, b, x = 0, 1, 1
    for fib in range(rng):
        a, b = b, x
        x = a + b
        counter += a
    return counter

def fibonacci(n):
    if n < 2:
        return 1
    else:
        return fibonacci(n-1) + fibonacci(n-2)