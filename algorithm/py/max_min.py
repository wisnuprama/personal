def max(container=list()):
    a = container[0]
    for i in range(1, len(container)):
        if container[i] > a:
            a = container[i]
    return a

def min(container=list()):
    a, n = container[0], len(container)
    for i in range(1, n):
        if container[i] < a:
            a = container[i]
    return a

lst = (5,33,4,6,73,48,7,58,95,3,5,74,75,105,79)
# string = 'abcdeDjaBDAUWbdaubUYAWYUVDtvVAdvwuyaVYTAVWDViVDAWhs'
max(lst)