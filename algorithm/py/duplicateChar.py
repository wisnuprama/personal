def isDuplicate(string):
    '[Done] exited with code=0 in 0.107 seconds'
    count = 1
    tmp = string[0] # temp char

    # start looping
    while count < len(string):
        for char in string[count:]:
            if char == tmp: return True
        # replace with next char
        tmp = string[count]
        count += 1
    return False

def isDuplicate2(string):
    '[Done] exited with code=0 in 0.092 seconds'
    lst = list() # initiate list as container

    # check every char with for loop
    for char in string:
        if char not in lst:
            lst.append(char)
        else:
            return True # if there is in list
    return False
