# GCD ALGORITHM USING RECURSION
# BASED ON EUCLIDEAN ALGORITHM
# created by Wisnu Pramadhitya R

def gcd(angka, angka2):
    if angka2 > 0:
        return gcd(angka2, angka % angka2)
    return angka

def gcd2(angka, angka2):
    while angka2 != 0:
        r = angka % angka2 # remainder
        angka = angka2
        angka2 = r
    return angka


if __name__ == "__main__":
    print(gcd(10200, 4000220))