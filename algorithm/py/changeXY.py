def changeXY(string):
    if len(string) == 0:
        return ''
    else:
        if string[:2] == 'xy':
            return '*'+changeXY(string[2:])
        else:
            return string[0] + changeXY(string[1:])

print(changeXY('xxyxyyyxy'))