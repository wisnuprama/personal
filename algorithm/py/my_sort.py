def sort(arr=list()):
    return insertion(arr)

def insertion(arr=list()):
    for i in range(1, len(arr)):
        key = arr[i]
        # insert arr[i] into the sorted sequence
        j = i - 1
        while j >= 0 and arr[j] > key:
            arr[j + 1] = arr[j]
            j -= 1
        arr[j + 1] = key
    return arr

lst = [x for x in range(0,100000)]