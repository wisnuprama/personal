"""
Source Code   : calendarComp.py
Author        : Wisnu Pramadhitya R
Created       : October 04 - Oktober 14, 2016
Description   : aplikasi Kalender sederhana yang mampu menampilkan kalender 
per bulan sesuai bulan dan tahun yang dimasukkan oleh pengguna.
"""
mdays = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
def isLeapYear(year):
    'Return True if leap year, False if not.'
    return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)

def getYearDays(frYear, toYear):
    'Return calculation of total days from 1800-year\nday: int total days\nyear: int 1800-2099\n'
    day = 0
    for i in range(frYear, toYear):
        if (isLeapYear(i)): day += 1 # +1 if the year is leap year
        day += 365
    return day

def getMonthDay(year, month):
    'Return calculation of total days from 1 January -> month\nday: \
    int total days\nyear = int 1800-2099\nmonth: int 1-12'
    day = 0
    for i in range(1, month):
        if (i == 2 and isLeapYear(year)): day += 29 # +29 if the year is leap year and february
        else: day += mdays[i]
    return day