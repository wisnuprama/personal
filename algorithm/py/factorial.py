state = [1]
def factorial(n, i):
    if n == 0:
        print(state[i])
        return
    state.append(state[i]*n)
    factorial(n-1, i+1)

def factorial2(n):
    if n == 0:
        return 1
    return factorial2(n-1)*n

def factorial3(n):
    k = 1
    while(n > 0):
        k *= n
        n -= 1
    return k


print(factorial3(100))