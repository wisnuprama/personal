from turtle import *

def koch(n):
    if n == 0:
        return 'F'
    tmp = koch(n-1)
    return tmp + 'L' + tmp + 'R' + tmp + 'L' + tmp

def drawSnowflake(n):
    s, t = Screen(), Turtle()
    directions = koch(n)
    t.speed(0)
    for i in range(n):
        for move in directions: # draw koch(n)
            if move == 'F':
                t.fd(3000/3**n)
            if move == 'L':
                t.lt(60)
            if move == 'R':
                t.rt(120)
        t.rt(120)

drawSnowflake(50) 