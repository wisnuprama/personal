def doubleS(string=str()):
    if len(string) == 0:
        return ''
    else:
        head, tail = string[0], string[1:]
        if head.lower() == 's':
            head = head*2
        return head + doubleS(tail)

print(doubleS('wosossow'))