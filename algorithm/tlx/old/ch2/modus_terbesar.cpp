#include <bits/stdc++.h>
using namespace std;

int main(){
    int arr[100001] {};
    int N;

    cin >> N;

    int i, terbesar=0;
    while(N--){
        cin >> i;
        arr[i]++;

        if(terbesar < i) terbesar = i;
    }


    int modus=0, big=0;
    for(int i=1; i <= terbesar; ++i){
        if(arr[i] > big){
            big = arr[i];
            modus = i;
        }
    }

    cout << modus << ' ';
    for(int i=1; i <= terbesar; ++i){
        if(i != modus && big == arr[i]) cout << i << ' ';
    }

    return 0;
}