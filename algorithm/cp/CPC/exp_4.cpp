#include <bits/stdc++.h>
#define LL long long
using namespace std;

LL exp(int n, LL wow){
    LL x;
    if(wow%2 == 0 && wow != 0){
        x = exp(n, wow/2);
        return x*x; 
    }
    else if (wow%2!=0 && wow!=1){
        x = exp(n, wow/2);
        return x*x*n;
    }
    else if (wow == 1) return wow;

    return 1;
}

int main(){
    int n = 4;
    int m = 600000;

    cout << exp(n, m);
}