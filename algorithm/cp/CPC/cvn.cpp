#include <bits/stdc++.h>

char alpha[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

std::string convert(int col){
    std::string str = "";

    while(col > 0){
        if(col % 26 == 0){
            col = col / 26 -1;
            str += alpha[25];
        } else {
            int c = col % 26 -1;
            col /= 26;

            str += alpha[c];
        }
    }

    std::reverse(str.begin(), str.end());
    return str;
}

int main(){
    int n;

    std::cin >> n;

    int v[n] = {};
    for(int i=0; i < n; ++i){
        std::cin >> v[i];
    }

    for(int j=0; j < n; ++j){
        std::cout << "CASE #" << j+1 << ": " << convert(v[j]) << std::endl;
    }
}

