#include <bits/stdc++.h>
using namespace std;

void merge(int a[], int tmp[], int first, int middle, int last){
    int p1 = first;
    int p2 = middle;
    int p = first;

    while(p <= last){
        if(p1 < middle && (p2>last || a[p1] < a[p2])){
            tmp[p++] = a[p1++];
        } else {
            tmp[p++] = a[p2++];
        }
    }
    
    for(int i=first; i <=last; ++i){
        a[i] = tmp[i];
    }
}

void mergesort(int arr[], int tmp[], int first, int last){
    if(first < last){
        int middle = (first + last + 1) / 2;
        mergesort(arr, tmp, first, middle-1);
        mergesort(arr, tmp, middle, last);
        merge(arr, tmp, first, middle, last);
    }
}

void sort(int arr[], int size){
    int tmp[size] {};
    mergesort(arr, tmp, 0, size-1);
}

bool search(int x, int a[], int size){
    int beg = 0;
    int ends = size - 1;

    while(beg < ends){
        int mid = (beg + ends) / 2;
        if(a[mid] == x){
            return x;
        } else if(a[mid] < x){
            beg = mid + 1;
        } else {
            ends = mid - 1;
        }
    }

    return 0;
}

void subset(vector<string> &x, int pos, string cut, vector<string> &res)
{
    if(pos == x.size()) {
        res.push_back(cut);            
        return;            
    }
    subset(x, pos+1, cut, res);
    subset(x, pos+1, cut + " " + x[pos], res);
}

int main(){
    
    int n, m;
    cin >> n >> m;

    int wow[n] {};
    vector<vector<int>> sub;

    for(int i=0; i < n; ++i){
        cin >> wow[i];
    }

    set<int> set(begin(wow), end(wow));
    
    int counting = 0;
    for(int j=0; j<n; ++j){
        int lol = wow[j];
        cout << lol << " ";
    }
    
    cout << counting;
}