// PROBLEM SET: Not Afraid
#include<string>
#include<iostream>

int main() {

    std::string input, line;
    // number of universe and number of group
    int n;
    int m;
    int size;
    int num;
    bool afraid;
    const int OFFSET = 20005;
    const int CENTER0 = 10000;

    // ask universe and number of group
    std::cin >> n >> m;

    while (--m >= 0) {
        afraid = true;
        bool adagaksih[OFFSET] = {};
        
        std::cin >> size;
        if(size < 2) break;

        for(int i=0; i<size; ++i) {
            std::cin >> num;
            num += CENTER0;
            adagaksih[num] = true;
        }

        // from 1 to n (inclusive)
        for(int i=0; i<=n; ++i) {
            //std::cout << CENTER0+i << " " << CENTER0-i << std::endl;
            if(adagaksih[CENTER0+i] && adagaksih[CENTER0-i]){
                afraid = false;
                break;
            }
        }
        
        // gotcha!
        if(afraid){
            std::cout << "YES";
            break;
        }
    }

    if(!afraid) std::cout << "NO";    
    return 0;
}