#include <iostream>
#include <string>
#include <algorithm>
#include <cmath>
#include <ctype.h>
#include <stdio.h>
#define LL long long

// pre-defined
char alpha[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

// XXYY
std::string popSys(std::string row, LL col){
    std::string str = "";

    while(col > 0){
        if(col % 26 == 0){
            col = col / 26 -1;
            str += alpha[25];
        } else {
            int c = col % 26 -1;
            col /= 26;

            str += alpha[c];
        }
    }

    std::reverse(str.begin(), str.end());
    return str + row;
}

// RXCY
std::string popOth(std::string row, std::string col) {
    const int BASE = 26;
    LL iniCol = 0;

    for(int i=0, n=col.size(); i < col.size(); ++i){
        iniCol += std::pow(BASE, i) * ((int) col[--n] - 64);
    }

    return "R" + row + "C" + std::to_string(iniCol);
}

std::string convert(std::string in){
    bool rxcx = false;
    int posC = in.find('C');
    std::string res;

    if(in[0] == 'R' && ('0' <= in[1] && in[1] <= '9') && posC > 1) rxcx = true;

    std::string rr, cc;
    if(rxcx){
        rr = in.substr(1, posC-1);
        cc = in.substr(posC+1);
        long col = std::atoi(cc.c_str());
        res = popSys(rr, col);

    } else {
        rr = "";
        cc = "";
        for(int i=0; i < in.size(); ++i){
            if('0' <= in[i] && in[i] <= '9') rr+=in[i];
            else cc += in[i];
        }

        res = popOth(rr, cc);
    }

    return res;
}

int main(){
    // untuk A - 64 Z 89
    // +25
    std::string input;
    int n;

    scanf("%d\n", &n);
    std::string mau_di_keluarin[n] = {};

    for(int i=0; i<n; ++i){
        getline(std::cin, mau_di_keluarin[i]);
    }

    for(int j=0; j<n; ++j){
        std::cout << convert(mau_di_keluarin[j]) << std::endl;
    }

    return 0;
}