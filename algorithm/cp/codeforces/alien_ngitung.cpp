#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main(){
    vector<int> trace;

    int n,m;
    cin >> n;
    m=n;

    while(n > 1){
        if(n % 2 == 0){
            trace.push_back(n/=2);
        } else {
            trace.push_back(--n);
        }
    }

    cout << trace.size() << std::endl;

    for(int i=trace.size()-1; i >= 0; --i){
        cout << trace[i] << std::endl;
    }

    cout << m << std::endl;
    
}

