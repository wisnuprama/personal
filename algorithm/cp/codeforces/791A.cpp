// PROBLEM SET: Bear and Big Brother
#include<algorithm>
#include<stdio.h>

int bear(int a, int b, int res){
    if(a > b){
        return res;
    }
    return bear(a*3, b*2, res+1);
}


int main(){
    int b, bb;
    
    scanf("%d %d", &b, &bb);
    
    printf("%d", bear(b, bb, 0));

    return 0;
}
