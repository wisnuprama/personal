// PROBLEM SET: Theater Square
#include <iostream>
#include <cmath>
#define LL long long

int main(){
    double n;
    double m;
    double a;

    std::cin >> n >> m >> a; 

    // ini gak bisa dihitung perluas
    // jadi hitung dulu perpanjang dan perlebarnya butuh berapa
    // sehingga ketika kita sudah tahu butuh berapa, kita bisa mencari
    // berapa banyak yang dibutuhkan.
    // karena boleh lebih luas dari Theater Square nya jadi
    // gak masalah kalo misal lebih satu kramik, syarat:
    // lebih satu kramik ketika panjang/lebar dibagi panjang keramik
    // menghasilkan angka ganjil.
    
    LL row = ceil(n / a);
    LL col = ceil(m / a);

    LL res = row * col;

    std::cout << res << std::endl;
    return 0;
} 