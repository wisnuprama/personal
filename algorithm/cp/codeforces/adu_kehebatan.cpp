#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#define LL long long
using namespace std;

int main(){

    int n;
    LL m;
    cin >> n >> m;

    LL kotakAJAIB[n] = {};
    int counting = 0;
    
    for(int i=0; i<n; ++i){
        cin >> kotakAJAIB[i];
    }

    for(int i=0; i<n; ++i){
        LL a = kotakAJAIB[i];
        if(a <= m){
            counting++;
            for(int j=i+1; j<n; ++j){
                a += kotakAJAIB[j];

                if(a <= m) counting++;
                else break;
            }
        }

    }

    cout << counting;

    return 0;
    
}