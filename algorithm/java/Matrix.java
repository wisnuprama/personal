package java.algorithm;

public class Matrix{

    private int[][] mat;
    private int row;
    private int col;

    public static void main(String[] args){
        Matrix lol = new Matrix(3, 3);
        int o = 1;
        for(int i=0; i<2; ++i){
            for(int j=0; j<3;++j){
                lol.set(i, j, o++);
            } 
        }

        System.out.println(lol);
        System.out.println(lol.transpose());

        System.out.println(lol.sumDiagonal());

    }

    public Matrix(int row, int col){
        this.mat = new int[row][col];
        this.row = row;
        this.col = col;
    }

    public void set(int irow, int icol, int element){
        this.mat[irow][icol] = element;
    }

    public int get(int irow, int icol){
        return this.mat[irow][icol];
    }

    public int row(){
        return this.row;
    }

    public int col(){
        return this.col;
    }

    public int sumDiagonal(){
        if(this.row != this.col)
            return -1;
        
        int sum = 0;
        int i = this.row;

        while(--i >= 0){
            sum += this.mat[i][i];
        }

        return sum;
    }

    public Matrix sum(Matrix other){
        // check ordo
        if(this.row != other.row() && this.col != other.col())
            return new IllegalArgumentException();
        
        Matrix result = new Matrix(this.row, this.col);

        int sum;
        for(int i=0; i<this.row; ++i){
            for(int j=0; j<this.col; ++j){
                sum = 0;
                sum = this.mat[i][j] + other.get(i, j);
                result.set(i, j, sum);
            }
        }

        return result;
    }

    public boolean isIdentity(){
        int i=this.row;
        while(--i>=0){
            if(mat[i][i] != 1)
                return false;
        }

        //@TODO untuk 0

        return true;
    }

    public Matrix transpose(){
        // ordo di balik
        Matrix result = new Matrix(this.col, this.row);

        int element;
        for(int i=0; i<this.row; ++i){
            for(int j=0; j<this.col; ++j){
                element = this.mat[i][j];
                result.set(j, i, element);
            }
        }

        return result;
    }

    public String toString(){
        StringBuilder str = new StringBuilder();

        for(int i=0; i<this.row; ++i){
            str.append("| ");
            for(int j=0; j<this.col; ++j){
                str.append(this.mat[i][j] + " ");
            }
            str.append("|\n");
        }
        
        return str.toString();
    }
}