import java.util.*;

public class StringUtil
{
        private static String reverseString (String str, String result)
        {
            // implementasikan method ini
            if(str.length() == 0){
                return result;
            }

            char head = str.charAt(0);
            return reverseString(str.substring(1), head+result);
        }

        public static String reverse (String str)
        {
            return reverseString (str, "");
        }

        private static boolean isPalindromHelper(String str, int first, int last) {
            if (first >= last)
                return true;

            if (str.substring(last, last+1).matches("[0-9]"))
                return isPalindromHelper(str, first, last-1);

            else if (str.substring(first, first + 1).matches("[0-9]"))
                return isPalindromHelper(str, first+1, last);

            char head = str.charAt(first);
            char tail = str.charAt(last);

            if(head == tail)
                return isPalindromHelper(str, first+1, last-1);
            return false;

        }

        public static boolean isPalindromIgnoreNumber(String str){
            return isPalindromHelper(str, 0, str.length());
        }

        public static boolean isPalindromIgnoreCase(String str){
            return str.equalsIgnoreCase(StringUtil.reverse(str));
        }

        public static boolean isPalindrom(String str)
        {   
            // palindrom
            return str.equals(StringUtil.reverse(str));
        }

        public static List<String> subsetOfString(String str)
        {
            List<String> result = new ArrayList<>();
            subsetOfString(str, 0, "", result);
            return result;
        }

        private static void subsetOfString(String str, int pos, String cut, List<String> res)
        {
            if(pos == str.length()) {
                res.add(cut);
                return;
            }

            subsetOfString(str, pos+1, cut, res);
            subsetOfString(str, pos+1, cut + str.charAt(pos), res);
        }

        public static void main(String[] args){
            String x = "012";
            System.out.println(subsetOfString(x));
        }
}