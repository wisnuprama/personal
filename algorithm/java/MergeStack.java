import java.util.Stack;

public class MergeStack {
    public static void main(String args[]){
        Stack<Integer> s1 = new Stack<>();
        Stack<Integer> s2 = new Stack<>();

        Stack

        for(int i=1; i < 10; ++i){
            s1.push(i);
        }

        for(int i=0; i < 10; i+=2){
            s2.push(i);
        }

        System.out.println(s1);
        System.out.println(s2);
        
        System.out.println(mergeStack(s1, s2));
        
    }

    public static Stack<Integer> mergeStack(Stack<Integer> s1, Stack<Integer>  s2){
        Stack<Integer> temp = new Stack<>();

        while(!s1.isEmpty() || !s2.isEmpty()){
            if(!s1.isEmpty() && (s2.isEmpty() || s1.peek() > s2.peek())){
                temp.add(s1.pop());
            } else {
                temp.add(s2.pop());
            }
        }
        
        while(!temp.isEmpty()){
            s1.push(temp.pop());
        }

        return s1;
    }
}