import java.util.Arrays;

/**
 * Sorting array using mergesort
 */

public class MergeSort {

    private static void merge(int a[], int tmp[], int first, int middle, int last){
        // pointer untuk yang merge
        int p1 = first;
        int p2 = middle;

        // posisi untuk tmp array
        int p = first;

        while (p <= last) {
            // mencegah pointer array 1 overflow
            if (p1 < middle && (p2 > last || a[p1] < a[p2])) {
                tmp[p++] = a[p1++];
            }
            else {
                tmp[p++] = a[p2++];
            }
        }

        for (int i=first; i<=last; ++i) {
            a[i] = tmp[i];
        }
    }

    public static int[] mergesort(int[] arr){
        // array kosong atau sudah sorted := kardinalitas 1
        if(arr.length <= 1)
            return arr;

        int middle = arr.length / 2;
        
        // devide & conquer langsung arraynya
        int[] part1 = mergesort(Arrays.copyOfRange(arr, 0, middle));
        int[] part2 = mergesort(Arrays.copyOfRange(arr, middle, arr.length));
        
        return ArraysUtil.merge(part1, part2);
    }

    // mergesort
    private static void mergesort(int[] arr, int[] tmp, int first, int last){

        // check apakah index kiri masih lebih kecil dari index kanan
        // divide & conquer
        if(first < last) {
            int middle = (first + last + 1) / 2;
            mergesort(arr, tmp, first, middle - 1);
            mergesort(arr, tmp, middle, last);
            merge(arr, tmp, first, middle, last);
        }
    }

    // sort
    public static void sort(int[] arr){
        // array pembantu
        int[] tmp = new int[arr.length];

        mergesort(arr, tmp, 0, arr.length-1);
    }
}