import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Stack;

public class Tugas{
    
    public static void main(String args[]){
        String s = "Sophisti cated";
        int arr[] = {1, 2, 3, 4, 5};
        

        System.out.println(eraseDuplicate2(s));
    }

    public static String eraseDuplicate(String args){
        // using map to mapping each character and their numbers
        // implementing LinkedHashMap for well ordering character
        Map<Character, Integer> charMap = new LinkedHashMap<>();

        for(int i=0; i < args.length(); ++i){
            // get char at index i
            char c = args.charAt(i);
            
            // check if char at i is in map
            // if yes, +1 for char i
            if(charMap.containsKey(c)){
                // update for character c, +1
                charMap.put(c, charMap.get(c)+1);
            } else {
                charMap.put(c, 1);
            }
        }
        
        // construct string if the character has showed once only
        StringBuilder str = new StringBuilder();
        for(Character s : charMap.keySet()){
            if(charMap.get(s) == 1)
                str.append(s);    
        }

        return str.toString();
    }

    public static String eraseDuplicate2(String args){
        int alphabet[] = new int[26];

        for(int i=0; i < args.length(); ++i){
            try {
                char c = args.charAt(i);
                // true uppercase, false otherwise
                int index = (c <= 90) ? c-65 : c-97;
                alphabet[index]++;
            } catch (Exception e) {}
        }

       StringBuilder str = new StringBuilder();     
        for(int i=0; i < args.length(); ++i){
            try {
                char c = args.charAt(i);
                // true uppercase, false otherwise
                int index = (c <= 90) ? c-65 : c-97;

                if(alphabet[index] == 1)
                    str.append(c);
            } catch (Exception e) {}
        }

        return str.toString();
    }

    public static void sort(int arr[]){
        // implementing bubble sort
        for(int i=arr.length-1; i >= 0; i--){
            boolean swapped = false;

            for(int j=0; j < i; j++){
                /*
                    check if the right item of item at j is smaller than item at j
                    if true then swap it
                */
                if(arr[j] > arr[j+1]){
                    // swapping
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                    swapped = true;
                }
            }
            // check if no swap then array is sorted
            if(!swapped)
                return;
        }
    }

    public static void shiftArray(int[] arr, int shift){
        int temp[] = new int[arr.length];
        
        int j = 0;
        for(int i=shift; i < arr.length; ++i, ++j){
            temp[j] = arr[i];
        }

        for(int i=0; i<shift; ++i, ++j){
            temp[j] = arr[i];
        }

        for(int i=0; i<arr.length; ++i){
            arr[i] = temp[i];
        }
    }

}