import java.lang.Math;

public class MathUtil
{   
    private static final int[] primeSuccessor = {2,3,5,7,11};
    private static long faktorialHelper (int n, long m){
        if (n == 0) {
            return m;
        } else {
            return faktorialHelper(n-1, n*m);
        }
    }

    public static long faktorial(int n){
        return faktorialHelper(n, 1);
    }

    public static long kombinasi(int n, int r){
        long fakN = MathUtil.faktorial(n);
        long fakR = MathUtil.faktorial(r);
        long fakNR = MathUtil.faktorial(n-r);

        return fakN / (fakR * fakNR);
    }

    public static boolean compareFloat(double f1, double f2){
        final double eps = 1e-14;

        if (Math.abs(f1 - f2) < eps)
            return true;

        return false;
    }

    public static boolean isPrime(int n){

        for(int i:primeSuccessor){
            // is prime successor
            if(n == i) return true;

            // not a prime
            if(n % i == 0) return false;
        }

        return true;
    }
}