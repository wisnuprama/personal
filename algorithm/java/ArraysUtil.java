
/**
 * Created by wisnuprama
 * Project  : DDP2
 * Package  : ddp2.algoritma
 * Date     : 3/23/2017
 * Time     : 9:09 PM
 */

import java.util.Arrays;

public class  ArraysUtil {
    
    private static void rangeCheck(int arrayLength, int fromIndex, int toIndex) {
        if (fromIndex > toIndex) {
            throw new IllegalArgumentException(
                    "fromIndex(" + fromIndex + ") > toIndex(" + toIndex + ")");
        }
        if (fromIndex < 0) {
            throw new ArrayIndexOutOfBoundsException(fromIndex);
        }
        if (toIndex > arrayLength) {
            throw new ArrayIndexOutOfBoundsException(toIndex);
        }
    }

    // merge dua array yang berurutan

    /**
     * method helper merge untuk merge dua array.
     * @param arr array yang ingin di merge, ada baiknya sudah terurut
     * @param arr2 array kedua yang ingin di merge, ada baiknya sudah terurut
     * @param result array helper yang menampung hasil merge
     * @param first index 0 atau index pertama
     * @param middle index tengah (last + first) / 2
     * @param last index terakhir total panjang array - 1
     */
    public static void merging(int[] result, int[] tmp, int first, int middle, int last){

        // pointer arr
        int p1 = first;
        // pointer arr2
        int p2 = middle;
        // pointer result
        int p = first;

        // credit: skema intel developers
        while(p <= last){
            // kalo ada angka sama, yang didahulukan adalah yang p2
            // harapannya p2 kena last duluan
            if(p1 < middle && (p2 > last || tmp[p1] < tmp[p2])){
                result[p++] = tmp[p1++];
            }
            else {
                result[p++] = tmp[p2++];
            }
        }
    }

    /**
     * Merge dua array
     * @param arr array, yang ada baiknya sudah terurut
     * @param arr2 array, yang ada baiknya sudah terurut
     * @return array yang sudah merge
     */
    public static int[] merge(int[] arr, int[] arr2){
        // panjang total array penampung
        int n = arr.length + arr2.length;

        // array penampung
        int[] result = new int[n];
        int[] tmp = new int[n];
        
        /*BAKAL LAMA KALO ARRAYNYA BANYAK :(*/
        // jadikan satu dulu arraynya
        for(int i=0; i<arr.length; ++i)
            tmp[i] = arr[i];
        // melanjutkan index terakhir arr
        for(int i=arr.length, j=0; i < n; ++i)
            tmp[i] = arr2[j++];
        


        // untuk index tengah
        // index perpisahan dua array
        int middle = arr.length;

        merging(result, tmp, 0, middle, n-1);

        // proses merging
        return result;
    }

     private static int minHelper(int[] arr, int left, int right){
        if(left == right)
            return arr[left];

        if(right-left == 1)
            return arr[left] < arr[right] ? arr[left] : arr[right];

        int center = (left + right) / 2;
        int p1 = minHelper(arr, left, center);
        int p2 = minHelper(arr, center+1, right);

        return p1 < p2 ? p1 : p2;
    }

    public static int min(int[] arr){
        return minHelper(arr, 0, arr.length-1);
    }

    /**
     * 
     * @param arr array yang ingin dicopy
     * @param start index awal yang ingin dicopy (inclusive)
     * @param end index akhir yang ingin dicopy (exclusive)
     * @param newArr array penampung
     */
    private static void subArray(int[] arr, int start, int end, int[] newArr){
        if(start == end-1){
            newArr[start-1] = arr[start];
            return;
        }

        if(start == 0)
            newArr[start] = arr[start];
        else
            newArr[start-1] = arr[start];

        subArray(arr, start+1, end, newArr);
    }

    /**
     * lol
     * @param arr 
     * @param start
     */
    public static int[] subArray(int[] arr, int start){
        if(start <= 0 || start > arr.length-1)
            return arr;

        int[] result = new int[arr.length-(start)];
        subArray(arr, start, arr.length-1, result);
        
        return result;
    }

    public static int[] subArray(int[] arr, int start, int end){
        if(start < 0 || end <= start || end > arr.length)
            return arr; 

        int n = start + end;

        int[] result = new int[n];
        subArray(arr, start, end, result);
        
        return result;
    }

    /**
     * Menghitung jumlah even number di dalam sebuah array integer.
     * Method ini sebagai recursive helper.
     * @param arr array integer
     * @param pointer jumlah array
     * @param counter menghitung jumlah even number
     * @return jumlah even number
     */
    private static int countingEven(int[] arr, int pointer, int counter){
        if(pointer == 0) {
            if (arr[pointer] % 2 == 0)
                return counter+1;
            return counter;
        }

        if(arr[pointer--] % 2 == 0)
            return countingEven(arr, pointer, counter+1);
        return countingEven(arr, pointer, counter);
    }

    /**
     * Menghitung jumlah even number di dalam sebuah array integer.
     * @param arr array integer yang akan dihitung jumlah itemnya yang even number
     * @return jumlah even number
     */
    public static int countingEven(int[] arr){
        return countingEven(arr, arr.length, 0);
    }

    private static int binarySearch(int[] a, int fromIndex, int toIndex,
                                     int key) {
        int low = fromIndex;
        int high = toIndex - 1;

        while (low <= high) {
            int mid = (low + high) >>> 1;
            int midVal = a[mid];

            if (midVal < key)
                low = mid + 1;
            else if (midVal > key)
                high = mid - 1;
            else
                return mid; // ketemu yeay
        }
        return -(low + 1);  // gak ketemu :(
    }

    public static void main(String[] args){
        int[] ab = {2,3,5,6,8,12,16,90,120,130};
        int[] ac = {1,7,9,10,11,23,45,67,89,100,300,400};
        
        int n = ab.length + ac.length;
        int[] a = {9,8,7,6,56,7,8,8,7,8,6,7,5,4,3,5,6,7,8,98,78,3,47,7,66,5,669,966,46,663,65,45,2666,46,65,666,
                7,686,9,3,68,677,98,58,573,56,57,74,65,34,98,1,82,7,53,561,67,2,61,3,23,69,12,76,88,668,45,6,
                2,3,4,5,5,6,7,89,7,9,960,7,4,616,64,859,7,86,89,7,77,6,3,7,8,4,10};
        Arrays.sort(a);
        System.out.println(a.length);
        System.out.println(binarySearch(a, 0, a.length+1, 2666));
    }
}
