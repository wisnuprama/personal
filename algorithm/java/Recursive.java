import java.util.Arrays;
import java.util.Random;

public class Recursive{
    
    public static int trashing(int n){
        if(n == 1)
            return 1;
        
        return trashing(n-1) + n;
    }

    public static int bunnyEars(int n){
        if(n == 0)
            return 0;
        
        if(n%2 == 1) 
            return bunnyEars(n-1) + 2; 
        return bunnyEars(n-1) + 1;
    }

    public static String reverse(String str){
        if(str.length() == 0)
            return "";

        char head = str.charAt(0);
        String tail = str.substring(1);

        return reverse(tail) + head;
    }

    public static int jumlahA(String str){

        if(str.length() == 1){
            if(str.charAt(0) == target)
                return 1;
            return 0;
        }

        final char target = 'a';
        
        char head = str.charAt(0);
        if(head == target)
            return jumlahA(str.substring(1)) + 1;
        return jumlahA(str.substring(1));
        
    }

    public static int sumDigits(int number){
        if(number % 10 == number)
            return number;
        
        return sumDigits(number / 10) + number % 10;
    }

    public static int jumlahGenap(int number){
        int num = number % 10;

        if(num == number){

            if(num % 2 == 0)
                return 1;
            return 0;
        }
        
        if(num % 2 == 0)
            return jumlahGenap(number/10) + 1;
        return jumlahGenap(number/10);
    }
    
    public static int jumlah(int[] arr){

        if(arr.length == 0)
            return 0;
        
        return jumlah(Arrays.copyOfRange(arr, 1, arr.length)) + arr[0];
    }

    public static int jumlahRecc(int start, int[] arr){
        if(start == arr.length-1)
            return arr[start];

        return jumlahRecc(start+1, arr) + arr[start];

    }

    public static void printArray(int[] arr){
        if(arr.length == 0)
            return;

        System.out.println(arr[0]);
        printArray(Arrays.copyOfRange(arr, 1, arr.length));
    }

    public static void printItem(int start, int[] arr){
        if(start == arr.length-1){
            System.out.println(arr[start]);
            return;
        }
        printItem(start+1, arr);
        System.out.println(arr[start]);
    }

    public static void printReversedArray(int[] arr){
        printItem(0, arr);
    }

    public static String changeXY(String str){
        if(str.length() == 0)
            return "";

        char head = str.charAt(0);
        String tail = str.substring(1);

        if(head == 'x')
            head = 'y';

        return head + changeXY(tail);
    }

    // recursive helper method
    public static boolean isPalindromRec(String str, int first, int last){
        if(first >= last)
            return true;

        char head = str.charAt(first);
        char tail = str.charAt(last);

        if(head == tail)
            return isPalindromRec(str, first+1, last-1);
        return false;
    }

    public static boolean isPalindromNon(String str, int first, int last) {
        if (first >= last)
            return true;

        if (str.substring(last, last+1).matches("[0-9]"))
            return isPalindromNon(str, first, last-1);

        else if (str.substring(first, first + 1).matches("[0-9]"))
            return isPalindromNon(str, first+1, last);

        char head = str.charAt(first);
        char tail = str.charAt(last);

        if(head == tail)
            return isPalindromNon(str, first+1, last-1);
        return false;

    }

    public static boolean isPalindrom(String str){
        return isPalindromNon(str, 0, str.length()-1);
    }

    public static long fakHelp(int n, int result){
        if(n <= 0)
            return result;

        return fakHelp(n-1, result*n);
    }

    public static long faktorial(int n){
        return fakHelp(n, 1);
    }

    public static int luasKertasA(int panjangA0, int lebarA0, int k){
        if(k == 0)
            return panjangA0 * lebarA0;
        
        return luasKertasA(lebarA0, panjangA0 / 2, k-1);
    }
}