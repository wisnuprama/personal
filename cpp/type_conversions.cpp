/*
Script Name   : type_conversions.cpp
Author        : Wisnu Pramadhitya
Created       : July 17, 2016
Description   : Every object is given a type when it is defined. When needed, an object can converted
				to another type. Like char to int or int to double. This code give us example about how to convert
				in safe conversions ways.
*/

#include "std_lib_facilities.h"

int main()
{
	char c = 'x';	// var c as char with value 'x'
	int i = c;		// c converted to int
	int i2 = 'x';	// char 'x' converted to int
	char c2 = i2;   // i2('x') back to char by assign it to var c2 as char
	
	cout << c << ' ' << i << i2 << ' '<< c2 << endl; // print

	system("pause");
	return 0;
}
/*
	you must be realize when you converting char to int, it gives you 120. Why? It related to amount of memory. The meaning 
	of bits in memory is completely dependent on the type used to access it. a bit is a unit of computer memory that can hold 
	the value 0 and 1.

	00000000 00000000 00000000 011111000
	This is the setting of the bts of an area of memory (a word) that could be read as an int(120) or as a char('x', looking at
	the rightmost 8 bits only).

	These conversions list are safe:
		bool to char
		bool to int
		bool to double
		char to int
		char to double
		int to double

	source: Programming Principles and Practice using C++ 2nd edition
*/