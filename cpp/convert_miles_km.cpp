/*
Script Name   : convert_miles_km
Author        : Wisnu Pramadhitya R
Created       : September 4, 2016
Description   : Convert mile to km
Change	      : 							   
*/

#include "std_lib_facilities.h"
#include <iostream>
#include <algorithm>
#include <ctime>
using namespace std;
double miles;


void converter(double(miles)) {
	double to_km = miles * 1.609;
	string str_to_km = to_string(to_km);
	cout << "Jarak yang anda tempuh (miles) dalam kilometer berjumlah: " << str_to_km << endl;
}

int main() {
	printf("Masukan jarak dalam satuan miles: ");
	cin >> miles;
	converter(miles);
	
	return 0;
}