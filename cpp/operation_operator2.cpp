/*
Script Name   : operation_operator2.cpp
Author        : Wisnu Pramadhitya
Created       : July 15, 2016
Description   : Kelanjutan dari operation_operator.cpp, terdapat assigment merubah data dari int ke double. Ada kerja operators dalam string. Dan inisialisasi dan assignment (=)
*/

#include "std_lib_facilities.h"

int main()
{
	cout << "READ the CODE first and then you can run the program.\n" << endl;
	cout << "Please enter a integer value (n): "; // modify it to read int rather than a double
											 // note that sqrt() is not defined for an int: assign n to a double and take sqrt() of that
	int n;
	cin >> n;
	cout << "n == " << n
		<< "\nn+1 == " << n + 1
		<< "\nthree times n == " << 3 * n
		<< "\ntwice n == " << n + n
		<< "\nn squared == " << n*n
		<< "\nhalf of n == " << n / 2 << " : note that for integer division, if you input odd number (like 5) \nthe process would be: 5/2 is 2 not 2.5 or 3. \nTips: Assign it to double." 
	// modulo and reminder
		<< "\nmodulo by 2 of n == " << n % 2 << " : 0 means the input number is even; 1 means the input number is odd."
	// double n; if you do this, it will goes to 'error, redefinition': C2371
		<< "\nsquare root of n == " << sqrt(double(n))
		<< '\n' << endl;// another name for newline ("end of line") in output

	cout << "\n-------------------\nString has fewer operators."
		<< "\nPlease enter two names: ";
	string first; string second; // identify
	cin >> first >> second; // read two strings

	if (first == second) cout << "That's the same name twice\n" << endl; // check if your input is same or no
	else if (first < second) cout << first << " is alphabetically before " << second << endl; // check in alphabetical
	else if (first > second) cout << first << " is alphabetically after " << second << endl; // check in alphabetical

	cout << "\n----------------"
		<< "\nAssignment and Initialization\n"
		<< "In many ways, the most interisting operator is assignment, represented as =. It gives a variable a new value.\n" << endl;

	int a = 3; // a starts out with the value 3
	cout << "a = "<<a << endl;
	a = 4; // a gets the value 4 -- it becomes 4
	cout << "a = "<< a << endl; 
	int b = a; // b starts out with a copy of a's value -- that's, 4
	cout << "b = " << b << endl;
	b = a + 5; // b gets new value: a + 5 -- that's, 9
	cout << "b = " << b << endl;
	a = a + 7; // a gets new value: a + 7 -- that's, 11 
	cout << "a = " << a << endl;

	cout << "\nThat last assignment deserves notice. It is clearly shows that = does not mean equals.\n--Clearly a doesn't equal a+7. It's mean assignment" << endl;
	cout << "We can also illustrate assignments using strings: \n" << endl;
	
	string p = "alpha"; cout << "p = " << p << endl; 
	p = "beta"; cout << "p = " << p << endl;

	string q = p; cout << "q = " << q << endl;
	q = p + q + "gamma"; cout << "q = " << q << endl;

	p = p + "delta"; cout << "p = " << p << endl;

	cout << p + q + "wow!\n" << endl;
	

	system("pause");
	return 0;
}