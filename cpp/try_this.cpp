/*
Script Name   : try_this.cpp
Author        : Wisnu Pramadhitya
Created       : July 17, 2016
Description   : Find error from this code and fix it
*/
#include "std_lib_facilities.h"

int main() // change: Main -> main = confuse: behavior to not using capital letter. But it's not a fault.
{
	string s = "Goodbye, cruel world!"; // fix: STRING -> string
	cout << s << '\n'; /* fix: 1. cOut -> cout
							   2. wrong variable: S -> s */
	
	system("pause");
	return 0;
}
