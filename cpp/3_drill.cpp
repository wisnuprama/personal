/*
Script Name   : 03_drill.cpp
Author        : Wisnu Pramadhitya
Created       : July 17, 2016
Description   : Drill in chapter 3. 
Change		  : (July 22, 2016) -> ADD: new parameter in line 65: (person_age =! 18) to make it work.
								   ADD: new parameter (person_age < 110) in line 70 so the logic works like it should be.
								   REMOVE: parameter (person =! 18) and it is working. 
								   REMOVE: Unused comments
								   CHANGE: line 85, string(congrats) -> congrats
								   
*/

#include "../include/std_lib_facilities.h"

int main()
{
	cout << "Welcome.\nPlease, input your nickname: "; // prompt user to input user's nickname

	string nickname;
	cin >> nickname; // input user's nickname

	cout << "\nHello, " << nickname << ".\n" // greeting
		<< "Enter the nickname of the person you want to write to (recipient): "; // prompt user to input a person nickname

	string person_nickname;
	cin >> person_nickname; // input person nickname

	cout << "Please, enter a friend's nickname: "; // prompt user to input a user's friend's name

	string friend_name, friend_caller = " ";
	char friend_sex = 0;
	cin >> friend_name; // input friend nickname

	cout << "Enter your friend's sex [m/f]: "; // prompt user to input friend's sex
	cin >> friend_sex; // input
	if (friend_sex == 'm') { // statement for male 
		friend_caller = "him";
	}
	else if (friend_sex == 'f') { // statement for female 
		friend_caller = "her";
	}
	else {
		simple_error("error 404: Not Found"); // error if user input wrong option
		system("pause");
		return 0; // exit program
	}

	int person_age = 0; // declare person_age as int with initial value 0
	string congrats = "";
	cout << "Please enter your recipient's age: "; cin >> person_age;

	if ((person_age <= 0) || (person_age >= 110)){
		simple_error("You'are Kidding me!");
		system("pause");
		return 0;
	}
	else if ((person_age > 0) && (person_age <= 16)) {
		congrats = str( format("Next year you will be %d") % (person_age + 1)); // working
	}
	else if (person_age == 17) {
		congrats = "Congratulation, Next year you will be able to vote"; // SUDDENLY IT IS WORKING DUDE, I DON'T KNOW EXACTLY WHAT HAPPEN HERE. 
	}
	else if ((person_age >= 18) && (person_age < 65)) {
		congrats = "I hope you are enjoying your life";
	}
	else if ((person_age >= 65) && (person_age < 110)) {
		congrats = "I hope you are enjoying retirement";
	}
	else {
		simple_error("You'are Kidding me!");
		system("pause");
		return 0;
	}

	cout << "----------------------------------\n\n" // separator
		<< "Dear " << person_nickname << ",\n"													// a message for person from user.
		<< "How are you? I'm fine. I miss you! How's your holiday? Great? Me, well very great. "
		<< "Have you seen " << friend_name << " lately? " 
		<< "\nIf you see " << friend_name << " please ask " << friend_caller << " to call me."
		<< "\n\nI hear you just had a birthday and you are " << person_age << " years old.\n"
		<< congrats << ".\n"
		<< "\nAlright, I have something to talk about to you. Can we meet sometime? It would be nice!"
		<< "\nSo, give me your answer. Have a nice day! Bye!"
		<< "\n\n\t\t\t\t\t\t\t\t\tWith love,\n"
		<< "\t\t\t\t\t\t\t\t\t" << nickname
		<< "\n\n----------------------------------\n" << endl;


	system("pause");
	return 0;
}
