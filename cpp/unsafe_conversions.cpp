/*
Script Name   : unsafe_conversions.cpp
Author        : Wisnu Pramadhitya
Created       : July 17, 2016
Description   : Safe conversions are usually a boon to the programmer and simplify writing code. Unfortunately,
				C++ also allows for (implicit) unsafe conversions. By unsafe, we mean that a value can be implicitly turned into
				a value of another type that does not equal the original value.
*/

#include "std_lib_facilities.h"

int main()
{
	int a = 20000;
	char c = a; // try to squeeze a large int into a small char
	int b = c;
	if (a != b) // ! means "not equal"
		cout << "oops!: " << a << "! = " << b << endl; // usually, 32 is a common result
	else
		cout << "Wow! We have large characters" << endl;

	cout << "another example:\nType value\n";

	double d = 0;
	while (cin >> d) { // repeat the statement below
					  // as long as we type in numbers
		int i = d;					// try to squeeze a double into an int
		char c = i;					// try to squeeze an int into a char
		int i2 = c;					// get the integer value of the character
		
		cout << "d==" << d << '\n'            // the original double
			<< "i==" << i << '\n'		      // converted to int
			<< "i2==" << i2	<< '\n'			  // int value to char
			<< "char(" << c << ")\n" << endl; // the char
		
		system("pause"); 
		return 0;
	}	
	
}

/*
	You'll find that many input values produce "unreasonable" results.
	Basically, we are trying to put a gallon into a pint pot.

	all of the unsafe conversions:
		double to int
		double to char
		double to bool
		int to char
		int to bool
		char to bool 
	they are accepted by compiler even though they are unsafe. 
	by the time it's gonna be forgotten on lots of code.

	double x = 2.7;
	//lots of code
	int y = x;

	yes. y becomes 2. and we may have forgotten that x was a double not int.
*/