#include <iostream>
#include <cstdio>
using namespace std;

void wow(){
    int list[10] = {};
    for(int i=0; i<10; i++){
        list[i] = i*2;
    }

    for(auto x:list){ // works only if we have an nondynamic array
    // because compiler needs to know the begin and end
    // use compiler c++11
        printf("wow %d\n", x);
    }
}

int main(){
    wow();
}