/*
Script Name   : executable-code_s-placed-in-function.cpp
Author        : Wisnu Pramadhitya
Created       : July 06, 2016
Description   : Perpangkatan.
*/

#include <iostream>
using namespace std;

double square(double x) // square a double precision floating-point number
{
	return x*x;
}

void print_square(double x) // a "return type" void indicates that a function does not return a value
{
	cout << "the square of " << x << " is " << square(x) << "\n";
}

int main()
{
	print_square(1.234); // it would print the result of square of 1.234
}
