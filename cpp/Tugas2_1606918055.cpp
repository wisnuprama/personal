/*
Source Code   : Tugas2_1606918055.cpp
Author        : Wisnu Pramadhitya R
Description   : Calendar
Compiler      : gcc -std=c++11
*/

#include <iostream>
#include <cstdio>
#include <iomanip>
using namespace std;
// initiate array of month
// using it globally
int day_of_month[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

bool leapyear(int year) {
    // check if the year is a leap year
    return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
}

int getYearDays(int year) {
    // get total days from year 1800 to year
    int day = 0;
    
    for(int i = 1800; i < year; i++) {
        if (leapyear(i))
            day++; // leapyear +1 -> 366
        day += 365;
    }
    return day;
}

int getMonthDays(int year, int month){
    // get total days from January to month in that year
    int day = 0;
    
    for(int i = 1; i < month; i++) {
        if (i == 2 && leapyear(year))
            day += 29; // leapyear February +29
        else
            day += day_of_month[i];}

    return day;
}

void calendar(int year, int month) {
    // display calendar
    std::string name_of_month[13] = {"", "January", "February", "March", "April", "May", "June", \
                                    "July", "August", "September", "October", "November", "Desember"};

    if (leapyear(year))
        day_of_month[2] = 29; // change February to 29 if user input is a leapyear
    
    // calculate the first day of a month
    int start_day = (3 + getYearDays(year) + getMonthDays(year, month)) % 7;

    std::cout << name_of_month[month]+' ' << year 
                << "\n   S   M   T   W  Th   F   S" << std::endl;
    
    // print space for first day
    for(int start = 0; start < start_day; start++) {
        std::cout << "    ";
    }

    // print day by day until the end of a month
    for(int day = 1; day <= day_of_month[month]; day++) {
        // set free space:= 4
        std::cout << std::setw(4) << day;

        // newline after 7 days
        if ((day + start_day) % 7 == 0 || day == day_of_month[month])
            std::cout << '\n';
    }
}

int main() {
    int year = 2017;
    for(int month = 1; month<=12; ++month)
        calendar(year, month);

    return 0;
}