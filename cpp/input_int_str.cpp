/*
Script Name   : input_int_str.cpp
Author        : Wisnu Pramadhitya
Created       : July 11, 2016
Description   : Input string/integer and use them as an output
*/

#include "std_lib_facilities.h"
using namespace std;
// Always error when using std_lib_facilities.h even the header file is right there with source code. So I do it manually
// Found the error, it is because the old file. I downloaded new library

int main() // main function
{
	cout << "Please input your nickname (followed by 'enter'): " << endl; // output for ask users name
	
	string nickname; int age; // nickname is var of type string and age is var of type integer
	cin >> nickname;			// input username
	cout << "Input your age: " << endl; // ask user age
	cin >> age;				// input user age

	cout << "Hello, " << nickname << "!! You are " << age << " years old. Welcome!" << endl; // output that contains input from user

	system("pause"); // system pause, wait user to press enter to terminate the program
	return 0; // give main the zero, program fully stop
}