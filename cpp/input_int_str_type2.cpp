/*
Script Name   : input_int_str_type2.cpp
Author        : Wisnu Pramadhitya
Created       : July 12, 2016
Description   : Input output type 2. Beberapa cara yang lebih baik dan lebih cepat untuk input.
*/

#include "std_lib_facilities.h"
using namespace std;

int main()
{
	cout << "Please enter your data (Press 'enter' each input)" << endl; // output: ask users to enter their name and age

	string first_name = "???"; string last_name = "???"; // type string
	int dd = -1; int mm = -1; int yyyy = -1; // type integer for bornday
	int user_age = -1;	// type integer 
					   // '???' means "don't know the names" and -1 means "don't know the age".
					  // -1 is also for error code. when user input wrong data, it would result -1 in the form. 
	
	cout << "Full Name:" << endl; // output to ask user full name
	cin >> first_name >> last_name; // read strings
	cout << "Birthday (dd mm yyyy):" << endl; // output to ask user birthday
	cin >> dd >> mm >> yyyy;
	cout << "Age:" << endl; // output to ask user age
	cin >> user_age; // read integer 

	int user_age_month = user_age * 12; // user_age times by 12 to get total months.
	if (user_age == -1){
		user_age = -1;
		user_age_month = -1;
		cout << "Input wrong data" << endl;
		main();
	}
	else if (user_age < -1) {
		cout << "You can't input data below zero (0)" << endl;
		main();
	}

	int user_birthday = dd + 
	
	cout << "Hello, " << first_name << " (age " << user_age << ")\n" << endl; // output first name + age

	cout << "This your data:\n"; // form
	cout << "First Name\t: " << first_name << "\nLast Name \t: " << last_name << endl; // output name 
	cout << "Age (Year)\t: " << user_age; cout << " \nAge (month)\t: " << user_age_month << endl; // output age in year and month


	system("pause");
	return 0;
}