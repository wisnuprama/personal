/*
Script Name   : detect_repeated_world.cpp
Author        : Wisnu Pramadhitya
Created       : July 15, 2016
Description   : Little program that detects adjacent repeated words in a sequence of words. 
				Such a code is part of most grammar checker. 
Change		  : 1.1 Add: counter for the repeated word (3.6.1) with composite assignment operator.
*/

#include <iostream>
using std::string;

int main()
{
	int number_of_words = 0;
	string previous = "";
	string current = ""; 

	std::cout << "Input words: " << std::endl;
	std::cin >> previous;

	while (std::cin >> current) {
		
		if (previous == current) // check if the word is the same as last
			++number_of_words; // will increase word count
			std::cout << "Word number: " << number_of_words << std::endl;
			std::cout << "Repeated word: " << current << std::endl;
		previous = current;
	}

	return 0;
}
