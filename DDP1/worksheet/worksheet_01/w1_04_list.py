def main():
    lst = [0,3,4,7,2,8,6,5,9,1]

    mid_lst =  (len(lst)//2)
    b = lst[mid_lst]

    lst.sort()
    lst.reverse()

    x = lst[0]
    lst.remove(x)
    lst.append(x) 
    # lst.append(lst.pop()) # .pop will return the value, .remove not.
    lst.insert(3,4) # (index, value)

    print (lst)

main()

 