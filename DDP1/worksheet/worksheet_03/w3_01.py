"""
Script Name   : w3_01.py
Author        : Wisnu Pramadhitya R
Created       : September 30, 2016
Description   : 
	A polynomial of degree n with coefficients a0,a1,a2,a3,… ,an is the function
   
    𝑝(𝑥)=𝑎0+𝑎1𝑥+𝑎2𝑥2+𝑎3∗𝑥3+⋯+𝑎𝑛∗𝑥𝑛
   
    This function can be evaluated at different values of x. For example, if p(x) = 1+ 2x+x2, then p(2) = 1+ 2 ∗ 2+ 22 = 9. If p(x) = 1+ x2+ x4, 
    then p(2) = 21 and p(3) = 91. Write a function poly() that takes as input a list of coefficients a0, a1, a2, a3, …, an of a polynomial p(x) and a value x. 
    he function will return p(x), which is the value of the polynomial when evaluated at x. Note that the usage below is for the three examples shown.			   
"""


def poly(lst, x):
    'poly(lst,x): compute polynomial -> 𝑝(𝑥)=𝑎0+𝑎1𝑥+𝑎2𝑥^2+𝑎3∗𝑥^3+⋯+𝑎𝑛∗𝑥^𝑛, while lst is must list of coeffition and x is value of variable.'
    total, n = 0, 0
    for a in lst:
        func = (a * (x**n))
        total+= func
        n+= 1
    return total