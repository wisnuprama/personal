"""
Script Name   : w3_03ext.py
Author        : Wisnu Pramadhitya R
Created       : Oktober 01, 2016
Description   : 
    A substitution cipher for the digits 0, 1, 2, 3, . . . , 9 substitutes each digit in 0, 1, 2, 3, . . . , 9 with another digit in 0, 1, 2, 3, . . . , 9. 
    It can be represented as a 10-digit string specifying how each digit in 0, 1, 2, 3, . . . , 9 is substituted. For example, the 10-digit string '3941068257' 
    specifies a substitution cipher in which digit 0 is substituted with digit 3, 1 with 9, 2 with 4, and so on. To encrypt a nonnegative integer, 
    substitute each of its digits with the the digit specified by the encryption key.
    Implement function cipher() that takes as input a 10-digit string key and a digit string (i.e., the clear text to be encrypted) 
    and returns the encryption of the clear text.
"""

def cipher(message, key):
    encrypted = []
    for char in message:
        char = key[eval(char)]
        encrypted.append(char)
        
    return encrypted

'''def decipher(message, key):
    decrypted = []
    for char in message:
'''        

print(cipher('081317549551','9hdjsl746b'))

        
