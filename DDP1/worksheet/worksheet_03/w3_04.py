"""
Script Name   : w3_04.py
Author        : Wisnu Pramadhitya R
Created       : September 30, 2016
Description   : 
    The function avgavg() takes as input a list whose items are lists of three numbers. 
    Each three-number list represents the three grades a particular student received for a course. 
    For example, here is an input list for a class of four students:
                            [[95,92,86], [66,75,54],[89, 72,100],[34,0,0]]
    The function avgavg() should print, on the screen, two lines. The first line will contain a list containing every student’s average grade. 
    The second line will contain just one number: the average class grade, defined as the average of all student average grades.
"""

def avgavg(lst):
    'Calculate the average of students grade and class grade\nlst = [[three numbers],[x,y,z]]'
    avg_class, avg_student, i = 0, [], 0
    
    for avg in lst:
        for i in range(len(avg)):
            i+= 1
        avg = sum(avg) / i
        avg_student.append(avg)
        avg_class+= avg
    
    avg_class/= len(lst)
    
    tmp = '{}\n{}'.format(avg_student,avg_class)

    return tmp