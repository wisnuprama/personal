"""
Script Name   : w3_02.py
Author        : Wisnu Pramadhitya R
Created       : September 30, 2016
Description   : 
    Implement function evenrow() that takes a two-dimensional list of integers and returns True if each row of the 
    table sums up to an even number and False otherwise (i.e., if some row sums up to an odd number).
"""

def evenrow(twoDlst):
    'takes a two-dimensional list of integers and returns True if each row of the table sums up to an even number and False otherwise'
    checking = True
    for i in twoDlst:
        i = sum(i)

        if i % 2 == 0:
            checking = True
        else:
            checking = False
            break
    
    return checking