"""
Script Name   : w3_05.py
Author        : Wisnu Pramadhitya R
Created       : September 28, 2016
Description   : 
	The Heron method is a method the ancient Greeks used to compute the square root of a number n. The method generates a sequence 
    of numbers that represent better and better approximations for √n. The first number in the sequence is an arbitrary guess; 
    every other number in the sequence is obtained from the previous number prev using the formula

    1/2(𝑝𝑟𝑒𝑣+𝑛𝑝𝑟𝑒𝑣)
    
    Write function heron() that takes as input two numbers: n and error. The function should start with an initial guess of 1.0 for √n and then repeatedly 
    generate better approximations until the difference (more precisely, the absolute value of the difference) between successive 
    approximations is at most error.	   
"""

def heron(n, error):
    'Compute square root of a number(n) using the Heron method'
    prev, tmp = 1.0, 0.5 * (1 + n)
    while abs(tmp - prev) > error:
        prev, tmp = tmp, 0.5 * (tmp + n/tmp)
    
    return tmp
