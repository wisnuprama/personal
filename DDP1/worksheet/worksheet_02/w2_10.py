"""
Script Name   : w2_10.py
Author        : Wisnu Pramadhitya R
Created       : September 25, 2016
Description   : 
    Fungsi collision() mengecek apakah dua objek lingkaran saling bertubrukan; fungsi tersebut mengembalikan True 
    jika kedua objek bertubrukan, False jika tidak. Setiap objek lingkaran direpresentasikan dengan koordinat (x,y) yang merupakan titik pusatnya. 
    Fungsi menerima 6 bilangan sebagai input: koordinat (x1,y1) dan jari-jari r1 dari lingkaran pertama, dan koordinat (x2,y2) 
    serta jari-jari r2 dari lingkaran kedua.

    Toeri:
        Collision between circles is easy. Imagine there are two circles:

        C1 with center (x1,y1) and radius r1;
        C2 with center (x2,y2) and radius r2.
        Imagine there is a line running between those two center points. The distance from the center points to the edge of either circle is, 
        by definition, equal to their respective radii. So:

        if the edges of the circles touch, the distance between the centers is r1+r2;
        any greater distance and the circles don't touch or collide; and
        any less and then do collide.
        So you can detect collision if:

        (x2-x1)^2 + (y1-y2)^2 <= (r1+r2)^2
        meaning the distance between the center points is less than the sum of the radii.

        The same principle can be applied to detecting collisions between spheres in three dimensions.
"""

def collision(x1,y1,r1,x2,y2,r2):
    'Imagine there is a line running between those two center points. The distance from the center points to the edge of either circle is, by definition, equal to their respective radii.'
    if (x2-x1)**2 + (y1-y2)**2 <= (r1+r2)**2:
        return True
    else:
        return False 

collision(0,0,3,0,5,3)
collision(0,0,1.4,2,2,1.4)
