"""
Script Name   : w2_08.py
Author        : Wisnu Pramadhitya R
Created       : September 25, 2016
Description   : 
    Buatlah fungsi reverse_int() yang menerima tiga digit integer sebagai input dan mengembalikan integer kebalikannya. 
    Misal jika inputnya 123, maka fungsi tersebut harus mengembalikan 321. Anda tidak diperbolehkan menggunakan tipe data string untuk melakukan tugas ini. 
    Program Anda harus membaca input sebagai integer dan memprosesnya juga sebagai integer dengan operator seperti // atau %. 
    Anda boleh berasumsi bahwa input integernya tidak diakhiri dengan digit 0.			   
"""

def reverse_int(n):
    _0_sisa = (n % 100) 
    
    _0_ = n//100        # index 0
    _01_ = _0_sisa//10  # index 1
    _02_ = _0_sisa % 10 # index 2
    print()
    print(_02_,end='');print(_01_,end='');print(_0_,end='');

def reverse_int_list(n): # with this function, the input can much more rich than just 3 digits.
    int_list = [int(x) for x in str(n)]; int_list.reverse(); # listing with for-loop # reverse the list
    for x in range(len(int_list)): # indexing
        print(int_list[x],end='') # print item in the list 
        
n = int(input('Enter n: '))
if 100 <= n < 1000:
    reverse_int(n)
else:
    reverse_int_list(n)