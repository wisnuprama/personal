"""
Script Name   : w2_03.py
Author        : Wisnu Pramadhitya R
Created       : September 25, 2016
Description   : Buatlah program yang meminta non-empty list dari 
                user dan menampilkan pesan yang berisi elemen pertama dan terakhir dari list tersebut.						   
"""

item_lst = [] # empty list
i = 0 # initial value of variable i

while True:
    i+= 1 # i plus 1 every loop
    item = input('Input item {}\t: '.format(i)) # {} = i it would changes depend on loop number
    if item == '': # break the loop when no item to input in list
        print('Berhenti menambahkan list')
        break
    item_lst.append(item) #add item to list after check the item is empty or not. if there is item, it would add to list

str = 'The first list element is {}\nThe last list element is {}'.format(item_lst[0],item_lst[-1])
    # display fisrt item and last item in list
print(str)