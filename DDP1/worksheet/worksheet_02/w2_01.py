"""
Script Name   : w2_01.py
Author        : Wisnu Pramadhitya R
Created       : September 23, 2016
Description   : Buatlah program yang meminta sejumlah kata dalam list dari user, kemudian menampilkan semua kata selain ‘secret’.						   
"""

lst = ['cia','secret','mi6','isi','secret']
# eval works too

while True:
    input_usr = input('masukan kata: ')
    if input_usr == '': # break the while loop
        break
    lst.append(input_usr) # add item to list

lst_copy = lst[:] # copy all item in list lst to new list, so there is still original list
x = lst_copy.count('secret') # x = counting how many 'secret' in lst_copy
for i in range(x): # i looping for x times
    lst_copy.remove('secret') # remove 'secret'

print('this is the original list\t:',lst)
print('this is the final list\t\t:',lst_copy)