"""
Script Name   : w2_06.py
Author        : Wisnu Pramadhitya R
Created       : September 25, 2016
Description   : Buatlah program yang meminta integer positif n dan menampilkan di layar semua factor pembagi dari n. 
                Catatan : 0 bukan merupakan factor dari integer mana pun, dan n membagi dirinya sendiri.			   
"""

n = int(input('Enter n: '))

for i in range(1,n): # start the loop from one to n
    factor = n%i # find the factor, i is the factor
    if factor == 0: # if the modulo is zero
        print(i,end=' ')
   
print(n) # print n as n is the factor of n