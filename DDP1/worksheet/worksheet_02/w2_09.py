"""
Script Name   : w2_09.py
Author        : Wisnu Pramadhitya R
Created       : September 25, 2016
Description   : 
    Buatlah fungsi points() yang menerima input empat buah bilangan x1,y1,x2,y2 yang merupakan koordinat dari 
    dua titik (x1,y1) dan (x2,y2) pada bidang kartesius. Fungsi tersebut harus menghitung: 
    • Kemiringan garis yang melalui kedua titik tersebut, kecuali jika garisnya vertical. 
    • Jarak antara kedua titik tersebut. Fungsi tersebut harus menampilkan kemiringan dan jaraknya dalam format berikut. 
      Jika garisnya vertical, maka nilai kemiringan garis tersebut harus ‘infinity’. 
      Catatan: pastikan Anda mengkonversi nilai kemiringan dan jarak kedua titik ke dalam string sebelum menampilkannya di layar.
"""
from math import sqrt

def points(x1,y1,x2,y2):
    if x1 == x2:
        dist = y2 - y1
        print('The slope is infinity (∞) and the distance is {}'.format(dist))
    else:
        gradien = (y2 - y1)/(x2 - x1)
        dist = sqrt(abs((x2-x1))**2 + abs((y2-y1))**2)
        print('The slope is {} and the distance is {}'.format(gradien, dist))

points(0,0,1,1)