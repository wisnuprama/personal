"""
Script Name   : w2_02.py
Author        : Wisnu Pramadhitya R
Created       : September 25, 2016
Description   : Buatlah program yang meminta list mahasiswa dan menampilkan semua nama mahasiswa yang diawali huruf A sampai M.						   
"""

lst_name = [] # empty list

while True:
    name = str(input('Masukan nama ke dalam list: '))                                          
    if name == '': # break the while loop if no input
        break
    lst_name.append(name.capitalize()) # capitalize every input and then add it to the list
    
for i in lst_name: # loop for checking first character in every string in the list
    alpbt = ['A','B','C','D','E','F','G','H','I','J','K','L','M'] # list of alpabhet from A-M 
                                                                 # so we can check every first character in the string
    if i[0] in alpbt: # if first character of string i is in list alpbt, display i
        print(i)