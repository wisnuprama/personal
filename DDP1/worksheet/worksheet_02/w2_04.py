"""
Script Name   : w2_04.py
Author        : Wisnu Pramadhitya R
Created       : September 25, 2016
Description   : Buatlah program yang meminta integer positif n dari user dan menampilkan empat kelipatan pertama dari n					   
"""

n = int(input('Enter n\t:')) 

if n >= 0: # check if the int is positive
    for i in range(0,(n*3)+1,n): # loop for range n. Display from zero, the parameter of how many it would display depending on the multiple,
                                # and use step size of 5 
        print(i)
else: # if not, it would pass and exit
    exit(0)