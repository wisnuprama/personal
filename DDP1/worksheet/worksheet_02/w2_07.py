"""
Script Name   : w2_07.py
Author        : Wisnu Pramadhitya R
Created       : September 25, 2016
Description   : Buatlah fungsi reverse_string() yang menerima input string tiga huruf dan mengembalikan string kebalikannya.			   
"""

def reverse_string(inp_str): # make a function that can reverse string that contains 3 character
    inp_str = inp_str[-1] + inp_str[-2] + inp_str[-3] # reverse string
    return inp_str 

inp_str = str(input('Masukan string tidak lebih dari 3 karakter: '))
if len(inp_str) == 3: # check if the string is 3 character
    reverse_string(inp_str) # call func reverse_string
else:
    pass