"""
Script Name   : w2_05.py
Author        : Wisnu Pramadhitya R
Created       : September 25, 2016
Description   : Buatlah program yang meminta suatu integer n dari user dan menampilkan di layar pangkat dua dari 
                semua bilangan mulai dari 0 sampai n (ekslusif).			   
"""

n = int(input('Enter n: ')) + 1 # + 1 so then when input to range, we can calculate until n 
i = -1
for pwr in range(n): # start from zero until n
    i+= 1
    pwr = pwr**2 # power of 2
    print('{}^2 = {}'.format(i,pwr))

