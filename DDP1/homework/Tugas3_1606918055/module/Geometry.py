'''
Source Code   : Geometry.py
Author        : Wisnu Pramadhitya R
Created       : November 18-25, 2016
Description   : Module Geometry berisi object-object geometri:
                Point -> Shape -> Circle
                               -> Square -> Rectangle
'''
import math

class Point:
    'class point untuk penunjang sistem koordinat objek shape'
    def __init__(self, x=int(), y=int()):
        self.x = x
        self.y = y

    def setPosX(self, x=int()):
        self.x = x

    def setPosY(self, y=int()):
        self.y = y

    def getPoint(self):
        return Point(self.x, self.y)

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def movePoint(self, x=int(), y=int()):
        'Move point'
        self.x += x
        self.y += y

class Shape(Point):
    'class Shape sebagai dasar dari bentuk-bentuk geometri lain'
    def __init__(self, shpName=str(), posX=int(), posY=int(), fillColor=str(), lineColor=str()):
        Point.__init__(self, posX, posY)
        self.name = shpName
        self.color, self.line = fillColor, lineColor

    def setName(self, shpName=str()):
        self.name = shpName

    def getName(self):
        return self.name

    def setColor(self, fillColor=str()):
        self.color = fillColor

    def getColor(self):
        return self.color

    def setLine(self, fillColor=str()):
        self.line = fillColor

    def getLine(self):
        return self.line

    def getClassName(self):
        'Memanggil nama class dari objek'
        return self.__class__.__name__

class Square(Shape):
    'class Square'
    def __init__(self, shpName=str(), posX=int(), posY=int(), lengthIn=int(), fillColor=str(), lineColor=str()):
        Shape.__init__(self, shpName, posX, posY, fillColor, lineColor)
        self.length = lengthIn

    def setLength(self, length=int()):
        self.length = length

    def getLength(self):
        return self.length

    def getArea(self):
        '...'
        return self.length**2

    def getCircumterance(self):
        '...'
        return 4*(self.length)

class Rectangle(Square):
    'class Rectangle'
    def __init__(self, shpName=str(), posX=int(), posY=int(), lengthIn=int(), widthIn=int(), fillColor=str(), lineColor=str()):
        Square.__init__(self, shpName, posX, posY, lengthIn, fillColor, lineColor)
        self.width = widthIn

    def setWidth(self, widthIn=int()):
        self.width = widthIn

    def getWidth(self):
        return self.width

    def getArea(self):
        '...'
        return self.length * self.width

    def getCircumterance(self):
        '...'
        return 2*(self.length + self.width)

class Circle(Shape):
    'class Circle'
    def __init__(self, shpName=str(), posX=int(), posY=int(), rd=int(), fillColor=str(), lineColor=str()):
        Shape.__init__(self, shpName, posX, posY, fillColor, lineColor)
        self.radius = rd

    def setRadius(self, rd=int()):
        self.radius = rd

    def getRadius(self):
        return self.radius

    def getArea(self):
        '...'
        return math.pi * self.radius**2

    def getCircumterance(self):
        '...'
        return 2 * math.pi * self.radius