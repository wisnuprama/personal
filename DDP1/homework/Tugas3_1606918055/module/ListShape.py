'''
Source Code   : ListShape.py
Author        : Wisnu Pramadhitya R
Created       : November 18-25, 2016
Description   : Module berisi class ListShape(list) yang menghandle kebutuhan untuk mengatur module Geometry
                Object: ListShape, IllegalName, ObjectNotFound, ObjectAdded, ObjectMoved, AskDeleteCorrupt, AskDelete
                AskMove, DeleteObject
'''
from tkinter import messagebox
from module.Geometry import Circle, Square, Rectangle

class IllegalName:
    'Name Handler apabila name sudah terdaftar'
    def __init__(self, master=None):
        messagebox.showerror('IllegalName', self.__str__(), parent=master)
    def __str__(self):
        return 'Nama Objek sudah ada pada canvas!'

class ObjectNotFound:
    'Object Handler apabila objek tidak terdaftar'
    def __init__(self, master=None):
        messagebox.showwarning('Object', self.__str__(), parent=master)
    def __str__(self):
        return 'Nama objek tidak ditemukan'

class ObjectAdded:
    'Object Handler apabila objek berhasil didaftarkan'
    def __init__(self, master=None):
        messagebox.showinfo('Object', self.__str__(), parent=master)
    def __str__(self):
        return 'Objek berhasil ditambahkan!'

class ObjectMoved:
    'Objek Handler apabila objek berhasil dipindahkan'
    def __init__(self, master=None):
        messagebox.showinfo('Object', self.__str__(), parent=master)
    def __str__(self):
        return 'Objek berhasil dipindahkan!'

class AskDeleteCorrupt:
    'UserWarning untuk menanyakan user apabila ingin menghapus object yang korup'
    def __init__(self, master=None, name=str()):
        self.name = name
        self.result = messagebox.askyesno('Object', self.__str__(), parent=master)
    def __str__(self):
        return 'Objek {} sepertinya korup. Hapus Objek?'.format(self.name)
    def getResult(self):
        return self.result

class AskDelete:
    'UserWarning untuk menanyakan user apabila benar ingin menghapus object'
    def __init__(self, master=None, name=str()):
        self.name = name
        self.result = messagebox.askyesno('Object', self.__str__(), parent=master)
    def __str__(self):
        return 'Hapus Objek {}?'.format(self.name)
    def getResult(self):
        return self.result

class AskMove:
    'UserWarning untuk pemindahan objek'
    def __init__(self, master=None, name=str(), x=str(), y=str()):
        self.name, self.x, self.y = name, x, y
        self.result = messagebox.askokcancel('Object', self.__str__(), parent=master)
    def __str__(self):
        return 'Pindahkan objek {} ke X:{}, Y:{}?'.format(self.name, self.x, self.y)
    def getResult(self):
        return self.result

class DeleteObject:
    'Object Handler apabila object berhasil dihapus'
    def __init__(self, master=None):
        messagebox.showinfo('Object', self.__str__(), parent=master)
    def __str__(self):
        return 'Objek berhasil dihapus!'

class ListShape(list):
    'Objek inheritace dari list untuk container object Shape'
    def __init__(self, master=None):
        list.__init__(self)
        self.parent = master

    def checkObj(self, name):
        'Check apakah object dengan attribute nama sudah terdapat dalam list\n\
        return True apabila object sudah ada'
        for obj in self:
            if name == obj.getName(): return True
        return False

    def getObj(self, name):
        'Return object dalam list apabila sesuai dengan attribute nama'
        for obj in self:
            if name == obj.getName(): return obj

    def addCircle(self, shpName, pX, pY, radius, fill, outline):
        'Menambahkan object Circle ke dalam list'
        if self.checkObj(shpName) == False:
            createClass = Circle(shpName, pX, pY, radius, fill, outline)
            self.append(createClass)
            ObjectAdded()
        else: IllegalName(self.parent)

    def addSquare(self, shpName, pX, pY, length, fill, outline):
        'Menambahkan object Square ke dalam list'
        if self.checkObj(shpName) == False:
            createClass = Square(shpName, pX, pY, length, fill, outline)
            self.append(createClass)
            ObjectAdded()
        else: IllegalName(self.parent)

    def addRectangle(self, shpName, pX, pY, length, width, fill, outline):
        'Menambahkan object Rectangle ke dalam list'
        if self.checkObj(shpName) == False:
            createClass = Rectangle(shpName, pX, pY, length, width, fill, outline)
            self.append(createClass)
            ObjectAdded()
        else: IllegalName(self.parent)

    def moveObj(self, shpName=str(), xNew=int(), yNew=int()):
        'Memindahkan object dari satu titik ke titik lain'
        ask = AskMove(name=shpName, x=xNew, y=yNew)
        if self.checkObj(shpName) and ask == True:
            Obj = self.getObj(shpName)
            Obj.setPosX(xNew)
            Obj.setPosY(yNew)
            ObjectMoved()
        else: ObjectNotFound(self.parent)

    def delObj(self, shpName=str()):
        'Menghapus object dalam list'
        ask = AskDelete(name=shpName)
        if self.checkObj(shpName) and ask.getResult() == True:
            tempObj = self.getObj(shpName)
            i = self.index(tempObj)
            del self[i]
            DeleteObject(self.parent)
        else:
            ObjectNotFound()
    
    def chgColor(self, shpName=str(), color=str()):
        if self.checkObj(shpName):
            obj = self.getObj(shpName)
            obj.setColor(color)
        else:
            ObjectNotFound()
    
    def outlineColor(self, shpName=str(), color=str()):
        if self.checkObj(shpName):
            obj = self.getObj(shpName)
            obj.setLine(color)
        else:
            ObjectNotFound()