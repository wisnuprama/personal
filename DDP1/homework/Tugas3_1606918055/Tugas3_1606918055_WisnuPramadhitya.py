'''
Source Code   : Tugas3_1606918055_WisnuPramadhitya.py
Author        : Wisnu Pramadhitya R
Created       : November 18-25, 2016
Description   : Tugas 3 My Paint menggunakan Tkinter
'''
from tkinter import Tk, Canvas, messagebox, Frame, Label, Button, Text, Entry, Menu
from tkinter import LEFT, RIGHT, BOTTOM, TOP, BOTH, ALL, END, RAISED, NW, DISABLED, NORMAL
from module.ListShape import *

class IllegalCommand:
    'Message box yang dipanggil ketika command yang dimasukkan tidak sesuai'
    def __init__(self, master=None):
        messagebox.showerror('IllegalCommand', self.__str__(), parent=master)
    def __str__(self):
        return 'Perintah masukan tidak sesuai, ulangi kembali!'

class MyCanvas(Frame):
    'Class Canvas untuk menggambar'
    global lstShp
    def __init__(self, master=None):
        Frame.__init__(self, master)

        #inisiasi canvas
        self.canvas = Canvas(self, height=600, width=700, relief=RAISED, \
                                   borderwidth=3, bg='#ffffff')
        self.canvas.grid()

    def drawSquare(self, x, y, length, fillColor, lineColor):
        'menggambar persegi pada canvas'
        self.canvas.create_rectangle(x, y, x + length, y + length, fill=fillColor, outline=lineColor)

    def drawRectangle(self, x, y, width, height, fillColor, lineColor):
        'menggambar persegi panjang pada canvas'
        self.canvas.create_rectangle(x, y, x + width, y + height, fill=fillColor, outline=lineColor)

    def drawCircle(self, x, y, radius, fillColor, lineColor):
        'menggambar lingkaran pada canvas'
        self.canvas.create_oval(x, y, x+radius, y+radius, fill=fillColor, outline=lineColor)

    def renderImg(self):
        'Render image dari container'
        try:
            for shape in lstShp:
                if shape.getClassName() == 'Square':
                    self.drawSquare(shape.getX(), shape.getY(), shape.getLength(), \
                                    shape.getColor(), shape.getLine())

                elif shape.getClassName() == 'Rectangle':
                    self.drawRectangle(shape.getX(), shape.getY(), shape.getLength(), \
                                    shape.getWidth(), shape.getColor(), shape.getLine())

                elif shape.getClassName() == 'Circle':
                    self.drawCircle(shape.getX(), shape.getY(), shape.getRadius(), \
                                    shape.getColor(), shape.getLine())

        except:
            ask = AskDeleteCorrupt(name=shape.getName()) # ask user
            if ask.getResult() == True:
                lstShp.remove(shape) # remove bad object
                DeleteObject() # confirm user
                self.renderImg() # render again

    def refresh(self):
        'Untuk merefresh isi dalam canvas'
        self.canvas.delete(ALL) # delete all
        self.renderImg() # render

    def deleteAll(self):
        'Menghapus semua object dalam canvas dan list'
        lstShp.clear()
        self.canvas.delete(ALL)

class History(Frame):
    'Frame sebelah kiri untuk menangkap semua aktifitas user'
    global lstShp

    def __init__(self, master, canvas):
        Frame.__init__(self, master)
        self.grid()

        self.label = Label(self, text='History:')
        self.label.grid(row=0, column=1, sticky=NW, pady=2, padx=3)

        # textbox History
        self.textbox = Text(self, width=30, height=35, bg='#000000', fg='#00ff00')
        self.textbox.config(state=DISABLED)
        self.textbox.grid(row=1, columnspan=4)

        # user-guide button
        self.infobut = Button(self, text='How-To', relief=RAISED, command=self.howto)
        self.infobut.grid(row=2, column=1)

        # button render
        self.renderbut = Button(self, text='Render', relief=RAISED, command=canvas.renderImg)
        self.renderbut.grid(row=2, column=2)

        # button delete all object
        self.delbut = Button(self, text='Delete ALL', relief=RAISED, command=canvas.deleteAll)
        self.delbut.grid(row=2, column=3)

    def insertText(self, text=str()):
        'menambahkan text ke textbox'
        temp = '>>> {}\n'.format(text)
        self.textbox.config(state=NORMAL)
        self.textbox.insert(END, temp)
        self.textbox.config(state=DISABLED)

    def howto(self):
        'messagebox untuk menampilkan user-guide'
        message = '''Manual:
1. Membuat Lingkaran:
> addCircle [Nama] [X] [Y] [Radius] [Warna] [Warna Garis]

2. Membuat Persegi:
> addSquare [Nama] [X] [Y] [Sisi] [Warna] [Warna Garis]

3. Membuat Persegi Panjang:
> addRectangle [Nama] [X] [Y] [Panjang] [Lebar] [Warna] [Warna Garis]

4. Menampilkan gambar:
> render

5. Memindahkan Objek:
> move [Nama] [X baru] [Y baru]

6. Mengganti warna Objek:
> color [Nama] [new color]

7. Mengganti warna garis Objek:
> outline [Nama] [new color]

8. Menghapus Objek:
> delete [Nama]

9. Keluar:
> quit'''
        messagebox.showinfo('How-To', message)

class Terminal(Frame):
    'Menggantikan terminal sebagai input user'
    global lstShp

    def __init__(self, master, canvas, textbox):
        Frame.__init__(self, master)

        self.grid()
        self.canvas = canvas
        self.textbox = textbox

        # label terminal wannabe
        self.label = Label(self, text='Terminal WannaBe')
        self.label.grid(row=0, column=0)

        # terminal wannabe
        self.entryCmd = Entry(self, width = 100, fg='#00ff00', bg='#000000', insertbackground='#ffffff')
        self.entryCmd.bind('<Return>', self.setCommand) # press enter
        self.entryCmd.grid(row=1, column=0)

        # submit button
        self.submit = Button(self, text = "Submit", command = self.setCommand)
        self.submit.grid(row=2, column=0)

    def setCommand(self, event=None):
        'Command center'
        try:
            entry = str(self.entryCmd.get()) # get user's input
            self.textbox.insertText(entry) # menambahkan ke History
            cmd = entry.split()

            # need validation
            if cmd[0].lower() == 'addsquare':
                shpName, pX, pY,  = cmd[1], cmd[2], cmd[3]
                length, fill, line = cmd[4], cmd[5], cmd[6]
                lstShp.addSquare(shpName, int(pX), int(pY), int(length), fill, line)
                self.entryCmd.delete(0, END) # reset entry

            elif cmd[0].lower() == 'addrectangle':
                shpName, pX, pY = cmd[1], cmd[2], cmd[3]
                length, width =  cmd[4], cmd[5]
                fill, line = cmd[6], cmd[7]
                lstShp.addRectangle(shpName, int(pX), int(pY), int(length), int(width), fill, line)
                self.entryCmd.delete(0, END)

            elif cmd[0].lower() == 'addcircle':
                shpName, pX, pY, radius, fill, line = cmd[1], cmd[2], cmd[3], cmd[4], cmd[5], cmd[6]
                lstShp.addCircle(shpName, int(pX), int(pY), int(radius), fill, line)
                self.entryCmd.delete(0, END)

            elif cmd[0].lower() == 'move':
                shpName, xNew, yNew = cmd[1], cmd[2], cmd[3]
                lstShp.moveObj(shpName, int(xNew), int(yNew))
                self.canvas.refresh() # refresh
                self.entryCmd.delete(0, END)
            
            elif cmd[0].lower() == 'color':
                shpName, fill = cmd[1], cmd[2]
                lstShp.chgColor(shpName, fill)
                self.canvas.refresh()
                self.entryCmd.delete(0, END)
            
            elif cmd[0].lower() == 'outline':
                shpName, fill = cmd[1], cmd[2]
                lstShp.outlineColor(shpName, fill)
                self.canvas.refresh()
                self.entryCmd.delete(0, END)
            
            elif cmd[0].lower() == 'delete':
                shpName = cmd[1]
                lstShp.delObj(shpName) # menghapus object
                self.canvas.refresh() # menghapus isi canvas
                self.entryCmd.delete(0, END)

            elif cmd[0].lower() == 'render':
                self.canvas.renderImg()
                self.entryCmd.delete(0, END)

            elif cmd[0].lower() == 'quit':
                destroyMe()

            else: 
                IllegalCommand()
        except(IndexError, ValueError):
            IllegalCommand()
        except:
            IllegalCommand()
lstShp = ListShape() # memanggil objek list container untuk objek shape

class Application(Frame):
    'Aplikasi utama'
    def __init__(self, parent):
        Frame.__init__(self, parent)
        canvas = MyCanvas(self)
        leftFrame = History(self, canvas)
        terminal = Terminal(self, canvas, leftFrame)
        leftFrame.grid(row=0, column=1)
        canvas.grid(row=0, column=2)
        terminal.grid(rows=1, column=2)

def destroyMe():
    'menutup window'
    global root
    root.destroy()

def main():
    root = Tk()
    root.title('My Paint _ Wisnu Pramadhitya - 1606918055')
    root.winfo_toplevel() # root sebagai toplevel window
    root.geometry('960x690')

    app = Application(root)
    app.pack(expand=True, fill=BOTH)
    root.mainloop()

if __name__ == "__main__":
    main()
