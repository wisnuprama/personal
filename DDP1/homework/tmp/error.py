__all__ = ['IllegalMonthError', 'IllegalYearError', 'IllegalDataType']

# Exceptions raised for bad input
class IllegalMonthError(ValueError):
    def __init__(self, month):
        self.month = month
    def __str__(self):
        return "bad month number {}; must be 1-12".format(self.month)

class IllegalYearError(ValueError):
    def __init__(self, year):
        self.year = year
    def __str__(self):
        return "bad year number {}; must be 1800-2099".format(self.year)

class IllegalDataType(ValueError):
    def __init__(self, error):
        self.error = error
    def __str__(self):
        return 'bad input {}; must be <int>'.format(self.error)

for i in range(6):
        date += '\n  {:2}  {:2}  {:2}  {:2}  {:2}  {:2}  {:2}'.format(days[i][0], days[i][1], days[i][2], days[i][3], days[i][4], days[i][5], days[i][6])