"""
/*
Script Name   : Tugas1-1606918055.py
Author        : Wisnu Pramadhitya R (1606918055) - Sistem Informasi
Created       : September 17, 2016
Description   : Buatlah program dengan menggunakan module turtle dan fungsi-fungsinya yang sudah Anda pelajari di Lab01 untuk menggambar dua objek berikut.
*/
"""
from turtle import *
import math
screensize(800,480) # set screensize
speed(0) # set turtle's speed

# house
p_sq = 150 # length of square
pu(); setpos(-200,-50)
pd()
for i in range(4): # square
    fd(p_sq) 
    left(90)

p_m = math.sqrt(2) * p_sq # length of slash
left(45); fd(p_m)
for i in range(2): # triangle
    left(90)
    fd(p_m/2)
left(90); fd(p_m)

# smiley
r = 75 # for radius
r_eyes = 5 # for radius of an eye
deg_circle = 360 
pu()
left(45); fd(200)
pd()
circle(r, deg_circle)
    # eyes
pu(); left(90); fd(90) # right
right(90); fd(30)
fillcolor('black'); begin_fill()
circle(r_eyes, deg_circle)
setheading(180); fd(60); right(180) # left
circle(r_eyes, deg_circle)
end_fill()

# we can use func. turtle.dot(r_eyes) to make an eye

pu() # drawing smile
right(100); fd(50)
# right(90); fd(5) # for arrange manually if value of func.right() in line 50 is (90)
pd(); left(50)
circle(50,100)

hideturtle()
exitonclick()