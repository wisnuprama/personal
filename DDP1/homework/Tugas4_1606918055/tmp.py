#!/usr/bin/env python

from tkinter import *
from time import sleep
import numpy as np
import sys, argparse

# TODO
# 3D mode? I like pencil simple...



# Parse user arguments
parser = argparse.ArgumentParser(description='Interpreter of graphics')
parser.add_argument('--width', '-w', metavar='width', type=int, nargs=1,
                    help='Width of the scene', default = 500)
parser.add_argument('--height', '-y', metavar='height', type=int,
                    nargs=1, help='Height of the scene', default = 250)
parser.add_argument('--bg-color', '-b', metavar='bgcolor',
                    nargs=1, help='Background color', default = 'white')

args = parser.parse_args()

# Defs
master = Tk()
master.title('Pencil output')
w = Canvas(master, width=args.width, height=args.height, bg=args.bg_color) # TODO programable
w.pack()

current_color = 'black'

# Miniplot
data = np.array([[]]) # Array to plot. Is a Nx2 matrix.
def miniplot(xpos, ypos, xsize, ysize):
    global data
    global current_color
    # Draw the axis lines
    w.create_line(xpos,ypos,xpos+xsize,ypos, fill=current_color)
    w.create_line(xpos,ypos,xpos,ypos-ysize, fill=current_color)
    # Plot the data with lines. Normalize by the maximum element
    # the data so it fits in the plot and scale to occupy the plot.
    # Check if there is some data!
    if np.size(data) != 0:
        xdata = data[:,0]/max(data[:,0])*xsize
        ydata = data[:,1]/max(data[:,1])*ysize
        N = len(xdata)
        # Plot only if more than two points!
        if N>1:
            for i in range(0,N-1):
                # Remember to adapt y to canvas coords (upside down)
                frx = xdata[i] + xpos
                fry = ypos - ydata[i]
                tox = xdata[i+1] + xpos
                toy = ypos - ydata[i+1]
                w.create_line(frx,fry,tox,toy, fill=current_color)
        # Put some numbers to have a scale
        xlabelN = max(data[:,0])
        ylabelN = max(data[:,1])
        xlabelposx = xpos+xsize
        xlabelposy = ypos+10
        zerolabelposx = xpos-10
        zerolabelposy = ypos+10
        ylabelposx = xpos-10
        ylabelposy = ypos-ysize
        w.create_text(xlabelposx, xlabelposy, fill = current_color,anchor=NW ,text=xlabelN)
        w.create_text(ylabelposx, ylabelposy, fill = current_color,anchor=SE ,text=ylabelN)
        w.create_text(zerolabelposx, zerolabelposy, fill = current_color,anchor=NE ,text=0)

# Append data to the plot
def appendData(newx,newy):
    global data
    global plotcolor
    # If the array is empty, create a new one
    if np.size(data) == 0:
        data = np.array([[newx,newy]])
    else:
        newdata = np.array([[newx,newy]])
        data = np.concatenate((data,newdata),axis=0)


# Main method
def readin():
    while True:
        try:
            readcommand()
        except KeyboardInterrupt:
            print("Keyboard Interrupt. Exiting.")
            sys.exit()
        except EOFError:
            print("EOF error. Exiting.")
            sys.exit()


def readcommand():
    global current_color
    command = input()
    words = command.split()
    # Drawing commands
    if len(words) >= 1:
        if words[0] == 'rectangle':
            fromx = float(words[1])
            fromy = float(words[2])
            tox = float(words[3])
            toy = float(words[4])
            w.create_rectangle(fromx, fromy, tox, toy, fill=current_color, outline=current_color)
        elif words[0] == 'point':
            x = float(words[1])
            y = float(words[2])
            w.create_rectangle(x, y, x+1, y+1, outline=current_color)
        elif words[0] == 'line':
            fromx = float(words[1])
            fromy = float(words[2])
            tox = float(words[3])
            toy = float(words[4])
            w.create_line(fromx,fromy,tox,toy, fill=current_color)
        elif words[0] == 'changesize':
            width = float(words[1])
            height = float(words[2])
            w.config(width=width, height=height)
        elif words[0] == 'changebg':
            w.config(bg=words[1])
        elif words[0] == 'changecolor':
            current_color = words[1]
        elif words[0] == 'txt':
            if len(words) > 3:
                x = float(words[1])
                y = float(words[2])
                w.create_text(x, y,fill = current_color,anchor=NW ,text=words[3:])
        # Plotting commands
        elif words[0] == 'addplotpoints':
            if len(words) == 3:
                x = float(words[1])
                y = float(words[2])
                appendData(x,y)
        elif words[0] == 'plot':
            if len(words) == 5:
                xpos = float(words[1])
                ypos = float(words[2])
                xsize = float(words[3])
                ysize = float(words[4])
                miniplot(xpos, ypos, xsize, ysize)
        # Canvas commands
        elif words[0] == 'clear':
            w.delete(ALL)
        elif words[0] == 'update':
            master.update()
        elif words[0] == 'quit':
            sys.exit()
        else:
            print('Command', command, 'not recognized')
    else:
        print('Error. Empty command')



# Begin program
master.after(1000,readin)
mainloop()