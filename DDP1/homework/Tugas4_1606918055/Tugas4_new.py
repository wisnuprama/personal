'''
Source Code   : Tugas4_1606918055.py
Author        : Wisnu Pramadhitya R
Created       : Desember 10, 2016
Description   : Tugas 4 DDP 1, paint program MK.II
Problem       : > can't erase pencil because the mecanism
                  to make pencil as None object when run module
                  MyCanvas.selectObject() to prevent MyCanvas.dragObject()
                  move pencil.
'''
from tkinter import (Tk, Canvas, messagebox, Frame, Label, Button, Text, Entry, Toplevel,
                     Menu, OptionMenu, StringVar, colorchooser, Scale, IntVar, TclError,
                     filedialog)
from tkinter import (YES, LEFT, RIGHT, BOTTOM, TOP, BOTH, ALL, END, ACTIVE,
                     RAISED, NW, DISABLED, NORMAL, VERTICAL, HORIZONTAL, X, Y,
                     RIDGE)

class IllegalValue:
    'messagebox: warn user about their input - input not an integer'
    def __init__(self, master=None):
        messagebox.showwarning('Illegal Value', self.__str__(), parent=master)
    def __str__(self):
        return 'Not an integer!\nPlease, try again.'

class ValueTooSmall:
    'messagebox: warn user about their input - too small'
    def __init__(self, master=None):
        messagebox.showwarning('Too Small', self.__str__(), parent=master)
    def __str__(self):
        return 'The value must be >= 5, Please, try again.'

class AskDeleteALL:
    'messagebox: ask confirmation to clear canvas'
    def __init__(self, master=None):
        self.result = messagebox.askyesno('Clear Canvas', self.__str__(), parent=master)
    def __str__(self):
        return 'Are you sure want to clear the Canvas?'
    def get(self):
        return self.result

class AskQuit:
    'messagebox: ask confirmation to quit from application'
    def __init__(self, master=None):
        self.result = messagebox.askyesno('Quit', self.__str__(), parent=master)
    def __str__(self):
        return 'Are you sure want to quit'
    def get(self):
        return self.result

class PopUpWindow(Toplevel):
    'Pop-up Window, make dialog to interact with user'
    def __init__(self, master=None, title=str(), text=str(), inputType=str()):
        # set top level
        Toplevel.__init__(self, master)
        self.transient(master) # connect to main window that calls Pop-up
        self.master = master
        self.title(title)

        # fix size
        self.geometry('200x75')
        self.resizable(0, 0)

        # init
        self.initial_focus = self
        self.inputType = inputType
        self.result = None
        width = 10

        if inputType == 'int':
            self.result = IntVar()
            width = 5
            default = 5
        elif inputType == 'str':
            self.result = StringVar()
            width = 30
            default = str()

        # default GUI
        Label(self, text=text).pack()

        self.entry = Entry(self, width=width)
        self.entry.insert(0, default)
        self.entry.pack()
        self.buttonset()

        # focus to THIS window and freeze other
        self.grab_set() # not working, don't know why, it's the core
                        # error logs:
                        # _tkinter.TclError: grab failed: window not viewable. EDIT: it's working

        self.initial_focus.focus_set()
        self.wait_window(self)

    def buttonset(self):
        'Create button: OK and Cancel'
        # make own frame
        boxset = Frame(self)

        # set the buttons
        but_1 = Button(boxset, text="OK", width=10,
                       command=self.isOkay,
                       default=ACTIVE)
        but_1.pack(side=LEFT)

        but_0 = Button(boxset, text="Cancel", width=10,
                       command=self.cancel)
        but_0.pack(side=LEFT)

        # bind
        self.bind("<Return>", self.isOkay)
        self.bind("<Escape>", self.cancel)

        boxset.pack()

    def isOkay(self, event=None):
        'React after press okay'
        # make it presistent when input is not valid
        if not self.validate():
            self.initial_focus.focus_set() # put focus back
            return

        self.withdraw()
        self.update_idletasks()
        self.master.focus_set() # focus back to main window
        self.destroy()

    def cancel(self, event=None):
        'Close the pop-up window'
        # set value when user cancel the dialog
        if self.inputType == 'int':
            self.result.set(10)
        else:
            self.result.set('')

        self.master.focus_set() # focus back to main window
        self.destroy()

    def validate(self):
        'Validate the input'
        try:
            if self.inputType == 'int':
                # validate the input for int
                self.result.set(self.entry.get())
                testNum = self.result.get()
                if testNum >= 5:
                    return True
                else:
                    ValueTooSmall()
                    return False
            else:
                self.result.set(self.entry.get())
                self.result.get()
                return True

        except TclError:
            IllegalValue(self)
            return False

    def get(self):
        return self.result.get()

class ColorPicker:
    'colorchooser for fill color'
    def __init__(self, master=None, canvas=None, button_text=str()):
        self.master = master
        self.canvas = canvas
        #---
        self.color = ((0, 0, 0), '#000000') # default color

        # create button: call color chooser
        self.button_color = Button(master, text=button_text,
                                   bg=self.color[1], width=10,
                                   command=self.callPicker)
        self.button_color.pack()

    def callPicker(self):
        'Color Chooser'
        # call the color chooser
        self.color = colorchooser.askcolor(color='#000000', parent=self.master)
        self.canvas.setFillcolor(self.getColor())
        self.button_color.config(bg=self.getColor()) # change button's color

    def getColor(self):
        'return self.color: hex'
        return self.color[1]

class OutlinePicker(ColorPicker):
    'colorchooser for color outline'
    def __init__(self, master=None, canvas=None, button_text=str()):
        ColorPicker.__init__(self, master, canvas, button_text)

    def callPicker(self):
        # call the color chooser
        self.color = colorchooser.askcolor(color='#000000', parent=self.master)
        self.canvas.setOutline(self.getColor())
        self.button_color.config(bg=self.getColor()) # change button's color

class MenuBar:
    'Create Menubar'
    def __init__(self, master=None, canvas=None):
        # set parent
        self.master = master
        self.canvas = canvas

        # init
        self.result = None
        self.menu_value = IntVar()
        self.menu_value.set(0)

        # create a toplevel menu
        self.menubar = Menu(master)
        self.createSubmenu()

        # display the menu
        master.config(menu=self.menubar)

    def createSubmenu(self):
        'create submenu: FILE, FREE, SHAPE'
        # create submenu - FILE
        self.menu_file = Menu(self.menubar, tearoff=0)
        self.menu_file.add_command(label='Save', command=self.saveFile)
        self.menu_file.add_command(label='Exit', command=self.quit)
        self.menubar.add_cascade(label='File', menu=self.menu_file)

        # create submenu - FREE
        self.menu_basic = Menu(self.menubar, tearoff=1)

        def set_menu_value():
            # Pencil submenu is command,
            # it helps to changes the radiobutton value
            self.menu_value.set(2)

        self.menu_basic.add_radiobutton(label='Grab',
                                        variable=self.menu_value,
                                        value=1)
        self.menu_basic.add_command(label='Pencil',
                                    command=set_menu_value)
        self.menu_basic.add_radiobutton(label='Eraser',
                                        variable=self.menu_value,
                                        value=3)
        self.menu_basic.add_command(label='Text',
                                    command=self.applyMenuText)
        self.menubar.add_cascade(label='Basic', menu=self.menu_basic)

        # create submenu - SHAPE
        self.menu_shape = Menu(self.menubar, tearoff=1)

        self.menu_shape.add_radiobutton(label='Line',
                                        variable=self.menu_value,
                                        value=4)
        self.menu_shape.add_radiobutton(label='Free Circle',
                                        variable=self.menu_value,
                                        value=5)
        self.menu_shape.add_radiobutton(label='Free Rectangle',
                                        variable=self.menu_value,
                                        value=6)
        self.menu_shape.add_command(label='Circle',
                                    command=self.applyMenuCircle)
        self.menu_shape.add_command(label='Rectangle',
                                    command=self.applyMenuRect)
        self.menubar.add_cascade(label='Shape', menu=self.menu_shape)

    def applyMenuCircle(self):
        'Ask user to input radius'
        # set value
        self.menu_value.set(7)

        # ask user
        askRadius = PopUpWindow(self.master, title='Set Radius',
                                text='Radius?', inputType='int')
        self.result = (askRadius.get())

    def applyMenuRect(self):
        'Ask user to input width and height'
        # set value
        self.menu_value.set(8)

        # ask user
        askWidth = PopUpWindow(self.master, title='Set Width',
                               text='Width?', inputType='int')
        askHeight = PopUpWindow(self.master, title='Set Height',
                                text='Height?', inputType='int')
        self.result = (askWidth.get(), askHeight.get())

    def applyMenuText(self):
        'Ask user to input text'
        # set Value
        self.menu_value.set(9)

        # ask User
        askText = PopUpWindow(self.master, title='Text',
                              text='Text:', inputType='str')
        self.result = askText.get()

    def getRadioButtonValue(self):
        'get RadioButton Value'
        return self.menu_value.get()

    def get(self):
        'get result'
        return self.result

    def saveFile(self):
        'Save file as image'
        # init
        title = 'Save File'
        ext = [('PostScript', '*.ps')]
        file = filedialog.asksaveasfilename(parent=self.master,
                                            title=title,
                                            filetypes=ext,
                                            defaultextension='.ps')
        if file == None:
            return
        # using postscript for now
        self.canvas.postscript(colormode='color', file=file)

    def quit(self):
        ask = AskQuit(self.master)

        if ask.get() == False:
            return

        self.master.quit()

class ToolThickness:
    'Slider for set the outline thickness'
    def __init__(self, master=None, canvas=None):
        self.master = master
        self.canvas = canvas

        # call Scale
        self.scale = Scale(master, label='Thickness',
                            from_ = 0, to = 20,
                            orient = HORIZONTAL,
                            command = self.setThickness)
        self.scale.pack()

        # default
        self.scale.set(2)

    def setThickness(self, event):
        'automatically set value to var thickness on canvas'
        try:
            self.canvas.setThickness(int(event))
        except AttributeError:
            print('\'NoneType\': No object canvas has detected')

class TheCanvas(Frame):
    'Main Canvas'
    def __init__(self, master=None, menubar=None):
        Frame.__init__(self, master)
        self.master = master

        # init
        self.x, self.y = 0, 0
        self.start_x, self.start_y = None, None
        self.fillcolor = self.outline = '#000000'
        self.radius, self.width, self.height = 10, 10, 10
        self.selected_object = None
        self.thickness = 2

        self.canvas = Canvas(self, relief=RIDGE,
                             borderwidth=3, bg='#ffffff')

        # menubar
        self.menubar = MenuBar(master, self.canvas)

        # set main Canvas
        self.__draw_object = 0
        self.canvas.pack(side=TOP, fill=BOTH, expand=True)

        # bind
        self.canvas.bind("<ButtonPress-1>", self.isPressing)
        self.canvas.bind("<B1-Motion>", self.isMoving)
        self.canvas.bind("<ButtonRelease-1>", self.isReleased)

    def isPressing(self, event):
        'react when ButtonPress-1'
        self.canvas.config(cursor='arrow')
        self.canvas.tag_bind('ButtonPress-1', )
        # object to draw
        self.__draw_object = self.menubar.getRadioButtonValue()

        # start position
        self.start_x = event.x
        self.start_y = event.y

        # start drawing
        if self.__draw_object == 1:
            # grab
            self.canvas.config(cursor='center_ptr')
            self.selected_object = self.selectObject(self.start_x, self.start_y)

        elif self.__draw_object == 2:
            # pencil
            self.canvas.config(cursor='pencil')

        elif self.__draw_object == 3:
            # eraser
            self.canvas.config(cursor='box_spiral')
            self.deleteObject()

        elif self.__draw_object == 4:
            # create line
            self.canvas.config(cursor='cross')
            self.draw = self.canvas.create_line(self.start_x, self.start_y,
                                                self.start_x, self.start_y,
                                                fill=self.getOutline(),
                                                width=self.getThickness())
        elif self.__draw_object == 5:
            # create free oval
            self.canvas.config(cursor='cross')
            self.draw = self.canvas.create_oval(self.x, self.y,
                                                1, 1, fill=self.getFillcolor(),
                                                outline=self.getOutline(),
                                                width=self.getThickness())
        elif self.__draw_object == 6:
            # create free rect
            self.canvas.config(cursor='cross')
            self.draw = self.canvas.create_rectangle(self.x, self.y,
                                                     1, 1, fill=self.getFillcolor(),
                                                     outline=self.getOutline(),
                                                     width=self.getThickness())
        # under construction
        elif self.__draw_object == 7:
            # create circle by user input on pop up window
            self.canvas.config(cursor='cross')
            radius = self.menubar.get()
            self.setRadius(radius)
            self.createCircle(self.start_x, self.start_y, self.getRadius(),
                              fill=self.getFillcolor(), line=self.getOutline(),
                              width=self.getThickness())

        elif self.__draw_object == 8:
            # create rect by user input on pop up window
            self.canvas.config(cursor='cross')
            width, height = self.menubar.get()
            self.createRectangle(self.start_x, self.start_y, width, height,
                                fill=self.getFillcolor(), line=self.getOutline(),
                                width=self.getThickness())

        elif self.__draw_object == 9:
            # create Text
            self.canvas.config()
            text = self.menubar.get()
            self.createText((self.start_x, self.start_y),
                            fill=self.getFillcolor(),
                            text=text)

    def isMoving(self, event):
        'active when pointer moving - B1 motion'
        now_X, now_Y = event.x, event.y

        # moving
        if self.__draw_object == 1:
            # drag and move
            self.dragObject(now_X, now_Y)

        elif self.__draw_object == 2:
            # pencil
            self.draw = self.canvas.create_line(self.start_x, self.start_y,
                                                now_X, now_Y,
                                                fill=self.getFillcolor(), tags='pencil',
                                                width=self.getThickness())
            self.start_x, self.start_y = now_X, now_Y

        elif self.__draw_object == 3:
            # eraser
            # too sensitive
            self.canvas.config(cursor='box_spiral')
            self.deleteObject()

        elif 4 <= self.__draw_object <= 8:
            # move other
            self.canvas.coords(self.draw, self.start_x, self.start_y, now_X, now_Y)

    def isReleased(self, event):
        'Buttonrelease-1'
        pass

    def selectObject(self, x, y):
        'select object on canvas by clicking on it'
        objID = self.canvas.find_closest(x, y, halo=0, start=None)
        if 'pencil' in self.canvas.gettags(objID):
            return None # pencil as None
        return objID

    def dragObject(self, x, y):
        'drag and move object on canvas to new pos'
        if self.selected_object: # always true > 0
                diff_X = x - self.start_x
                diff_Y = y - self.start_y
                self.canvas.move(self.selected_object, diff_X, diff_Y)
                self.start_x, self.start_y = x, y

    def deleteObject(self):
        'delete selected object'
        objID = self.canvas.find_closest(self.start_x, self.start_y, halo=0)
        self.canvas.delete(objID)

    def createRectangle(self, x, y, length, height, fill, line, width):
        'draw rectangle by user input'
        self.canvas.create_rectangle(x-length, y-height, x+length, y+height,
                                    fill=fill,
                                    outline=line,
                                    width=width)

    def createCircle(self, x, y, radius, fill, line, width):
        'draw circle by user input'
        self.canvas.create_oval(x-radius, y-radius, x+radius, y+radius,
                                fill=fill,
                                outline=line,
                                width=width)

    def createText(self, pos, text, fill):
        self.canvas.create_text(pos, text=text, fill=fill)

    # setter-gather
    def setThickness(self, thick):
        self.thickness = thick

    def getThickness(self):
        return self.thickness

    def setFillcolor(self, color):
        self.fillcolor = color

    def setOutline(self, color):
        self.outline = color

    def getFillcolor(self):
        return self.fillcolor

    def getOutline(self):
        return self.outline

    def setRadius(self, radius):
        self.radius = radius

    def getRadius(self):
        return self.radius

    def setWidth(self, width):
        self.width = width

    def getWidth(self):
        return self.width

    def setHeight(self, height):
        self.height = height

    def getHeight(self):
        return self.height
    # ----------------------

    def deleteAll(self):
        'Clear ALL object from the Canvas'
        if len(self.canvas.find_all()) > 0:
            ask = AskDeleteALL(self)
            if ask.get() == True:
                self.canvas.delete(ALL)

class RightFrame(Frame):
    'Create frame for right side of canvas'
    def __init__(self, master=None, canvas=None):
        Frame.__init__(self, master)

        # create Object
        self.color = ColorPicker(self, canvas, button_text='Color')
        self.outline = OutlinePicker(self, canvas, button_text='Line')
        self.thickness = ToolThickness(self, canvas)
        self.clearALL = Button(self, text='Clear Canvas',
                               command=canvas.deleteAll).pack(side=BOTTOM)



class Application:
    'Main Application'
    def __init__(self, master=None):
        canvas = TheCanvas(master)
        canvas.pack(fill=BOTH, expand=True, side=LEFT)
        right = RightFrame(master, canvas).pack(fill=BOTH, expand=False, side=RIGHT)

if __name__ == '__main__':
    root = Tk()
    root.winfo_toplevel()
    root.geometry('800x600')
    root.title('Paint - 1606918055')
    main = Application(root)
    root.mainloop()