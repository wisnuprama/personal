# TUGAS 3 DDP 1
## Wisnu Pramadhitya Ramadhan 1606918055
### file: Tugas3_1606918055_WisnuPramadhitya.py, ListShape.py, Geometry.py 

## Synopsis

My Paint merupakan Tugas3 dari DDP 1 Fasilkom UI tahun ajaran 2016. Program ini dapat digunakan untuk menggambar objek lingkaran, persegi, dan persegi panjang dalam canvas yang telah disediakan berdasarkan koordinat yang diberikan user.
Beberapa method yang bisa dilakukan:

1. Menggambar -> Shape -> Circle
                       -> Square
                       -> Rectangle

2. Menghapus objek

3. Memindahkan objek

4. Merender objek yang telah didaftarkan

User Guide(command-entry):

1. Membuat Lingkaran:
> addCircle [Nama] [X] [Y] [Radius] [Warna] [Warna Garis]

2. Membuat Persegi:
> addSquare [Nama] [X] [Y] [Sisi] [Warna] [Warna Garis]

3. Membuat Persegi Panjang:
> addRectangle [Nama] [X] [Y] [Panjang] [Lebar] [Warna] [Warna Garis]

4. Menampilkan gambar:
> render

5. Memindahkan Objek:
> move [Nama] [X baru] [Y baru]

6. Menghapus Objek:
> delete [Nama]

7. Keluar:
> quit

## Module
1. Geometry: berisi kelas-kelas yang dibutuhkan untuk membuat objek Shape
2. ListShape: berisi kelas ListShape(list) dan handler. Kelas ListShape berfungsi sebagai container dari objek-objek Geometry dan memiliki beberapa method seperti menghapus, menambahkan, memindahkan, check apakah objek sudah terdaftar, dll dengan pengaturan sesuai ketentuan tugas.
3. Tkinter: GUI dan untuk menggambar (Canvas)

## Installation

1. Open Folder Tugas3_1606918055_WisnuPramadhitya

2. Open Tugas3_1606918055_WisnuPramadhitya on IDLE, or

3. (Windows) Run by click twice 'Tugas3_1606918055_WisnuPramadhitya.py' in the its folder or open manually with cmd 'python Tugas3_1606918055_WisnuPramadhitya.py'

4. (UNIX/Linux) Open in Terminal by 'python Tugas3_1606918055_WisnuPramadhitya.py'

Require:
> Python 3.x

> tkinter, Geometry, ListShape
