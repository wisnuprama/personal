def countdown(n):
    if n <= 0:
        'base case'
        print('wow')
    else:
        'Recursion case: recursive calls on input arguments that are closer to the base case'
        print(n)
        countdown(n-1)

def countdown1(n):
    while n > 0:
        print(n)
        n -= 1
    print('wow')

countdown(10)
'''
1. define one or more bases cases for which the problem is solved directly
2. express the solutin of the problem in terms of solution to subproblem of the problem
'''
    