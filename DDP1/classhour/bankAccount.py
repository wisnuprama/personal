class bankAccount:
  def __init__(self, balance = 0):
    self.balance = balance

  def deposist(self, balance):
    self.balance += balance
    
  def withdraw(self, balance):
    self.balance -= balance
    
  def transferTo(self, tujuan, uang):
    tujuan.deposist(uang)
    self.withdraw(uang)
    
  def getBalance(self):
    return self.balance