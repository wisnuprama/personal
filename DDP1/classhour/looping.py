def before0(table):
    ' It will print before 0 and break if num == 0'
    for row in table:
        for num in row:
            if num == 0:
                break
            print(num, end=' ')
        print()

def ignored0(table):
    'It will print before and after 0'
    for row in table:
        for num in row:
            if num == 0:
                continue
            print(num, end=' ')
        print()
table = [[0,1,10],[2,12,120]]
before0(table)
ignored0(table)