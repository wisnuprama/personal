def Arithmetic(lst):
    if len(lst) < 2:
        return True
    
    diff = lst[1] - lst[0]
    for i in range(1, len(lst),-1):
        if lst[i+1] - lst[i] != diff:
            return False

    return True

print(Arithmetic([1,2,3,4,5]))