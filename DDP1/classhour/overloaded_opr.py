class Pecahan:
  '..'
  def __init__(self, pembilang = 0, penyebut = 0):
    self.pembilang = pembilang
    self.penyebut = penyebut
  
  def getPembilang(self): 
    return self.pembilang

  def getPenyebut(self): 
    return self.penyebut

  def getFPB(self, pembilang, penyebut):
    if penyebut > 0:
      self.getFPB(penyebut, pembilang % penyebut)
    return pembilang

  def sederhana(self):
    if self.penyebut % self.pembilang == 0:
      fpb = self.getFPB(self.pembilang, self.penyebut)
      self.pembilang = self.pembilang / fpb
      self.penyebut = self.penyebut / fpb
    elif self.pembilang % self.penyebut == 0:
      self.pembilang = self.pembilang // self.penyebut
      self.penyebut = 1
    ###
    
  def __repr__(self):
    return 'Pec({}, {})'.format(self.pembilang, self.penyebut)
  
  