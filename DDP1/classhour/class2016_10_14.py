'''
# class definition
class <class name>: -> class <class name>(object):
class <class name> (<super class>):

object is a built-in class that has no Attribute
object is the top class in python

# inheritance (pewarisan)
    hubungan antar dua kelas
    
    polimorfisme
        + overloading
        + overriding
    
    overloading -> some classes that have same implementation but have different behavior
            like int() + int(), list() + list()
    overriding -> new class that can almost inherit attributes from an existing class, but not quite same.
            same behavior but different implementation. We change the implementation of same method from superclass (override)
# superclass method can be inherited as-is, overridden, or extended
'''