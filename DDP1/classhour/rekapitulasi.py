class Items:
  '...'
  def __init__(self, name = '', harga = 0):
    self.name = name
    self.harga = harga
  
  def getName(self):
    return self.name

  def getHarga(self):
    return self.harga
  
class Keranjang:
  '...'
  lstItem = []

  def addItem(self, item):
    self.lstItem.append(item)
  
  def getTermahal(self):
    self.harga = self.lstItem[0].getHarga()
    self.termahal = None
    for item in self.lstItem:
      if item.getHarga() > self.harga:
        self.harga = item.getHarga()
        self.termahal = (item.getName(), item.getHarga())
    return self.termahal
  
  def getTotal(self):
    self.total = 0
    for item in self.lstItem:
      self.total += item.harga
    return self.total

def main():
  cart = Keranjang()
  count, wow = 1, 1
  jumlahItem = int(input('Masukan banyaknya item: '))
  lst = []

  while count <= jumlahItem:
    namaItem = str(input('Nama item ke-{}: '.format(count)))
    hargaItem = int(input('Harga item ke-{}: '.format(count)))
    
    item = Items(namaItem, hargaItem)
    cart.addItem(item)

    count += 1

  print(cart.getTermahal())
  print(cart.getTotal())

if __name__ == "__main__":
  main()