'''
OKT 21 2016 - Asistensi
Wisnu Pramadhitya Ramadhan
'''

'''
Materi:
--Exception--
Menangkap error!
BaseException
 +-- SystemExit
 +-- KeyboardInterrupt
 +-- GeneratorExit
 +-- Exception
      +-- StopIteration
      +-- StopAsyncIteration
      +-- ArithmeticError
      |    +-- FloatingPointError
      |    +-- OverflowError
      |    +-- ZeroDivisionError
      +-- AssertionError
      +-- AttributeError
      +-- BufferError
      +-- EOFError
      +-- ImportError
      +-- LookupError
      |    +-- IndexError
      |    +-- KeyError
      +-- MemoryError
      +-- NameError
      |    +-- UnboundLocalError
      +-- OSError
      |    +-- BlockingIOError
      |    +-- ChildProcessError
      |    +-- ConnectionError
      |    |    +-- BrokenPipeError
      |    |    +-- ConnectionAbortedError
      |    |    +-- ConnectionRefusedError
      |    |    +-- ConnectionResetError
      |    +-- FileExistsError
      |    +-- FileNotFoundError
      |    +-- InterruptedError
      |    +-- IsADirectoryError
      |    +-- NotADirectoryError
      |    +-- PermissionError
      |    +-- ProcessLookupError
      |    +-- TimeoutError
      +-- ReferenceError
      +-- RuntimeError
      |    +-- NotImplementedError
      |    +-- RecursionError
      +-- SyntaxError
      |    +-- IndentationError
      |         +-- TabError
      +-- SystemError
      +-- TypeError
      +-- ValueError
      |    +-- UnicodeError
      |         +-- UnicodeDecodeError
      |         +-- UnicodeEncodeError
      |         +-- UnicodeTranslateError
      +-- Warning
           +-- DeprecationWarning
           +-- PendingDeprecationWarning
           +-- RuntimeWarning
           +-- SyntaxWarning
           +-- UserWarning
           +-- FutureWarning
           +-- ImportWarning
           +-- UnicodeWarning
           +-- BytesWarning
           +-- ResourceWarning

'''

def ion2e(string):
    if string[-3::1] == 'ion':
        return string[:len(string)-3] + 'e'
    return string
print(ion2e('congratulation'))

def catdog(string):
    cat, dog = 0, 0
    for i in range(len(string)-2):
        if string[i:i+3] == 'cat':
            cat += 1
        elif string[i:i+3] == 'dog':
            dog += 1
    if cat == dog: return False
    else: return True
print(catdog('cacatdodog'))

def pyramid(n):
    for i in range(n):
        for j in range(1,i):
            if (j % 2 == 0): print('*',end='')
            else: print('-',end='')
        print()
pyramid(10)

def samakaki(n):
    for i in range(n):
        j = 0
        while (j < n - i - 1):
            print(' ', end='')
            j += 1
        for k in range(i+1):
            print('*', end=' ')
        print()
print(samakaki(4))

# average word lenght in text
# line = file.read()
string = 'gvwda wvda ywdvyvdy agv wdyvawt dva davwdygavwd yvadva dyv'
string_split = string.split(' ')
count = 0 # counting character
for i in string_split:
    count += len(i) 
print(count / len(string_split)) # mean

def dollar(string): 
    "Mengganti semua karakter yang sama dengan huruf depan ke '$'" 
    count = ''
    for i in range(1, len(string)):
        if string[0] == string[i]: count += '$'
        else: count += string[i]
    return string[0] + count
print(dollar('restart'))

def ispalindrone(string):
    return string[::-1] == string # str[str:end:step]
print(ispalindrone('wow'))