"""
Source Code   : lab05a.py
Author        : Wisnu Pramadhitya R
Created       : October 05, 2016
Description   : 					   
"""
col,row = -1,-1
axis,absis = 0,0 # y baris x kolom
noBoom,boom = '-','#'

while col < 0 and row < 0: # input !< 0
    col,row = eval(input('Masukan luas arena(..,..): '))
axis,absis = eval(input('Masukan lokasi bom(..,..): '))


#arena = [[noBoom for x in range(panjang)] for y in range(lebar)]
#print(arena)

for j in range(row):
    for i in range(col):
        if j == (absis-1) and i == (axis-1):
            print(boom, end=' ') 
        elif j == (absis-1) and i == (axis-2):
            print(boom, end=' ')
        elif j == (absis) and i == (axis-1):
            print(boom, end=' ')
        elif j == (absis-1) and i == (axis):
            print(boom, end=' ')
        elif j == (absis-2) and i == (axis-1):
            print(boom, end=' ')        
        else:
            print(noBoom, end=' ')
    print()
