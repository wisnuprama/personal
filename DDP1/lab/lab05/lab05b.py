"""
Source Code   : lab05a.py
Author        : Wisnu Pramadhitya R
Created       : October 05, 2016
Description   : Operasi pertambahan matriks (pertambahan && pertambahan diagonal)	   
"""
import sys
#matriks = [['1','2','3'],['4','5','6'],['7','8','9']]
#matriks2 = [['0','1','2'],['3','4','5'],['6','7','8']]

def elemen(matriks,num):
    '''
    Menambahkan elemen matriks pada matriks pertama
    num: jumlah baris dan kolom (n*n) == 3*3
    '''
    for i in range(num):
        elemen = input('Masukan elemen pada matriks: ')
        matriks.append(elemen.split(' ')) # menambahkan nilai ke list matriks
    return matriks 

def elemen2(matriks2,num):
    '''
    Menambahkan elemen matriks pada matriks kedua
    num: jumlah baris dan kolom (n*n) == 3*3
    '''
    for i in range(num):
        elemen = input('Masukan elemen pada matriks: ')
        matriks2.append(elemen.split(' ')) # menambahkan nilai ke list matriks2
    return matriks2

def matriksPrint(cetak):
    '''
    Melakukan cetak matriks
    cetak: matriks (list)
    '''
    for row in cetak: # baris
        for elemen in row: # elemen dalam baris
            print('  {:3}'.format(elemen), end=' ')
        print()

def matriksTambah(matriks,matriks2,num):
    '''
    Melakukan pertambahan matriks
    matriks: matriks pertama (list)
    matriks2: matriks kedua (list)
    num: bentuk matriks (n*n)
    '''
    for i in range(num): #index baris
        for j in range(num): # index dalam baris
            matriks[i][j] = str(eval(matriks[i][j]) + eval(matriks2[i][j]))
    return matriks

def matriksDiagonal(matriks,matriks2,num):
    '''
    Melakukan pertambahan diagonal pada matriks
    matriks: matriks pertama (list)
    matriks2: matriks kedua (list)
    num: bentuk matriks (n*n)
    '''
    var = 0
    for i in range(num): # index baris
        for j in range(num): # index dalam baris
            if i == j:
                # index diagonal sama
                tmp = eval(matriks[i][j]) + eval(matriks2[i][j]) 
                total+= tmp # nilai akhir pertambahan diagonal
    print('{}'.format(total),end='\n')

def main():
    '''
    Main operasi dalam program
    '''
    matriks, matriks2, num = [],[],0 # inisiasi 
    
    n_matriks = int(input('Masukan jumlah baris dan kolom: '))
    elemen(matriks,n_matriks) # memanggil elemen() untuk meminta isian matriks pertama

    while True:
        menu = input('Masukan operasi yang ingin dilakukan: ')    
        menu = menu.upper() # normalisasi input
        
        if menu == 'CETAK':
            matriksPrint(matriks) # memanggil matriksPrint() untuk mencetak matriks ke display
        elif menu == 'TAMBAH':
            elemen2(matriks2,n_matriks) # memanggil elemen2() imbas dari user menginginkan pertambahan matriks dari dua matriks
            matriksTambah(matriks,matriks2,n_matriks) # memanggil matriksTambah() untuk pertambahan matriks
        elif menu == 'DIAGONAL':
            matriksDiagonal(matriks,matriks2,n_matriks) # memanggil matriksDiagonal() untuk pertambahan matriks diagonal
        else:
            input("Press ENTER to exit")
            sys.exit(0) # exit()


#matriksDiagonal(matriks,matriks2,3)
if __name__ == "__main__":
    main()