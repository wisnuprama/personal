class Trainer:
    '...'
    def __init__(self, namaTrainer = ''):
      self.namaTrainer = namaTrainer
      self.lstGhokemon = []
    def getNamaTrainer(self):
      return self.namaTrainer
    def setNamaTrainer(self, nama = ''):
      self.namaTrainer = nama
      return self.namaTrainer
    def addGhokemon(self, ghokemon):
      self.lstGhokemon.append(ghokemon)
      return '{} telah berhasil ditambahkan'.format(ghokemon.getNamaGhk())
    def eraseGhokemon(self, namaGhk):
      try:
        index = []
        for ghk in range(len(self.lstGhokemon)):
          if self.lstGhokemon[ghk].getNamaGhk() == namaGhk:
            index.append(ghk)
        del self.lstGhokemon[index[0]]
        print('{} telah berhasil dibuang'.format(namaGhk))
      except IndexError:
        print('{} tidak ditemukan'.format(namaGhk))
    def showGhokemon(self):
      for ghk in self.lstGhokemon:
        print(ghk.getNamaGhk(), end=' ')
      print()
    def showTypeGhokemon(self, tipe):
      print('Tipe {}'.format(tipe))
      for ghk in self.lstGhokemon:
        if ghk.getTipeGhk().upper() == tipe.upper():
          print(ghk.getNamaGhk(), end=' ')
      print()

class Ghokemon:
    '...'
    def __init__(self, nama = '', hp = 0, tipe = ''):
      self.namaGhk = nama
      self.hpGhk = hp
      self.tipeGhk = tipe
    def setNamaGhk(self, nama): 
      self.namaGhk = nama
      return self.namaGhk
    def setHpGhk(self, hp):
      self.hpGhk = hp 
      return self.hpGhk
    def setTipeGhk(self, tipe):
      self.tipeGhk = tipe
      return self.tipeGhk
    def getNamaGhk(self): return self.namaGhk
    def getHpGhk(self): return self.hpGhk
    def getTipeGhk(self): return self.tipeGhk

class Battleground:
    '...'
    def __init__(self, ghoke, ghoke2):
      self.battlelst = [ghoke, ghoke2]
    def compareType(self):
      self.ghoke, self.ghoke2 = self.battlelst[0], self.battlelst[1]
      tipeGhk, tipeGhk2 = self.ghoke.getTipeGhk().upper(), self.ghoke2.getTipeGhk().upper()
      if tipeGhk == tipeGhk2:
        return self.ghoke.getHpGhk() > self.ghoke2.getHpGhk()
      elif (tipeGhk == 'air' and tipeGhk2 == 'api') or (tipeGhk == 'api' and tipeGhk2 == 'air'):
        if tipeGhk == 'air':
          return True
        else:
          return False
      elif (tipeGhk == 'air' and tipeGhk2 == 'rumput') or (tipeGhk == 'rumput' and tipeGhk2 == 'air'):
        if tipeGhk == 'rumput':
          return True
        else:
          return False
      else:
        if tipeGhk == 'api':
          return True
        else:
          return False
    def takeGhokemon(self, trainer, ghoke):
      nameGhk, nameTrainer = ghoke.getNameGhk(), trainer.getNamaTrainer()
      choice = str(input('Apakah anda ingin mengambil {} (Ya/Tidak)?'.format(nameGhk)))
      if 'IYA' in choice.upper():
        trainer.addGhokemon(ghoke)
        print('Santaimon berhasil ditambahkan\n{} mengambil {}'.format(nameTrainer, namaGhk))
      elif 'TIDAK' in choice.upper():
        print('{} melanjutkan perjalanan'.format(nameTrainer))


def main():
    trainer = Trainer('Honey')
    ghokemon = Ghokemon('Pikadut', 100, 'api')
    trainer.addGhokemon(ghokemon)

    while True:
      cmd = str(input('Masukan perintah: '))
      cmdLst = cmd.split()  
      if 'LIHAT' in cmd.upper():
        if 'AIR' in cmd.upper():
          trainer.showTypeGhokemon(cmdLst[1])
        elif 'API' in cmd.upper():
          trainer.showTypeGhokemon(cmdLst[1])
        elif 'RUMPUT' in cmd.upper():
          trainer.showTypeGhokemon(cmdLst[1])
        else:
          trainer.showGhokemon()
      elif 'BUANG' in cmd.upper():
        trainer.eraseGhokemon(cmdLst[1])
      elif 'TAMBAH' in cmd.upper():
        print(trainer.addGhokemon(Ghokemon(cmdLst[1], cmdLst[2], cmdLst[3])))
      elif 'LAWAN' in cmd.upper():
        ghokeLawan = Ghokemon(cmdLst[1], cmdLst[2], cmdLst[3])
        # menentukan ghokemon
        for ghokemon in trainer.lstGhokemon:
          battle = Battleground(ghokemon, ghokeLawan)
          if battle == True:
            print('{0} memilih {1}\n{0} menang'.format())
        
      elif 'ISTIRAHAT' in cmd.upper():
        return 0

if __name__ == "__main__":
    main()