from math import pi

class Tabung:
  '...'
  def __init__(self, radius = 0, height = 0):
    self.radius = radius
    self.height = height
  def getRadius(self):
    return self.radius
  def getHeight(self):
    return self.height
  def chgHeight(self, height):
    self.height = height
    return self.height
  def chgRadius(self, radius):
    self.radius = radius
    return self.radius
  def getCircumterance(self):
    return 2*pi*self.radius
  def getAreaCircle(self):
    return pi*self.radius**2
  def getAreaTabung(self):
    return 2 * self.getAreaCircle() + self.height * self.getCircumterance()
  def getVolume(self):
    return self.getAreaCircle() * self.height
    
def main():
  humanIn = str(input('Masukan atribut dari tabung(jari-jari dan tinggi): '))
  attr = humanIn.split() 
  tab = Tabung(int(attr[0]), int(attr[1]))
  
  while True:
    cmd = str(input('Masukan perintah: '))
    if 'AMBIL TINGGI' in cmd.upper():
      print(tab.getHeight())    
    elif 'AMBIL JARI-JARI' in cmd.upper():
      print(tab.getRadius())   
    elif 'LUAS PERMUKAAN' in cmd.upper():
      print('{}'.format(float(int(tab.getAreaTabung()))))
    elif 'VOLUME' in cmd.upper():
      print('{}'.format(float(int(tab.getVolume()))))    
    elif 'UBAH TINGGI' in cmd.upper():
      temp = cmd.split()
      tab.chgHeight(int(temp[2]))
    elif 'UBAH JARI' in cmd.upper():
      temp = cmd.split()
      tab.chgRadius(int(temp[2]))
    elif 'SELESAI' in cmd.upper():
      return False

if __name__ == "__main__":
  main()