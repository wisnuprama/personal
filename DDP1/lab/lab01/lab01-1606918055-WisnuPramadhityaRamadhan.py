# name   : t01-1606918055-WisnuPramadhityaRamadhan.py
# subject: DDP-1
# date   : Sep 07, 2016
# desc   : drawing pentagon with turtle module

import turtle

turtle.color('blue') #set color of line to blue
turtle.pendown() #pen is ready for drawing

turtle.left(108) #set angle for 108 degrees to the left
turtle.forward(100) #go forward for 100 units

turtle.right(72) #set second angle for 72 degrees to the right,
                #so the turtle changes the direction to the right
turtle.forward(100) #go forward for 100 units

turtle.right(72) 
turtle.forward(100) 

turtle.right(72)
turtle.forward(100) 

turtle.right(72) 
turtle.forward(100) #finish

turtle.hideturtle() #get rid the turtle

input('Press Enter to continue')
turtle.bye() # exit turtle
