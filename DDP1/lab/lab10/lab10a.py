from tkinter import Tk, Button, Frame, Label, RAISED

counter = 0

class Counting(Frame):

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.pack()

        self.counter = Label(self, text=counter)
        self.increaseBut = Button(self, text='Increase', relief=RAISED, command=self.increase)
        self.decreaseBut = Button(self, text='Decrease', relief=RAISED, command=self.decrease)

        self.counter.pack()
        self.increaseBut.pack()
        self.decreaseBut.pack()

    def increase(self):
        global counter
        counter += 1
        self.counter.config(text=counter)

    def decrease(self):
        global counter
        counter -= 1
        self.counter.config(text=counter)

root = Tk()
root.geometry('300x100')
apps = Counting(root)
apps.pack()
root.mainloop()
