from tkinter import Tk, Button, Frame, Label, Entry, messagebox, StringVar


def getDecimal(biner):
    try:
        return int(biner, 2)
    except ValueError:
        messagebox.showerror('ERROR', 'INPUT BUKAN BINER!')

class DataEntry(Frame):
    '...'
    def __init__(self, master):
        Frame.__init__(self, master)
        self.pack()

        self.labelData = Label(self, text='Data:')
        self.entryBiner = Entry(self, width=10)
        self.entryBiner.bind('<Return>', self.setCommand)
        self.button = Button(self, text='Hasil', command=self.setCommand)
        
        self.res = StringVar()
        self.result = Label(self, textvariable=self.res)

        self.labelData.grid(row=0, column=0)
        self.entryBiner.grid(row=0, column=1)
        self.button.grid(row=1, column=0)
        self.result.grid(row=2, columnspan=4)

    
    def setCommand(self, event=None):
        usrInput = self.entryBiner.get()
        print(usrInput)
        temp = getDecimal(str(usrInput))
        if temp != None:
            self.res.set(temp)

root = Tk()
root.geometry('300x75')
app = DataEntry(root)
app.pack()
root.mainloop()
