"""
Source Code   : lab06c.py
Author        : Wisnu Pramadhitya R
Created       : MM DD, YY
Description   : 				   
"""

infile = open(input('Masukan nama file input: '),'r')
kata = int(input('Masukan jumlah kata: '))
outfile = open(input('Masukan nama file output: '),'w')

reading = (infile.readline()).split()
reading = reading[::-1]
print(reading)

counter = 0
for i in range(kata):
    if (counter == len(reading)): 
        counter = 0
    outfile.write('{}\n'.format(reading[counter]))
    counter+= 1
    
outfile.close();infile.close()
