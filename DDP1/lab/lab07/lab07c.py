"""
Source Code   : lab07c.py
Author        : Wisnu Pramadhitya R
Created       : Oktober 26, 2016
Description   : SimpleMo v0.0.7   
"""
import sys
def simpan(kegiatan, waktu):
    waktu = waktu.replace('.',':')
    return '{} - {}'.format(kegiatan, waktu)

def lihat(memo):
    for i in range(1,len(memo)):
        print('{}. {}'.format(i, memo[i]))

def tandai(counter, memo):
    memo[counter] = '{} #SUDAH'.format(memo[counter])
    return memo

def error(i):
    try:
        memo[i]
        return True
    except IndexError: 
        print('Tidak ada jadwal nomor {}'.format(i))

memo = ['',]
counter = 1

while True:
    perintah = input('Masukan perintah: ').upper()
    if perintah == 'SIMPAN':
        kegiatan = input('Masukan kegiatan: ')
        waktu = input('Masukan waktu: ')
        memo.append(simpan(kegiatan, waktu))
        print('Jadwal berhasil disimpan')
    elif perintah == 'LIHAT': lihat(memo)
    elif perintah == 'TANDAI': 
        memo = tandai(counter, memo)
        counter += 1
        print('Jadwal berhasil ditandai')
    elif perintah == 'HAPUS':
        while True:
            noHapus = int(input('Masukan nomor kegiatan: '))
            if error(noHapus) == True: 
                del memo[noHapus]
                print('Jadwal berhasil dihapus')
                counter -= 1
                break
    elif perintah == 'KELUAR': sys.exit()
    else:
        print('Perintah tidak dikenal')