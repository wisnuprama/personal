"""
Source Code   : lab07a.py
Author        : Wisnu Pramadhitya R
Created       : Oktober 26, 2016
Description   : SimpleMo v0.0.1	   
"""
import sys
def simpan(i, kegiatan, waktu):
    waktu = waktu.replace('.',':')
    return '{}. {} - {}\n'.format(i, kegiatan, waktu)

def lihat(memo):
    return memo[:-1] # return memo dari 0 hingga karakter sebelum karakter akhir

memo = ''
i = 1 # nomor memo

while True:
    perintah = input('Masukan perintah: ').upper()
    if perintah == 'SIMPAN':
        kegiatan = input('Masukan kegiatan: ')
        waktu = input('Masukan waktu: ')
        memo += simpan(i, kegiatan, waktu)
        i += 1
        print('Jadwal berhasil disimpan')
    elif perintah == 'LIHAT': print(lihat(memo))
    elif perintah == 'KELUAR': sys.exit()
    else:
        print('Perintah tidak dikenal')