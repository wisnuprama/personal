enemy_name = input('Masukkan nama musuh: ')
enemy_health = int(input('Masukkan health point musuh: '))

skill_slap = 10
skill_punch = 20
skill_kick = 30

while enemy_health > 0:
    
    inp_skill = str(input('Masukkan skill: '))
    inp_skill = inp_skill.capitalize()

    if inp_skill == 'Slap':
        enemy_health = enemy_health - skill_slap
        out_enemy_health = 'HP {} menjadi {}'.format(enemy_name,enemy_health)
        print(out_enemy_health)
    elif inp_skill == 'Punch':
        enemy_health = enemy_health - skill_punch
        out_enemy_health = 'HP {} menjadi {}'.format(enemy_name,enemy_health)
        print(out_enemy_health)
    elif inp_skill == 'Kick':
        enemy_health = enemy_health - skill_kick
        out_enemy_health = 'HP {} menjadi {}'.format(enemy_name,enemy_health)
        print(out_enemy_health)
    else:
        print('Skill yang dimasukkan tidak terdaftar')
    
print('Kamu menang!')
