num = int(input('Masukkan batas angka untuk berhitung: '))

for i in range(1,num+1):
    if i == num: # it will detect if the i is the last number
                #will print without comma at the end of loop
        if i % 3 == 0 and i % 5 == 0:
            print('Fazz Pezz')
        elif i % 3 == 0:
            print('Fazz')
        elif i % 5 == 0:
            print('Pezz')
        elif i % 5 != 0 and i % 3 != 0:
            print(i)
    elif i % 3 == 0 and i % 5 == 0:
        print('Fazz Pezz', end=', ')
    elif i % 3 == 0:
        print('Fazz', end=', ')
    elif i % 5 == 0:
        print('Pezz', end=', ')
    elif i % 5 != 0 and i % 3 != 0:
        print(i, end=', ')
