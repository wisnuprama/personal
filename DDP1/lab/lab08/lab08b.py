"""
Source Code   : lab08b.py
Author        : Wisnu Pramadhitya R
Created       : October 02, 2016
Description   : Mengurutkan nilai peserta	   
"""

def getKey(self, dict):
     'return max dari dict: class<dict> mempunyai limitasi ketika menggunakan fungsi max() -> max < 100'
     value = list(dict.values())
     key = list(dict.keys())
     return key[value.index(self(value))] # get name key from max value

def score(self, dict):
    tmpNilai = dict.copy()
    lst = []
    counter = 0
    while counter < 3:
        lst.append(getKey(self, tmpNilai))
        tmpNilai.pop(getKey(self, tmpNilai))
        counter += 1
    return lst

def output(dict, lst):
    for name in lst:
        print('{}({})'.format(name, dict[name]))
    
def main():    
    peserta = {}

    totalPeserta = int(input('Jumlah Peserta: '))
    i = 1
    while i <= totalPeserta:
        nilaiPeserta = int(input('Masukan nilai peserta {:2}: '.format(i)))
        peserta['Peserta {}'.format(i)] = nilaiPeserta
        i += 1

    print('3 Tertinggi:')
    output(peserta, score(max, peserta))
    print('3 Terendah: ')
    output(peserta, score(min, peserta))

if __name__ == "__main__":
    main()