def newspace(self, dict):
    self = {}
    while True:
        perintah = input('Masukan perintah: ')
        if 'DECLARE' in perintah.upper():
            perintahTMP = perintah.split()
            try:
                self[perintahTMP[1]] = None
            except IndexError: print('There is no variable declared')
        elif 'ASSIGN' in perintah.upper():
            try:
                perintahTMP = perintah.split()
                if perintahTMP[1] in self:
                    self[perintahTMP[1]] = perintahTMP[2]
                else: print('Variable \'{}\' has not declared'.format(perintahTMP[1]))
            except IndexError: print('No assignment to variable \'{}\''.format(perintahTMP[1]))
        elif 'PRINT' in perintah.upper():
            perintahTMP = perintah.split()
            try:
                if perintahTMP[1] in self:
                    if self[perintahTMP[1]] != None:
                        print('{}'.format(self[perintahTMP[1]]))
                    else: 
                        print('{} is unassigned'.format(perintahTMP[1]))
                elif perintahTMP[1] in dict:
                    if dict[perintahTMP[1]] != None:
                        print('{}'.format(dict[perintahTMP[1]]))
                    else: 
                        print('{} is unassigned'.format(perintahTMP[1]))
                else:
                    print('\'{}\' is doesn\'t exist'.format(perintahTMP[1]))
            except IndexError: print()
        elif 'NEW SCOPE' in perintah.upper():
            temp = self.copy()
            temp = temp.update(dict)
        elif 'EXIT SCOPE' in perintah.upper():
            return False # stop the loop
        else: print('Perintah tidak sesuai'); continue

def main():
     scope = {}
     newspace('scope2', scope)

if __name__ == "__main__":
    main()