"""
Source Code   : lab08a.py
Author        : Wisnu Pramadhitya R
Created       : October 02, 2016
Description   : honey belajar namespace	   
"""

def newspace(self, dict):
    'Create new name scope'
    self = {}
    while True:
        perintah = input('Masukan perintah: ')
        if 'DECLARE' in perintah.upper():
            perintahTMP = perintah.split()
            try:
                if perintahTMP[1] in self: 
                    print('{} is already exist in this scope'.format(perintahTMP[1]))
                else: self[perintahTMP[1]] = None # init value
            except IndexError: print('There is no variable declared')
        elif 'ASSIGN' in perintah.upper():
            try:
                perintahTMP = perintah.split()
                if perintahTMP[1] in self: self[perintahTMP[1]] = perintahTMP[2] # assign to dict(self)
                else: print('Variable {} has not declared'.format(perintahTMP[1])) # no var in dict(self)
            except IndexError: print('No assignment to variable {}'.format(perintahTMP[1])) # no assignment to var
        elif 'PRINT' in perintah.upper():
            perintahTMP = perintah.split()
            try:
                if perintahTMP[1] in self: # var in dict(self)
                    if self[perintahTMP[1]] != None: print('{}'.format(self[perintahTMP[1]]))
                    else: print('{} is unassigned'.format(perintahTMP[1])) # value is None
                elif perintahTMP[1] in dict: # var in dict(dict)
                    if dict[perintahTMP[1]] != None: print('{}'.format(dict[perintahTMP[1]]))
                    else: print('{} is unassigned'.format(perintahTMP[1])) # value is None
                else: print('{} doesn\'t exist'.format(perintahTMP[1])) # no var
            except IndexError: print()
        elif 'EXIT SCOPE' in perintah.upper(): return False # stop the loop
        else: print('Perintah tidak sesuai'); continue

def main():
     scope = {}
     while True:
        perintah = input('Masukan perintah: ')
        if 'DECLARE' in perintah.upper():
            perintahTMP = perintah.split()
            try:
                if perintahTMP[1] in scope: # var does exist
                    print('{} is already exist in this scope'.format(perintahTMP[1]))
                else: scope[perintahTMP[1]] = None # init value
            except IndexError: print('There is no variable declared')
        elif 'ASSIGN' in perintah.upper():
            try:
                perintahTMP = perintah.split()
                if perintahTMP[1] in scope: scope[perintahTMP[1]] = perintahTMP[2] # assign var:value to dict(scope)
                else: print('Variable {} has not declared'.format(perintahTMP[1])) # var doesn't exist
            except IndexError: print('No assignment to variable {}'.format(perintahTMP[1])) # no value
        elif 'PRINT' in perintah.upper():
            try:
                perintahTMP = perintah.split()
                if scope[perintahTMP[1]] != None: print('{}'.format(scope[perintahTMP[1]])) # print
                else: print('{} is unassigned'.format(perintahTMP[1])) # no value
            except IndexError: print()
            except KeyError: print('{} doesn\'t exist'.format(perintahTMP[1])) # no var in dict(scope)
        elif 'NEW SCOPE' in perintah.upper(): newspace('scope2', scope) # call new namespace 
        elif 'EXIT SCOPE' in perintah.upper(): return False # exit
        else: print('Perintah tidak sesuai'); continue

if __name__ == "__main__":
    main()