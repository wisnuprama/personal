"""
Source Code   : lab08c.py
Author        : Wisnu Pramadhitya R
Created       : October 02, 2016
Description   : debugging	   
"""

b, c, d =  2, 3, 4 # global variable

def mystery3(a):
    print('Nilainya adalah', a)

def mystery2(a):
    mystery3(a)
    return a

def mystery1(a, b, c, d):
    temp = mystery2(a) + mystery2(b) + mystery2(c) + mystery2(d)
    print('Program selesai')
    return temp

def mystery(a):
    print('Program dimulai')
    b = a + 1
    c = b + 1
    d = c + 1
    e = mystery1(a, b, c, d)
    print('Hasilnya adalah', e)

def main():
    a = 1
    mystery(a)
    mystery3(a)
    mystery3(b)
    mystery3(c)
    mystery3(d)

if __name__ == "__main__": 
    main()