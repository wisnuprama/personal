def counting(num=int(), target=int(), digit=int()):
    if num==0:
        return 0

    times = 1
    for i in range(digit-1):
        times = (times * 10) + 1

    if num%(10**digit) == times * target:
        return 1+counting(num//10,target,digit)
    else:
        return counting(num//10,target,digit)

if __name__ == "__main__":
    while True:
        num = int(input('Masukan angka: '))
        target = int(input('Masukan target: '))
        print(counting(num, target, 1) + counting(num,target,3))