row = column = 5
lstP0 = list()
lstP1 = list()
menang = 3

for i in range(row):
    lst2 = list()
    for j in range(column):
        lst2.append(False)
    lstP0.append(lst2); lstP1.append(lst2)

def printlst(lst):
    for i in lst:
        print(i, end='\n')

def check(lst, x, y):
    count = 0 
    while x >= 0:
        x_ = x - 1
        if lst[x_][y] == True:
            count += 1
        else:
            break

    while x < row:
        x_ = x + 1
        if lst[x_][y] == True:
            count += 1
        else:
            break


player = 0
while True:
    usr_input = eval(input('player {} (x,y): '.format(player)))
    x, y = usr_input[0], usr_input[1]
    try:
        if player == 0:
            lstP0[x][y] = True
            printlst(lstP0)
            player = 1
            if check(lstP0, x, y) == menang:
                break
        else:
            lstP1[x][y] = True
            printlst(lstP1)
            player = 0
    except IndexError:
        print('Error: no coords')
        continue