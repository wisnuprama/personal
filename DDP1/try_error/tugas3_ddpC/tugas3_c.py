'''
    Docs
'''

def getKeyKelas(dictionary, find):
    'return max dari dict() mempunyai limitasi ketika menggunakan fungsi max() -> max < 100'
    value = list(dictionary.values())
    key = list(dictionary.keys())
    return key[value.index(find)] # get key name from value

def checkAllClass(mahasiswa=None):
    'Get Class that contains the target name'
    # need to change how it works, return True or return the Target?
    # what is the best?
    key = list(mapKelas.keys())
    for target in key:
        if mapKelas[target].checkMahasiswa(mahasiswa):
            return target # return class Kelas target
    return mahasiswa

def openClass(namaKelas=None):
    'Initiate new Class and mapping it with dict(mapKelas)'
    opnKelas = {namaKelas:Kelas()} # mapping Class with dict()
    mapKelas.update(opnKelas)

def sort(arr=list()):
    'Sorting using insertion algorithm'
    for i in range(1, len(arr)):
        key = arr[i]
        j = i - 1
        while j >= 0 and arr[j] > key:
            arr[j + 1] = arr[j]
            j -= 1
        arr[j + 1] = key
    return arr

class Kelas(dict):
    '...'
    def checkMahasiswa(self, nama=None):
        'return True if the name is in dict(self.data), False otherwise;\n'
        for nm in self.keys():
            if nm.lower() == nama.lower(): return True
        return False

    def getName(self, nama=None):
        'return the real name from dict to fix the differentiation from user input'
        if self.checkMahasiswa(nama):
            for nm in self.keys():
                if nm.lower() == nama.lower():
                    return nm
        else: return nama

    def addMahasiswa(self, mahasiswa=None):
        'Add name to the dict(self.nama) if the name isn\'t in there'
        if self.checkMahasiswa(mahasiswa) == False:
            formatNilai = {'Quiz':[0, 0], 'Tugas':[0, 0, 0], 'Ujian':[0, 0]}
            self[mahasiswa] = formatNilai
            print('{} Berhasil dimasukkan ke dalam buku.'.format(mahasiswa))
        else:
            print('Siswa sudah terdaftar')

    def updateNilai(self, mahasiswa, berkas, nilai):
        'updating the score of a mahasiswa'
        try:
            mahasiswa = self.getName(mahasiswa)
            for i in range(len(nilai)):
                if nilai[i] != '':
                    self[mahasiswa][berkas.capitalize()][i] = eval(nilai[i])
            print('Menambah nilai {} pada mahasiswa {}'.format(berkas, mahasiswa))

        except KeyError: print('Nama {} tidak ditemukan di kelas {}'.format(mahasiswa, getKeyKelas(mapKelas, self)))
        except (IndexError, TypeError): print('Format masukan nilai tidak sesuai')

    def getBerkas(self, namaBerkas):
        '''sorting the master bundle dict(self.data[name]) depending on parameter -> namaBerkas
        into format dict(name:list(score))'''
        gather = {} # dict{name:berkas} # berkas = [nilai]
        for nm in self.keys():
            tmp = self[nm]
            for berkas in tmp.keys():
                if berkas == namaBerkas:
                    gather[nm] = tmp[berkas]
        return gather

    def getAvgScore(self, berkas=None, index=0):
        berkas = berkas.capitalize()
        dScore, gather = self.getBerkas(berkas), 0
        for nm in dScore:
            tempLst = dScore[nm]
            gather += tempLst[index-1]

        outFormat = 'Rata-rata nilai {} {} pada kelas {} adalah {}'.format(berkas, index, \
                    getKeyKelas(mapKelas, self), gather/len(dScore))
        return outFormat

    def getSummary(self, kelas=None, nama=None):
        'show summary of a mahasiswa(nama)'
        if self.checkMahasiswa(nama):
            nama = self.getName(nama)

            # mapping the score in dict() -> nama:score depending on bundle
            listQuiz, listTugas, listUjian = self.getBerkas('Quiz'), self.getBerkas('Tugas'), self.getBerkas('Ujian')

            # calculating the score
            nilaiQuiz, nilaiTugas, nilaiUjian = sum(listQuiz[nama]) / 2, sum(listTugas[nama]) / 3, sum(listUjian[nama]) / 2
            nilaiAkhir = 0.2 * nilaiTugas + 0.3 * nilaiQuiz + 0.5 * nilaiUjian

            outformat = 'Nama\t\t: {}\nKelas\t\t: {}\nQuiz\t\t: {:.2f}\n\
Tugas\t\t: {:.2f}\nUjian\t\t: {:.2f}\nNilai Akhir\t: {:.2f}'.format(nama, kelas, \
            nilaiQuiz, nilaiTugas, nilaiUjian, nilaiAkhir)
            return outformat

    def getSearch(self, berkas=None, index=0, min=0, max=100):
        'Find all the names within the interval -> min, max'
        berkas = berkas.capitalize()
        dScore, gather = self.getBerkas(berkas), list()
        for nm in dScore:
            tempLst = dScore[nm]
            if float(min) <= tempLst[index-1] <= float(max):
                gather.append(nm)
        
        return gather

# ---------------------

mapKelas = {}

def main():
    'Main program'
    while True:
        cmd = str(input('Masukan perintah: '))
        tempCmd = cmd.split() # mapping command
        try:
            if 'ADD' in cmd.upper():
                tempData = tempCmd[1].split(';')
                namaMhs, namaKls = tempData[0], tempData[1].upper()
                
                terdaftar = False
                for kls in mapKelas:
                    if mapKelas[kls].checkMahasiswa(namaMhs):
                        print('Mahasiswa {} sudah terdaftar'.format(namaMhs))
                        terdaftar = True # benar bahwa telah terdaftar

                # membuat Kelas jika kelas belum terdaftar
                if terdaftar == False:
                    if namaKls not in mapKelas:
                        openClass(namaKls)

                    mapKelas[namaKls].addMahasiswa(namaMhs)

            elif 'UPDATE' in cmd.upper():
                tempData = tempCmd[1].split(';')
                namaMhs, berkas, nilai = tempData[0], tempData[1], tempData[2].split(',')

                namaKls = mapKelas[checkAllClass(namaMhs)] # here is the bug
                namaKls.updateNilai(namaMhs, berkas, nilai)

            elif 'AVERAGE' in cmd.upper():
                tempData = tempCmd[1].split(';')
                namaKls, namaBerkas, index = mapKelas[tempData[0].upper()], tempData[1], int(tempData[2])
                print(namaKls.getAvgScore(namaBerkas, index))

            elif 'SUMMARY' in cmd.upper():
                namaMhs = tempCmd[1]
                namaKls = checkAllClass(namaMhs) # get key
                print(mapKelas[namaKls].getSummary(namaKls, namaMhs)) # avoiding from double execution

            elif 'SEARCH' in cmd.upper():
                lstNama = []
                tempData = tempCmd[1].split(';')
                namaBerkas, index, interval = tempData[0], int(tempData[1]), tempData[2].split('-')
                
                min, max = interval[0], interval[1]
                
                for nmKelas in mapKelas.keys(): # gather the name in all class
                    lstNama += mapKelas[nmKelas].getSearch(namaBerkas, index, min, max)
                lstNama = sort(lstNama)
                count = 1
                for nm in lstNama:
                    print('{:3}. {}'.format(count, nm)) # show name vertically by number
                    count += 1

        except KeyError: print('Error key')
        except IndexError: print('Error index')

if __name__ == "__main__":
    main()