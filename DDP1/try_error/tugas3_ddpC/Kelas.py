'''
    Docs
'''

class Kelas(dict):
    '...'
    def checkMahasiswa(self, nama=None):
        'return True if the name is in dict(self.data), False otherwise;\n'
        for nm in self.keys():
            if nm.lower() == nama.lower(): return True
        return False

    def getName(self, nama=None):
        'return the real name from dict to fix the differentiation from user input'
        if self.checkMahasiswa(nama):
            for nm in self.keys():
                if nm.lower() == nama.lower():
                    return nm
        else: return nama

    def addMahasiswa(self, mahasiswa=None):
        'Add name to the dict(self.nama) if the name isn\'t in there'
        if self.checkMahasiswa(mahasiswa) == False:
            formatNilai = {'Quiz':[0, 0], 'Tugas':[0, 0, 0], 'Ujian':[0, 0]}
            self[mahasiswa] = formatNilai
            print('{} Berhasil dimasukkan ke dalam buku.'.format(mahasiswa))
        else:
            print('Siswa sudah terdaftar')

    def updateNilai(self, mahasiswa, berkas, nilai):
        'updating the score of a mahasiswa'
        try:
            mahasiswa = self.getName(mahasiswa)
            for i in range(len(nilai)):
                if nilai[i] != '':
                    self[mahasiswa][berkas.capitalize()][i] = eval(nilai[i])
            print('Menambah nilai {} pada mahasiswa {}'.format(berkas, mahasiswa))

        except KeyError: print('Nama {} tidak ditemukan di kelas {}'.format(mahasiswa, self))
        except (IndexError, TypeError): print('Format masukan nilai tidak sesuai')

    def getBerkas(self, namaBerkas):
        '''sorting the master bundle dict(self.data[name]) depending on parameter -> namaBerkas
        into format dict(name:list(score))'''
        gather = {} # dict{name:berkas} # berkas = [nilai]
        for nm in self.keys():
            tmp = self[nm]
            for berkas in tmp.keys():
                if berkas == namaBerkas:
                    gather[nm] = tmp[berkas]
        return gather

    def getAvgScore(self, berkas=None, index=0):
        berkas = berkas.capitalize()
        dScore, gather = self.getBerkas(berkas), 0
        for nm in dScore:
            tempLst = dScore[nm]
            gather += tempLst[index-1]

        outFormat = 'Rata-rata nilai {} {} pada kelas {} adalah {}'.format(berkas, index, \
                    self, gather/len(dScore))
        return outFormat

    def getSummary(self, kelas=None, nama=None):
        'show summary of a mahasiswa(nama)'
        if self.checkMahasiswa(nama):
            nama = self.getName(nama)

            # mapping the score in dict() -> nama:score depending on bundle
            listQuiz, listTugas, listUjian = self.getBerkas('Quiz'), self.getBerkas('Tugas'), self.getBerkas('Ujian')

            # calculating the score
            nilaiQuiz, nilaiTugas, nilaiUjian = sum(listQuiz[nama]) / 2, sum(listTugas[nama]) / 3, sum(listUjian[nama]) / 2
            nilaiAkhir = 0.2 * nilaiTugas + 0.3 * nilaiQuiz + 0.5 * nilaiUjian

            outformat = 'Nama\t\t: {}\nKelas\t\t: {}\nQuiz\t\t: {:.2f}\n\
Tugas\t\t: {:.2f}\nUjian\t\t: {:.2f}\nNilai Akhir\t: {:.2f}'.format(nama, kelas, \
            nilaiQuiz, nilaiTugas, nilaiUjian, nilaiAkhir)
            return outformat

    def getSearch(self, berkas=None, index=0, min=0, max=100):
        'Find all the names within the interval -> min, max'
        berkas = berkas.capitalize()
        dScore, gather = self.getBerkas(berkas), list()
        for nm in dScore:
            tempLst = dScore[nm]
            if float(min) <= tempLst[index-1] <= float(max):
                gather.append(nm)
        return gather
        