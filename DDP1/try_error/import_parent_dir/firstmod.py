import subdir.submod as mod # import from sub directory
print(mod._wow_(3))

import os
import sys
sys.path.insert(0, os.getcwd()) # automatically import in parent directory
print(sys.path)

# if we want to import all module from subdir, append __all__ = ['module's name in subdir', ..., ..] 
# in __init__.py of subdir folder 
# update when there is new module
# see python documentation in chapter 6. Modules
