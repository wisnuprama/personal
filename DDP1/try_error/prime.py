def noPrime(n, m):
    lst = []
    basic = (2, 3, 5, 7, 11)
    for num in range(n, m):
        if num in basic: continue
        elif (num % 2 == 0 or num % 3 == 0 or num % 5 == 0 or num % 7 == 0):
            lst.append(num)
    print(lst)

noPrime(1,50)