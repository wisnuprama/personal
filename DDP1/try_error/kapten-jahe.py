import turtle

turtle.speed(0)
turtle.down()
for i in range(2):
    turtle.setheading(270)
    for i in range(180):
        turtle.forward(1)
        turtle.left(1)

turtle.forward(330/3.14)
for i in range(90):
    turtle.forward(1)
    turtle.left(1)
turtle.forward(258)
for i in range(90):
    turtle.left(1)
    turtle.forward(1)
turtle.left(90)
turtle.forward(50)
turtle.left(180)
turtle.forward(50)
turtle.left(90)
for i in range(90):
    turtle.left(1)
    turtle.forward(1)
turtle.forward(48)
turtle.left(90)
turtle.forward(360/3.14)
turtle.backward(360/3.14)
turtle.right(90)
turtle.forward(37)
turtle.right(90)
turtle.forward(50)
turtle.hideturtle()
turtle.up()
turtle.forward(150)
turtle.write("-Kapten Jahe", align='center', font=('Arial',20,'normal'))
