"""
Given a non-empty string and an int n, return a new string where the char at index n has been removed. 
The value of n will be a valid index of a char in the original string (i.e. n will be in the range 0..len(str)-1 inclusive).
"""

def missing_char(str, n):
  x = str[0:n]+str[n+1:len(str)] 
  # str pertama 0 hingga n: membuat variable baru berisi string tanpa char ke n di string tersebut
  # str kedua n+1: menambahkan char-char dari indeks ke n+1 ke akhir ke dalam x 
  return x

def main():
    y = str(input("Input string \t: "))
    n = int(input("Input integer\t: "))
    
    missing_char(y, n)

main()