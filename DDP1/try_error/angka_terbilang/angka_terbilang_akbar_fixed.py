def program(bil):
    
    #Variabel
    sat = ['','satu','dua','tiga','empat','lima','enam','tujuh','delapan','sembilan']
    bel = ['','sebelas','dua belas','tiga belas','empat belas','lima belas','enam belas','tujuh belas','delapan belas','sembilan belas']
    pul = ['', 'sepuluh','dua puluh','tiga puluh','empat puluh','lima puluh','enam puluh', 'tujuh puluh', 'delapan puluh','sembilan puluh']
    rat = ['','seratus','dua ratus','tiga ratus','empat ratus','lima ratus','enam ratus','tujuh ratus','delapan ratus','sembilan ratus']
    rib = ['','seribu','dua ribu','tiga ribu','empat ribu','lima ribu','enam ribu','tujuh ribu','delapan ribu','sembilan ribu']

    if 0 < bil < 10 :
        print ( sat [ bil % 10 ] )
    elif 10 < bil < 20 :
        print ( bel [ bil % 10 ] )
    elif 10 <= bil < 100 :
        print ( ( pul [ bil // 10 ] ) + ' ' + ( sat [ bil % 10 ] ) )
    elif 100 <= bil < 1000 and 10 < bil % 100 < 20 :
        print (( rat [ bil // 100 ] ) + ' ' + (bel [ bil % 10 ] ) )
    elif 100 <= bil < 1000:
        print ( ( rat [ bil // 100 ] ) + ' ' + ( pul [ ( bil % 100 ) // 10 ] ) + ' ' + ( sat [ bil % 10 ] ) )
    elif 1000 <= bil < 10000 and 10 < bil % 100 < 20 :
        print (( rib [ bil // 1000 ] ) + ' ' + ( rat [ ( bil % 1000 ) // 100 ] ) + ' ' + (bel [ bil % 10 ] ))
    elif 1000 <= bil < 10000 :
        print ( ( rib [ bil // 1000 ] ) + ( rat [ ( bil % 1000 ) // 100 ] ) + ' ' + ( pul [ ( bil % 100 ) // 10 ] ) + ( sat [ bil % 10 ] ) )
    elif 10000 < bil < 20000 and 10 < bil % 100 < 20 :
        print ( ( bel [ (bil // 1000 ) - 10 ] ) + ' ribu ' + ( rat [ ( bil % 1000 ) // 100 ] ) + ' ' + ( bel [ bil % 10 ] ) )
    elif 10000 < bil < 20000 :
        print ( ( bel [ (bil // 1000 ) % 10 ] ) + ' ribu ' + ( rat [ ( bil % 1000 ) // 100 ] ) + ' ' + ( pul [ ( bil % 100 ) // 10 ] ) + ' ' + ( sat [ bil % 10 ] ) )
    elif 10000 <= bil < 100000 and 10 < bil % 100 < 20 :
        print ( ( pul [ bil // 10000 ] ) + ' ' + ( sat [ ( bil % 10000 ) // 1000 ] ) + ' ribu ' + ( rat [ ( bil % 1000 ) // 100 ] ) + ' ' + ( ( bel [ bil % 10] ) ) )
    elif 10000 <= bil < 100000 :
        print ( ( pul [ bil // 10000 ] ) + ' ' + ( sat [ ( bil % 10000 ) // 1000 ] ) + ' ribu ' + ( rat [ ( bil % 1000 ) // 100 ] ) + ' ' + ( pul [ ( bil % 100 ) // 10 ] ) + ' ' + ( sat [ bil % 10 ] ) )
    elif bil == 100000:
        print ('seratus ribu')
    
    main()

def main(): # tambahan oleh Wisnu
    bil = input('Masukkan angka: ')

    checking = True
    for i in bil:
        if (i != '0' and i != '1' and i != '2' and i != '3' and i != '4' and i != '5'
            and i != '6' and i != '7' and i != '8' and i != '9'):
            checking = False
            print('Bukan integer')
            break
    if checking == True:
        x =  int(bil)
        program(x)

main()