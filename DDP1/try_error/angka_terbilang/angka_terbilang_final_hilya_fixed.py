satuan = ['nol','satu','dua','tiga','empat','lima','enam','tujuh','delapan','sembilan','sepuluh','sebelas','dua belas','tiga belas','empat belas','lima belas','enam belas','tujuh belas','delapan belas','sembilan belas']
puluhan = ['','','dua puluh','tiga puluh','empat puluh','lima puluh','enam puluh','tujuh puluh','delapan puluh','sembilan puluh']
ratusan = ['','seratus','dua ratus','tiga ratus','empat ratus','lima ratus','enam ratus','tujuh ratus','delapan ratus','sembilan ratus']
ribuan = ['','seribu','dua ribu','tiga ribu','empat ribu','lima ribu','enam ribu','tujuh ribu','delapan ribu','sembilan ribu']

angka = input('Masukkan angka : ') #string

mengecek = True #variable pemeriksa #tambahan Wisnu
for i in angka:
    if (i != '0' and i != '1' and i != '2' and i != '3' and i != '4' and 
        i != '5' and i != '6' and i != '7' and i != '8' and i != '9'):
        mengecek = False
        print('Input yang dimasukan bukan bilangan bulat')
        break
if mengecek == True:
    angka = eval(angka)

    rts = angka//100; rb = angka//1000; plhrb = angka//10000; rms = (angka%10000)%1000; mdl = angka%1000; 

    if angka>=0 and angka<20:
        print ('Angka Terbilang : ',satuan[angka].capitalize())
    elif angka>=20 and angka<100:
        print ('Angka Terbilang : ',puluhan[angka//10].capitalize(), satuan[angka%10])
    elif angka>=100 and angka<1000:
        if (angka%100)==1:
            print ('Angka Terbilang : ',ratusan[rts].capitalize(), satuan[angka%100])
        else:
            print ('Angka Terbilang : ',ratusan[rts].capitalize(), puluhan[(angka%100)//10], satuan[(angka%100)%10])
    elif angka>=1000 and angka<10000:
        if (mdl%100)//10==1:
            print ('Angka Terbilang : ',ribuan[rb].capitalize(), ratusan[mdl//100], satuan[(mdl%100)])
        else:    
            print ('Angka Terbilang : ',ribuan[rb].capitalize(), ratusan[mdl//100], puluhan[(mdl%100)//10], satuan[(mdl%100)%10])   
    elif angka>=10000 and angka<20000:
        if ((angka%10000)%100)//10==1:
            print ('Angka Terbilang : ',satuan[rb].capitalize(),'ribu', ratusan[rms//100], satuan[rms%100])
        else:    
            print ('Angka Terbilang : ',satuan[rb].capitalize(),'ribu', ratusan[rms//100], puluhan[(rms%100)//10], satuan[(rms%100)%10])
    elif angka>=20000 and angka<100000:
        if ((angka%10000)%100)//10==1:
            print ('Angka Terbilang : ',puluhan[plhrb].capitalize(), ribuan[(angka%10000)//1000], ratusan[rms//100], satuan[rms%100])
        else:
            print ('Angka Terbilang : ',puluhan[plhrb].capitalize(), ribuan[(angka%10000)//1000], ratusan[rms//100], puluhan[(rms%100)//10], satuan[(rms%100)%10])
    elif angka==100000:
        print ('Angka Terbilang : Seratus ribu')
    elif angka>=100000:
        print ('Angka yang Anda masukkan melibihi 100000, tidak valid!')