import java.io.*;
import java.util.*;

/**
 * TP3 SDA Tree
 * Name : Wisnu Pramadhitya Ramadhan
 * NPM  : 1606918055
 */
public class SDA1606918055T3 {

    static final BufferedReader IN = new BufferedReader(new InputStreamReader(System.in));
    static final PrintWriter OUT = new PrintWriter(new OutputStreamWriter(System.out));

    public static void main(String[] args) throws Exception {

        String buffer;
        String result;
        final FileSystem fileSystem = new FileSystem();

        while ((buffer = IN.readLine()) != null && buffer.length() > 0) {

            String[] input = buffer.split(" ");

            switch (input[0]) {

                case "add":
                    Folder newFolder = new Folder(null, input[1]);
                    fileSystem.add(newFolder, input[2]);
                    break;

                case "insert":
                    File newFile = new File(null, input[1],
                            Integer.parseInt(input[2]));
                    result = fileSystem.insert(newFile, input[3]);
                    if (result != null)
                        OUT.println(result);
                    break;

                case "remove":
                    OUT.println(fileSystem.remove(input[1]));
                    break;

                case "search":
                    OUT.print(fileSystem.search(input[1]));
                    break;

                case "print":
                    OUT.print(fileSystem.print(input[1]));
                    break;

            }
        }

        OUT.flush();
    }
}

/**
 * Class FileSystem
 * Kelas operasional yang mengatur file dan folder
 */
class FileSystem {

    Folder root;
    HashMap<String, Folder> indexer;

    public FileSystem() {
        root = new Folder(null, "root");
        indexer = new HashMap<>();
        indexer.put("root", root);
    }

    /**
     * Membuat spacing untuk penampilan sesuai level/depth folder/file
     *
     * @param sb    StringBuilder
     * @param level level/depth folder/file
     */
    private static void spacing(StringBuilder sb, int level) {
        for (int i = 0; i < level; i++)
            sb.append("  ");
    }

    /**
     * Mencari folder yang dapat diisi oleh file. Keadaannya mengecek apakah folder empty
     * atau jika folder contain files dan jika files type nya sama atau jika isinya folder maka dia ambil semua child
     * dan cari semua folder child tersebut yang available, jika tidak ada return null.
     *
     * @param current Folder target
     * @param type    type files
     * @return Folder yang dapat diisi atau null.
     */
    private static Folder searchAvailableFolder(Folder current, String type) {
        if (current.isEmpty()) return current;
        else if (current.isContainingFile()) {
            File firstFile = (File) current.getFirst();
            if (firstFile.type.equals(type))
                return current;

        } else {
            Collection<Node> child = current.getChildren();
            for (Node c : child) {
                Folder tmp = searchAvailableFolder((Folder) c, type);
                if (tmp != null)
                    return tmp;
            }
        }

        return null;
    }

    /**
     * Menambahkan sebuah folder baru ke folder tujuan.
     * Jika folder berisi file, maka file dipindahkan ke folder baru dan folder baru
     * dimasukan ke folder tujuan.
     *
     * @param newFolder  folder baru yang di input
     * @param parentName nama folder parent
     */
    public void add(Folder newFolder, String parentName) {
        Folder parent = this.indexer.get(parentName);

        if (parent.isContainingFile()) {
            // case dimana folder berisi file
            // transfer file
            newFolder.contents = parent.contents;
            for (Node node : parent.getChildren())
                node.setParent(newFolder);
            newFolder.setSize(parent.getSize());
            // set new to parent
            parent.contents = new TreeMap<>();
        }

        parent.put(newFolder);
        indexer.put(newFolder.getName(), newFolder);
    }

    /**
     * Menambahkan file baru ke folder tujuan.
     *
     * @param newFile file baru
     * @param parent  folder tujuan
     * @return String berhasil, null jika file tidak dapat dimasukkan.
     */
    public String insert(File newFile, String parent) {
        Folder parentFolder = indexer.get(parent);
        // mencari folder yang dapat diisi file
        Folder target = searchAvailableFolder(parentFolder, newFile.type);

        if (target != null) {
            // ditemukan
            target.put(newFile);
            return newFile.getName() + "." + newFile.type + " added to " + target.getName();
        }

        // case harus naik ke level atasnya karena tidak ada lagi di level tersebut dan level dibawahnya.
        while (parentFolder.getParent() != null) {
            TreeMap<String, Node> currentFolder = parentFolder.getParent().contents;
            // Check folder dari folder parentFolder ke ke kanan
            Collection<Node> contents = currentFolder.tailMap(parentFolder.getName()).values();
            target = insertHelper(newFile, contents);
            if (target != null)
                return newFile.getName() + "." + newFile.type + " added to " + target.getName();

            // Check folder dari head ke folder parentFolder
            contents = currentFolder.headMap(parentFolder.getName()).values();
            target = insertHelper(newFile, contents);
            if (target != null)
                return newFile.getName() + "." + newFile.type + " added to " + target.getName();

            // ke level atasnya lagi hingga null => parent root
            parentFolder = parentFolder.getParent();
        }

        return null;
    }

    /**
     * Menambahkan newFile ke kemungkinan folder yang ada
     * @param newFile file yang akan diinput
     * @param content folder yang adalah children dari parent folder
     * @return
     */
    private Folder insertHelper(File newFile, Collection<Node> content) {
        for (Node folder : content) {

            Folder target = searchAvailableFolder((Folder) folder, newFile.type);
            if (target != null) {
                target.put(newFile);
                return target;
            }
        }

        return null;
    }

    /**
     * Remove Node dari FileSystem berdasarkan nama
     * @param name Nama Node
     * @return String berhasil di removed
     */
    public String remove(String name) {

        // it's folder
        if (indexer.containsKey(name)) {
            Folder target = indexer.get(name);
            // mengambil semua folder yang menjadi child dari child child yang ada
            List<Node> removed = target.getAllChildren();

            for (Node folder : removed) {
                Folder tmp = (Folder) folder;
                indexer.remove(tmp.getName());
            }
            target.delete();
            return "Folder " + name + " removed";

        } else {
            // files
            int counter = 0;
            final Stack<Node> DFS = new Stack<>();
            DFS.push(root);

            // traversing semua folder dari root menggunakan DFS (stack)
            while (!DFS.isEmpty()) {
                Folder current = (Folder) DFS.pop();
                if (current.isEmpty()) continue;
                if (current.isContainingFolder()) {
                    // jika contain folder, maka tambahkan folder ke dfs
                    for (Node folder : current.getChildren()) {
                        DFS.push(folder);
                    }
                }

                // jika folder berisi file maka delete files
                if (current.isContainingFile()) {
                    for (Node node : current.getChildren()) {
                        File file = (File) node;
                        if (name.equals(file.getName())) {
                            file.delete();
                            counter++;
                        }
                    }
                }
            }

            return counter + " File " + name + " removed";
        }
    }

    /**
     * Search folder/file berdasarkan nama
     * @param name nama folder/file
     * @return String tree
     */
    public String search(String name) {
        StringBuilder sb = new StringBuilder();
        if (indexer.containsKey(name)) {
            // folder
            Folder target = indexer.get(name);
            final Stack<Folder> PATH = new Stack<>();

            // menggunakan stack untuk membuat jalur ke target
            while (target != null) {
                PATH.push(target);
                target = target.getParent();
            }

            // print folder
            int counter = 0;
            while (!PATH.isEmpty()) {
                Folder current = PATH.pop();
                spacing(sb, counter);
                sb.append("> " + current.getName() + "\n");
                counter++;
            }

        } else {
            // files
            final Stack<Folder> DFS = new Stack<>();
            final TreeSet<Folder> PRINTED = new TreeSet<>();
            DFS.push(root);

            while (!DFS.isEmpty()) {
                Folder current = (Folder) DFS.pop();

                if (current.isEmpty()) continue; // jika kosong lanjut
                if (current.isContainingFolder()) {
                    // kalo isi nya folder di reverse kunjungannya
                    final Stack<Folder> REVERSE = new Stack<>();
                    for (Node folder : current.getChildren())
                        REVERSE.push((Folder) folder);

                    while (!REVERSE.isEmpty())
                        DFS.push(REVERSE.pop());
                }

                if (current.isContainingFile()) {

                    for (Node node : current.getChildren()) {
                        File file = (File) node;
                        if (name.equals(file.getName())) {
                            // ada file yang sama
                            Folder dest = current;
                            final Stack<Node> PATH = new Stack<>();
                            PATH.push(file);
                            // create path ke file
                            while (dest != null) {
                                PATH.push(dest);
                                dest = dest.getParent();
                            }

                            // PRINT
                            int counter = 0;
                            while (!PATH.isEmpty()) {
                                Node now = PATH.pop();
                                if (now.isFolder()) {
                                    Folder folder = (Folder) now;
                                    if (!PRINTED.contains(folder)) {
                                        spacing(sb, counter);
                                        sb.append("> " + folder.getName() + "\n");
                                    }
                                    PRINTED.add(folder);

                                } else {

                                    File target = (File) now;
                                    spacing(sb, counter);
                                    sb.append("> " + target.getName() + "." + target.type + "\n");
                                }

                                counter++;

                            }
                        }
                    }
                }
            }
        }

        return sb.toString();
    }

    /**
     * Print dari berdasarkan nama folder
     * @param folder folder tujuan
     * @return String tree
     */
    public String print(String folder) {
        StringBuilder sb = new StringBuilder();
        Folder current = indexer.get(folder);
        printHelper(current, 0, sb);
        return sb.toString();
    }

    /**
     * Fungsi helper untuk print
     * @param current Folder current
     * @param level level/depth
     * @param sb StringBuilder
     */
    public void printHelper(Folder current, int level, StringBuilder sb) {
        spacing(sb, level);
        sb.append("> " + current.getName() + " " + current.getSize() + "\n");
        if (current.isEmpty())
            return;

        Collection<Node> children = current.getChildren();
        // print dibagi berdasarkan isi folder/file
        if (current.isContainingFolder()) {
            for (Node node : children) {
                printHelper((Folder) node, level + 1, sb);
            }

        } else {
            for (Node node : children) {
                File nowFile = (File) node;
                spacing(sb, level + 1);
                sb.append("> " + nowFile.getName() + "." + nowFile.type + " " + nowFile.getSize() + "\n");
            }

        }

        return;
    }

}


/**
 * Class Node yang akan di extends ke Folder dan File
 */
class Node implements Comparable<Node> {
    private Folder mParent;
    private String mName;
    private int mSize;

    public Node(Folder mParent, String mName, int mSize) {
        this.mParent = mParent;
        this.mName = mName;
        this.mSize = mSize;
    }

    /**
     * Check apakah instance ini adalah instance dari class Folder
     * @return
     */
    public boolean isFolder() {
        return this instanceof Folder;
    }

    @Override
    public int compareTo(Node o) {
        return mName.compareTo(o.mName);
    }

    /**
     * Increment size dari node
     * @param size
     */
    public void incrementSize(int size) {
        this.mSize += size;
    }

    /**
     * Get parent node
     * @return
     */
    public Folder getParent() {
        return mParent;
    }

    /**
     * set parent node
     * @param parent
     */
    public void setParent(Folder parent) {
        mParent = parent;
    }

    public String getName() {
        return mName;
    }

    public int getSize() {
        return mSize;
    }

    public void setSize(int size) {
        this.mSize = size;
    }

    /**
     * Delete instance node ini dari parent
     */
    public void delete() {
        mParent.remove(mName);
        mParent = null;
    }
}


/**
 * Class Folder
 */
class Folder extends Node {

    TreeMap<String, Node> contents;
    static final int DEFAULT_SIZE = 1;

    public Folder(Folder mParent, String mName) {
        super(mParent, mName, DEFAULT_SIZE);
        contents = new TreeMap<>();
    }

    /**
     * Check apakah folder empty
     * @return
     */
    public boolean isEmpty() {
        return contents.isEmpty();
    }


    /**
     * Mengambil semua Folder anak dari anak dan anak-anaknya (ke level bawahnya)
     * File tidak termasuk
     * @return
     */
    public List<Node> getAllChildren() {
        List<Node> children = new ArrayList<>(this.contents.size());
        final Stack<Node> DFS = new Stack<>();
        DFS.push(this);
        while (!DFS.isEmpty()) {

            Folder current = (Folder) DFS.pop();
            if (current.isContainingFile())
                continue;

            children.add(current);
            for (Node folder : current.getChildren()) {
                DFS.push(folder);
            }
        }

        return children;
    }

    /**
     * Mengambil semua anak dari level ini.
     * @return
     */
    public Collection<Node> getChildren() {
        return contents.values();
    }

    /**
     * Update size folder
     * @param size
     */
    private void updateSize(int size) {
        Folder current = this;
        while (current != null) {
            current.incrementSize(size);
            current = current.getParent();
        }
    }


    public Node get(String name) {
        return contents.get(name);
    }

    /**
     * Put Node ke folder ini, sekaligus set parent dari node yang dimasukkan dan update size sekalian
     * @param node
     */
    public void put(Node node) {
        this.contents.put(node.getName(), node);
        node.setParent(this);
        int tmpSize = node.getSize();
        if (node.isFolder())
            tmpSize = DEFAULT_SIZE;
        this.updateSize(tmpSize);
    }

    /**
     * get first node dari folder ini
     * @return Node pertama
     */
    public Node getFirst() {
        if (contents.isEmpty())
            return null;
        String firstKey = contents.firstKey();
        return contents.get(firstKey);
    }

    /**
     * check apakah folder ini contain file
     * @return True jika iya mengandung file di level ini.
     */
    public boolean isContainingFile() {
        return this.getFirst() instanceof File;
    }

    /**
     * check apakah folder di level ini mengandung folder.
     * @return True jika iya mengandung folder di level ini.
     */
    public boolean isContainingFolder() {
        return this.getFirst() instanceof Folder;
    }

    /**
     * remove node berdasarkan nama dan update size folder ini
     * @param name
     */
    public void remove(String name) {
        Node n = contents.remove(name);
        if (n != null) this.updateSize(-n.getSize());
    }
}


/**
 * Class File
 */
class File extends Node {

    String type;

    public File(Folder mParent, String name, int mSize) {
        super(mParent, name.split("\\.")[0], mSize);
        type = name.split("\\.")[1];
    }
}
