//import java.io.OutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.PrintWriter;
//import java.io.BufferedReader;
//import java.io.InputStreamReader;
//import java.util.StringTokenizer;
//import java.util.List;
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.TreeMap;
//import java.util.TreeSet;
//import java.util.Stack;
//
//public class SDA1606862753T3 {
//    public static void main(String[] args) {
//        InputStream inputStream = System.in;
//        OutputStream outputStream = System.out;
//        InputReader in = new InputReader(inputStream);
//        PrintWriter out = new PrintWriter(outputStream);
//        Solution solution = new Solution(in, out);
//        solution.run();
//        out.close();
//    }
//
//    static class InputReader {
//        public BufferedReader reader;
//        public StringTokenizer tokenizer;
//
//        public InputReader(InputStream stream) {
//            reader = new BufferedReader(new InputStreamReader(stream), 32768);
//            tokenizer = null;
//        }
//
//        public String nextLine() {
//            try {
//                return reader.readLine();
//            } catch (IOException e) {
//                return null;
//            }
//        }
//
//        public String next() {
//            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
//                try {
//                    tokenizer = new StringTokenizer(reader.readLine());
//                } catch (IOException e) {
//                    throw new RuntimeException(e);
//                }
//            }
//            return tokenizer.nextToken();
//        }
//
//        public int nextInt() {
//            return Integer.parseInt(next());
//        }
//
//        public long nextLong() {
//            return Long.parseLong(next());
//        }
//    }
//
//    /**
//     * --- Solution starts here ---
//     */
//    static class Solution {
//        InputReader in;
//        PrintWriter out;
//
//        public Solution(InputReader in, PrintWriter out) {
//            this.in = in;
//            this.out = out;
//        }
//
//        public void run() {
//            FileSystem fileSystem = new FileSystem();
//
//            String currentLine;
//            while ((currentLine = in.nextLine()) != null) {
//                String parts[] = currentLine.split(" ");
//                if (parts[0].equals("add")) {
//                    String name = parts[1];
//                    String parent = parts[2];
//
//                    fileSystem.addFolder(name, parent);
//                } else if (parts[0].equals("insert")) {
//                    String fileName = parts[1];
//                    int fileSize = Integer.parseInt(parts[2]);
//                    String parent = parts[3];
//
//                    Folder dest = fileSystem.addFile(new File(fileName, fileSize), parent);
//                    if (dest != null) {
//                        out.println(fileName + " added to " + dest.getName());
//                    }
//                } else if (parts[0].equals("print")) {
//                    String folderName = parts[1];
//                    out.print(fileSystem.print(folderName));
//                } else if (parts[0].equals("remove")) {
//                    String name = parts[1];
//                    out.println(fileSystem.remove(name));
//                } else if (parts[0].equals("search")) {
//                    String name = parts[1];
//                    out.print(fileSystem.search(name));
//                }
//            }
//        }
//    }
//}
//
//class FileSystem {
//    Folder root;
//    TreeMap<String, Folder> folders;
//
//    public FileSystem() {
//        root = new Folder("root", null);
//        folders = new TreeMap<String, Folder>();
//        folders.put("root", root);
//    }
//
//    public void addFolder(String name, String parent) {
//        Folder parentFolder = folders.get(parent);
//        folders.put(name, parentFolder.insertFolder(name));
//    }
//
//    public Folder addFile(File file, String parent) {
//        String targetFolder = parent;
//        Folder parentFolder = folders.get(targetFolder);
//
//        Folder result = parentFolder.insertFile(file);
//        if (result != null) {
//            return result;
//        }
//
//        parentFolder = parentFolder.getParent();
//        while (parentFolder != null) {
//            TreeMap<String, Folder> currentFolders = parentFolder.folderMap();
//
//            for (Folder folder : currentFolders.tailMap(targetFolder, false).values()) {
//                result = folder.insertFile(file);
//                if (result != null) {
//                    return result;
//                }
//            }
//
//            for (Folder folder : currentFolders.headMap(targetFolder, false).values()) {
//                result = folder.insertFile(file);
//                if (result != null) {
//                    return result;
//                }
//            }
//
//            targetFolder = parentFolder.getName();
//            parentFolder = parentFolder.getParent();
//        }
//
//        return null;
//    }
//
//    public String print(String name) {
//        return folders.get(name).print();
//    }
//
//    public String remove(String name) {
//        Folder folder = folders.get(name);
//
//        if (folder != null) {
//            List<Folder> removed = folder.remove();
//            for (Folder aFolder : removed) {
//                folders.remove(aFolder.getName());
//            }
//
//            return "Folder " + name + " removed";
//        }
//
//        int count = root.removeFile(name);
//        return count + " File " + name + " removed";
//    }
//
//    public String search(String name) {
//        return root.search(name);
//    }
//}
//
//class Folder {
//    private TreeMap<String, Folder> folders;
//    private TreeSet<File> files;
//
//    private Folder parent;
//    private String name;
//    private String fileType;
//    private int size;
//
//    public Folder(String name, Folder parent) {
//        this.parent = parent;
//        this.name = name;
//        size = 1;
//    }
//
//    public Folder(String name, Folder parent, String fileType, TreeSet<File> files) {
//        this(name, parent);
//        this.files = files;
//        this.fileType = fileType;
//
//        int total = 0;
//        if (files != null) {
//            for (File file : files) {
//                total += file.getSize();
//            }
//        }
//
//        addSize(total);
//    }
//
//    public Folder insertFolder(String name) {
//        if (folders == null) {
//            folders = new TreeMap<String, Folder>();
//        }
//
//        Folder newFolder = new Folder(name, this, fileType, files);
//        folders.put(name, newFolder);
//        fileType = null;
//        files = null;
//
//        updateSize(size + 1);
//
//        return newFolder;
//    }
//
//    private static String formatResult(String name, int size, int depth) {
//        String spaces = "";
//        for (int i = 0; i < 2 * depth; i++) {
//            spaces += " ";
//        }
//
//        return spaces + "> " + name + " " + size + "\n";
//    }
//
//    private static String formatResult(String name, int depth) {
//        String spaces = "";
//        for (int i = 0; i < 2 * depth; i++) {
//            spaces += " ";
//        }
//
//        return spaces + "> " + name + "\n";
//    }
//
//    public String print() {
//        Stack<Folder> dfs = new Stack<Folder>();
//        Stack<Folder> temp = new Stack<Folder>();
//        Stack<Integer> depths = new Stack<Integer>();
//
//        String result = "";
//
//        depths.push(0);
//        dfs.push(this);
//
//        while (!dfs.empty()) {
//            Folder current = dfs.pop();
//            Integer depth = depths.pop();
//
//            result += formatResult(current.getName(), current.getSize(), depth);
//
//            TreeSet<File> fileSet = current.fileSet();
//            if (fileSet != null) {
//                for (File file : fileSet) {
//                    result += formatResult(file.getName() + "." + file.getType(), file.getSize(), depth + 1);
//                }
//            }
//
//            Collection<Folder> folderSet = current.folderSet();
//            if (folderSet != null) {
//                for (Folder folder : folderSet) {
//                    temp.push(folder);
//                }
//
//                while (!temp.empty()) {
//                    dfs.push(temp.pop());
//                    depths.push(depth + 1);
//                }
//            }
//        }
//
//        return result;
//    }
//
//    public String search(String name) {
//        List<String> path = new ArrayList<String>();
//        List<Boolean> printed = new ArrayList<Boolean>();
//        Stack<Integer> depths = new Stack<Integer>();
//        Stack<Folder> dfs = new Stack<Folder>();
//        Stack<Folder> temp = new Stack<Folder>();
//        String result = "";
//
//        dfs.push(this);
//        depths.push(0);
//
//        int lastPrinted = 0, prevDepth = -1;
//
//        while (!dfs.empty()) {
//            Folder current = dfs.pop();
//            int depth = depths.pop();
//
//
//            while (path.size() - 1 >= depth) {
//                path.remove(path.size() - 1);
//                printed.remove(printed.size() - 1);
//            }
//
//            path.add(current.getName());
//            printed.add(false);
//
//            TreeSet<File> fileSet = current.fileSet();
//            if (fileSet != null) {
//                for (File file : fileSet) {
//                    if (file.getName().equals(name)) {
//                        for (int i = 0; i < path.size(); i++) {
//                            if (!printed.get(i)) {
//                                result += formatResult(path.get(i), i);
//                                printed.set(i, true);
//                            }
//                        }
//                        result += formatResult(file.getName() + "." + file.getType(), depth + 1);
//                        lastPrinted = path.size();
//                    }
//                }
//            }
//
//            Collection<Folder> folderSet = current.folderSet();
//            if (folderSet != null) {
//                for (Folder folder : folderSet) {
//                    if (folder.getName().equals(name)) {
//                        for (int i = 0; i < path.size(); i++) {
//                            result += formatResult(path.get(i), i);
//                        }
//                        result += formatResult(folder.getName(), depth + 1);
//                    } else {
//                        temp.push(folder);
//                        depths.push(depth + 1);
//                    }
//                }
//
//                while (!temp.empty()) {
//                    dfs.push(temp.pop());
//                }
//            }
//
//            prevDepth = depth;
//        }
//
//        return result;
//    }
//
//    public Folder insertFile(File file) {
//        Stack<Folder> dfs = new Stack<Folder>();
//        Stack<Folder> temp = new Stack<Folder>();
//        dfs.push(this);
//
//        while (!dfs.empty()) {
//            Folder current = dfs.pop();
//
//            if (current.canContainFile(file)) {
//                current.addFile(file);
//                return current;
//            }
//
//            Collection<Folder> folderSet = current.folderSet();
//            // reversing folder order
//            if (folderSet != null) {
//                for (Folder folder : folderSet) {
//                    temp.push(folder);
//                }
//                while (!temp.empty()) {
//                    dfs.push(temp.pop());
//                }
//            }
//        }
//
//        return null;
//    }
//
//    public void addFile(File file) {
//        if (files == null) {
//            files = new TreeSet<File>();
//            fileType = file.getType();
//        }
//
//        files.add(file);
//        updateSize(size + file.getSize());
//    }
//
//    public List<Folder> remove() {
//        Stack<Folder> dfs = new Stack<Folder>();
//        dfs.push(this);
//
//        // get list of folders that deleted
//        List<Folder> removed = new ArrayList<Folder>();
//        while (!dfs.empty()) {
//            Folder current = dfs.pop();
//
//            Collection<Folder> folderSet = current.folderSet();
//            if (folderSet != null) {
//                for (Folder folder : folderSet) {
//                    removed.add(folder);
//                    dfs.add(folder);
//                }
//            }
//
//            current.folders = null;
//            current.files = null;
//        }
//
//        // remove from parent and update size
//        this.parent.folderMap().remove(name);
//        this.parent.refresh();
//        updateSize(0);
//
//        return removed;
//    }
//
//    public int removeFile(String name) {
//        Stack<Folder> dfs = new Stack<Folder>();
//        dfs.push(this);
//
//        int count = 0;
//        while (!dfs.empty()) {
//            Folder now = dfs.pop();
//
//            TreeSet<File> currentFiles = now.fileSet();
//            if (currentFiles != null) {
//                for (File file : currentFiles) {
//                    if (file.getName().equals(name)) {
//                        currentFiles.remove(file);
//                        now.updateSize(now.getSize() - file.getSize());
//                        count += 1;
//                    }
//                }
//            }
//
//            Collection<Folder> folderSet = now.folderSet();
//            if (folderSet != null) {
//                for (Folder folder : folderSet) {
//                    dfs.push(folder);
//                }
//            }
//
//            now.refresh();
//        }
//
//        return count;
//    }
//
//    public boolean canContainFile(File file) {
//        // already contains folders
//        if (folders != null && folders.size() > 0)
//            return false;
//
//        return fileType == null || fileType.equals(file.getType());
//    }
//
//    public void updateSize(int newSize) {
//        int delta = newSize - size;
//
//        Folder current = this;
//        while (current != null) {
//            current.addSize(delta);
//            current = current.getParent();
//        }
//    }
//
//    public void refresh() {
//        if (files != null && files.size() == 0) {
//            fileType = null;
//            files = null;
//        }
//
//        if (folders != null && folders.size() == 0) {
//            folders = null;
//        }
//    }
//
//    private int getSize() {
//        return size;
//    }
//
//    public void addSize(int delta) {
//        size += delta;
//    }
//
//    public Folder getParent() {
//        return parent;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public TreeSet<File> fileSet() {
//        return files;
//    }
//
//    public Collection<Folder> folderSet() {
//        if (folders == null)
//            return null;
//
//        return folders.values();
//    }
//
//    public TreeMap<String, Folder> folderMap() {
//        return folders;
//    }
//}
//
//class File implements Comparable<File> {
//    private String name;
//    private int size;
//    private String type;
//
//    public File(String fileName, int size) {
//        String[] parts = fileName.split("\\.");
//        this.name = parts[0];
//        this.size = size;
//        this.type = parts[1];
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public int getSize() {
//        return size;
//    }
//
//    public int compareTo(File another) {
//        int cmp = name.compareTo(another.getName());
//        if (cmp != 0) {
//            return cmp;
//        }
//
//        cmp = type.compareTo(another.getType());
//        if (cmp != 0) {
//            return cmp;
//        }
//
//        return size - another.getSize();
//    }
//}