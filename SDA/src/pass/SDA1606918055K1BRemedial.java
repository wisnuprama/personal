package pass;

import java.io.*;
import java.lang.*;

public class SDA1606918055K1BRemedial {

    static int len = 0;

    public static void main(String[] args) throws IOException {
        // for I/O
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String inputs[] = null;
        int n, k, count;

        // You can change string tokenizer's delimeter
        n = Integer.parseInt(br.readLine());

        // Data Structure below or as static final
        long lst[] = new long[n];
        // --------------------

        inputs = br.readLine().split(" ");
        len = inputs.length;

        for(int i = 0; i < len; ++i) {
            lst[i] += Integer.parseInt(inputs[i]);
        }

        long maxSum = findMaxSubarraySum(lst);

        long maxCorner = 0;
        for(int i = 0; i < len; ++i) {
            long l = lst[i];
            maxCorner += l;
            lst[i] = -l;
        }

        maxCorner += findMaxSubarraySum(lst);

        long out = (maxCorner > maxSum) ? maxCorner : maxSum;
        System.out.println(out);
    }

    static long findMaxSubarraySum(long lst[]) {
        long max = 0;
        long maxTemp = 0;

        for(int i = 0; i < len; ++i) {
            maxTemp += lst[i];

            if(maxTemp < 0) maxTemp = 0;
            if(max < maxTemp) max = maxTemp;
        }

        return max;
    }
}