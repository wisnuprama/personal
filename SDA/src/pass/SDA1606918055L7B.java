package pass;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class SDA1606918055L7B {

    static final BufferedReader IN = new BufferedReader(new InputStreamReader(System.in));
    static final PrintWriter OUT = new PrintWriter(new OutputStreamWriter(System.out));

    public static void main(String args[]) throws Exception {
        BSTAVL<Long> treeAVL = new BSTAVL<>();

        int N = Integer.parseInt(IN.readLine());
        StringTokenizer st = new StringTokenizer(IN.readLine());

        long[] key = new long[N];
        int[] jumlah = new int[N];

        int j = 0;
        while(st.hasMoreTokens()) {
            key[j++] = Long.parseLong(st.nextToken());
        }

        for(int i = N-1; i >= 0; i--) {
            jumlah[i] = treeAVL.insert(key[i]);
        }

        for(int i = 0; i < N-1; i++)
            OUT.print(jumlah[i] + " ");

        OUT.println(jumlah[N-1]);
        OUT.flush();
    }

}

class BSTAVL<E extends Comparable<? super E>> {

    static class Node<E> {
        E elem;
        Node<E> left;
        Node<E> right;
        int height;
        int size;

        public Node(E elem) {
            this.elem = elem;
            height = 1;
            size = 0;
        }

        static int size(Node t){
            if(t == null) return 0;
            return t.size;
        }
    }

    private Node<E> root;
    private int counter;

    public BSTAVL() {
        this.counter = 0;
    }

    public E findMin() {
        Node<E> tmp = findMin(root);
        if (tmp != null)
            return tmp.elem;
        return null;
    }

    public E findMax() {
        Node<E> tmp = findMax(root);
        if (tmp != null)
            return tmp.elem;
        return null;
    }

    public int insert(E x) {
        this.counter = 0;
        root = insert(x, root);
        return counter;
    }

    private Node<E> insert(E x, Node<E> t) {
        if (t == null)
            // TO DO : Lengkapi bagian ini
            t = new Node<E>(x);

        int compareResult = x.compareTo(t.elem);
        if (compareResult < 0) {
            // TO DO : Lengkapi bagian ini
            this.counter += 1 + Node.size(t.right);
            t.left = insert(x, t.left);
        } else if (compareResult > 0) {
            // TO DO : Lengkapi bagian ini
            t.right = insert(x, t.right);
        }

        return balance(t);
    }

    public void remove(E x) {
        root = remove(x, root);
    }

    private Node<E> remove(E x, Node<E> t) {
        if (t == null) {
            return t;   // Item not found; do nothing
        }

        int compareResult = x.compareTo(t.elem);

        if (compareResult < 0) {
            // TO DO : Lengkapi bagian ini
            t.left = remove(x, t.left);
        } else if (compareResult > 0) {
            // TO DO : Lengkapi bagian ini
            t.right = remove(x, t.right);
        } else if (t.left != null && t.right != null) {
            // TO DO : Lengkapi bagian ini
            t.elem = findMax(t.left).elem;
            t.left = remove(t.elem, t.left);
        } else {
            // TO DO : Lengkapi bagian ini
            if (t.left == null && t.right == null)
                t = null;
            else if (t.left == null)
                t = t.right;
            else if (t.right == null)
                t = t.left;
        }
        return balance(t);

    }

    private static final int ALLOWED_IMBALANCE = 1;

    private Node<E> balance(Node<E> t) {
        if (t == null) {
            // TO DO : Lengkapi bagian ini
            return t;
        }

        if (height(t.left) - height(t.right) > ALLOWED_IMBALANCE) {
            if (height(t.left.left) >= height(t.left.right)) {
                // TO DO : Lengkapi bagian ini
                t = rotateWithLeftChild(t);
            } else {
                // TO DO : Lengkapi bagian ini
                t = doubleWithLeftChild(t);
            }
        } else {
            if (height(t.right) - height(t.left) > ALLOWED_IMBALANCE) {
                if (height(t.right.right) >= height(t.right.left)) {
                    // TO DO : Lengkapi bagian ini
                    t = rotateWithRightChild(t);
                } else {
                    // TO DO : Lengkapi bagian ini
                    t = doubleWithRightChild(t);
                }
            }

        }

        // TO DO : Lengkapi bagian ini (update height dari node)
        t.height = 1 + Math.max(height(t.left), height(t.right));
        t.size = 1 + Node.size(t.left) + Node.size(t.right);

        return t;
    }

    private Node<E> findMin(Node<E> t) {
        if (t == null) {
            // TO DO : Lengkapi bagian ini
            return t;
        }

        while (t.left != null) {
            // TO DO : Lengkapi bagian ini
            t = t.left;
        }

        return t;
    }

    private Node<E> findMax(Node<E> t) {
        if (t == null) {
            // TO DO : Lengkapi bagian ini
            return t;
        }

        while (t.right != null) {
            // TO DO : Lengkapi bagian ini
            t = t.right;
        }

        return t;
    }

    private int height(Node<E> t) {
        return t == null ? -1 : t.height;
    }

    public boolean isEmpty() {
        return root == null;
    }

    private void inOrder(Node<E> node, List<E> list) {
        if (node == null) return;

        inOrder(node.left, list);
        list.add(node.elem);
        inOrder(node.right, list);
    }

    public List<E> inOrder() {
        List<E> list = new ArrayList<>();
        inOrder(root, list);
        return list;
    }

    private Node<E> rotateWithLeftChild(Node<E> k2) {
        Node<E> k1 = k2.left;
        k2.left = k1.right;
        k1.right = k2;

        // TO DO : Lengkapi bagian ini (update height dari k1 dan k2)
        k2.height = 1 + Math.max(height(k2.left), height(k2.right));
        k1.height = 1 + Math.max(height(k1.left), k2.height);
        k2.size = Node.size(k2.left) + Node.size(k2.right) + 1;
        k1.size = Node.size(k1.left) + Node.size(k1.right) + 1;
        return k1;
    }

    private Node<E> rotateWithRightChild(Node<E> k1) {
        // TO DO : Lengkapi bagian ini
        Node<E> k2 = k1.right;
        k1.right = k2.left;
        k2.left = k1;
        k1.height = 1 + Math.max(height(k1.left), height(k1.right));
        k2.height = 1 + Math.max(height(k2.right), k1.height);
        k1.size = 1 + Node.size(k1.left) + Node.size(k1.right);
        k2.size = 1 + Node.size(k2.left) + Node.size(k2.right);
        return k2;
    }

    private Node<E> doubleWithLeftChild(Node<E> k3) {
        // TO DO : Lengkapi bagian ini
        k3.left = rotateWithRightChild(k3.left);
        return rotateWithLeftChild(k3);
    }

    private Node<E> doubleWithRightChild(Node<E> k1) {
        // TO DO : Lengkapi bagian ini
        k1.right = rotateWithLeftChild(k1.right);
        return rotateWithRightChild(k1);
    }
}