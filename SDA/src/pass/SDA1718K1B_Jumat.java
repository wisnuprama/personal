package pass;

import java.io.*;
import java.util.*;

/**
 * Created by wisnuprama on 9/22/2017.
 */
public class SDA1718K1B_Jumat {

    public static void main(String args[]) throws IOException {

        int N, P;
        String inputs[] = null;
        String in;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        TreeSet<Kandidat> kandidats = new TreeSet<>();
        ArrayList<String> namaKandidat = new ArrayList<>();

        inputs = br.readLine().split(" ");
        N = Integer.parseInt(inputs[0]);
        P = Integer.parseInt(inputs[1]);

        String nama, agensi;
        int nilai;
        while((in = br.readLine())!=null && in.length()!=0){

            inputs = in.split(" ");
            nama = inputs[0];
            agensi = inputs[2];
            nilai = Integer.parseInt(inputs[1]);

            if(namaKandidat.contains(nama)) {
                continue;
            } else {
                Kandidat knd = new Kandidat(nama, agensi, nilai);
                namaKandidat.add(nama);
                kandidats.add(knd);
            }

        }

        TreeMap<String, Agensi> terpilih = new TreeMap<>();

        for(Kandidat knd : kandidats) {

            if(--P < 0) break;

            if(terpilih.containsKey(knd.agensi)) {
                terpilih.get(knd.agensi).jumlah++;
            } else {
                terpilih.put(knd.agensi, new Agensi(knd.agensi, 1));
            }
        }

        TreeSet<Agensi> outputs = new TreeSet<>();
        for(Map.Entry entry : terpilih.entrySet()) {
            outputs.add((Agensi) entry.getValue());
        }

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        for(Agensi ag : outputs) {
            String out = "Jumlah kandidat terpilih dari agensi " + ag.nama + " adalah: " + ag.jumlah + "\n";
            bw.write(out);
            bw.flush();
        }
    }
}

class Agensi implements Comparable<Agensi> {
    String nama;
    int jumlah;

    public Agensi(String nama, int jumlah) {
        this.nama = nama;
        this.jumlah = jumlah;
    }

    @Override
    public int compareTo(Agensi o) {
        if(jumlah == o.jumlah) {
            return nama.compareTo(o.nama);
        }

        return jumlah < o.jumlah ? 1 : -1;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;

        Agensi agensi = (Agensi) o;
        return nama.equals(agensi.nama);
    }

    @Override
    public int hashCode() {
        int result = nama != null ? nama.hashCode() : 0;
        result = 31 * result + jumlah;
        return result;
    }

    @Override
    public String toString() {
        return "Agensi{" +
                "nama='" + nama + '\'' +
                ", jumlah=" + jumlah +
                '}';
    }
}

class Kandidat implements Comparable<Kandidat> {

    String nama;
    String agensi;
    int nilai;

    public Kandidat(String nama, String agensi, int nilai) {
        this.nama = nama;
        this.agensi = agensi;
        this.nilai = nilai;
    }

    @Override
    public int compareTo(Kandidat o) {
        return nilai <= o.nilai ? 1 : -1;
    }

    @Override
    public boolean equals(Object o) {
        Kandidat kandidat = (Kandidat) o;
        return nama.equals(kandidat.nama);
    }

    @Override
    public int hashCode() {
        int result = nama != null ? nama.hashCode() : 0;
        result = 31 * result + nilai;
        return result;
    }

    @Override
    public String toString() {
        return nama + " " + agensi + " " + nilai;
    }
}

