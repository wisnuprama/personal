package pass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
/**
 * Created by wisnuprama on 9/15/2017.
 */

public class SDA1718L2B_Jumat {
    static final LinkedList<String> GARASI = new LinkedList<>();
    static final String LIST_MOGOK[] = new String[1000000];
    static final StringBuilder STRING_BUILDER = new StringBuilder();

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String in = null;
        while ((in = br.readLine()) != null && in.length() != 0) {
            String[] inputs = in.split(" ");

            switch (inputs[0]) {
                case "MASUK":

                    // DECIDE INPUT KE TIMUR ATAU BARAT
                    if (inputs[2].equals("BARAT")) {
                        GARASI.addFirst(inputs[1]);
                    } else {
                        GARASI.add(inputs[1]);
                    }

                    STRING_BUILDER.append(inputs[1] + " masuk melalui pintu " + inputs[2] + "\n");
                    break;
                case "KELUARKAN":
                    String mobil = inputs[1];
                    int limit = GARASI.size();
                    int pos = GARASI.indexOf(mobil);
                    String leftBlocker = "";
                    String rightBlocker = "";

                    if (pos == -1) {
                        STRING_BUILDER.append(mobil + " tidak ada di garasi\n");
                        break;
                    }

                    if(LIST_MOGOK[GARASI.indexOf(mobil)] != null){
                        STRING_BUILDER.append("Mobil " + mobil + " sedang mogok\n");
                        break;
                    }

                    int right = pos + 1;
                    boolean isRight = true;
                    while (right < limit) {
                        String carMogok = LIST_MOGOK[right++];
                        if (carMogok != null) {
                            isRight = false;
                            rightBlocker = carMogok;
                            break;
                        }
                    }


                    int left = pos - 1;
                    boolean isLeft = true;
                    while (left >= 0) {
                        String carMogok = LIST_MOGOK[left--];
                        if (carMogok != null) {
                            isLeft = false;
                            leftBlocker = carMogok;
                            break;
                        }
                    }

                    left = pos - 1;
                    right = limit - pos + 1;

                    boolean n = left < right ? true : false;

                    if (isLeft && n) {
                        GARASI.remove(mobil);
                        STRING_BUILDER.append(mobil + " keluar melalui pintu BARAT\n");
                        break;
                    }

                    if (isRight && !n) {
                        GARASI.remove(mobil);
                        STRING_BUILDER.append(mobil + " keluar melalui pintu TIMUR\n");
                        break;
                    }

                    STRING_BUILDER.append(mobil + " tidak bisa keluar, mobil " + leftBlocker + " dan " + rightBlocker + " sedang mogok\n");

                    break;
                case "MOGOK":
                    LIST_MOGOK[GARASI.indexOf(inputs[1])] = inputs[1];
                    break;
                case "SERVIS":
                    LIST_MOGOK[GARASI.indexOf(inputs[1])] = null;
                    break;

            }
        }

        System.out.print(STRING_BUILDER);
    }
}
/*
    static void keluar(String mobil) {
        boolean leftBlock = true;
        boolean rightBlock = true;
        // POSISI MOBIL DI GARASI
        int pos = GARASI.indexOf(mobil);
        int left = pos - 1;
        int right = pos + 1;
        String leftBlocker = "";
        String rightBlocker = "";

        if (pos == -1) {
            STRING_BUILDER.append(mobil + " tidak ada di garasi\n");
            return;
        }

        if (LIST_MOGOK.contains(mobil)) {
            STRING_BUILDER.append("Mobil " + mobil + " sedang mogok\n");
            return;
        }

        while (leftBlock || rightBlock) {
            if (left < 0) {
                GARASI.poll(mobil);
                STRING_BUILDER.append(mobil + " keluar melalui pintu BARAT\n");
                return;
            }
            if (right == GARASI.size()) {
                GARASI.poll(mobil);
                STRING_BUILDER.append(mobil + " keluar melalui pintu TIMUR\n");
                return;
            }

            if (!LIST_MOGOK.contains(GARASI.get(left)) && leftBlock) {
                --left;
            } else {
                leftBlocker = GARASI.get(left);
                leftBlock = false;
            }

            if (!LIST_MOGOK.contains(GARASI.get(right)) && rightBlock) {
                ++right;
            } else {
                rightBlocker = GARASI.get(right);
                rightBlock = false;
            }
        }

        STRING_BUILDER.append(mobil + " tidak bisa keluar, mobil " + leftBlocker + " dan " + rightBlocker + " sedang mogok\n");
    }
}
*/
