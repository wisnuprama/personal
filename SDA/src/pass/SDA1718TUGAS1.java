package pass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Wisnu Pramadhitya Ramadhan 1606918055
 * SDA-A / Ka Nanda
 * Tugas 1
 * 9/17/2017.
 */
public class SDA1718TUGAS1 {

    static final String VIEW = "VIEW";
    static final String BACK = "BACK";
    static final String NEWTAB = "NEWTAB";
    static final String CHANGETAB = "CHANGETAB";
    static final String HISTORY = "HISTORY";

    public static void main(String args[]) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringBuilder stringBuilder = new StringBuilder();

        int iteration = Integer.parseInt(br.readLine());

        /*
            set for history because it wants the be unique
            for tab's container, i'm using arraylist because
            we want to add only, arraylist is fast for add and get is O(1)
         */
        Set<String> history = new TreeSet<>();
        List<Tab> tabs = new ArrayList<>();
        Tab nowTab = new Tab(); // FIRST TAB
        tabs.add(nowTab);
        String link = null;

        String inputs[] = null;
        loop: while(--iteration>=0) {
            inputs = br.readLine().split(";");
            String command = inputs[0];

            if(VIEW.equals(command)) {
                String kucing = inputs[1]+":"+inputs[2];
                link = "VIEWING(["+kucing+"])\n";

                /*
                    process the link
                    get the latest tab and add it to that tab
                    add the link to history
                 */
                nowTab.view(link);
                history.add(kucing+"\n");
                stringBuilder.append(link);

            } else if (BACK.equals(command)) {
                link = nowTab.back(); // tab.back() mengembalikan link
                stringBuilder.append(link);

            } else if(HISTORY.equals(command)) {
                /*
                    cek apakah history masih kosong, jika iya output: NO RECORD
                    jika tidak maka outputnya adalah sebanyak baris m dengan tiap
                    barisnya adalah link yang pernah dikunjungi.
                    urutan keluaran secara leksikografi
                 */
                if(history.isEmpty()) stringBuilder.append("NO RECORD\n");
                else for (String li : history) stringBuilder.append(li);

            } else if(NEWTAB.equals(command)) {
                /*
                    menambahkan tab baru.
                    nowTab berubah ke tab baru.
                 */
                nowTab = new Tab();
                tabs.add(nowTab);

            } else if(CHANGETAB.equals(command)) {
                /*
                    mengganti tab ke ke tab ke-i.
                    Jika tidak ditemukan tab ke-i maka keluarkan output NO TAB
                 */
                try {
                    int iTab = Integer.parseInt(inputs[1]);
                    nowTab = tabs.get(iTab); // mungkin akan throws exception
                    stringBuilder.append("TAB: " + iTab + "\n");

                } catch (IndexOutOfBoundsException e) {
                    stringBuilder.append("NO TAB\n");
                }

            }
        }

        System.out.print(stringBuilder);
    }
}

/**
 * Class Tab berupa class menyimpan aktifitas user secara temporal untuk fitur back
 * dengan mengimplementasikan stack.
 */
class Tab {
    Stack<String> temporary;

    Tab() {
        temporary = new Stack<>();
    }

    /**
     * view merupakan method menambahkan link ke tab
     * @param link String
     */
    void view(String link) {
        temporary.push(link);
    }

    /**
     * back merupakan method untuk kembali ke link sebelumnya
     * @return String link atau EMPTY TAB jika tab kosong
     */
    String back() {
        try {

            /*
                below is two line which would throws an exception
             */
            temporary.pop();
            return temporary.peek();

        } catch (EmptyStackException e) {
            return "EMPTY TAB\n";
        }
    }
}
