package pass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by wisnuprama on 15/09/17.
 */
public class SDA_ASEL {
    public static void main(String[] args) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> timeline = new ArrayList<>();
        HashSet<String> history = new HashSet<>();
        int timeline_pointer = 0;

        String in = br.readLine();
        while (in != null) {
            String[] inputs = in.split(" ");

            switch (inputs[0]) {
                case "GO_TO":
                    System.out.println(1);
                    break;
                case "BACK":
                    System.out.println(2);
                    break;
                case "FORWARD":
                    System.out.println(3);
                    break;
                case "CHECK_HISTORY":
                    System.out.println(4);
                    break;
            }
            System.out.println(inputs[0]);
            in = br.readLine();
        }
    }
}
