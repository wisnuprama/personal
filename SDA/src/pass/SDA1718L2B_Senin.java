package pass;

import java.io.*;
import java.util.*;

/**
 * Created by wisnuprama on 9/13/2017.
 */
public class SDA1718L2B_Senin {

    static final String DAFTAR = "DAFTAR";
    static final String KELUAR = "KELUAR";
    static final String MUDA = "MUDA";

    public static void main(String args[]) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        TreeSet<Integer> tree = new TreeSet<>();
        StringBuilder stringBuilder = new StringBuilder();
        int in;

        String sInput = null;
        while((sInput = br.readLine()) != null && sInput.length() != 0) {
            // split ;
            String parse[] = sInput.split(" ");

            switch (parse[0]) {

                case DAFTAR:
                    in = Integer.parseInt(parse[1]);
                    tree.add(in);
                    stringBuilder.append("Anak dengan umur "+ in +" telah terdaftar\n");
                    break;
                case KELUAR:
                    in = Integer.parseInt(parse[1]);
                    if(tree.contains(in)) {
                        tree.remove(in);
                        stringBuilder.append("Anak dengan umur "+ in +" sudah keluar\n");
                    } else {
                        stringBuilder.append("Anak dengan umur "+ in +" tidak ditemukan\n");
                    }
                    break;
                case MUDA:
                    in = Integer.parseInt(parse[1]);
                    Object[] arr = tree.toArray();
                    stringBuilder.append("Anak termuda ke "+in+" berumur "+ arr[in-1] + "\n");

            }
        }

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        bw.write(stringBuilder.toString());
        bw.flush();

    }

}
