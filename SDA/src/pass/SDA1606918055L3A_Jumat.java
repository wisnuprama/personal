package pass;

import java.io.*;
import java.util.Arrays;

/**
 * Created by Wisnu Pramadhitya  on 28/09/17.
 *
 * SOAL A LAB 3
 */
public class SDA1606918055L3A_Jumat {

    static final PrintWriter PRINT = new PrintWriter(new OutputStreamWriter(System.out));

    public static void main(String args[]) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String in = null;
        int n;

        // DATA STRUCTURE
        Stack<Integer> stack = new Stack<>();

        // START
        n = Integer.parseInt(br.readLine());

        read:while(--n >= 0 && ((in=br.readLine())!=null && in.length()!=0)) {

            int len = in.length();
            stack.clear();

            for(int i=0; i < len; ++i ) {
                char c = in.charAt(i);
                int target = 0;

                /**
                 * Idenya adalah ketika char L V <, push ke stack
                 * dengan indexnya masing masing
                 * L <-> O = 1
                 * V <-> E = 2
                 * < <-> 3 = 3
                 *
                 * ketika yang masuk adalah kelompok O E 3, pop dari stack
                 * dan hasil yang di pop harus sesuai pasangannya.
                 *
                 * adalah ketika stacknya belum habis karena ganjil atau genap namun jumlah
                 * tidak kelompok 1 dan 2 tidak sama, jadi tidak bagus.
                 *
                 */
                if(c == 'L' || c == 'V' || c == '<') {
                    if(c == 'L') target = 1;
                    else if(c == 'V') target = 2;
                    else target = 3;

                    stack.push(target);

                } else {
                    if (stack.isEmpty()) {
                        PRINT.println("TIDAK BAGUS");
                        PRINT.flush();
                        continue read;
                    }

                    if (c == 'O') target = 1;
                    else if (c == 'E') target = 2;
                    else target = 3;

                    int fromStack = stack.pop();

                    if (fromStack != target) {
                        PRINT.println("TIDAK BAGUS");
                        PRINT.flush();
                        continue read;
                    }
                }
            }

            if(!stack.isEmpty()) {
                PRINT.println("TIDAK BAGUS");
                PRINT.flush();
                continue read;
            }

            PRINT.println("BAGUS");
            PRINT.flush();
        }

        PRINT.close();
    }

    static <E> void itIsAMatch(Stack<E> st, int p, int length) {
        if(length <= 0) return;

    }

}

/**
 * STACK
 * @param <T>
 */
class Stack<T> {
    Object[] arr;
    int size;

    public Stack() {
        size = 0;
        arr = new Object[1000];
    }

    void push(T e) {
        arr[size++] = e;
    }

    T pop() {
        T tmp = (T) arr[--size];
        arr[size] = null;
        return tmp;
    }

    T peek() {
        return (T) arr[size-1];
    }

    boolean isEmpty() {
        return size == 0;
    }

    void clear() {
        size = 0;
        arr = new Object[1000];
    }

    public String toString() {
        Object[] tmp = new Object[size];
        for(int i=0; i < size; ++i) {
            if(arr[i] != null)
                tmp[i] = arr[i];
        }

        return Arrays.toString(tmp);
    }
}
