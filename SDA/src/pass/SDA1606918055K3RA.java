package pass;

import java.io.InputStreamReader;
import java.util.*;
import java.io.BufferedReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;


/**
 8
 4 6 2 3 5 1 8 7
 */
public class SDA1606918055K3RA {

    public static void main(String args[]) throws Exception {
        final BufferedReader IN = new BufferedReader(new InputStreamReader(System.in));
        final PrintWriter OUT = new PrintWriter(new OutputStreamWriter(System.out));
        BSTree<Integer> bsTree = new BSTree<>();

        final int N = Integer.parseInt(IN.readLine());

        StringTokenizer st = new StringTokenizer(IN.readLine());

        while (st.hasMoreElements()) {
            bsTree.add(Integer.parseInt(st.nextToken()));
        }

        OUT.println(bsTree.levelOrder());
        OUT.flush();
    }

}


class BSTree<E extends Comparable<E>> {

    private static class Node<E extends Comparable<E>> {

        E elem;
        Node<E> left;
        Node<E> right;
        Node<E> parent;

        public Node(E elem, Node<E> left, Node<E> right, Node<E> parent) {
            this.elem = elem;
            this.left = left;
            this.right = right;
            this.parent = parent;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Node<?> node = (Node<?>) o;

            return elem != null ? elem.equals(node.elem) : node.elem == null;
        }

        @Override
        public int hashCode() {
            return elem != null ? elem.hashCode() : 0;
        }
    }

    private Node<E> root;
    private int size;

    public BSTree() {
        root = null;
        size = 0;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public boolean add(E elem) {

        boolean res = false;

        if (root == null) {
            root = new Node<E>(elem, null, null, null);
            res = true;

        } else {

            Node<E> parent = null;
            Node<E> current = root;
            while (current != null) {

                E currElem = current.elem;
                parent = current;
                if (elem.compareTo(currElem) < 0) {
                    // traverse to the left
                    current = current.left;
                    if (current == null) // add
                        parent.left = new Node<E>(elem, null, null, parent);

                } else if (elem.compareTo(currElem) > 0) {
                    // traverse to the right
                    current = current.right;
                    if (current == null) // add
                        parent.right = new Node<E>(elem, null, null, parent);

                } else {
                    // Ketika elemen sama tidak dimasukan
                    return res;
                }

            }
            res = true;
            this.size++;
        }

        return res;
    }

    private Node<E> find(E elem) {

        Node<E> res = null;

        if (root != null) {

            Node<E> current = root;
            boolean found = false;
            while (!found && current != null) {

                E currElem = current.elem;
                if (elem.compareTo(currElem) < 0) { // traverse to the left
                    current = current.left;

                } else if (elem.compareTo(currElem) > 0) { // traverse to the right
                    current = current.right;

                } else {

                    // found it
                    res = current;
                    found = true;
                }
            }
        }

        return res;
    }

    public boolean remove(E elem) {

        boolean res = false;
        Node<E> node = find(elem);

        if (node != null) {

            if (node.left == null) {
                if (node.right == null) {
                    if (root == node) {
                        root = null;

                    } else if (node.elem.compareTo(node.parent.elem) < 0)
                        node.parent.left = null;
                    else
                        node.parent.right = null;

                } else {
                    if (root == node) {
                        root = node.right;
                        root.parent = null;

                    } else if (node.elem.compareTo(node.parent.elem) < 0) {
                        node.parent.left = node.right;
                        node.right.parent = node.parent;

                    } else {
                        node.parent.right = node.right;
                        node.right.parent = node.parent;
                    }
                }

            } else {

                if (node.right == null) {

                    if (root == node) {
                        root = node.left;
                        root.parent = null;

                    } else if (node.elem.compareTo(node.parent.elem) < 0) {
                        node.parent.left = node.left;
                        node.left.parent = node.parent;

                    } else {
                        node.parent.right = node.left;
                        node.left.parent = node.parent;

                    }

                } else {

                    Node<E> successor = minNode(node.right);
                    E tempElem = successor.elem;

                    if (root == node) {
                        if (successor.right == null) {
                            if (successor.elem.compareTo(successor.parent.elem) < 0)
                                successor.parent.left = null;
                            else
                                successor.parent.right = null;

                        } else {
                            if (successor.elem.compareTo(successor.parent.elem) < 0) {
                                successor.parent.left = successor.right;
                                successor.right.parent = successor.parent;

                            } else {
                                successor.parent.right = successor.right;
                                successor.right.parent = successor.parent;

                            }

                        }

                        root.elem = tempElem;

                    } else {
                        if (successor.right == null) {
                            if (successor.elem.compareTo(successor.parent.elem) < 0)
                                successor.parent.left = null;
                            else
                                successor.parent.right = null;

                        } else {
                            if (successor.elem.compareTo(successor.parent.elem) < 0)
                                successor.parent.left = null;
                            else {
                                successor.parent.right = successor.right;
                                successor.right.parent = successor.parent;

                            }
                        }

                        node.elem = tempElem;
                    }

                }

            }

            res = true;
        }

        return res;
    }

    public E min() {
        return minNode(root).elem;
    }

    private Node<E> minNode(Node<E> node) {

        Node<E> res = null;
        if (node != null) {

            Node<E> current = node;
            // traverse to the left until current = null
            while (current != null) {
                res = current;
                current = current.left;
            }
        }

        return res;
    }

    public E max() {
        return maxNode(root).elem;
    }

    private Node<E> maxNode(Node<E> node) {

        Node<E> res = null;
        if (node != null) {
            // traverse to the right until current =  null
            Node<E> current = node;
            while (current != null) {
                res = current;
                current = current.right;
            }

        }

        return res;
    }

    public boolean contains(E elem) {
        Node<E> res = this.find(elem);
        return res != null;
    }

    public String levelOrder() {
        Queue<Node> queue = new ArrayDeque<>();
        Node<E> current = root;

        StringBuilder sb = new StringBuilder();
        queue.add(current);
        while (!queue.isEmpty()) {
            Node<E> tmp = queue.poll();
            sb.append(tmp.elem).append(' ');

            if (tmp.right != null) queue.add(tmp.right);
            if (tmp.left != null) queue.add(tmp.left);
        }

        return sb.substring(0, sb.length()-1);
    }
}