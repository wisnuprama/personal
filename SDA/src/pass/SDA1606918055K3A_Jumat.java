package pass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * TEMPLATE KUIS 3 A JUMAT
 * ANDA BOLEH MEMODIFIKASI TEMPLATE INI SESUKA HATI ANDA
 * ANDA JUGA BOLEH TIDAK MENGGUNAKAN TEMPLATE INI
 * ANDA BISA MENGERJAKAN PADA BAGIAN TODO ATAU BAGIAN YANG LAINNYA
 *
 * SEMANGAT :)
 */
public class SDA1606918055K3A_Jumat {

    public static void main(String[] args){
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(System.in));
            int maxOrangDiBus = Integer.parseInt(reader.readLine());
            Bus bus = new Bus(maxOrangDiBus);
            int m = Integer.parseInt(reader.readLine());
            for(int i = 0 ; i < m ; i++){
                StringTokenizer input = new StringTokenizer(reader.readLine());
                String command = input.nextToken();
                if(command.equals("NAIK")){
                    String nama = input.nextToken();
                    int umur = Integer.parseInt(input.nextToken());
                    System.out.println(bus.naik(new Penumpang(nama, umur, -1)));
                }else if(command.equals("TURUN")){
                    String nama = input.nextToken();
                    int umur = Integer.parseInt(input.nextToken());
                    System.out.println(bus.turun(new Penumpang(nama,umur,-1)));
                }else if(command.equals("MELIHAT")){
                    System.out.println(bus.melihat(Integer.parseInt(input.nextToken()), Integer.parseInt(input.nextToken())));
                }
            }
        } catch (IOException e) {

        } finally {
            if(reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                }
            }
        }


    }

    static class Penumpang implements Comparable<Penumpang>{

        private String nama;
        private int umur;
        private int waktuMenaikiBus;

        public Penumpang(String nama, int umur, int waktuMenaikiBus) {
            this.nama = nama;
            this.umur = umur;
            this.waktuMenaikiBus = waktuMenaikiBus;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public int getUmur() {
            return umur;
        }

        public void setUmur(int umur) {
            this.umur = umur;
        }

        public int getWaktuMenaikiBus() {
            return waktuMenaikiBus;
        }

        public void setWaktuMenaikiBus(int waktuMenaikiBus) {
            this.waktuMenaikiBus = waktuMenaikiBus;
        }

        @Override
        public int compareTo(Penumpang o) {

            if(this.umur == o.umur){

                if(this.nama.equals(o.nama)) // PALSU
                    return 0;

                if(this.waktuMenaikiBus == o.waktuMenaikiBus)
                    return o.waktuMenaikiBus - this.waktuMenaikiBus;

                return this.nama.compareTo(o.nama);
            }

            return o.umur - this.umur;
        }
    }

    static class Bus{

        private int maxOrang;
        private BSTOrang bstOrang;

        public Bus(int maxOrang) {
            this.maxOrang = maxOrang;
            bstOrang = new BSTOrang();
        }

        public String naik(Penumpang penumpang){
            if(bstOrang.getSize() == maxOrang){
                bstOrang.removeLast();
            }

            penumpang.setWaktuMenaikiBus(bstOrang.getSize() + 1);

            String output = penumpang.getNama() + " dengan umur " + penumpang.getUmur() + " tahun menaiki bus";

            Penumpang result = bstOrang.get(penumpang);
            if(result != null){
                output = result.getNama() + " dengan umur " + result.getUmur() + " tahun yang menaiki bus pada waktu ke-" + result.getWaktuMenaikiBus() + " dikeluarkan dari bus";
                result.setWaktuMenaikiBus(penumpang.getWaktuMenaikiBus());
            }else{
                bstOrang.add(penumpang);
            }
            return output;
        }

        public String turun(Penumpang penumpang){
            Penumpang realOne = bstOrang.get(penumpang);
            boolean isSuccess = bstOrang.remove(penumpang);
            if(isSuccess)
                return realOne.getNama() + " dengan umur " + realOne.getUmur() + " tahun yang menaiki bus pada waktu ke-"+realOne.getWaktuMenaikiBus()+", turun dari bus";
            return null;
        }

        public String melihat(int start, int finish){
            List<Penumpang> list = bstOrang.asList();
            Penumpang p = null;
            if(start > bstOrang.getSize())
                return "belum ada yang menaiki bus";

            else if(start == finish){

                int n = start;
                if(n >= list.size())
                    n = list.size()-1;

                p = list.get(n);
                return p.nama + " dengan umur " + p.umur + " tahun";
            }
            else {
                StringBuilder sb = new StringBuilder();
                int to = maxOrang < finish ? maxOrang : finish;
                for(int i=start-1; i<to-1; ++i){
                    p = list.get(i);
                    sb.append(p.nama + " dengan umur " + p.umur + " tahun, ");
                }

                p = list.get(to-1);
                sb.append(p.nama + " dengan umur " + p.umur + " tahun");
                return sb.toString();
            }
        }

    }

    static class BSTOrang{

        class Node{
            Penumpang penumpang;
            Node left;
            Node right;
            Node parent;

            public Node(Penumpang penumpang, Node parent) {
                this.penumpang = penumpang;
                this.parent = parent;
            }

        }

        private Node root;
        private int size;

        public BSTOrang(){
            size = 0;
        }

        public int getSize() {
            //TODO
            return size;
        }

        public Penumpang get(Penumpang penumpang){

            Node e = find(penumpang);
            if(e == null)
                return null;
            return e.penumpang;
        }

        public Node find(Penumpang penumpang) {

            Node res = null;

            if(root != null) {
                Node current = root;
                boolean found = false;

                while(!found && current != null){

                    Penumpang curP = current.penumpang;

                    if(penumpang.compareTo(curP) < 0)
                        current = current.left;
                    else if(penumpang.compareTo(curP) > 0)
                        current = current.right;
                    else {

                        res = current;
                        found = true;

                    }

                }
            }

            return res;

        }

        public boolean add(Penumpang penumpang){
            return addHelper(root, penumpang);

        }

        private boolean addHelper(Node node, Penumpang penumpang) {
            //TODO
            boolean res = false;
            if(root == null){
                root = new Node(penumpang, null);
                this.size++;
                res = true;

            } else {

                Node parent = null;
                Node current = root;

                while(current != null){
                     Penumpang curP = current.penumpang;
                     parent = current;

                     if(penumpang.compareTo(curP) < 0){
                         current = current.left;
                         if(current == null)
                             parent.left = new Node(penumpang, parent);

                     } else if(penumpang.compareTo(curP) > 0){
                         current = current.right;
                         if(current == null)
                             parent.right = new Node(penumpang, parent);

                     } else {
                         // punya nama dan umur yang sama
                         this.remove(current.penumpang);
                         this.addHelper(this.root, penumpang);
                     }

                }

                res = true;
                this.size++;

            }

            return res;
        }

        public boolean remove(Penumpang penumpang){
            //TODO
            boolean res = false;
            Node node = find(penumpang);

            if(node != null){

                if(node.left == null){

                    if(node.right == null) {
                        if(root == node)
                            root = null;

                        else if(node.penumpang.compareTo(node.parent.penumpang) < 0)
                            node.parent.left = null;
                        else
                            node.parent.right = null;

                    } else {
                        if(root == node){
                            root = node.right;
                            root.parent = null;

                        } else if(node.penumpang.compareTo(node.parent.penumpang) < 0){
                            node.parent.left = node.right;
                            node.right.parent = node.parent;

                        } else {
                            node.parent.right = node.right;
                            node.right.parent = node.parent;

                        }

                    }

                } else {

                    if(node.right == null){

                        if(root == node) {
                            root = node.left;
                            root.parent = null;

                        } else if(node.penumpang.compareTo(node.parent.penumpang) < 0){
                            node.parent.left = node.left;
                            node.left.parent = node.parent;

                        } else {
                            node.parent.right = node.left;
                            node.left.parent = node.parent;

                        }


                    } else {
                        Node successor = minNode(node.right);
                        Penumpang tmpP = successor.penumpang;

                        if(root == node){

                            if(successor.right == null){

                                if(successor.penumpang.compareTo(successor.parent.penumpang) < 0)
                                    successor.parent.left = null;
                                else
                                    successor.parent.right = null;

                            } else {

                                if(successor.penumpang.compareTo(successor.parent.penumpang) < 0){
                                    successor.parent.left = successor.right;
                                    successor.right.parent = successor.parent;

                                } else {
                                    successor.parent.right = successor.right;
                                    successor.right.parent = successor.parent;

                                }

                                root.penumpang = tmpP;

                            }

                        } else {

                            if(successor.right == null){

                                if(successor.penumpang.compareTo(successor.parent.penumpang) < 0)
                                    successor.parent.left = null;
                                else
                                    successor.parent.right = null;

                            } else {
                                if(successor.penumpang.compareTo(successor.parent.penumpang) < 0)
                                    successor.parent.left = null;
                                else {
                                    successor.parent.right = successor.right;
                                    successor.right.parent = successor.parent;

                                }

                            }

                            node.penumpang = tmpP;
                        }
                    }
                }

                this.size--;
                res = true;
            }
            return res;
        }

        public Penumpang removeLast(){

            List<Penumpang> list = new ArrayList<>(this.getSize());
            inOrder(root, list);

            int maxTime = Integer.MIN_VALUE;
            Penumpang targetP = null;

            for(Penumpang p : list){

                if(p.getWaktuMenaikiBus() > maxTime){
                    maxTime = p.getWaktuMenaikiBus();
                    targetP = p;
                }

            }

            this.remove(targetP);

            return targetP;
        }

        private Node minNode(Node node){
            Node res = null;

            if(node != null){
                Node current = node;

                while(current != null){
                    res = current;
                    current = current.left;
                }
            }

            return res;
        }

        private void inOrder(Node node, List<Penumpang> listP){

            if(node == null) return;

            inOrder(node.left, listP);
            listP.add(node.penumpang);
            inOrder(node.right, listP);

        }

        public List<Penumpang> asList(){
            List<Penumpang> list = new ArrayList<>(getSize());
            inOrder(root, list);
            return list;
        }

    }
}