package pass;

import java.io.*;
import java.util.StringTokenizer;

/**
 *
 * NAMA : Wisnu Pramadhitya Ramadhan
 * NPM  : 1606918055
 * KELAS: A
 *
 * SOAL A
 */

public class SDA1606918055L4A {

    public static void main(String args[]) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter print = new PrintWriter(new OutputStreamWriter(System.out));
        int n = Integer.valueOf(reader.readLine());
        int arr[] = new int[n];

        /*
            input angka
         */
        StringTokenizer st = new StringTokenizer(reader.readLine());
        for(int i=0; i<n; ++i) {
            int k = Integer.parseInt(st.nextToken());
            arr[i] = k;
        }

        /*
            sorting angka dengan quicksort
         */
        quicksort(arr, 0, arr.length-1);

        // print output
        for(int i=0; i<arr.length; ++i){
            print.print(arr[i] + " ");
        }

        print.flush();
    }

    /**
     * method sort dengan menggunakan algoritma quicksort
     * @param arr arr yang akan di sort
     * @param lo index terbawah
     * @param hi index terakhir
     */
    static void quicksort(int arr[], int lo, int hi) {

        int i = lo, j = hi, mid = (hi - lo) / 2 + lo;

        /*
            menentukan pivot, berupa data tengah array
         */
        int pivot = arr[mid];

        while(i <= j) {

            // nilai yang lebih kecil dari kiri hingga lebih besar dari pivot
            while (arr[i] < pivot) ++i;
            // mencari bagian dari kanan lebih besar dair pivot
            while (arr[j] > pivot) --j;

            /*
                swap nilai i dan j jika
                nilai kiri lebih kecil dari kanan
             */
            if (i <= j) {
                // SWAP
                int tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;

                ++i;
                --j;
            }
        }

        if(lo < j) quicksort(arr, lo, j);
        if(i < hi) quicksort(arr, i, hi);
    }
}
