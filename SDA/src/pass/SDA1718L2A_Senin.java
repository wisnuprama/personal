package pass;

import java.io.*;
import java.util.HashMap;

public class SDA1718L2A_Senin {

    static final String ORDER = "ORDER";
    static final String CEK = "CEK";
    static final String PROCESS = "PROCESS";

    static HashMap<String, Pesanan> mOrderan;
    static StringBuilder stringBuilder;

    public static void main(String args[]) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        mOrderan = new HashMap<>();
        stringBuilder = new StringBuilder();

        String sInput = null;
        while((sInput = br.readLine()) != null && sInput.length() != 0) {
            // split ;
            String parse[] = sInput.split(";");

            switch (parse[0]) {
                case ORDER:
                    mOrderan.put(parse[1], new Pesanan(
                                                parse[2],
                                                Integer.parseInt(parse[3]))
                                );

                    bw.write("Order​ ​"+ parse[1] +" ​telah​ ​diterima\n");
                    bw.flush();
                    break;
                case PROCESS:
                    if(mOrderan.containsKey(parse[1])) {
                        mOrderan.get(parse[1]).process();
                        bw.write("Order " + parse[1] + " telah ditangani\n");
                        bw.flush();
                    } else {
                        bw.write("Nomor​ ​order​ ​tidak​ ​ditemukan\n");
                        bw.flush();
                    }
                    break;
                case CEK:
                    int count = 0;
                    for(Pesanan p : mOrderan.values()) {
                        if(p.menu.equals(parse[1])) {
                            count += p.quantity;
                        }
                    }

                    bw.write("Menu " + parse[1] + " akan dipesan sejumlah " + count +"\n");
                    bw.flush();
                    break;
            }

        }
    }
}

class Pesanan {

    String menu;
    int quantity;

    public Pesanan(String menu, int quantity) {
        this.menu = menu;
        this.quantity = quantity;
    }

    void process(){
        this.quantity = 0;
    }
}