package pass;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Arrays;

/**
 * @author Wisnu Pramadhitya Ramadhan
 * 1606918055
 * <p>
 * Implementasi Binary Heap
 */

public class SDA1606918055L6A {

    static final BufferedReader IN = new BufferedReader(new InputStreamReader(System.in));
    static final PrintWriter OUT = new PrintWriter(new OutputStreamWriter(System.out));

    public static void main(String args[]) throws Exception {

        BinaryHeap heap = new BinaryHeap();

        String input = null;
        int elem;
        while ((input = IN.readLine()) != null && input.length() > 0) {

            String[] parse = input.split(" ");

            switch (parse[0]) {

                case "INSERT":
                    elem = Integer.parseInt(parse[1]);
                    if (!heap.contains(elem))
                        heap.add(elem);

                    OUT.println("elemen dengan nilai "+elem+" telah ditambahkan");
                    break;

                case "REMOVE":
                    if (heap.isEmpty())
                        OUT.println("min heap kosong");
                    else {
                        OUT.println(heap.poll() + " dihapus dari heap");
                    }
                    break;

                case "NUM_PERCOLATE_UP":
                    elem = Integer.parseInt(parse[1]);
                    if (heap.isEmpty() || !heap.contains(elem))
                        OUT.println("percolate up 0");
                    else {
                        OUT.println("percolate up " + heap.getPercolateUpOf(elem));
                    }

                    break;

                case "NUM_PERCOLATE_DOWN":
                    elem = Integer.parseInt(parse[1]);
                    if (heap.isEmpty() || !heap.contains(elem))
                        OUT.println("percolate down 0");
                    else {
                        OUT.println("percolate down " + heap.getPercolateDownOf(elem));
                    }
                    break;

                case "NUM_ELEMENT":
                    OUT.println("element " + heap.size());
                    break;
            }

        }

        OUT.flush();
    }

}


/**
 * implementasi binary heap
 */
class BinaryHeap {

    private static final int DEFAULT_SIZE = 10000;
    private int size;
    private Node[] arr;

    /**
     * class berisi elemen yang akan disimpan pada heap
     * menghitung jumlah operasi percolate up dan down yang terjadi pada elemen elem
     */
    static class Node implements Comparable<Node> {
        int elem;
        int up;
        int down;

        public Node(Integer elem) {
            this.elem = elem;
        }

        @Override
        public int compareTo(Node o) {
            return elem - o.elem;
        }

        @Override
        public String toString() {
            return elem + "";
        }
    }

    public BinaryHeap() {
        size = 0;
        arr = new Node[DEFAULT_SIZE + 1];
    }

    /**
     * check apakah heap kosong apa tidak
     *
     * @return boolean
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * return banyak item pada heap
     *
     * @return int
     */
    public int size() {
        return size;
    }

    /**
     * compare elemen dari dua node
     *
     * @param e1 node1
     * @param e2 node2
     * @return int
     */
    private int compare(Node e1, Node e2) {
        return e1.compareTo(e2);
    }

    /**
     * doubling array jika array penuh
     */
    public void doubleArray() {
        Node[] newArr = new Node[this.arr.length + DEFAULT_SIZE];
        System.arraycopy(arr, 0, newArr, 0, this.arr.length);
        this.arr = newArr;
    }

    /**
     * melihat item pertama heap
     *
     * @return
     */
    public int peek() {
        return arr[1].elem;
    }

    /**
     * menambahkan e ke dalam heap
     *
     * @param e int
     * @return boolean true jika berhasil ditambahkan
     */
    public boolean add(int e) {
        if (size + 1 == arr.length) doubleArray();

        int hole;
        Node ee = new Node(e);
        arr[0] = ee;

        // percolate up pada heap
        for (hole = ++size; compare(ee, arr[hole / 2]) < 0; hole /= 2) {
            arr[hole] = arr[hole / 2];
            ee.up++;
        }

        arr[hole] = ee;
        return true;
    }

    /**
     * mengeluarkan item pertama dari queue
     *
     * @return int item
     */
    public int poll() {
        if (isEmpty()) return -1;

        int minItem = peek();
        arr[1] = arr[size--];

        percolateDown(1);
        return minItem;
    }

    /**
     * percolate down pada heap
     *
     * percolate down menjadi helper dari poll yang remove item dari queue
     * dengan meletakan last ke root (di poll).
     * hole di percolated down melalui child minimum hingga item bisa di
     * letakan tanpa melanggar sifat urutan di heap.
     *
     * worst/avg: O(log(N))
     *
     * @param hole
     */
    private void percolateDown(int hole) {

        int child;
        Node tmp = arr[hole];

        int tmpHole;

        for (tmpHole = hole; tmpHole * 2 <= size; tmpHole = child) {
            child = tmpHole * 2;
            tmp.down++;
            if (child != size && compare(arr[child + 1], arr[child]) < 0)
                child++;

            if (compare(arr[child], tmp) < 0)
                arr[tmpHole] = arr[child];
            else
                break;
        }

        arr[tmpHole] = tmp;
    }

    /**
     * mengambil jumlah perlocate down yang pernah dilakukan dari int yang diminta
     * implementasi menggunakan linear search
     *
     * @param e int target
     * @return int jumlah
     */
    public int getPercolateDownOf(int e) {
        for (int i = 0; i < size; ++i)
            if (e == arr[i].elem)
                return arr[i].down;
        return -1;
    }

    /**
     * mengambil jumlah perlocate up yang pernah dilakukan dari int yang diminta
     * implementasi menggunakan linear search
     * 
     * @param e int target
     * @return int jumlah
     */
    public int getPercolateUpOf(int e) {
        for (int i = 0; i < size; ++i)
            if (e == arr[i].elem)
                return arr[i].up;
        return -1;
    }

    /**
     * mengecek apakah e sudah ada dalam heap
     * implementasi search menggunakan linear search 
     *
     * @param e int target
     * @return boolean true jika sudah ada dalam heap, false sebaliknya
     */
    public boolean contains(int e) {
        for (int i = 0; i < size; ++i)
            if (arr[i].elem == e)
                return true;

        return false;
    }

    public String toString() {
        return Arrays.toString(arr);
    }
}