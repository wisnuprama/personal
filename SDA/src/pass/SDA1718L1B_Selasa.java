package pass;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by wisnuprama on 9/5/2017.
 */
public class SDA1718L1B_Selasa {

    public static void main(String args[]){
        String input = null;
        BufferedReader br;
        int max = 0;
        int min = 0;
        int temp;

        try {
            br = new BufferedReader(new InputStreamReader(System.in));
            int n = Integer.parseInt(
                    br.readLine()
            );
            for(int i=0; i < n; ++i) {
                temp = Integer.parseInt(
                        br.readLine()
                );

                if (i == 0) {
                    min = max = temp;
                }

                max = Math.max(max, temp);
                min = Math.min(min, temp);
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            System.out.println(max - min);
        }
    }
}