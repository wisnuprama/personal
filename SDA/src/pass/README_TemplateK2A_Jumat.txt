Terdapat 3 versi template:
V1: Terdapat sebuah class terpisah untuk menghandle setiap command
V2: Handling command dilakukan di method terpisah dalam Main Class
V3: Handling command dilakukan di dalam blok IF setiap command

Silahkan menggunakan template yang anda suka. Anda diperbolehkan mengubah isi template seperlunya. Penggunaan template bersifat tidak wajib.