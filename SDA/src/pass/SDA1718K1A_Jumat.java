package pass;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Created by wisnuprama on 9/22/2017.
 */
public class SDA1718K1A_Jumat {

    static ArrayList<Integer> rakMakanan = new ArrayList<>();
    static HashMap<String, Integer> pelanggan = new HashMap<>();

    public static void main(String args[]) throws IOException {

        int N, J;
        String inputs[] = null;
        String in, nama;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        J = Integer.parseInt(br.readLine());

        StringTokenizer st = new StringTokenizer(br.readLine());
        while(st.hasMoreTokens()) {
            rakMakanan.add(Integer.parseInt(st.nextToken()));
        }

        boolean FLAG = false;

        while(--J >= 0) {
            inputs = br.readLine().split(" ");
            nama = inputs[0];
            N = Integer.parseInt(inputs[1]);

            if(FLAG) {
                N = getTable(N);
            } else {
                FLAG = true;
            }

            pelanggan.put(nama, N);
        }

        System.out.println(pelanggan);

        while((in = br.readLine())!=null && in.length()!=0){
            inputs = in.split(" ");

            if("MAKAN".equalsIgnoreCase(inputs[1])) {
                nama = inputs[0];

            }
        }
    }

    static int getTable(int p) {
        boolean didudukin = pelanggan.values().contains(p);

        if(!didudukin && rakMakanan.get(p) == 1) {
            return p;
        }

        boolean didudukinKanan = true;
        boolean diddudukinKiri = false;
        int countKiri = 0;
        int countkKanan = 0;

        int indexkeKiri = p - 1;
        while(diddudukinKiri) {

            if(indexkeKiri == -1) indexkeKiri = rakMakanan.size() -1;

            if(pelanggan.values().contains(indexkeKiri)) {
                countKiri++;
                indexkeKiri--;
            } else diddudukinKiri = false;
        }

        int indexkeKanan = p + 1;
        while(didudukinKanan) {

            if(indexkeKanan == rakMakanan.size()) indexkeKanan = 0;

            if(pelanggan.values().contains(indexkeKanan)) {
                countkKanan++;
                indexkeKanan++;
            } else didudukinKanan = true;
        }

        p = indexkeKiri <= indexkeKanan ? indexkeKiri : indexkeKanan;
        return p;
    }
}
