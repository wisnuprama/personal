package pass;

import java.io.*;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created by wisnuprama on 9/22/2017.
 */
public class SDA1718K1B_Senin {

    static final TreeMap<String, String> maps = new TreeMap<>();

    public static void main(String args[]) throws IOException {

        int N;
        String inputs[] = null;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        N = Integer.parseInt(br.readLine());

        while(--N >= 0) {
            inputs = br.readLine().split(" ");
            boolean FLAG = true;

            for(Map.Entry entry : maps.entrySet()) {
                if(entry.getValue().equals(inputs[0])) {

                    entry.setValue(inputs[1]);
                    FLAG = false;
                    break;
                }
            }

            if(FLAG) maps.put(inputs[0], inputs[1]);
        }

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        bw.write(Integer.toString(maps.size())+"\n");
        bw.flush();
        for(Map.Entry entry : maps.entrySet()) {
            bw.write(entry.getKey() + " " + entry.getValue() + "\n");
            bw.flush();
        }

        br.close();
        bw.close();
    }
}
