package pass;

import java.io.*;
import java.util.*;

/**
 * Created by wisnuprama on 27/09/17.
 */
public class SDA1606918055K1ARemedial {

    public static void main(String args[]) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        // data structure
        List<Lokasi> locs = new ArrayList<>();
        Map<String, Lokasi> mapping = new HashMap<>();

        int n = Integer.parseInt(br.readLine());
        String inputs[] = null;

        while(--n >= 0) {
            inputs = br.readLine().split(" ");

            if(mapping.containsKey(inputs[0])) {
                Lokasi l = mapping.get(inputs[0]);
                l.jumlah = Integer.parseInt(inputs[1]);
            } else {
                Lokasi l = new Lokasi(inputs[0], Integer.parseInt(inputs[1]));
                mapping.put(inputs[0], l);
                locs.add(l);
            }
        }

        Collections.sort(locs);
        int count = 0;
        for(Lokasi l : locs) {
            bw.write(l.toString());
            bw.flush();
            count += l.jumlah;
        }

        bw.write("Total sandera yang diselamatkan: " + count);
        bw.flush();

        br.close();
        bw.close();
    }
}


class Lokasi implements Comparable<Lokasi> {
    String nama;
    int jumlah;

    public Lokasi(String nama, int jumlah) {
        this.nama = nama;
        this.jumlah = jumlah;
    }

    @Override
    public int compareTo(Lokasi lokasi) {
        if(jumlah == lokasi.jumlah)
            return nama.compareTo(lokasi.nama);

        return this.jumlah < lokasi.jumlah ? 1 : -1;
    }

    @Override
    public String toString() {
        return nama + " " + jumlah + "\n";
    }
}