package pass;

import java.io.*;

/*
    Nama: Wisnu Pramadhitya Ramadhan 1606918055
    Tanggal: 8 September 2017
    Soal: A - Ange’s Secret Message
 */
public class SDA1718L1A_Jumat {

    private static final int mA = 65;
    private static final int mZ = 90;
    private static final int mAa = 97;
    private static final int mZz = 122;

    public static void main(String args[]){

        BufferedReader br = null;
        BufferedWriter bw = null;
        StringBuilder input = new StringBuilder();
        int k;
        String out = "";

        try {
            br = new BufferedReader(new InputStreamReader(System.in));

            k = Integer.parseInt(br.readLine());

            String in = null;

            // input text
            while((in = br.readLine()) != null && in.length() != 0){
                input.append(in).append('\n');
            }

            // process the text
            out = decrypt(k, input.toString().toCharArray());

            // output
            bw = new BufferedWriter(new OutputStreamWriter(System.out));
            bw.write(out);
            bw.flush();

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Untuk mendekrip teks yang diberikan
     * @param code kode yang digunakan untuk memecahkan pesan
     * @param arr text yang diberikan dalam bentuk array of char
     * @return String yang sudah hasil dekripsi
     */
    private static String decrypt(int code, char[] arr) {

        for(int i=0; i < arr.length; ++i){
            // @c: char di index i
            char c = arr[i];

            // char di cek apakah alpabhet
            if(mA <= c && c <= mZ){
                // @n normalisasi ke 0 dengan mengurangkan dengan A/a
                int n = c - mA;
                // @c: hasil normalisasi dikurangi dengan kode dan
                // di modulo dengan 26 yaitu jumlah alphabet
                // lalu di normalisasi kembali ke bentuk awal sehingga mendapatkan char hasil shift.
                c = (char) ((mod((n - code)) + mA));

            // untuk alphabet kecil
            } else if(mAa <= c && c <= mZz) {
                int n = c - mAa;
                c = (char) (mod((n - code)) + mAa);
            }

            arr[i] = c; // change at index i
        }

        return new String(arr);
    }

    /**
     * Untuk modulo sehingga tidak negatif
     * @param code kode untuk shift
     * @return int hasil mod
     */
    private static int mod(int code){
        return ((code % 26) + 26) % 26;
    }
}
