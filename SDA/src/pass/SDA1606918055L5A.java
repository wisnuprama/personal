//package pass;
//
//import java.io.InputStreamReader;
//import java.util.*;
//import java.io.BufferedReader;
//import java.io.OutputStreamWriter;
//import java.io.PrintWriter;
//
///**
// * Lab5A
// * Kelas Binary Search Tree
// * Mahasiswa tidak diwajibkan menggunakan template ini, namun sangat disarankan menggunakan template ini
// * Pada template ini, diasumsikan kelas yang ingin dipakai mengimplementasikan (implements) interface Comparable
// * NOTE : Tidak semua method yang dibutuhkan sudah disediakan templatenya pada kelas ini sehingga mahasiswa harus menambahkan sendiri method yang dianggap perlu
// * @author Jahns Christian Albert
// * @author Wisnu Pramadhitya Ramadhan 1606918055
// *
//*/
//public class SDA1606918055L5A {
//
//    static final String NOT_FOUND = "Tidak ada elemen pada tree";
//
//	public static void main(String args[]) throws Exception {
//		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//		PrintWriter print = new PrintWriter(new OutputStreamWriter(System.out));
//		BSTree<String> bsTree = new BSTree<>();
//
//		String input = null;
//
//		while((input = br.readLine()) != null && input.length() > 0){
//            String[] in = input.split(";");
//            String elem = null;
//            boolean res;
//
//            switch (in[0]) {
//
//                case "ADD":
//                    elem = in[1];
//                    res = bsTree.add(elem);
//
//                    // result
//                    if(res)
//                        print.println(elem+" berhasil ditambahkan ke dalam tree");
//                    else
//                        print.println(elem+" sudah dimasukkan sebelumnya");
//
//                    break;
//
//                case "REMOVE":
//                    elem = in[1];
//                    res = bsTree.remove(elem);
//
//                    if(res)
//                        print.println(elem+" berhasil dihapus dari tree");
//                    else
//                        print.println(elem+" tidak ditemukan");
//
//                    break;
//
//                case "CONTAINS":
//                    elem = in[1];
//                    res = bsTree.contains(elem);
//
//                    if(res)
//                        print.println(elem+" terdapat pada tree");
//                    else
//                        print.println(elem+" tidak terdapat pada tree");
//
//                    break;
//
//                case "MAX":
//
//                    String max = null;
//                    if(bsTree.isEmpty() || (max=bsTree.max()) == null)
//                        print.println(NOT_FOUND);
//                    else
//                        print.println(max+" merupakan elemen dengan nilai tertinggi");
//
//                    break;
//
//                case "MIN":
//                    String min = null;
//                    if(bsTree.isEmpty() || (min=bsTree.min()) == null)
//                        print.println(NOT_FOUND);
//                    else
//                        print.println(min+" merupakan elemen dengan nilai terendah");
//
//                    break;
//
//                case "PREORDER":
//
//                    if(bsTree.isEmpty())
//                        print.println(NOT_FOUND);
//                    else
//                        print.println(join(
//                                bsTree.preOrder()));
//
//                    break;
//
//                case "POSTORDER":
//                    if(bsTree.isEmpty())
//                        print.println(NOT_FOUND);
//                    else
//                        print.println(join(
//                                bsTree.postOrder()
//                        ));
//
//                    break;
//
//                case "ASCENDING":
//                    if(bsTree.isEmpty())
//                        print.println(NOT_FOUND);
//                    else
//                        print.println(join(
//                                bsTree.inOrderAscending()
//                        ));
//
//                    break;
//
//                case "DESCENDING":
//                    if(bsTree.isEmpty())
//                        print.println(NOT_FOUND);
//                    else
//                        print.println(join(
//                                bsTree.inOrderDescending()
//                        ));
//                        break;
//
//                case "PRINT":
//                    bsTree.printLevelOrder();
//            }
//        }
//        print.flush();
//	}
//
//	public static String join(List<String> list){
//	    int len = list.size();
//	    StringBuilder str = new StringBuilder();
//	    for(int i=0; i < len; ++i){
//	        String s = list.get(i);
//	        if(i != len-1)
//	            s+= ';';
//	        str.append(s);
//        }
//	    return str.toString();
//    }
//}
//
//
//class BSTree<E extends Comparable<E>> {
//	/**
//	  *
//	  * Kelas yang merepresentasikan node pada tree
//	  * @author Jahns Christian Albert
//	  *
//	*/
//	private static class Node<E extends Comparable<E>> {
//
//		E elem;
//		Node<E> left;
//		Node<E> right;
//		Node<E> parent;
//
//		/**
//		 *
//		 * @param elem
//		 * @param left
//		 * @param right
//		 * @param parent
//		 */
//		public Node(E elem, Node<E> left, Node<E> right, Node<E> parent){
//			this.elem = elem;
//			this.left = left;
//			this.right = right;
//			this.parent = parent;
//		}
//
//        @Override
//        public boolean equals(Object o) {
//            if (this == o) return true;
//            if (o == null || getClass() != o.getClass()) return false;
//
//            Node<?> node = (Node<?>) o;
//
//            return elem != null ? elem.equals(node.elem) : node.elem == null;
//        }
//
//        @Override
//        public int hashCode() {
//            return elem != null ? elem.hashCode() : 0;
//        }
//    }
//
//	private Node<E> root;
//	private int size;
//	/**
//	  * Constructor Kelas Binary Search Tree
//	*/
//	public BSTree(){
//		root = null;
//		size = 0;
//	}
//
//	/**
//	  *
//	  * Mengetahui apakah tree kosong atau tidak
//	  * @return true jika kosong, false jika sebaliknya
//	  *
//	*/
//	public boolean isEmpty(){
//		return root == null;
//	}
//
//	/**
//	  *
//	  * Menambahkan objek ke dalam tree
//	  * @param elem yang ingin ditambahkan
//	  * @return true jika elemen berhasil ditambahkan, false jika elemen sudah terdapat pada tree
//	  *
//	*/
//	public boolean add(E elem){
//
//		boolean res = false;
//
//		if(root == null){
//		    root = new Node<E>(elem, null, null, null);
//            res = true;
//
//		} else {
//
//			Node<E> parent = null;
//			Node<E> current = root;
//			while(current != null){
//
//				E currElem = current.elem;
//                parent = current;
//				if(elem.compareTo(currElem) < 0){
//					// traverse to the left
//				    current = current.left;
//					if(current == null) // add
//                        parent.left = new Node<E>(elem, null, null, parent);
//
//				} else if(elem.compareTo(currElem) > 0){
//					// traverse to the right
//				    current = current.right;
//					if(current == null) // add
//					    parent.right = new Node<E>(elem, null, null, parent);
//
//				} else {
//					// Ketika elemen sama tidak dimasukan
//                    return res;
//				}
//
//			}
//			res = true;
//            this.size++;
//		}
//
//		return res;
//	}
//
//	/**
//	  *
//	  * Mendapatkan node dengan elemen tertentu
//	  * @param elem yang ingin dicari nodenya
//	  * @return node dari elemen pada parameter, null jika tidak ditemukan
//	  *
//	*/
//	private Node<E> find(E elem){
//
//		Node<E> res = null;
//
//		if(root != null){
//
//			Node<E> current = root;
//			boolean found = false;
//			while(!found && current != null){
//
//				E currElem = current.elem;
//				if(elem.compareTo(currElem) < 0){ // traverse to the left
//			        current = current.left;
//
//				} else if(elem.compareTo(currElem) > 0){ // traverse to the right
//					current = current.right;
//
//				} else {
//
//				    // found it
//					res = current;
//					found = true;
//				}
//			}
//		}
//
//		return res;
//	}
//
//	/**
//	 *
//	 * Menghapus objek dari tree, menggunakan successor inorder untuk menghapus elemen yang memiliki left node dan right node
//	 * Manfaatkan method minNode(Node<E> node) untuk mencari successor inorder
//	 * @param elem yang ingin dihapus
//	 * @return true jika elemen ditemukan dan berhasil dihapus, false jika elemen tidak ditemukan
//	 *
//	*/
//	public boolean remove(E elem){
//
//	    boolean res = false;
//	    Node<E> node = find(elem);
//
//	    if(node != null){
//
//	        if(node.left == null){
//	            if(node.right == null){
//	                if(root == node){
//	                    root = null;
//
//	                }
//	                else if(node.elem.compareTo(node.parent.elem) < 0)
//	                    node.parent.left = null;
//	                else
//	                    node.parent.right = null;
//
//	            } else {
//	                if(root == node){
//	                    root = node.right;
//	                    root.parent = null;
//
//                    } else if(node.elem.compareTo(node.parent.elem) < 0){
//	                    node.parent.left = node.right;
//	                    node.right.parent = node.parent;
//
//                    } else {
//                        node.parent.right = node.right;
//                        node.right.parent = node.parent;
//                    }
//	            }
//
//            } else {
//
//                if(node.right == null){
//
//                    if(root == node){
//                        root = node.left;
//                        root.parent = null;
//
//                    } else if(node.elem.compareTo(node.parent.elem) < 0){
//                        node.parent.left = node.left;
//                        node.left.parent = node.parent;
//
//                    } else {
//                        node.parent.right = node.left;
//                        node.left.parent = node.parent;
//
//                    }
//
//                } else {
//
//                    Node<E> successor = minNode(node.right);
//                    E tempElem = successor.elem;
//
//                    if(root == node){
//                        if(successor.right == null){
//                            if(successor.elem.compareTo(successor.parent.elem) < 0)
//                                successor.parent.left = null;
//                            else
//                                successor.parent.right = null;
//
//                        } else {
//                            if(successor.elem.compareTo(successor.parent.elem) < 0){
//                                successor.parent.left = successor.right;
//                                successor.right.parent = successor.parent;
//
//                            } else {
//                                successor.parent.right = successor.right;
//                                successor.right.parent = successor.parent;
//
//                            }
//
//                        }
//
//                        root.elem = tempElem;
//
//                    } else {
//                        if(successor.right == null){
//                            if(successor.elem.compareTo(successor.parent.elem) < 0)
//                                successor.parent.left = null;
//                            else
//                                successor.parent.right = null;
//
//                        } else {
//                            if(successor.elem.compareTo(successor.parent.elem) < 0)
//                                successor.parent.left = null;
//                            else {
//                                successor.parent.right = successor.right;
//                                successor.right.parent = successor.parent;
//
//                            }
//                        }
//
//                        node.elem = tempElem;
//                    }
//
//                }
//
//            }
//
//            res = true;
//        }
//
//        return res;
//	}
//
////	private Node<E> removeHelper(Node<E> current, E elem) {
////
////	    if(current == null) return current;
////
////	    if(elem.compareTo(current.elem) < 0)
////	        current.left = removeHelper(current.left, elem);
////	    else if(elem.compareTo(current.elem) > 0)
////	        current.right = removeHelper(current.right, elem);
////	    else {
////	        this.size--;
////
////	        if(current.left == null) return current.right;
////	        else if(current.righ t == null) return current.left;
////
////	        current.elem = this.minNode(current.right).elem;
////	        current.right = removeHelper(current.right, current.elem);
////        }
////
////        return current;
////    }
//
//	/**
//	 *
//	 * Mencari elemen dengan nilai paling kecil pada tree
//	 * @return elemen dengan nilai paling kecil pada tree
//	 *
//	*/
//	public E min(){
//		return minNode(root).elem;
//	}
//
//	/**
//	  *
//	  * Method untuk mengembalikan node dengan elemen terkecil pada suatu subtree
//	  * Hint : Manfaatkan struktur dari binary search tree
//	  * @param node root dari subtree yang ingin dicari elemen terbesarnya
//	  * @return node dengan elemen terkecil dari subtree yang diinginkan
//	  *
//	*/
//	private Node<E> minNode(Node<E> node){
//
//		Node<E> res = null;
//		if(node != null){
//
//			Node<E> current = node;
//            // traverse to the left until current = null
//			while(current != null){
//			    res = current;
//			    current = current.left;
//            }
//		}
//
//		return res;
//	}
//
//	/**
//	 *
//	 * Mencari elemen dengan nilai paling besar pada tree
//	 * @return elemen dengan nilai paling besar pada tree
//	 *
//	*/
//	public E max(){
//		return maxNode(root).elem;
//	}
//
//	/**
//	  *
//	  * Method untuk mengembalikan node dengan elemen terbesar pada suatu subtree
//	  * Hint : Manfaatkan struktur dari binary search tree
//	  * @param node root dari subtree yang ingin dicari elemen terbesarnya
//	  * @return node dengan elemen terbesar dari subtree yang diinginkan
//	  *
//	*/
//	private Node<E> maxNode(Node<E> node){
//
//		Node<E> res = null;
//		if(node != null){
//            // traverse to the right until current =  null
//			Node<E> current = node;
//			while(current != null){
//			    res = current;
//			    current = current.right;
//            }
//
//		}
//
//		return res;
//	}
//
//	/**
//	  *
//	  * Mengetahui apakah sebuah objek sudah terdapat pada tree
//	  * Asumsikan jika elem.compareTo(otherElem) == 0, maka elem dan otherElem merupakan objek yang sama
//	  * Hint : Manfaatkan method find
//	  * @param elem yang ingin diketahui keberadaannya dalam tree
//	  * @return true jika elemen ditemukan, false jika sebaliknya
//	  *
//	*/
//	public boolean contains(E elem){
//	    Node<E> res = this.find(elem);
//		return res != null;
//	}
//
//	/**
//	  * Mengembalikan tree dalam bentuk pre-order
//	  * @return tree dalam bentuk pre-order sebagai list of E
//	*/
//	public List<E> preOrder(){
//
//		List<E> list = new ArrayList<>(this.size); // default menggunakan LinkedList, silahkan menggunakan List yang sesuai dengan Anda
//		preOrder(root,list);
//		return list;
//
//	}
//
//	/**
//	  *
//	  * Method helper dari preOrder()
//	  * @param node pointer
//	  * @param list sebagai akumulator
//	  * @return kumpulan elemen dari subtree yang rootnya adalah node parameter dengan urutan pre-order
//	  *
//	*/
//	private void preOrder(Node<E> node, List<E> list){
//
//		if(node == null) return;
//
//		list.add(node.elem);
//		preOrder(node.left, list);
//		preOrder(node.right, list);
//	}
//
//	/**
//	  * Mengembalikan tree dalam bentuk post-order
//	  * @return tree dalam bentuk post-order sebagai list of E
//	*/
//	public List<E> postOrder(){
//
//		List<E> list = new ArrayList<E>(this.size); // default menggunakan LinkedList, silahkan menggunakan List yang sesuai dengan Anda
//		postOrder(root,list);
//		return list;
//
//	}
//
//	/**
//	  *
//	  * Method helper dari postOrder()
//	  * @param node pointer
//	  * @param list sebagai akumulator
//	  * @return kumpulan elemen dari subtree yang rootnya adalah node parameter dengan urutan post-order
//	  *
//	*/
//	private void postOrder(Node<E> node, List<E> list){
//
//	    if(node == null) return;
//
//	    postOrder(node.left, list);
//	    postOrder(node.right, list);
//	    list.add(node.elem);
//
//	}
//
//
//	/**
//	  * Mengembalikan tree dalam bentuk in-order secara ascending
//	  * @return tree dalam bentuk in-order secara ascending sebagai list of E
//	*/
//	public List<E> inOrderAscending(){
//
//		List<E> list = new ArrayList<E>(size);
//        inOrderAscending(root,list);
//        return list;
//	}
//
//	/**
//	  *
//	  * Method helper dari inOrderAscending()
//	  * @param node pointer
//	  * @param list sebagai akumulator
//	  * @return kumpulan elemen dari subtree yang rootnya adalah node parameter dengan urutan in-order secara ascending
//	  *
//	*/
//	private void inOrderAscending(Node<E> node, List<E> list){
//	    if(node == null)
//	        return;
//
//	    inOrderAscending(node.left, list);
//	    list.add(node.elem);
//	    inOrderAscending(node.right, list);
//	}
//
//
//	/**
//	  * Mengembalikan tree dalam bentuk in-order secara descending
//	  * @return tree dalam bentuk in-order secara descending sebagai list of E
//	*/
//	public List<E> inOrderDescending(){
//
//        List<E> list = new ArrayList<E>(size);
//	    inOrderDescending(root,list);
//        return list;
//	}
//
//	/**
//	  *
//	  * Method helper dari inOrderDescending()
//	  * @param node pointer
//	  * @param list sebagai akumulator
//	  * @return kumpulan elemen dari subtree yang rootnya adalah node parameter dengan urutan in-order descending
//	  *
//	*/
//	private void inOrderDescending(Node<E> node, List<E> list) {
//
//        if (node == null)
//            return;
//
//        inOrderDescending(node.right, list);
//        list.add(node.elem);
//        inOrderDescending(node.left, list);
//    }
//
//    public void printLevelOrder(){
//	    Queue<Node> queue = new ArrayDeque<>();
//        Node<E> current = root;
//
//        queue.add(current);
//        while(!queue.isEmpty()){
//            Node<E> tmp = queue.poll();
//            System.out.print(tmp.elem + ";");
//
//            if(tmp.left!=null) queue.add(tmp.left);
//            if(tmp.right!=null) queue.add(tmp.right);
//        }
//    }
//}