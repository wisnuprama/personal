package pass;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.*;

/**
 * @author Wisnu Pramadhitya Ramadhan
 * 1606918055
 * <p>
 * Implementassi fungsi hashtable dan hashfunction
 */

public class SDA1606918055L6B {

    static final BufferedReader IN = new BufferedReader(new InputStreamReader(System.in));
    static final PrintWriter OUT = new PrintWriter(new OutputStreamWriter(System.out));

    /**
     * Class Peminjaman yang menyimpan nama buku dan daftar peminjam
     */
    static class Peminjaman {
        List<String> peminjam;
        String buku;

        public Peminjaman(String buku) {
            this.buku = buku;
            this.peminjam = new LinkedList<>();
        }

        /**
         * Tambahkan nama peminjam ke daftar
         * @param nama String nama peminjam
         * @return boolean
         */
        boolean addPeminjam(String nama) {
            return this.peminjam.add(nama);
        }

        /**
         * Menghapus nama dari daftar
         * @param nama String nama peminjam
         * @return boolean
         */
        boolean removePeminjam(String nama) {
            return this.peminjam.remove(nama);
        }

        /**
         * Print daftar peminjam berdasarkan leksikografis
         */
        void daftarPeminjam() {
            Collections.sort(this.peminjam);

            Iterator<String> iter = this.peminjam.listIterator();
            int size = this.peminjam.size();

            for (int i = 0; i < size - 1 && iter.hasNext(); ++i)
                OUT.print(iter.next() + " ");

            OUT.println(iter.next());
        }

    }

    /**
     * class Node berisi list yang buku yang collide akibat hashcode yang sama
     * @param <E>
     */
    static class Node<E extends Peminjaman> {
        List<E> collide;

        public Node() {
            this.collide = new ArrayList<>();
        }

        /**
         * tambahkan Peminjaman
         * @param elem
         */
        public void add(E elem) {
            this.collide.add(elem);
        }

        /**
         * Ambil peminjaman berdasarkan nama buku
         * @param buku
         * @return
         */
        public E get(String buku) {
            for (E e : this.collide)
                if (e.buku.equals(buku))
                    return e;

            return null;
        }

        @Override
        public String toString() {
            return collide.toString();
        }
    }

    public static void main(String args[]) throws Exception {

        Node<Peminjaman> peminjaman[] = new Node[100005];
        String input = null;
        int bookhash;
        String nama, buku;
        while ((input = IN.readLine()) != null && 0 < input.length()) {

            String parse[] = input.split(" ");

            switch (parse[0]) {

                case "PINJAM":
                    nama = parse[1];
                    buku = parse[2];

                    bookhash = hashFunction(buku);
                    Node<Peminjaman> pmj = peminjaman[bookhash];

                    // jika pada index hash tersebut belum ada node
                    if (pmj == null)
                        peminjaman[bookhash] = pmj = new Node<>();


                    Peminjaman p = pmj.get(buku);
                    // check apakah peminjaman pada node tersebut sudah ada
                    if(p == null) {
                        p = new Peminjaman(buku);
                        pmj.add(p);
                    }

                    p.addPeminjam(nama);
                    break;

                case "DAFTAR_PEMINJAM":
                    buku = parse[1];

                    bookhash = hashFunction(buku);
                    peminjaman[bookhash].get(buku).daftarPeminjam();
                    break;

                case "KEMBALI":
                    nama = parse[1];
                    buku = parse[2];

                    bookhash = hashFunction(buku);
                    peminjaman[bookhash].get(buku).removePeminjam(nama);
                    break;
            }
        }

        OUT.flush();
    }


    private static final int TABLE_SIZE = 1009;

    /**
     * Mengembalikan int kode hash dari String yang diberikan
     * @param s String
     * @return int hashcode
     */
    public static int hashFunction(String s) {

        int hash = 0;

        for (int i = 0; i < s.length(); ++i) {
            hash += 31 * hash + (s.charAt(i) - 'a') * (i+1);
        }

        hash %= TABLE_SIZE;

        if (hash < 0)
            hash += TABLE_SIZE;

        return hash;
    }

}