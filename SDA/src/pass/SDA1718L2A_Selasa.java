//package pass;
//
//import java.io.*;
//import java.util.*;
//import java.lang.*;
//
///**
// * Created by wisnuprama on 9/14/2017.
// */
//public class SDA1718L2A_Selasa {
//
//	static final String GO_TO = "GO_TO";
//	static final String BACK = "BACK";
//	static final String FORWARD = "FORWARD";
//	static final String CHECK_HISTORY = "CHECK_HISTORY";
//	static final String DEFAULT = "default";
//
//    public static void main(String args[]) throws IOException {
//
//        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//        String input = null;
//
//        final StringBuilder str = new StringBuilder();
//        final LinkedList<String> history = new LinkedList<>();
//    	final Stack<String> historyStack = new Stack<>();
//
//    	String pop = null;
//    	String peek = null;
//    	historyStack.push(DEFAULT);
//
//    	ArrayList<String> strings = new ArrayList<>();
//        while((input = br.readLine()) != null && input.length()!=0) {
//            strings.add(input);
//        }
//
//        for(String in : strings) {
//            String lstInput[] = in.split(" ");
//
//            String command = lstInput[0];
//            String link = null;
//
//            switch(command) {
//                case GO_TO:
//                    link = lstInput[1];
//                    historyStack.push(link);
//                    history.add(link);
//                    if(!forwardStack.empty())
//                        forwardStack = new Stack<>();
//
//                    str.append(link + "\n");
//                    break;
//                case BACK:
//                    if(checkBack(historyStack)){
//                        pop = historyStack.pop();
//                        forwardStack.push(pop);
//
//                        peek = historyStack.peek();
//                        str.append(peek + "\n");
//                        history.add(peek);
//
//                    } else {
//                        str.append("Tidak bisa melakukan operasi BACK\n");
//                    }
//                    break;
//                case FORWARD:
//                    if(forwardStack.empty()) {
//                        str.append("Tidak bisa melakukan operasi FORWARD\n");
//                    } else {
//                        pop = forwardStack.pop();
//                        historyStack.push(pop);
//                        str.append(pop + "\n");
//                        history.add(pop);
//                    }
//
//                    break;
//                case CHECK_HISTORY:
//                    link = lstInput[1];
//                    int lastHistory = history.lastIndexOf(link);
//
//                    if(lastHistory==-1) {
//                        str.append("Link belum pernah dikunjungi\n");
//                    } else {
//                        str.append("Link terakhir kali dikunjungi pada urutan ke "+ (lastHistory+1) +"\n");
//                    }
//                    break;
//            }
//        }
//
//        System.out.println(str.toString());
//
//    }
//
//    public static boolean checkBack(Stack<String> st) {
//    	Stack<String> tmp = new Stack<>();
//    	try {
//
//    		if(st.empty()){
//    			return false;
//			}
//
//    		tmp.push(st.pop());
//			tmp.push(st.pop());
//			st.push(tmp.pop());
//			st.push(tmp.pop());
//    		return true;
//
//    	} catch(EmptyStackException e) {
//    		st.push(tmp.pop());
//    		return false;
//    	}
//    }
//}
