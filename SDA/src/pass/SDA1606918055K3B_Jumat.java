package pass;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

public class SDA1606918055K3B_Jumat {

    private static final BufferedReader IN = new BufferedReader(new InputStreamReader(System.in));



    public static void main(String args[]) throws Exception {

        int N = Integer.parseInt(IN.readLine());

        StringTokenizer st = new StringTokenizer(IN.readLine());

        PriorityQueue<Integer> pq = new PriorityQueue<>(N, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2-o1;
            }
        });

        while(st.hasMoreTokens())
            pq.add(Integer.parseInt(st.nextToken()));

        int counter = 0;

        while(pq.size() > 1){

//            pq.poll();
//            pq.poll();
//            System.out.println(pq);
            int n = pq.poll() - pq.poll();
//            System.out.println(pq.poll() + " " + pq.poll());

            if(n != 0)
                pq.add(n);

            counter += n;
        }

        System.out.println(counter);

    }
}
