package pass;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.*;

/**
 * @author Wisnu Pramadhitya Ramadhan
 * TP2 SDA
 * Idenya traversing ke semua berdasarkan urutan menggunakan memoisasi dan rekursi backtrack.
 *
 * Change:
 * pass.HeroSquare using priority queue instead of treeset: karena menggunakan iterator harusnya sama cepatnya
 */
public class SDA1606918055T2 {

    // DATA STRUCTURE
    static Map<String, Hero> heroMap = new HashMap<>();
    static Square MAP[][];
    static TreeSet<Hero> heroOfGudako = null;

    // GUDAKO
    static int maxRow = -1;
    static int maxCol = -1;
    static Square START = null;
    static long levelGudako = 1;
    static long manaGudako = -1;
    static long MAX_MANA = -1;
    static int timeGudako = Integer.MAX_VALUE-10;

    // INFRATRUCTURE
    static PrintWriter pw = new PrintWriter(new OutputStreamWriter(System.out));

    public static void main(String args[]) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer stoken = new StringTokenizer(br.readLine(), " ");
        final int jP, jS, jD, r, c;
        jP = Integer.parseInt(stoken.nextToken());
        jS = Integer.parseInt(stoken.nextToken());
        jD = Integer.parseInt(stoken.nextToken());
        manaGudako = Integer.parseInt(stoken.nextToken());
        MAX_MANA = manaGudako; // Inisiasi MAX MANA untuk regain
        r = Integer.parseInt(stoken.nextToken());
        c = Integer.parseInt(stoken.nextToken());

        MAP = new Square[r][c];
        maxRow = r-1;
        maxCol = c-1;
        heroOfGudako = new TreeSet<>();

        // CREATE HERO
        for(int i=0; i<jP; ++i){
            stoken = new StringTokenizer(br.readLine(), ";");
            String heroName = stoken.nextToken();
            heroMap.put(heroName, new Hero(
                    heroName,
                    1,
                    Long.parseLong(stoken.nextToken()),
                    Long.parseLong(stoken.nextToken()),
                    stoken.nextToken()
                )
            );
        }

        // HERO
        for(int i=0; i<jS; ++i) {
            stoken = new StringTokenizer(br.readLine(), ";");

            int yr = Integer.parseInt(stoken.nextToken())-1;
            int xc = Integer.parseInt(stoken.nextToken())-1;

            // ARRANGE HERO FROM MAP
            TreeSet<Hero> qHero = new TreeSet<>();
            StringTokenizer stNames = new StringTokenizer(stoken.nextToken(), ",");

            // put hero in set of hero for this square
            while(stNames.hasMoreTokens()){
                String heroName = stNames.nextToken();
                qHero.add(heroMap.get(heroName));
            }

            // put hero in map
            MAP[yr][xc] = new HeroSquare(yr, xc, Square.HERO, qHero);
        }

        // DUNGEON
        for(int i=0; i<jD; ++i){
            stoken = new StringTokenizer(br.readLine(), ";");

            int yr = Integer.parseInt(stoken.nextToken())-1;
            int xc = Integer.parseInt(stoken.nextToken())-1;

            // put dungeon in map
            MAP[yr][xc] = new DungeonSquare(
                    yr,
                    xc,
                    Square.DUNGEON,
                    Long.parseLong(stoken.nextToken()),
                    Long.parseLong(stoken.nextToken()),
                    stoken.nextToken(),
                    Integer.parseInt(stoken.nextToken())
                    );
        }

        /**
         * Mapping sisanya yang masih null artinya bukan hero atau pun dungeon
         * misal start dan . dan tembok
         */
        String input;
        int yr = 0;
        while((input=br.readLine())!=null && input.length()>0){

            for(int xc=0; xc<input.length(); ++xc){
                char mark = input.charAt(xc);

                if(MAP[yr][xc] == null)
                    MAP[yr][xc] = new Square(yr, xc, mark);

                // tandain start nya
                if(mark == Square.START){
                    START = MAP[yr][xc];
                }
            }

            yr++;
        }

        // MULAI TRAVERSING
        traversing(START.r, START.c);

        // akhir petualangan gudako
        pw.println("Akhir petualangan Gudako");
        pw.println("Level Gudako: " + levelGudako);
        pw.println("Level pahlawan:");

        List<Hero> heroEnd = new ArrayList<>(heroOfGudako); // dari tree ke array
        // sort hero berdasarkan level dan lifetimenya
        Collections.sort(heroEnd, new Comparator<Hero>() {
            @Override
            public int compare(Hero o1, Hero o2) {
                if(o1.level == o2.level)
                    return o1.lifetime < o2.lifetime ? 1 : -1;
                return o1.level < o2.level ? 1 : -1;
            }
        });

        for(Hero hero : heroEnd)
            pw.println(hero.name + ": " + hero.level);

        pw.flush();
    }

    /**
     * Mengunjungi setiap node (pass.Square) yang ada di map
     * Jika sampai di pass.HeroSquare maka akan summoning dan jika berada di pass.DungeonSquare maka akan bertarung atau lari
     * Using DFS
     *
     * worst: O(rc) -> visit all squares
     *
     * @param r titik r yang akan dikunjungi
     * @param c titik c yang akan dikunjungi
     */
    private static void traversing(int r, int c){
        // BASE CASE
        if(r < 0 || c < 0 || r > maxRow || c > maxCol) return;
        if(MAP[r][c].MARK == Square.STONE) return;
        if(MAP[r][c].visited) return; // check memoisasi O(log N)

        // case ketemu hero
        if(MAP[r][c].MARK == Square.HERO) pw.println(summoning( (HeroSquare) MAP[r][c] ));
        // case ketemu dungeon
        else if(MAP[r][c].MARK == Square.DUNGEON) pw.println(dungeon( (DungeonSquare) MAP[r][c] ));

        MAP[r][c].visited = true; // visited
        manaGudako = MAX_MANA; // regain mana after dungeon and hero square

        // backtracking atas kanan bawah kiri
        traversing(r-1, c); // atas
        traversing(r, c+1); // kanan
        traversing(r+1, c); // bawah
        traversing(r, c-1); // kiri
    }


    /**
     * Mengambil hero yang ada di pass.HeroSquare sesuai mana gudako
     * @param heroSquare hero square yang dikunjungi
     * @return String hasil summoning
     */
    static String summoning(HeroSquare heroSquare){

        int r = heroSquare.r, c = heroSquare.c;
        boolean isSummoning = false;
        List<Hero> newFollower = new ArrayList<>(heroSquare.qHero.size());
        /*
            for the perfomance, please check below in the pass.HeroSquare docs
         */
        for(Hero hero : heroSquare.qHero) {
            /*
                cek apakah hero bisa ikut gudako apa tidak
                syaratnya mana gudako harus mencukup untuk membawa hero
             */
            if(manaGudako >= hero.mana) {
                hero.lifetime = timeGudako--;
                newFollower.add(hero);
            }

            // kurangi mana gudako dan cek
            manaGudako -= hero.mana;
            if(manaGudako <= 0) break;
        }

        if(!newFollower.isEmpty())
            isSummoning = true;

        // TO STRING
        StringBuilder sb = new StringBuilder();
        sb.append(r+1).append(",").append(c+1).append(" ");
        if(isSummoning){
            sb.append("Pahlawan yang ikut:");
            for(Hero hr : newFollower)
                sb.append(hr.name).append(",");

            heroOfGudako.addAll(newFollower);

        } else
            sb.append("tidak ada pahlawan yang ikut ");

        return sb.substring(0, sb.length()-1);

    }


    /**
     * Mengunjungi dungeon dan bertarung! jika kekuatan gudako tidak melebihi dungeon maka lari!
     * @param dSq pass.DungeonSquare yang akan dikunjungi
     * @return String hasil pertarungan
     */
    static String dungeon(DungeonSquare dSq){

        /*
            Prioritas Pahlawan yang diajak bertarung adalah pahlawan yang memiliki kekuatan
            di dalam Dungeon tersebut paling tinggi (bukan​ ​kekuatan​ ​asli​ ​namun​ ​kekuatan
            setelah​ ​di​ ​hitung​ ​perbandingan​ ​tipe​ ​senjatanya)​ jika ada 2 atau lebih pahlawan yang
            kekuatannya sama maka diurutkan berdasarkan lama​ ​ikut​ ​dengan​ ​Gudako​. Yang lebih
            awal mengikuti Gudako lebih diprioritaskan daripada yang baru ikut (jika​ ​Gudako
            mengambil​ ​2​ ​pahlawan​ ​atau​ ​lebih​ ​dalam​ ​1​ ​petak​ ​maka​ ​yang​ ​muncul​ ​pertama​ ​di
            output​ ​adalah​ ​yang​ ​di​ ​ambil​ ​pertama)
         */

        int r = dSq.r, c = dSq.c;
        List<Hero> heroBattleList = calculateGudakoPower(dSq.maxHero, dSq.weaponType);
        StringBuilder sb = new StringBuilder();
        sb.append(r+1).append(",").append(c+1);

        // hitung power dari semua hero yang dapat bertarung di dungeon ini
        long gPower = 0;
        for(Hero hrb : heroBattleList)
            gPower += hrb.battlePower;

        // gudako power isn't enough to penetrate dungeon
        if(gPower < dSq.defense) {
            return sb.toString()+" RUN, kekuatan maksimal sejumlah: " + gPower;
        }

        /*
            tambah level gudako dan hero karena menang battlePower
            gudako += banyak hero * level dungeon
            hero += level dungeon
         */
        sb.append(" BATTLE, kekuatan: ").append(gPower).append(", pahlawan: ");
        levelGudako += heroBattleList.size() * dSq.level;

        for(Hero hero : heroBattleList){

            // tambah level hero dengan level dungeon
            hero.level += dSq.level;
            sb.append(hero.name).append(",");
        }

        return sb.substring(0, sb.length()-1);
    }


    /**
     * Hitung Gudako power untuk pertarungan di dungeon
     * @param dungeonMaxHero maximum jumlah hero yang dapat dibawa oleh gudako di dungeon tersebut
     * @param weapon jenis senjata di dungein tersebut
     * @return List hero-hero yang dapat dibawa oleh gudako sesuai persyaratan
     */
    static List<Hero> calculateGudakoPower(int dungeonMaxHero, String weapon){
        /*
            calculate to gpower from gudako's hero
            only dungeonMaxHero heroes can be the hero for this battle
         */

        List<Hero> heroForBattle = new ArrayList<>(dungeonMaxHero);
        for(Hero hero : heroOfGudako) {

            /*
                hitung kekuatan hero sesuai dengan aturan pertarungan di dungeon
                Dungeon\Pahlawan |      Pedang              |    Panah
                ----------------------------------------------------------------
                Pedang           | kekuatan_asli            |  kekuatan_asli * 2
                Panah            | floor(kekuatan_asli / 2) |    kekuatan_asli

             */
            long hpower = 0;
            if(Hero.PEDANG.equals(weapon)){
                // case dungeon pedang
                if(Hero.PEDANG.equals(hero.weapon))
                    hpower += hero.strength;
                else
                    hpower += hero.strength * 2;

            } else {
                // case dungeon panah
                if(Hero.PEDANG.equals(hero.weapon))
                    hpower += floorDiv(hero.strength, 2);
                else
                    hpower += hero.strength;
            }

            hero.battlePower = hpower;
            heroForBattle.add(hero);
        }

        // sort berdasarkan kekuatan di battle ini dan lama ikut gudako
        Collections.sort(heroForBattle, new Comparator<Hero>() {
            @Override
            public int compare(Hero o1, Hero o2) {
                if(o1.battlePower == o2.battlePower)
                    return o2.lifetime > o1.lifetime ? 1 : -1;

                return o2.battlePower > o1.battlePower ? 1 : -1;
            }
        });

        if(dungeonMaxHero > heroForBattle.size())
            return heroForBattle;
        return heroForBattle.subList(0, dungeonMaxHero);
    }

    /**
     * Pembagian floor
     * @param x pembilang
     * @param y penyebut
     * @return
     */
    static long floorDiv(long x, long y) {
        long r = x / y;
        // jika signs nya berbeda dan mod nya tidak nol, maka bulatkan ke bawah
        if ((x ^ y) < 0 && (r * y != x)) r--;

        return r;
    }

    /**
     * Print peta dan menandai x pada posisi r,c
     * @param r posisi r
     * @param c posisi c
     */
    static void views(int r, int c){
        for(int i=0; i<=maxRow; ++i) {
            for (int j = 0; j <=maxCol; ++j) {
                if (i == r && j == c)
                    pw.print("X|");
                else
                    pw.print(MAP[i][j].toString() + "|");
            }
            pw.println();
        }
        pw.println();
    }

    /**
     * Print peta
     */
    static void views(){
        for(Square[] sqArr : MAP) {
            for (Square sq : sqArr)
                pw.print(sq.toString()+"|");
            pw.println();
        }
    }

}


/**
 * Class pass.Hero untuk setiap hero yang ada di peta
 */
class Hero implements Comparable<Hero> {

    String name;
    long level;
    long mana;
    long strength;
    int lifetime;
    String weapon;
    long battlePower;

    static final String PEDANG = "pedang";
    static final String PANAH = "panah";

    public Hero(String name, long level, long mana, long strength, String weapon) {
        this.name = name;
        this.level = level;
        this.mana = mana;
        this.strength = strength;
        this.weapon = weapon;
        // inisiasi nilai awal -1
        this.lifetime = -1;
        this.battlePower = -1;
    }

    /**
     * diurutkan dari kekuatan paling tinggi ke rendah, jika sama mana terendah ke tinggi
     * @param o
     * @return
     */
    @Override
    public int compareTo(Hero o) {
        // from bigger to smaller
        if(strength == o.strength)
            return mana > o.mana ? 1 : -1;
        return strength > o.strength ? -1 : 1;
    }

    @Override
    public String toString() {
        return name+":"+strength+";"+ mana;
    }
}


/**
 * Class pass.Square untuk pass.Square di Map
 * visited adalah jika true maka telah divisit sebelumnya
 */
class Square {
    int r, c;
    final char MARK;
    boolean visited;

    static final char WAY = '.';
    static final char STONE = '#';
    static final char START = 'M';
    static final char DUNGEON = 'D';
    static final char HERO = 'S';

    public Square(int r, int c, char MARK) {
        this.r = r;
        this.c = c;
        this.MARK = MARK;
        this.visited = false;
    }

    @Override
    public String toString() {
        return MARK+"";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Square square = (Square) o;

        if (r != square.r) return false;
        if (c != square.c) return false;
        return MARK == square.MARK;
    }

    @Override
    public int hashCode() {
        int result = r;
        result = 31 * result + c;
        result = 31 * result + (int) MARK;
        return result;
    }
}

/**
 * pass.Square untuk hero-hero
 * Using treeset add is O(log N)
 * TreeSet iteration is of course O(N) [using for each statement], as can be expect from any sensible tree-walking
 * algorithm [O(log N) for find the first node and walking is O(N)]
 */
class HeroSquare extends Square {
    TreeSet<Hero> qHero;

    public HeroSquare(int r, int c, char MARK, TreeSet<Hero> qHero) {
        super(r, c, MARK);
        this.qHero = qHero;
    }

//    @Override
//    public String toString() {
//        return super.toString() + qHero;
//    }
}


/**
 * pass.Square untuk dungeon
 */
class DungeonSquare extends Square {
    long defense;
    long level;
    String weaponType;
    int maxHero;

    public DungeonSquare(int r, int c, char MARK, long defense, long level, String weaponType, int maxHero) {
        super(r, c, MARK);
        this.defense = defense;
        this.level = level;
        this.weaponType = weaponType;
        this.maxHero = maxHero;
    }

//    @Override
//    public String toString() {
//        return super.toString()+"["+defense+"]";
//    }
}