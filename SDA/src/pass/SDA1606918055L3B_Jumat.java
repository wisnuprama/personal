package pass;

import java.io.*;
import java.util.*;


/**
 * Wisnu Pramadhitya Ramadhan 1606918055 kelas A
 *
 * Lab 4 B
 */
public class SDA1606918055L3B_Jumat {
    final static int SIZE = 10000;
    final static int MEMO[] = new int[SIZE + 5];

    public static void main(String[] args)
    {   
        int Q, N;
        FastReader scan =new FastReader();
        PrintWriter print = new PrintWriter(new OutputStreamWriter(System.out));

        for(int i=0; i < SIZE+5; ++i){
            MEMO[i] = Integer.MAX_VALUE;
        }

        // PRECOMPUTED
        MEMO[1] = 1;
        MEMO[2] = 2;
        MEMO[3] = 3;

        Q = scan.nextInt();

        while (--Q >= 0) {
            N = scan.nextInt();
            print.println(countingLaders(N));
            print.flush();
        }
    }

    static int countingLaders(int n) {
        // jika sudah dihitung return yang sudah dihitung
        if(MEMO[n] != Integer.MAX_VALUE) return MEMO[n];

        /*
         calculate it and save to memo
         cek apakah ini merupakan faktor dari n
         */

        for(int i=(int) Math.sqrt(n); i > 1; i--) {
            if(n % i == 0){
                int div = countingLaders(n/i) + 1;
                // Jika faktor save to memo
                MEMO[n] = (div < MEMO[n]) ? div : MEMO[n];
            }
        }

        // hitung jika dia jump 1 step
        int minimum = countingLaders(n - 1) + 1;
        // return minimum value
        return MEMO[n] = (MEMO[n] < minimum) ? MEMO[n] : minimum;
    }

    static class FastReader {
        BufferedReader br;
        StringTokenizer st;
 
        public FastReader() {
            br = new BufferedReader(new
                     InputStreamReader(System.in));
        }
 
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                }
                catch (IOException  e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }
 
        int nextInt() {
            return Integer.parseInt(next());
        }
 
        long nextLong() {
            return Long.parseLong(next());
        }
 
        double nextDouble() {
            return Double.parseDouble(next());
        }
 
        String nextLine() throws IOException { return br.readLine(); }
    }
}