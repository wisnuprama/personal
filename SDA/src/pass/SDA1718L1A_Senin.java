package pass;

import java.io.*;
import java.util.*;

/**
 * Created by wisnuprama on 08/09/17.
 */
public class SDA1718L1A_Senin {

    public static void main(String args[]){

        BufferedReader br = null;
        String input = null;
        String out = "";
        Set<String> dict = null;

        try {
            br = new BufferedReader(new InputStreamReader(System.in));
            String arr[] = br.readLine().split(" ");
            dict = new HashSet<>(Arrays.asList(arr));

            // read the text
            String in = null;
            input = "";
            while((in = br.readLine()) != null && in.length() != 0){
                input += in + '\n';
            }

            List<String> text = new LinkedList<>();
            for(int j=0; j < input.length(); ++j) {
                char c = input.charAt(j);
                text.add(c+"");
            }

            for(int i=0; i < text.size(); ++i) {
                String c = text.get(i);
                if(!dict.contains(c)){
                    out += c;
                }
            }

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
            bw.write(out);
            bw.flush();

        } catch (IOException e){
            e.printStackTrace();

        }
    }
}
