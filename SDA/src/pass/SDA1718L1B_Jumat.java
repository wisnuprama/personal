package pass;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.*;
// Masukkan program yang Anda ingin import dibawah kalimat ini

// Masukkan program yang Anda ingin import diatas kalimat ini

/*
    Nama: Wisnu Pramadhitya Ramadhan 1606918055
    Tanggal: 8 September 2017
    Soal: B - Sutefu’s Card Game
 */

/**
	*
	* Template kode yang Anda bisa pakai untuk menyelesaikan soal Tutorial 1B
	* Penggunaan template bersifat opsional namun mahasiswa sangat disarankan untuk menggunakan template ini
	* Jangan mengubah nama kelas dan menghapus kode import diatas. Pengubahan dapat menyebabkan program mengalami error.
	* @author Jahns Christian Albert
	*
*/
public class SDA1718L1B_Jumat {
	
	/**
		*
		* Method main untuk mengambil input dan menampilkan output
		* Jangan mengubah method ini. Pengubahan method dapat menyebabkan program mengalami error
		*
	*/
	public static void main(String[] args) throws IOException {
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(in.readLine());
		
		int n = Integer.parseInt(st.nextToken());
		int k = Integer.parseInt(st.nextToken());
		long[] arr = new long[n];
		
		st = new StringTokenizer(in.readLine());
		
		for(int i = 0; i < n; i++){
			arr[i] = Long.parseLong(st.nextToken());
		}
		
		System.out.println(solution(arr,k));
		
	}
	
	/**
		*
		* Method untuk menyelesaikan soal 1B
		* To do : Lengkapi method ini
		* @param arr dari bilangan bulat (long) yang ingin dicari jumlah pasangan yang "berpasangan"
		* @param k bilangan bulat positif sebagai key
		* @return jumlah pasangan yang berpasangan (long)
		*
	*/
	private static long solution(long[] arr, long k){
        long count = 0;
        long freq[] = new long[(int) k];

        /*
            memetakan jumlah frequensi hasil modulo
            key: hasil mod value: jumlah yang hasil mod value tersebut
         */
        for (long anArr : arr) {
            // key berupa hasil mod dengan k
            long key = anArr % k;
            freq[(int) key]++; // menambahkan jumlah key tsb
        }

        /*
            CONSTRAINT:
            1. ketika k adalah genap, maka kelompok
               yang tidak memiliki pasangan
            2. kelompok nol tidak berpasangan karena kelompok 0 ditambah pasangannya jika di mod k harus tetap 0
         */
        for(int i=0; i <= k/2; ++i){
            long val1 = freq[i];
            // memasangkan dengan k-1
            if(i != 0 && i != k - i){
                // get another key
                int pairKey = (int) k - i;

                // banyaknya pasangan kartu adalah kombinasi dari banyaknya frequensi kelompok hasil mod
                count += (val1 * freq[pairKey]); //
            } else {
                /*
                    kombinasi pada kelompok yang tidak memiliki pasangan sehingga hanya bisa
                    dipasangkan dengan anggota kelompok itu sendiri.
                 */
                count += ((val1 - 1) * val1) / 2;
            }
        }

		return count;
	}
}