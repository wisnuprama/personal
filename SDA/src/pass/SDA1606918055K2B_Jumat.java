import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class SDA1606918055K2B_Jumat {

    public static void main(String[] args) throws IOException {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)));

        long inputCount = Integer.parseInt(in.readLine());

        start:for (long i = 0; i < inputCount; i++) {

            String inp = in.readLine();
            final int TARGET = Integer.parseInt(in.readLine());

            StringTokenizer st = new StringTokenizer(inp);
            HashSet<Integer> set = new HashSet<>();

            int temp = 1;
            while(st.hasMoreTokens()) {
                int n = Integer.parseInt(st.nextToken());

                if(temp * n == TARGET) {
                    out.println("Ada");
                    continue start;
                }

                set.add(n);
                temp = n;
            }

            ArrayList<Integer> integer = new ArrayList<>(set);

            HashSet<ArrayList<Integer>> res = new HashSet<>();
            subset(integer, 0, new ArrayList<Integer>(), res);

            finding:for(ArrayList<Integer> subset : res) {

                if(subset.isEmpty()) continue;

                int find = 1;

                for(int f : subset){
                    if(f == 0){
                        if(f == TARGET) {
                            out.println("Ada");
                            continue start;
                        }
                        continue finding;
                    }

                    find *= f;
                }

                if(find == TARGET) {
                    out.println("Ada");
                    continue start;
                }
            }
            out.println("Tidak Ada");
        }

        out.close();
    }

    static void subset(ArrayList<Integer> arr, int pos, ArrayList<Integer> cut, HashSet<ArrayList<Integer>> res){
        if(pos == arr.size()){
            res.add(cut);
            return;
        }

        subset(arr, pos+1, (ArrayList<Integer>) cut.clone(), res);
        cut.add(arr.get(pos));
        subset(arr, pos+1, cut, res);
    }
}
