package pass;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by wisnuprama on 10/28/2017.
 */
public class SDA1606918055L5B {

    public static void main(String args[]) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter pw = new PrintWriter(new OutputStreamWriter(System.out));

        BSTreeMap<String, Integer> recruitment = new BSTreeMap<>();
        pw.flush();

        String name;
        int score;
        boolean status;

        String input = null;
        while((input = br.readLine()) != null && input.length() > 0) {

            String in[] = input.split(";");
            String cmd = in[0];

            switch (cmd) {

                case "REGISTER":
                    name = in[1];
                    score = Integer.parseInt(in[2]);
                    status = false;
                    if(!recruitment.contains(name))
                        status = recruitment.add(name, score);

                    if(status)
                        pw.println(name+":"+score+" berhasil ditambahkan");
                    else
                        pw.println(name+" sudah terdaftar di dalam sistem");

                    break;

                case "RESIGN":
                    name = in[1];
                    status = recruitment.remove(name);
                    if(status)
                        pw.println(name+" mengundurkan diri");
                    else
                        pw.println(name+" tidak ditemukan di dalam sistem");
                    break;

                case "RETEST":
                    name = in[1];
                    score = Integer.parseInt(in[2]);
                    status = false;
                    if(!recruitment.isEmpty())
                        status = recruitment.add(name, score);

                    if(status)
                        pw.println(name+":"+score+" perubahan berhasil");
                    else
                        pw.println(name+" tidak ditemukan di dalam sistem");
                    break;

                case "SMARTEST":
                    if(recruitment.isEmpty())
                        pw.println("Tidak ada siswa yang terdaftar dalam sistem");
                    else
                        pw.println(recruitment.getSmartest());

                    break;

                case "RANKING":
                    if(recruitment.isEmpty())
                        pw.println("Tidak ada siswa yang terdaftar dalam sistem");
                    else {
                        pw.println(recruitment.getRanking());
                    }

                    break;
            }

        }

        //pw.println(recruitment.inOrderAscending());
        pw.flush();
    }


}


class BSTreeMap<K extends Comparable<K>, V extends Comparable<V>> {
    /**
     *
     * Kelas yang merepresentasikan node pada tree
     * @author Jahns Christian Albert
     *
     */
    private static class Node<K extends Comparable<K>, V extends Comparable<V>> {

        K key;
        V elem;
        Node<K, V> left;
        Node<K, V> right;
        Node<K, V> parent;

        /**
         *
         * @param elem
         * @param left
         * @param right
         * @param parent
         */
        public Node(K key, V elem, Node<K, V> left, Node<K, V> right, Node<K, V> parent){
            this.key = key;
            this.elem = elem;
            this.left = left;
            this.right = right;
            this.parent = parent;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Node<?, ?> node = (Node<?, ?>) o;

            return key != null ? key.equals(node.key) : node.key == null;
        }

        @Override
        public int hashCode() {
            return key != null ? key.hashCode() : 0;
        }
    }

    private Node<K, V> root;
    private int size;

    /**
     * Constructor Kelas Binary Search Tree
     */
    public BSTreeMap(){
        root = null;
        size = 0;
    }

    /**
     *
     * Mengetahui apakah tree kosong atau tidak
     * @return true jika kosong, false jika sebaliknya
     *
     */
    public boolean isEmpty(){
        return root == null;
    }

    /**
     *
     * Menambahkan objek ke dalam tree
     * @param key yang ingin ditambahkan
     * @return true jika elemen berhasil ditambahkan, false jika elemen sudah terdapat pada tree
     *
     */
    public boolean add(K key, V elem){

        if(root == null){
            root = new Node<>(key, elem, null, null, null);
            this.size++;

        } else {

            Node<K, V> parent = null;
            Node<K, V> current = root;
            // traverse to another node
            while(current != null){

                K currKey = current.key;
                parent = current; // update the parent fromm current because we want to traverse to another node
                    // ke kiri
                if(key.compareTo(currKey) < 0){
                    current = current.left;
                    if(current == null) { // if current null means kita di ujung
                        parent.left = new Node<>(key, elem, null, null, parent);
                        this.size++;
                    }

                    // ke kanan
                } else if(key.compareTo(currKey) > 0){
                    current = current.right;
                    if(current == null) { // if current null means kita di ujung
                        parent.right = new Node<>(key, elem, null, null, parent);
                        this.size++;
                    }

                } else {
                    // update the elemen because the key is already here
                    current.elem = elem;
                    return true;
                }

            }
        }

        return true;
    }

    /**
     *
     * Mendapatkan node dengan elemen tertentu
     * @param key yang ingin dicari nodenya
     * @return node dari elemen pada parameter, null jika tidak ditemukan
     *
     */
    private Node<K, V> find(K key){

        Node<K, V> res = null;

        if(root != null){

            Node<K, V> current = root;
            boolean found = false;
            while(!found && current != null){

                K currKey = current.key;
                if(key.compareTo(currKey) < 0){
                    // traverse to left
                    current = current.left;

                } else if(key.compareTo(currKey) > 0){
                    current = current.right; // traverse to right

                } else {
                    // we found it
                    res = current;
                    found = true;
                }
            }
        }

        return res;
    }

    public V get(K key) {
        return this.find(key).elem;
    }

    /**
     *
     * Menghapus objek dari tree, menggunakan successor inorder untuk menghapus elemen yang memiliki left node dan right node
     * Manfaatkan method minNode(Node<K, V> node) untuk mencari successor inorder
     * @param key yang ingin dihapus
     * @return true jika elemen ditemukan dan berhasil dihapus, false jika elemen tidak ditemukan
     *
     */
    public boolean remove(K key){
        int prevSize = this.size;
        this.root = removeHelper(root, key);
        return prevSize > this.size;
    }

    private Node<K, V> removeHelper(Node<K, V> current, K key) {

        if(current == null) return current;

        else if(key.compareTo(current.key) < 0)
            current.left = removeHelper(current.left, key);
        else if(key.compareTo(current.key) > 0)
            current.right = removeHelper(current.right, key);
        else {
            // the key is same
            this.size--;

            if(current.left == null) return current.right;
            else if(current.right == null) return current.left;

            // get the minimum node in the right child of the deleted node
            Node<K, V> curr = this.minNode(current.right);
            current.key = curr.key;
            current.elem = curr.elem;
            current.right = removeHelper(current.right, current.key);
        }

        return current;
    }

    /**
     *
     * Method untuk mengembalikan node dengan elemen terkecil pada suatu subtree
     * Hint : Manfaatkan struktur dari binary search tree
     * @param node root dari subtree yang ingin dicari elemen terbesarnya
     * @return node dengan elemen terkecil dari subtree yang diinginkan
     *
     */
    private Node<K, V> minNode(Node<K, V> node){

        Node<K, V> res = null;
        if(node != null){

            Node<K, V> current = node;

            // traverse to the left until null
            while(current != null){
                res = current;
                current = current.left;
            }
        }

        return res;
    }


    /**
     *
     * Method untuk mengembalikan node dengan elemen terbesar pada suatu subtree
     * Hint : Manfaatkan struktur dari binary search tree
     * @param node root dari subtree yang ingin dicari elemen terbesarnya
     * @return node dengan elemen terbesar dari subtree yang diinginkan
     *
     */
    private Node<K, V> maxNode(Node<K, V> node){

        Node<K, V> res = null;
        if(node != null){

            // traverse to right until null
            Node<K, V> current = node;
            while(current != null){
                res = current;
                current = current.right;
            }

        }

        return res;
    }

    /**
     *
     * Mengetahui apakah sebuah objek sudah terdapat pada tree
     * Asumsikan jika elem.compareTo(otherElem) == 0, maka elem dan otherElem merupakan objek yang sama
     * Hint : Manfaatkan method find
     * @param key yang ingin diketahui keberadaannya dalam tree
     * @return true jika elemen ditemukan, false jika sebaliknya
     *
     */
    public boolean contains(K key){
        Node<K, V> res = this.find(key);
        return res != null;
    }

    /**
     * Mengembalikan tree dalam bentuk in-order secara ascending
     * @return tree dalam bentuk in-order secara ascending sebagai list of K, V
     */
    public List<V> inOrderAscending(){

        List<V> list = new ArrayList<>(size);
        inOrderAscending(root,list);
        return list;
    }

    /**
     *
     * Method helper dari inOrderAscending()
     * @param node pointer
     * @param list sebagai akumulator
     * @return kumpulan elemen dari subtree yang rootnya adalah node parameter dengan urutan in-order secara ascending
     *
     */
    private void inOrderAscending(Node<K, V> node, List<V> list){
        if(node == null)
            return;

        inOrderAscending(node.left, list);
        list.add(node.elem);
        inOrderAscending(node.right, list);
    }


    /**
     * Mengembalikan tree dalam bentuk in-order secara descending
     * @return tree dalam bentuk in-order secara descending sebagai list of K, V
     */
    public List<V> inOrderDescending(){

        List<V> list = new ArrayList<>(size);
        inOrderDescending(root,list);
        return list;
    }

    /**
     *
     * Method helper dari inOrderDescending()
     * @param node pointer
     * @param list sebagai akumulator
     * @return kumpulan elemen dari subtree yang rootnya adalah node parameter dengan urutan in-order descending
     *
     */
    private void inOrderDescending(Node<K, V> node, List<V> list) {
        // using Morris traversal
        if (node == null)
            return;

        inOrderDescending(node.right, list);
        list.add(node.elem);
        inOrderDescending(node.left, list);
    }

    private List<Node<K, V>> inOrderAscendingNode(){
        List<Node<K, V>> list = new LinkedList<>();
        inOrderAscendingNodeHelper(root, list);
        return list;
    }

    private void inOrderAscendingNodeHelper(Node<K, V> node, List<Node<K, V>> list){
        if(node == null)
            return;
        inOrderAscendingNodeHelper(node.left, list);
        list.add(node);
        inOrderAscendingNodeHelper(node.right, list);
    }

    public String getSmartest() {
        if(root == null){
            return "";
        }

        List<Node<K, V>> listOfNodes = inOrderAscendingNode();
        StringBuilder stringBuilder = new StringBuilder();

        V max = listOfNodes.get(0).elem;
        for(Node<K, V> n : listOfNodes){

            if(max.compareTo(n.elem) < 0)
                max = n.elem;

        }

        int len = listOfNodes.size();
        for(int i=0; i<len; ++i){

            Node<K, V> node = listOfNodes.get(i);
            if(max.compareTo(node.elem) == 0){

                stringBuilder.append(node.key+", ");
            }
        }

        String res = stringBuilder.toString();
        return res.substring(0, res.length()-2) + " : " + max;

    }

    public String getRanking() {
        // if tree is empty
        if(root == null){
            return "";
        }

        // get all nodes in ascending
        List<Node<K, V>> listOfNodes = inOrderAscendingNode();
        // ranking the names, their score as the key
        Map<V, List<K>> ranking = new HashMap<>();
        StringBuilder stringBuilder = new StringBuilder();

        // mengumpulkan semua key secara terurut
        Set<V> setVasKey = new HashSet<>();
        for(Node<K, V> n : listOfNodes)
            setVasKey.add(n.elem);

        List<V> sortedVasKey = new ArrayList<>(setVasKey);
        int len = sortedVasKey.size();
        Collections.sort(sortedVasKey, new Comparator<V>(){
            @Override
            public int compare(V o1, V o2) {
                return o2.compareTo(o1);
            }
        });

        // accumulate the name to their score
        for(int i=0; i < len; ++i){
            V v = sortedVasKey.get(i);
            List<K> nameInI = new ArrayList<>();
            ranking.put(v, nameInI);

            // we use linkedlist so we can make it queue alike but flexible
            // poll the the node if we have put it to nameInI
            Iterator<Node<K, V>> iterator = listOfNodes.iterator();
            while(iterator.hasNext()){
                Node<K, V> node = iterator.next();
                if(v.compareTo(node.elem) == 0){
                    nameInI.add(node.key);
                    iterator.remove();
                }
            }

            Collections.sort(nameInI);

        }

        // TO STRING
        for(int i=0; i<len; ++i){
            stringBuilder.append((i+1)+". ");

            List<K> setOfNameInThisScore = ranking.get(sortedVasKey.get(i));
            int toLast = setOfNameInThisScore.size()-1;
            for(K name : setOfNameInThisScore){

                if(toLast != 0)
                    stringBuilder.append(name+", ");
                else
                    stringBuilder.append(name+" : "+sortedVasKey.get(i));

                toLast--;
            }

            stringBuilder.append("\n");
        }


        String res = stringBuilder.toString();
        return res.substring(0, res.length()-1);
    }
}