package pass;

import java.io.*;
import java.util.*;

public class SDA1606918055L4B {

    // DATA STRUCTURE
    static final LinkedList<Kartu> INHAND = new LinkedList<>();

    public static void main(String args[]) throws IOException {
        String nama = null;
        int power;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter print = new PrintWriter(new OutputStreamWriter(System.out));

        String inputs[] = null;
        String in = null;
        boolean NEED_SORTED = false;
        while((in = reader.readLine())!=null && in.length() > 0) {
            inputs = in.split(" ");

            String cmd = inputs[0];
            switch (cmd){
                case "PICK":
                    nama = inputs[1];
                    power = Integer.valueOf(inputs[2]);
                    Kartu picked = new Kartu(nama, power);
                    INHAND.add(picked);
                    sort(INHAND);
                    print.println(nama + " dengan power " + power + " diambil");
                    break;

                case "ATTACK":
                    if(INHAND.isEmpty()){
                        print.println("Tidak bisa melakukan Attack");

                    } else {
                        Kartu kartu = INHAND.poll();
                        print.println(kartu.nama + " " + kartu.power + " dikeluarkan");
                    }

                    break;
                case "DEFENSE":
                    if(INHAND.size() < 3){
                        print.println("Tidak bisa malakukan Defense");
                    } else {
                        for(int i=0; i<3; ++i){
                            Kartu k = INHAND.pollLast();
                            print.println(k.nama + " " + k.power + " dikeluarkan");
                        }
                    }
                    break;

                case "SEE":
                    if(!"CARD".equals(inputs[1])) break;

                    if(INHAND.isEmpty()) {
                        print.println("Kartu kosong");

                    } else {
                        for(Kartu k : INHAND) {
                            print.println(k.nama + " "  + k.power);
                        }
                    }

                    break;
            }
        }

        print.flush();
    }

    static <T extends Comparable<? super T>> void sort(List<T> list) {
        Object arr[] = list.toArray();

        // Iterator
        ListIterator iterator = list.listIterator();
        // SORT
        quicksort(arr, 0, arr.length-1);

        int len = arr.length;

        for(int i=0; i < len; ++i) {
            iterator.next();
            iterator.set((T)arr[i]);
        }
    }

    static <T> void quicksort(T arr[], int lo, int hi) {

        int i = lo, j = hi, mid = (hi + lo) / 2;

        T pivot = arr[mid];

        while(i <= j){

            while(((Comparable) pivot).compareTo(arr[i]) > 0) ++i;
            while(((Comparable) pivot).compareTo(arr[j]) < 0) --j;

            if(i <= j) {
                // SWAP
                T tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;

                ++i; --j;
            }
        }

        if(lo < j) quicksort(arr, lo, j);
        if(i < hi) quicksort(arr, i, hi);
    }
}

class Kartu implements Comparable<Kartu> {

    String nama;
    int power;

    public Kartu(String nama, int power) {
        this.nama = nama;
        this.power = power;
    }

    @Override
    public int compareTo(Kartu kartu) {

        if(this.power == kartu.power)
            return nama.compareTo(kartu.nama);

        return power < kartu.power ? 1 : -1;
    }

    @Override
    public String toString() {
        return nama + " " + power;
    }
}