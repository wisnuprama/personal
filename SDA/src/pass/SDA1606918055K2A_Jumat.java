
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.IOException;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Stack;
import java.util.StringTokenizer;

// TODO: Ganti nama Main class sesuai soal
public class SDA1606918055K2A_Jumat
{
	public static void main(String[] args) throws IOException
	{
		// BufferedReader, untuk membaca input
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		// PrintWriter, untuk output. Penggunaannya seperti System.out
		PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)));
		
		// Baca input baris pertama: banyak 
		long inputCount = Integer.parseInt(in.readLine());
		
		// Inisialisasi objek untuk sistem
		SistemBankUnik sbu = new SistemBankUnik();
		
		// Loop untuk setiap baris input
		for (long i = 0; i < inputCount; i++) {
			// Baca input, tokenize dengan spasi
			String inp = in.readLine();
			StringTokenizer st = new StringTokenizer(inp);
			
			// Token pertama: jenis perintah
			String command = st.nextToken();
			
			if (command.equals("ANTRI")) {
				String nama = st.nextToken();
				long saldo = Integer.parseInt(st.nextToken());
				sbu.antri(nama, saldo);
				out.println(nama + " masuk ke dalam antrian");
			}
			else if (command.equals("PROSES")) {
				out.print(sbu.proses());
			}
			else if (command.equals("PENARIKAN")) {
				String nama = st.nextToken();
				out.print(sbu.penarikan(nama));
			}
		}
		
		// Close output
		out.close();
	}
}

/**
* Class yang merepresentasikan Sistem Bank Unik
*/
class SistemBankUnik
{
	// TODO: Tambahkan instance variable yang dibutuhkan di sini
	Stack<Nasabah> stack;
	Queue<Nasabah> queue;
	long min;
	
	/**
	* Constructor kosong
	*/
	public SistemBankUnik()
	{
		// TODO: Lengkapi constructor ini
        stack = new Stack<>();
        queue = new ArrayDeque<>();
        min = Integer.MAX_VALUE;
	}
	
		/**
	* Method untuk menghandle perintah "ANTRI"
	*/
	public void antri(String nama, long saldo)
	{
		// TODO: Implementasikan untuk perintah "ANTRI"
        Nasabah n = new Nasabah(nama, saldo);
        queue.add(n);
	}
	
	/**
	* Method untuk menghandle perintah "PROSES"
	*/
	public String proses()
	{
		// TODO: Implementasikan untuk perintah "PROSES"
        if(queue.isEmpty())
            return "Antrian kosong\n";

        long maxSaldo = min*1000;
        Nasabah n = queue.poll();

        if(n.tabungan > maxSaldo)
            return "Saldo " + n.nama + " melebihi saldo maksimal: " + maxSaldo + "\n";

        if(n.tabungan < min)
            min = n.tabungan;

        stack.push(n);
        return n.nama + " berhasil menabung sebesar " + n.tabungan + "\n";
	}
	
	/**
	* Method untuk menghandle perintah "PENARIKAN"
	*/
	public String penarikan(String nama)
	{
		// TODO: Implementasikan untuk perintah "PENARIKAN"

        if(stack.isEmpty())
            return nama + " tidak berhasil melakukan penarikan\n";

        Nasabah n = stack.peek();
        if(!n.nama.equals(nama))
            return nama + " tidak berhasil melakukan penarikan\n";

        stack.pop();
        this.min = Integer.MAX_VALUE;
        Object[] nss = stack.toArray();
        for(int i=0; i < nss.length; ++i) {
            Nasabah ns = (Nasabah) nss[i];

            if(ns.tabungan < this.min)
                this.min = ns.tabungan;
        }

        return n.nama + " berhasil melakukan penarikan sebesar " + n.tabungan + "\n";
	}
}

class Nasabah {

    long tabungan;
    String nama;

    public Nasabah(String nama, long tabungan) {
        this.tabungan = tabungan;
        this.nama = nama;
    }
}