package pass;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class SDA1606918055K3RB {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String heapLine = in.nextLine();

        ArrayList<Integer> heap = new ArrayList<Integer>();

        StringTokenizer st = new StringTokenizer(heapLine);
        while (st.hasMoreTokens()) {
            heap.add(Integer.parseInt(st.nextToken()));
        }

        MyHeap mHeap = new MyHeap(heap);
        int index = in.nextInt();
        int newValue = in.nextInt();

        mHeap.changeValue(index, newValue);

        mHeap.printList();
    }
}

class MyHeap {

    private ArrayList<Integer> data;

    public MyHeap(){
        data = new ArrayList<>();
    }

    public MyHeap(ArrayList<Integer> data) {
        this.data = data;
    }

    private void percolateUp(int index){
        int parent = parentOf(index);
        int value = data.get(index);
        while (index > 0 && value < data.get(parent)) {
            data.set(index, data.get(parent));
            index = parent;
            parent = parentOf(index);
        }
        data.set(index, value);
    }

    private void percolateDown(int index){
        int size = data.size();
        int value = data.get(index);
        while (index < size){
            int childIndex = leftChildOf(index);
            if(childIndex < size){
                //choose the smallest child
                if((rightChildOf(index) < size) && data.get(rightChildOf(index)) < data.get(childIndex)){
                    childIndex = rightChildOf(index);
                }

                if(data.get(childIndex) < value){
                    data.set(index, data.get(childIndex));
                    index = childIndex;
                }else{
                    data.set(index, value);
                    return;
                }
            }else{
                data.set(index, value);
                return;
            }
        }
    }

    public void changeValue(int index, int newValue) {

        // set new data
        int tmp = this.data.get(index);
        this.data.set(index, newValue);


        if(newValue < tmp)
            this.percolateUp(index);
        else
            this.percolateDown(index);
    }

    public void printList() {

        for (Integer value : data) {
            System.out.print(value + " ");
        }
    }

    private int parentOf(int index){
        return index == 1 ? 0 : (index - 1) / 2;
    }

    private int leftChildOf(int index){
        return 2 * index + 1;
    }

    private int rightChildOf(int index){
        return 2 * (index + 1);
    }

    public boolean isEmpty(){
        return data.isEmpty();
    }
}
