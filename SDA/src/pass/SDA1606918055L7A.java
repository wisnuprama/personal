package pass;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class SDA1606918055L7A {

    static final BufferedReader IN = new BufferedReader(new InputStreamReader(System.in));
    static final PrintWriter OUT = new PrintWriter(new OutputStreamWriter(System.out));

    static class Book implements Comparable<Book> {
        String name;
        int popularity;

        public Book(String name, int popularity) {
            this.name = name;
            this.popularity = popularity;
        }

        @Override
        public int compareTo(Book o) {
            return popularity - o.popularity;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public static void main(String args[]) throws Exception {
        BSTreeAVL<Book> bookstore = new BSTreeAVL<>();

        Book book;
        String input, buku;
        int pop;
        while ((input = IN.readLine()) != null && input.length() > 0) {

            String[] parse = input.split(" ");

            switch (parse[0]) {

                case "ADD":
                    buku = parse[1];
                    pop = Integer.parseInt(parse[2]);
                    book = new Book(buku, pop);
                    bookstore.insert(book);
                    OUT.println("Komik " + buku + " sudah disimpan dalam JaringToon");
                    break;

                case "REMOVE":
                    buku = parse[1];
                    pop = Integer.parseInt(parse[2]);
                    if(bookstore.isEmpty())
                        OUT.println("Tidak ada komik dalam JaringToon");
                    else {
                        book = new Book(buku, pop);
                        bookstore.remove(book);
                        OUT.println("Komik " + buku + " sudah dihapus dari JaringToon");
                    }
                    break;

                case "POPULARITY":
                    if (bookstore.isEmpty())
                        OUT.println("Tidak ada komik dalam JaringToon");

                    else {

                        Book top = bookstore.findMax();
                        Book bottom = bookstore.findMin();
                        if(top == bottom)
                            OUT.println("Hanya ada komik " + bottom.name);
                        else
                            OUT.println("Tertinggi " + top.name + "; Terendah " + bottom.name);
                    }
                    break;

                case "PRINT":
                    if (bookstore.isEmpty())
                        OUT.println("Tidak ada komik dalam JaringToon");
                    else {
                        // IN ORDER
                        List<Book> books = bookstore.inOrder();
                        OUT.print("In Order: ");
                        for (int i = 0; i < books.size() - 1; ++i)
                            OUT.print(books.get(i).name + "; ");
                        OUT.println(books.get(books.size() - 1).name);

                        // PRE ORDER
                        books = bookstore.preOrder();
                        OUT.print("Pre Order: ");
                        for (int i = 0; i < books.size() - 1; ++i)
                            OUT.print(books.get(i).name + "; ");
                        OUT.println(books.get(books.size() - 1).name);

                        // POST ORDER
                        books = bookstore.postOrder();
                        OUT.print("Post Order: ");
                        for (int i = 0; i < books.size() - 1; ++i)
                            OUT.print(books.get(i).name + "; ");
                        OUT.println(books.get(books.size() - 1).name);

                    }
                    break;

            }

        }

        OUT.flush();
    }

}

class BSTreeAVL<E extends Comparable<? super E>> {

    static class Node<E> {
        E elem;
        Node<E> left;
        Node<E> right;
        int height;

        public Node(E elem) {
            this.elem = elem;
            height = 1;
        }
    }

    static final PrintWriter OUT = SDA1606918055L7A.OUT;

    private int size;
    private Node<E> root;

    public BSTreeAVL() {
        root = null;
        size = 0;
    }

    public E findMin() {
        Node<E> tmp = findMin(root);
        if (tmp != null)
            return tmp.elem;
        return null;
    }

    public E findMax() {
        Node<E> tmp = findMax(root);
        if (tmp != null)
            return tmp.elem;
        return null;
    }

    public void insert(E x) {
        root = insert(x, root);
    }

    private Node<E> insert(E x, Node<E> t) {
        if (t == null)
            // TO DO : Lengkapi bagian ini
            t = new Node<E>(x);


        int compareResult = x.compareTo(t.elem);
        if (compareResult < 0) {
            // TO DO : Lengkapi bagian ini
            t.left = insert(x, t.left);
        } else if (compareResult > 0) {
            // TO DO : Lengkapi bagian ini
            t.right = insert(x, t.right);
        }

        return balance(t);
    }

    public void remove(E x) {
        root = remove(x, root);
    }

    private Node<E> remove(E x, Node<E> t) {
        if (t == null) {
            return t;   // Item not found; do nothing
        }

        int compareResult = x.compareTo(t.elem);

        if (compareResult < 0) {
            // TO DO : Lengkapi bagian ini
            t.left = remove(x, t.left);
        } else if (compareResult > 0) {
            // TO DO : Lengkapi bagian ini
            t.right = remove(x, t.right);
        } else if (t.left != null && t.right != null) {
            // TO DO : Lengkapi bagian ini
            t.elem = findMax(t.left).elem;
            t.left = remove(t.elem, t.left);
        } else {
            // TO DO : Lengkapi bagian ini
            if (t.left == null && t.right == null)
                t = null;
            else if (t.left == null)
                t = t.right;
            else if (t.right == null)
                t = t.left;
        }
        return balance(t);

    }

    private static final int ALLOWED_IMBALANCE = 1;

    private Node<E> balance(Node<E> t) {
        if (t == null) {
            // TO DO : Lengkapi bagian ini
            return t;
        }

        if (height(t.left) - height(t.right) > ALLOWED_IMBALANCE) {
            if (height(t.left.left) >= height(t.left.right)) {
                // TO DO : Lengkapi bagian ini
                t = rotateWithLeftChild(t);
                OUT.println("Lakukan rotasi sekali pada " + t.elem);
            } else {
                // TO DO : Lengkapi bagian ini
                t = doubleWithLeftChild(t);
                OUT.println("Lakukan rotasi dua kali pada " + t.elem);
            }
        } else {
            if (height(t.right) - height(t.left) > ALLOWED_IMBALANCE) {
                if (height(t.right.right) >= height(t.right.left)) {
                    // TO DO : Lengkapi bagian ini
                    t = rotateWithRightChild(t);
                    OUT.println("Lakukan rotasi sekali pada " + t.elem);
                } else {
                    // TO DO : Lengkapi bagian ini
                    t = doubleWithRightChild(t);
                    OUT.println("Lakukan rotasi dua kali pada " + t.elem);
                }
            }

        }

        // TO DO : Lengkapi bagian ini (update height dari node)
        t.height = Math.max(height(t.left), height(t.right)) + 1;

        return t;
    }

    private Node<E> findMin(Node<E> t) {
        if (t == null) {
            // TO DO : Lengkapi bagian ini
            return t;
        }

        while (t.left != null) {
            // TO DO : Lengkapi bagian ini
            t = t.left;
        }

        return t;
    }

    private Node<E> findMax(Node<E> t) {
        if (t == null) {
            // TO DO : Lengkapi bagian ini
            return t;
        }

        while (t.right != null) {
            // TO DO : Lengkapi bagian ini
            t = t.right;
        }

        return t;
    }

    private int height(Node<E> t) {
        return t == null ? -1 : t.height;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public int size() {
        return size;
    }

    private void preOrder(Node<E> node, List<E> list) {
        if (node == null) return;

        list.add(node.elem);
        preOrder(node.left, list);
        preOrder(node.right, list);
    }

    public List<E> preOrder() {
        List<E> list = new ArrayList<>(size);
        preOrder(root, list);
        return list;
    }

    private void postOrder(Node<E> node, List<E> list) {
        if (node == null) return;

        postOrder(node.left, list);
        postOrder(node.right, list);
        list.add(node.elem);
    }

    public List<E> postOrder() {
        List<E> list = new ArrayList<>(size);
        postOrder(root, list);
        return list;
    }

    private void inOrder(Node<E> node, List<E> list) {
        if (node == null) return;

        inOrder(node.left, list);
        list.add(node.elem);
        inOrder(node.right, list);
    }

    public List<E> inOrder() {
        List<E> list = new ArrayList<>(size);
        inOrder(root, list);
        return list;
    }

    private Node<E> rotateWithLeftChild(Node<E> k2) {
        Node<E> k1 = k2.left;
        k2.left = k1.right;
        k1.right = k2;

        // TO DO : Lengkapi bagian ini (update height dari k1 dan k2)
        k2.height = Math.max(height(k2.left), height(k2.right)) + 1;
        k1.height = Math.max(height(k1.left), k2.height) + 1;

        return k1;
    }

    private Node<E> rotateWithRightChild(Node<E> k1) {
        // TO DO : Lengkapi bagian ini
        Node<E> k2 = k1.right;
        k1.right = k2.left;
        k2.left = k1;
        k1.height = Math.max(height(k1.left), height(k1.right)) + 1;
        k2.height = Math.max(height(k2.right), k1.height) + 1;
        return k2;
    }

    private Node<E> doubleWithLeftChild(Node<E> k3) {
        // TO DO : Lengkapi bagian ini
        k3.left = rotateWithRightChild(k3.left);
        return rotateWithLeftChild(k3);
    }

    private Node<E> doubleWithRightChild(Node<E> k1) {
        // TO DO : Lengkapi bagian ini
        k1.right = rotateWithLeftChild(k1.right);
        return rotateWithRightChild(k1);
    }
}