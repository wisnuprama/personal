package pass;

import java.io.*;
import java.util.*;

/**
 * Created by wisnuprama on 9/15/2017.
 */
public class SDA1718L2A_Jumat {



    public static void main(String args[]) throws IOException {
        String npm = null;
        int halaman;

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringBuilder str = new StringBuilder();
        String input = null;

        // untuk queue printing
        final Queue<Pelanggan> QUEUE_PRINT = new PriorityQueue<>();
        // untuk data pelanggan
        final TreeSet<Pelanggan> SET_OF_PELANGGAN = new TreeSet<>();

        readInput:while((input=br.readLine())!=null && input.length()!=0) {
            String arrInput[] = input.split(" ");

            if(arrInput.length > 1 && "SUBMIT".equals(arrInput[1])){
                npm = arrInput[0];
                halaman = Integer.parseInt(arrInput[2]);
                // membuat objek pelanggan
                Pelanggan pelanggan = new Pelanggan(npm, halaman, Pelanggan.mDALAM_ANTREAN);

                if(QUEUE_PRINT.contains(pelanggan)){
                    // berarti masih terdapat di dalam queue
                    str.append("Harap tunggu hingga submisi sebelumnya selesai diproses\n");
                    continue readInput;
                }

                if(halaman <= 10) {
                    // yang dapat di submit hanya yang halamannya kurang dari 10
                    QUEUE_PRINT.add(pelanggan);
                    SET_OF_PELANGGAN.add(pelanggan);
                    str.append("Submisi " + npm + " telah diterima\n");

                } else {
                    str.append("Jumlah halaman submisi " + npm + " terlalu banyak\n");
                }

            } else if("PRINT".equals(arrInput[0])) {

                if(QUEUE_PRINT.isEmpty()) {
                    // mengecek queue kosong
                    str.append("Antrean kosong\n");
                    continue readInput; // lompat ke input
                }
                int countHalaman = 0;
                while(QUEUE_PRINT.size() > 0) {
                    // selama di queue terdapat print
                    Pelanggan p = QUEUE_PRINT.poll();
                    if(countHalaman>=10 || (countHalaman + p.quantity)>10){
                        // check apakah masih bisa print jika lebih dari 10 masuk kesini
                        // dan dimasukan kembali ke queue
                        QUEUE_PRINT.add(p);
                        continue readInput; // lompat ke input
                    }

                    p.status = Pelanggan.mSELESAI; // mengubah status jadi selesai
                    str.append("Submisi " + p.mNpm + " telah dicetak sebanyak " + p.quantity + " halaman\n");
                    //SET_OF_PELANGGAN.add(p);

                    countHalaman += p.quantity; // menghitung halaman telah di print dalam sekali
                }
            } else if(arrInput.length > 1 && "CANCEL".equals(arrInput[1])) {
                npm = arrInput[0];
                // mengambil pelanggan dalam queue
                Pelanggan p = searchPelanggan(QUEUE_PRINT, npm);

                if(p == null) {
                    // jika tidak terdapat dalam queue lompat ke readInput
                    str.append(npm + " tidak ada dalam antrean\n");
                    continue readInput; // jump to readInput label if p is null (not found)
                }

                QUEUE_PRINT.remove(p); // poll pelanggan from QUEUE_PRINT
                p.status = Pelanggan.mDIBATALKAN;
                str.append("Submisi " + npm + " dibatalkan\n");

            } else if("STATUS".equals(arrInput[0])) {
                npm = arrInput[1];
                // mengambil pelanggan dalam queue
                Pelanggan p = searchPelanggan(SET_OF_PELANGGAN, npm);

                if(p != null && p.status == Pelanggan.mSELESAI) {
                    // jika pelanggan statusnya selesai
                    str.append("Submisi " + p.mNpm + " sudah diproses\n");

                } else if(p != null && p.status == Pelanggan.mDALAM_ANTREAN) {
                    // jika pelanggan statusnya dalam queue
                    str.append("Submisi " + p.mNpm + " masih dalam antrean\n");
                } else {
                    // jika tidak ada dalam sistem
                    str.append(npm + " tidak ada dalam sistem\n");
                }


            }
        }
        // mengeluarkan output program
        System.out.print(str.toString());
    }

    /**
     * Mencari pelanggan
     * @param col
     * @param npm
     * @return pelanggan jika ketemu, null jika tidak
     */
    public static Pelanggan searchPelanggan(Collection<Pelanggan> col, String npm){
        for(Pelanggan p : col) {
            if(p.mNpm.equals(npm)) {
                return p;
            }
        }
        return null;
    }
}

/**
 * KELAS PELANGGAN MENYIMPAN DATA PELANGGAN BERUPA NPM, HALAMAN DAN STATUS
 */
class Pelanggan implements Comparable<Pelanggan> {

    static final int mDIPROSES = 1;
    static final int mDIBATALKAN = 2;
    static final int mDALAM_ANTREAN = 3;
    static final int mSELESAI = 4;

    String mNpm;
    int quantity;
    int status;

    public Pelanggan(String npm, int quantity, int status) {
        this.mNpm = npm;
        this.quantity = quantity;
        this.status = status;
    }

    @Override
    public int compareTo(Pelanggan o) {
        return mNpm.compareTo(o.mNpm);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pelanggan pelanggan = (Pelanggan) o;

        return mNpm != null ? mNpm.equals(pelanggan.mNpm) : pelanggan.mNpm == null;
    }

    @Override
    public int hashCode() {
        int result = mNpm != null ? mNpm.hashCode() : 0;
        result = 31 * result + quantity;
        result = 31 * result + status;
        return result;
    }
}
