package pass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by wisnuprama on 9/5/2017.
 */
public class SDA1718L1A_Selasa {

        static final String mLala = "LALA";
        static final String mMomo = "MOMO";
        static final String mNana = "NANA";
        static final String mRito = "RITO";

    public static void main(String args[]){

        Map<String, Integer> mapCalled = new HashMap<>();
        mapCalled.put(mLala, 0);
        mapCalled.put(mMomo, 0);
        mapCalled.put(mNana, 0);
        Set<String> keys = mapCalled.keySet();
        BufferedReader br = null;

        try {
            br = new BufferedReader(new InputStreamReader(System.in));
            String input = br.readLine();

            int n = Integer.parseInt(input);

            while(--n >= 0) {
                input = br.readLine();

                switch (input.toUpperCase()) {

                    case mLala:
                        mapCalled.put(mLala, mapCalled.get(mLala) + 1);
                        break;

                    case mMomo:
                        mapCalled.put(mMomo, mapCalled.get(mMomo) + 1);
                        break;

                    case mNana:
                        mapCalled.put(mNana, mapCalled.get(mNana) + 1);
                        break;

                    case mRito:

                        for (String key : keys) {
                            int x = mapCalled.get(key);

                            if (--x < 0)
                                mapCalled.put(key, 0);
                            else
                                mapCalled.put(key, x);
                        }

                        break;
                }

            }

        } catch (IOException e){
            e.printStackTrace();

        } catch (Exception e){
            e.printStackTrace();
        }

        boolean flagDraw = false;
        String name = null;
        int x = 0;

        for(String key : keys) {
            int score = mapCalled.get(key);

            if (score > x) {
                x = score;
                name = key;
                flagDraw = false;

            } else if (score == x) {
                flagDraw = true;
            }
        }

        if (flagDraw) {
            System.out.println("DRAW " + x);
        } else {
            System.out.println(name + " " + x);
        }
    }
}
