import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.*;

public class SDA1606918055T3 {

    private static final BufferedReader IN = new BufferedReader(new InputStreamReader(System.in));
    private static final PrintWriter OUT = new PrintWriter(new OutputStreamWriter(System.out));

    public static void main(String args[]) throws Exception {

        FileSystem fileSystem = new FileSystem();

        String input;
        String name;
        String ext;
        String destination;
        String out;
        boolean result;
        Node node;
        while ((input = IN.readLine()) != null && input.length() > 0) {

            String[] parse = input.split(" ");
            String cmd = parse[0];

            switch (cmd) {

                case "add":
                    name = parse[1];
                    destination = parse[2];
                    fileSystem.insert(new Directory(name), destination);
                    break;

                case "insert":
                    name = parse[1];
                    int size = Integer.parseInt(parse[2]);
                    destination = parse[3];
                    parse = name.split("\\.");

                    name = parse[0];
                    ext = parse[1];

                    node = new File(name, ext, size);
                    result = fileSystem.insert(node, destination);

                    if (result)
                        OUT.println(node + " added to " + node.getParentDir().getName());

                    break;

                case "remove":
                    name = parse[1];

                    List<Node> nodeRemoved = fileSystem.remove(name);

                    if (!nodeRemoved.isEmpty()) {

                        node = nodeRemoved.get(FileSystem.DEFAULT_INDEX_FOLDER);

                        if (node.isDirectory())
                            OUT.println("Folder " + node.getName() + " removed");
                        else
                            OUT.println(nodeRemoved.size() + " File " + node.getName() + " removed");
                    }

                    break;

                case "search":
                    name = parse[1];
                    out = fileSystem.search(name);
                    if (out != null)
                        OUT.println(out);
                    break;

                case "print":
                    name = parse[1];
                    OUT.println(fileSystem.print(name));
                    break;

                case "recommend":
                    String prefix = parse[1];
                    name = parse[2];
                    OUT.println(fileSystem.recommend(prefix, name));
                    break;

                case "moveHelper":
                    name = parse[1];
                    destination = parse[2];
                    out = fileSystem.moveHelper(name, destination);
                    if (out != null) OUT.println(out);
                    break;

                case "cut":
                    name = parse[1];
                    String parent = parse[2];
                    destination = parse[3];
                    OUT.println(fileSystem.cut(name, parent, destination));
                    break;
            }

        }

        OUT.flush();
    }
}


class FileSystem {

    private Directory root;
    private Map<String, List<Node>> indexTable;

    private static final String ROOT = "root";
    public static final int DEFAULT_INDEX_FOLDER = 0;
    private static final int INIT_CAPACITY_FOLDER = 1;
    private static final int INIT_CAPACITY_FILE = 50;

    FileSystem() {
        root = new Directory(ROOT);
        indexTable = new HashMap<>(); // get, put, find, remove O(1), the worst is O(logN) when using bad hashcode (java 8)

        List<Node> init = new ArrayList<>(INIT_CAPACITY_FOLDER);
        init.add(root);
        indexTable.put(root.getName(), init);
    }

    /**
     * Nama file unik
     * Folder tidak dapat menampung keduanya (folder ^ file)
     * Jika file dimasukan ke dalam folder yang berisi folder, maka file dimasukan ke folder yang berada di folder tsb
     * folder yang dipilih adalah:
     * - folder urutan pertama
     * <p>
     * Jika folder dimasukan ke dalam folder yang berisi file2, maka
     * file di folder parent dimasukan ke folder baru,
     * dan folder baru dimasukan ke folder parent
     * <p>
     * di dalam folder hanya ada ekstensi unik, jadi
     * hanya ada satu ekstensi saja di dalam folder
     * <p>
     * jika sebuah file dimasukan ke dalam folder yang memiliki file berekstensi
     * berbeda, maka file tsb dimasukan ke foldr selanjutnya dari urutan
     * leksiko, hingga menemukan folder kosong atau eks sama.
     * Jika folder selanjutnya berisi folder, maka dimasukan ke dalam folder
     * maka diisi ke dalam folder tsb
     * jika tidak ada folder pada urutan tsb, maka memutar kembali ke urutan folder 1
     * jika setelah 1 putaran tidak ada, maka naik ke parent, dan lakukan hal sama
     * jika tetap tidak ada makan tidak jadi dimasukan
     */

    public boolean insert(Node node, String to) {

        if (node == null || to == null)
            return false;

        if (indexTable.containsKey(to)) {
            try {
                Node parentDir = indexTable.get(to).get(DEFAULT_INDEX_FOLDER);
                if (parentDir.isDirectory())
                    return insertHelper(node, (Directory) parentDir);

            } catch (IndexOutOfBoundsException e) {
                return false;
            }

        }

        return false;
    }

    private boolean insertHelper(Node node, Directory parentDir) {

        // insert node to directory
        if (parentDir == null || node == null)
            return false;

        boolean res = parentDir.insert(node);

        if (!res) {
            /*
             * failed to insert to directory because:
             *  - node is a file and has different ext with file inside the parent folder
             *  - node is a file, but the content in parent folder is folder
             *  - node is a folder, but the content in parent folder is file
             */
            Collection<Node> tmpCollection;
            Directory tmpDir;

            if (node.isDirectory()) {
                // CASE 3: FOLDER CONTAINS FILE, BUT NODE IS DIR
                tmpCollection = parentDir.removeAll();
                tmpDir = (Directory) node;

                // SWAP
                tmpDir.insertAll(tmpCollection);
                res = parentDir.insert(tmpDir);

            } else {

                if (parentDir.getfileExtAttr().equals(Directory.DIR_EXT)) {
                    // CASE 4: FOLDER CONTAINS FOLDER, BUT NODE IS FILE
                    res = insertHelper(node, (Directory) parentDir.getFirstNode());

                } else if (parentDir.getParentDir() != null && !FileSystem.ROOT.equals(parentDir.getName())) {
                    // CASE 5: FOLDER CONTAINS FILE, BUT DIFFERENT EXT

                    Collection<Node> gParentContent = parentDir.getParentDir().getContents();
                    res = this.tryToInsertToSomeFolder(gParentContent, parentDir, node);

                    if (!res) {

                        Directory ggParent = parentDir.getParentDir().getParentDir();
                        if (ggParent == null)
                            return false;

                        Collection<Node> grandParentContent = ggParent.getContents();
                        res = this.tryToInsertToSomeFolder(grandParentContent, parentDir.getParentDir(), node);

                    }
                }
            }
        }

        /*
         * tambah/update list untuk menyimpan data file/folder di indextable
         */
        if (res) {

            String name = node.getName();
            List<Node> nodeList;

            if (indexTable.containsKey(name)) {

                indexTable.get(name).add(node);

            } else {

                if (node.isDirectory())
                    nodeList = new ArrayList<>(INIT_CAPACITY_FOLDER);
                else
                    nodeList = new ArrayList<>(INIT_CAPACITY_FILE);

                nodeList.add(node);
                res = indexTable.put(name, nodeList) == null;
            }
        }

        return res;
    }


    private boolean tryToInsertToSomeFolder(Collection<Node> directories, Directory oldParent, Node nodeToInsert) {

        boolean res = false;
        for (Node dir : directories) {

            if (dir != oldParent) {
                res = ((Directory) dir).insert(nodeToInsert);
                if (res) break;
            }
        }

        return res;
    }

    public List<Node> remove(String name) {
        List<Node> listOfNode = indexTable.remove(name); // O(1) for lookup
        System.out.println(indexTable);
        if (listOfNode == null)
            return new ArrayList<>();

        // start removing
        for (Node n : listOfNode) {
            n.remove();
        }

        return listOfNode; // O(logN)
    }

    public String search(String keyword) {

        if (keyword == null)
            return "";

        if (root.isEmpty() && root.getName().equals(keyword))
            return root.toString();

        HashMap<String, Boolean> marker = new HashMap<>();
        marker.put(keyword, true);
        List<Node> found = indexTable.get(keyword);
        this.traverseToMark(found, marker);

        StringBuilder sb = new StringBuilder();
        if (marker.get(root.getName())) {
            sb.append("> ").append(root).append('\n');
            this.searchHelper(keyword, root, 2, marker, sb);
            return sb.substring(0, sb.length() - 1);
        }

        return null;
    }

    private void searchHelper(String keyword, Directory current, int count,
                              HashMap<String, Boolean> isFound, StringBuilder sb) {

        if (current == null || !isFound.containsKey(current.getName()))
            return;

        Collection<Node> nodes = current.getContents();

        for (Node node : nodes) {
            if (isFound.containsKey(node.getName()) && isFound.get(node.getName())) {
                FileSystem.dup(sb, DELIMITER, count);
                sb.append("> ").append(node).append('\n');

                if (Directory.DIR_EXT.equals(current.getfileExtAttr()))
                    this.searchHelper(keyword, (Directory) node, count + 2, isFound, sb);
            }
        }

    }

    private void traverseToMark(List<Node> foundTarget, HashMap<String, Boolean> isFound) {

        // O(NM)
        for (Node node : foundTarget) {

            // set true to mark
            Node current = node.getParentDir();
            while (current != null) {
                // mark the node and traversing up to the parent
                if (isFound.containsKey(current.getName()))
                    break;

                isFound.put(current.getName(), true);
                current = current.getParentDir();
            }

        }

    }

    public String print(String foldername) {
        Directory dir;

        if (ROOT.equals(foldername))
            dir = root;
        else {
            Node tmp = indexTable.get(foldername).get(DEFAULT_INDEX_FOLDER);

            if (tmp.isDirectory())
                dir = (Directory) tmp;
            else
                return null;
        }

        StringBuilder sb = new StringBuilder();

        sb.append("> ").append(dir).append(" ").append(dir.getSize()).append('\n');
        printHelper(sb, dir, 2);

        return sb.substring(0, sb.length() - 1);
    }

    private static final char DELIMITER = ' ';

    private void printHelper(StringBuilder sb, Directory parentDir, int count) {

        if (parentDir == null)
            return;

        Collection<Node> nodeCollection = parentDir.getContents();
        for (Node n : nodeCollection) {

            FileSystem.dup(sb, DELIMITER, count);
            sb.append("> ").append(n).append(" ").append(n.getSize()).append('\n');

            if (Directory.DIR_EXT.equals(parentDir.getfileExtAttr()))
                printHelper(sb, (Directory) n, count + 2);
        }
    }

    public String recommend(String prefix, String foldername) {

        // @TODO UNCHECK
        Directory dir = (Directory) indexTable.get(foldername).get(DEFAULT_INDEX_FOLDER);
        Collection<Node> nodeCollection = dir.getContents();

        StringBuilder sb = new StringBuilder();
        for (Node node : nodeCollection)
            if (node.getName().startsWith(prefix))
                sb.append(node).append('\n');

        return sb.substring(0, sb.length() - 1);
    }

    private void moveHelper(Node node, Directory toDir) {
        node.remove(); // first remove this dir from old parent
        toDir.insert(node); // insert dir to new parent
    }

    public String moveHelper(String folder, String toFolder) {

        // @TODO STILL UNCHECK

        Directory dir = (Directory) indexTable.get(folder).get(DEFAULT_INDEX_FOLDER);
        Directory toDir = (Directory) indexTable.get(toFolder).get(DEFAULT_INDEX_FOLDER);
        // check parent if toFolder is parent of dir, langunsg/tidak langsung
        // LOG N
        if (dir.isChildOf(toDir))
            return folder + " is inside " + toFolder;

        moveHelper(dir, toDir);
        return null;
    }

    public String cut(String filename, String sourceFolder, String destinationFolder) {

        /**
         * There is two method for solve this problem,
         * we can get list of file from map, and solve this with NlogM i think
         * or we can get the sourceFolder form map, and solve this with traversing of all childnode
         * because we dont know exactly where the f*ck that sh*t from sourceFolder, there is
         * a posibility where sourceFolder has thousand of folder with thousand of folder inside.
         */
        // @TODO UNCHECK
        // @TODO error jika pindah file ke folder yg sama

        List<Node> fileList = indexTable.get(filename);
        // get parent dir
        Directory parDir = (Directory) indexTable.get(sourceFolder).get(DEFAULT_INDEX_FOLDER);
        // get destination dir
        Directory toDir = (Directory) indexTable.get(destinationFolder).get(DEFAULT_INDEX_FOLDER);

        int counter = 0;
        for (Node file : fileList) {
            // N
            if (file.isChildOf(parDir)) {
                // LOG M
                counter++;
                moveHelper(file, toDir);
            }

        }

        return counter + " File " + filename + " moved to " + destinationFolder;
    }

    @Override
    public String toString() {
        return print("root");
    }

    private static void dup(StringBuilder sb, char dup, int count) {
        while (count-- > 0)
            sb.append(dup);
    }

}


//====================================================================================

abstract class Node implements Comparable<Node> {

    private String mName;
    private Directory mParentDir;
    private String mExt;
    private int mSize;

    public static final String DEFAULT_EXT = null;

    public Node(String name, String ext, int size, Directory parentDir) {
        this.mName = name;
        this.mParentDir = parentDir;
        this.mExt = ext;
        this.mSize = size;
    }

    public boolean isDirectory() {
        return this instanceof Directory;
    }

    public boolean isChildOf(Directory dir) {

        Directory curr = mParentDir;
        while (curr != null) {

            if (curr == dir)
                return true;

            curr = curr.getParentDir();
        }

        return false;
    }

    @Override
    public int compareTo(Node node) {
        return mName.compareTo(node.mName);
    }

    public boolean remove() {
        if (mParentDir == null)
            return false;

        Directory tmp = this.mParentDir;
        this.mParentDir = null;

        return tmp.removeNode(this);
    }

    public String getName() {
        return mName;
    }

    public void setParentDir(Directory parentDir) {
        this.mParentDir = parentDir;
    }

    public Directory getParentDir() {
        return mParentDir;
    }

    public String getExt() {
        return mExt;
    }

    public int getSize() {
        return mSize;
    }

    @Override
    public String toString() {
        return mName;
    }

}


class Directory extends Node {

    private Map<String, Node> mContents;
    private String mfileExtAttr;
    private Node firstNode;

    public static final String DIR_EXT = "DIR";
    public static final int DIR_SIZE = 1;

    Directory(String name) {
        super(name, DIR_EXT, DIR_SIZE, null);
        mContents = new TreeMap<>();
        mfileExtAttr = DEFAULT_EXT;
        firstNode = null;
    }

    public boolean isEmpty() {
        return mContents.isEmpty() && mfileExtAttr == DEFAULT_EXT;
    }

    protected boolean removeNode(Node node) {
        Node n = this.mContents.remove(node.getName());

        // reset the directory's content attribute
        if (this.mContents.isEmpty()) {
            this.mfileExtAttr = DEFAULT_EXT;
            this.firstNode = null;
        }

        return n != null;
    }

    public boolean insert(Node node) {

        boolean res = false;

        if (this.isEmpty()) {
            // FIRST TIME := EMPTY DIR
            res = this.mContents.put(node.getName(), node) == null;
            this.mfileExtAttr = node.getExt();

        } else if (this.mfileExtAttr.equals(node.getExt())) {
            // CASE 1: FOLDER CONTAINS FILE AND NODE IS FILE /
            // CASE 2: FOLDER CONTAINS FOLDER AND NODE IS FOLDER
            res = this.mContents.put(node.getName(), node) == null;
        }

        if (res) {
            node.setParentDir(this);
            this.setFirstNode(node);
        }

        return res;
    }

    public boolean insertAll(Collection<Node> collection) {

        if (collection == null || collection.isEmpty())
            return false;

        // IT SHOULD BE ASCENDING
        Iterator<Node> iter = collection.iterator();

        Node n = iter.next(); // it's checked in the first if
        if (!this.isEmpty() && !this.mfileExtAttr.equals(n.getExt()))
            return false;

        // MUST BE O(N) because we need to change stuff inside the node: parent folder
        boolean res = this.insert(n);
        while (iter.hasNext()) {
            if (!res) // @TODO debug insertAll
                System.out.println(this + ": INSERTALL(): collection has item with different ext/key has exist");
            res = this.insert(iter.next());
        }

        return res;
    }

    private void setFirstNode(Node node) {

        if (this.firstNode == null || this.firstNode.compareTo(node) > 0)
            this.firstNode = node;

    }

    public Node getFirstNode() {
        return firstNode;
    }

    public Collection<Node> removeAll() {
        Collection<Node> tmp = this.mContents.values();
        this.mContents = new TreeMap<>();
        this.mfileExtAttr = DEFAULT_EXT;

        return tmp;
    }

    public boolean clearAll() {
        this.mContents = new TreeMap<>();
        this.mfileExtAttr = DEFAULT_EXT;
        return true;
    }

    public String getfileExtAttr() {
        return mfileExtAttr;
    }

    public Collection<Node> getContents() {
        return mContents.values();
    }

    @Override
    public int getSize() {

        int sizeNow = super.getSize();

        if (this.isEmpty())
            return sizeNow;

        for (Node n : this.getContents()) {
            sizeNow += n.getSize();
        }

        return sizeNow;
    }
}


class File extends Node {

    File(String name, String ext, int size) {
        super(name, ext, size, null);
    }

    @Override
    public String toString() {
        return super.toString() + "." + this.getExt();
    }
}
