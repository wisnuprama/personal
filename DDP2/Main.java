import java.util.ArrayList;
import java.util.List;

public class Main
{       
    private int nomor;
    public static void main(String[] args)
    {
        String maba = "abc";
        System.out.println(perm(maba));
    }

    public static List<String> perm(String str){
        ArrayList<String> result = new ArrayList<>();
        perm(str, 0, "", result);
        return result;
    }

    private static void perm(String potongSajaDiriku, int pos, String inilho, ArrayList res){
        if(pos == potongSajaDiriku.length()) {
            res.add(inilho);
            return;
        }

        perm(potongSajaDiriku, pos+1, inilho, res);
        perm(potongSajaDiriku, pos+1, inilho + potongSajaDiriku.charAt(pos), res);
    }
}