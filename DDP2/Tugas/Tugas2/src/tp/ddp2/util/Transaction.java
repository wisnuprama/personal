package tp.ddp2.util;

import java.time.LocalDateTime;

/**
 * Created by wisnuprama on 4/24/2017.
 */
public class Transaction {
    private double biaya;
    private double jarak;
    private String dateTime;

    public Transaction(String dateTime, double jarak, double biaya) {
        this.dateTime = dateTime;
        this.biaya = biaya;
        this.jarak = jarak;
    }

    public double getBiaya() {
        return biaya;
    }

    public double getJarak() {
        return jarak;
    }

    public String getDateTime() {
        return dateTime;
    }
}
