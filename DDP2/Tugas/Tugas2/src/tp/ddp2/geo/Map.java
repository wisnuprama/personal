package tp.ddp2.geo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Map {

    public static final Integer WIDTH = 100;
    public static final Integer HEIGHT = 50;
    private Character map[][] = new Character[HEIGHT][WIDTH];
    private static final String filename = "map.txt";

    public Map() {
        initMap();
    }

    public Character get(int posI, int posJ) {
        return map[posI][posJ];
    }

    public void set(Character c, int posI, int posJ) {
        map[posI][posJ] = c;
    }

    private void initMap() {
        BufferedReader br = null;
        FileReader fr = null;

        try {

            fr = new FileReader(Map.filename);
            br = new BufferedReader(fr);

            String currentLine;

            int i = 0;
            while ((currentLine = br.readLine()) != null) {
                for (int j = 0; j < WIDTH; j++) {
                    map[i][j] = currentLine.charAt(j);
                }
                i++;
            }

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (br != null)
                    br.close();

                if (fr != null)
                    fr.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }
    }

    public final String print() {
        StringBuilder strMAP = new StringBuilder();
        int jjPosAlphabet = 81;
        int jjPosNumber = 0;


        strMAP.append("   ");
        for (int j = 0; j < Map.WIDTH; j++) {
            if (j % 10 == 0) strMAP.append((char) jjPosAlphabet++);
            else strMAP.append(" ");
        }

        strMAP.append("\n");

        strMAP.append("   ");
        for (int j = 0; j < Map.WIDTH; j++) {
            strMAP.append(jjPosNumber++);
            if (jjPosNumber >= 10) jjPosNumber = 0;
        }

        strMAP.append("\n");

        int iiPosAlphabet = 65;
        int iiPosNumber = 0;

        for (int i = 0; i < Map.HEIGHT; i++) {

            if (i % 10 == 0) strMAP.append((char) iiPosAlphabet++);
            else strMAP.append(" ");

            strMAP.append(iiPosNumber++);
            if (iiPosNumber >= 10) iiPosNumber = 0;

            strMAP.append(" ");

            for (int j = 0; j < Map.WIDTH; j++) {
                strMAP.append(this.map[i][j]);
            }
            strMAP.append("\n");
        }

        return strMAP.toString();
    }

}
