package tp.ddp2.geo;

import java.util.HashMap;

/**
 * Created by wisnuprama on 4/18/2017.
 * GPS digunakan untuk menunjang para ojek dalam bekerja. GPS digunakan untuk mencari mencari lokasi dalam peta
 * dan memberikan arah untuk para owjek sehingga dapat mencapai tujuan dengan jalan terdekat. GPS juga bisa menampilkan
 * map dan map yang memiliki arah. GPS juga memberikan informasi mengenai jarak antar dua lokasi.
 */

public class GPS extends Map {

    private int shorthestPath[];
    private int shorthestLength;
    private int startRow;
    private int startCol;
    private int endRow;
    private int endCol;

    // CONST
    private static final char START = 'S';
    private static final char FINISH = 'F';
    public static final Character[] ROW = {'A','B','C','D','E'};
    public static final Character[] COL = {'Q','R','S','T','U','V','W','X','Y','Z'};
    private static final int INTERVAL = 10;
    public static final int INFINITE = Map.HEIGHT * Map.WIDTH + 1;

    private static final java.util.Map<Character, Integer> GEOMAP = new HashMap();

    public GPS() {
        super();
        startRow = 0;
        startCol = 0;
        endRow = 0;
        endCol = 0;
        shorthestLength = INFINITE;
        shorthestPath = new int[Map.WIDTH*Map.HEIGHT];

        int intv = 0;
        for(char r: ROW){
            GEOMAP.put(r, intv);
            intv+=INTERVAL;
        }

        intv =0;
        for(char c: COL){
            GEOMAP.put(c, intv);
            intv+=INTERVAL;
        }
    }

    /**
     * Mengkonversi koordinat dari standard RYCX ke dalam bentuk Point (x,y).
     * @param R alphabetical untuk penanda row
     * @param y koordinat row
     * @param C alphabetical untuk column
     * @param x koordinat column
     * @return Point yang menunjukan koordinat dalam format x, y
     */
    public static Point geoLoc(char R, int y, char C, int x){
        int h = GEOMAP.get(R);
        int w = GEOMAP.get(C);

        return new Point(h+y, x+w);
    }

    /**
     * Mengecek koordinat tersebut berupa jalan atau tidak.
     * @param p koordinat yang akan dicek
     * @return true jika benar bahwa di koordinat tersebut berupa jalan.
     */
    public boolean isStreet(Point p){
        return super.get(p.getY(), p.getX()) == ' ';
    }

    /**
     * Menandai koordinat tersebut dengan kode unik untuk setiap koordinatnya
     * @param row koordinat row
     * @param col koordinat column
     * @return nilai unik untuk setiap koordinatnya
     */
    private int hashPoint(int row, int col){
        return row*Map.WIDTH+col;
    }

    /**
     * Mengecek apakah jalan tersebut sudah pernah dilalui atau tidak ketika GPS mencari jalan tercepat.
     * @param row koordinat row
     * @param col koordinat column
     * @param path daftar koordinat yang sedang dilalui
     * @return true jika sudah pernah dilalui, false sebaliknya
     */
    private boolean wasHere(int row, int col, int path[]){
        int target = hashPoint(row, col);

        for(int x=0; x < path.length;++x){
            if(path[x] == target) return true;
        }

        return false;
    }

    /**
     * Mencari jalan terdekat dalam sebuah map dari dua buah lokasi yang diberikan.
     * @param start lokasi awal
     * @param end lokasi tujuan
     */
    public void findPath(Point start, Point end){
        this.startRow = start.getY();
        this.startCol = start.getX();
        this.endRow= end.getY();
        this.endCol = end.getX();

        if(super.get(startRow, startCol) == '#' || super.get(endRow,endCol) == '#') return;
        // helper wow
        this.shorthestLength = INFINITE;

        final int SIZE = Map.WIDTH*Map.HEIGHT;
        // reset
        this.shorthestPath = new int[SIZE];

        // helper
        int path[] = new int[SIZE];
        findPath(startRow, startCol, path, 0);
    }

    /**
     * Method helper untuk mencari jarak terdekat dari dua lokasi.
     * @param row koordinat row
     * @param col koordinat column
     * @param path daftar koordinat yang pernah dicek.
     * @param length jarak saat ini
     */
    private void findPath(int row, int col, int path[], int length){
        if(row<0 || col<0 || row >= Map.HEIGHT || col >= Map.WIDTH) return;
        if(super.get(row, col) == '#') return; // tembok boom
        if(wasHere(row, col, path)) return; // pernah kesini bosen :(
        if(length > this.shorthestLength) return;

        if(row == endRow && col == endCol)
        {
            if(length <= this.shorthestLength) {
                this.shorthestLength = length;
                this.shorthestPath = new int[length];
                System.arraycopy(path, 0, this.shorthestPath, 0, length);
            }
            return;
        }

        // YOWMANNNN
        int copyPath[] = new int[length + 1];
        System.arraycopy(path, 0, copyPath, 0, length);
        copyPath[length++] = hashPoint(row, col);

        findPath(row-1, col, copyPath, length);
        findPath(row, col-1, copyPath, length);
        findPath(row, col+1, copyPath, length);
        findPath(row+1, col, copyPath, length);
    }

    /**
     * Mengembalikan nilai jarak dari dua lokasi yang dicari.
     * @return jarak dua lokasi yang telah dicari
     */
    public double getShorthestLength() {
        return shorthestLength * 0.1;
    }

    /**
     * Mengembalikan String map yang sudah ditandai dengan '.'. Titik tersebut menunjukan jalan yang akan dilalui dari
     * dua lokasi yang dicari.
     * @return map yang sudah ditandai
     */
    public String showPath(){
        StringBuilder strMAP = new StringBuilder();
        int jjPosAlphabet = 81;
        int jjPosNumber = 0;


        strMAP.append("   ");
        for (int j = 0; j < Map.WIDTH; j++) {
            if (j % 10 == 0) strMAP.append((char) jjPosAlphabet++);
            else strMAP.append(" ");
        }

        strMAP.append("\n");

        strMAP.append("   ");
        for (int j = 0; j < Map.WIDTH; j++) {
            strMAP.append(jjPosNumber++);
            if (jjPosNumber >= 10) jjPosNumber = 0;
        }

        strMAP.append("\n");

        int iiPosAlphabet = 65;
        int iiPosNumber = 0;

        for (int i = 0; i < Map.HEIGHT; i++) {

            if (i % 10 == 0) strMAP.append((char) iiPosAlphabet++);
            else strMAP.append(" ");

            strMAP.append(iiPosNumber++);
            if (iiPosNumber >= 10) iiPosNumber = 0;

            strMAP.append(" ");

            for (int j = 0; j < Map.WIDTH; j++) {
                if(i==startRow && j==startCol) strMAP.append(START);
                else if(i==endRow && j == endCol) strMAP.append(FINISH);

                //TODO BUG KALO GAK NEMU PATH PASTI ARRAYNYA 0 SEHINGGA YAUDAH 0 TRUE TEMBOK UJUNG 0
                else if(i == 0 && j == 0) strMAP.append(super.get(i, j));
                else if(wasHere(i,j,shorthestPath)) strMAP.append('.');
                else strMAP.append(super.get(i, j));
            }
            strMAP.append("\n");
        }

        return strMAP.toString();
    }
}

