package tp.ddp2.transportation;

import tp.ddp2.geo.Point;

/**
 * Created by wisnuprama on 4/18/2017.
 */
public interface Transportation {
    final int PERKILOMETER = 1;
    void move(double jarak);
}
