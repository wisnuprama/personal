package tp.ddp2.gui;

import tp.ddp2.Main;
import tp.ddp2.geo.*;
import tp.ddp2.geo.Point;
import tp.ddp2.owjek.OwjekExclusive;
import tp.ddp2.owjek.OwjekRegular;
import tp.ddp2.owjek.OwjekSporty;
import tp.ddp2.owjek.OwjekType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by wisnuprama on 4/26/2017.
 *
 */
public class Ngojek extends JFrame {
    private static final int WINDOWS_WIDTH = Map.WIDTH + 300;
    private static final int WINDOWS_HEIGHT = Map.HEIGHT + 300;

    private JButton go;
    private JRadioButton regJek, sportJek, excJek;
    private JLabel startLabel, destLabel;
    private JTextField startTextField, destTextField;
    private JPanel radiobuttonPanel, userInput, panelButton, containerAll;
    private JTextArea summary;
    private int radioChooser = -1;
    private GPS gps;

    public Ngojek() throws HeadlessException {
        gps = new GPS();
        setSize(WINDOWS_WIDTH, WINDOWS_HEIGHT);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        createButton();
        createLabel();
        createtextField();
        createRadioButton();
        createTextArea();
        createPanel();
        setVisible(true);
    }

    private void createTextArea(){

        summary = new JTextArea();
        summary.setSize(1000, 1000);
    }

    private void createButton(){
        this.go = new JButton("GO!");
        this.go.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String loc1 = startTextField.getText();
                String loc2 = destTextField.getText();
                System.out.println(loc1 + " " + loc2);
                Point st = Main.convert(loc1);
                Point dt = Main.convert(loc2);

                if(st == null || dt == null)
                    JOptionPane.showMessageDialog(null, st.toString() + " " + dt.toString());

                OwjekType owjek;

                if(radioChooser == 1){
                    owjek = new OwjekSporty(gps);
                } else if(radioChooser == 2){
                    owjek = new OwjekExclusive(gps);
                } else {
                    owjek = new OwjekRegular(gps);
                }

                owjek.owRide(st, dt);
                summary.setText(owjek.summary());
                summary.setEditable(false);

                JFrame x = new JFrame();
                x.add(summary);
                x.setSize(1000,1000);
                x.setVisible(true);
            }
        });
    }

    private void createLabel(){
        this.startLabel = new JLabel("From :");
        this.destLabel = new JLabel("To :");
    }

    private static final int LONG_TEXT_FIELD_LOC = 4;
    private void createtextField(){
        this.startTextField = new JTextField(LONG_TEXT_FIELD_LOC);
        this.destTextField = new JTextField(LONG_TEXT_FIELD_LOC);
    }

    private void createRadioButton(){

        class setRadiobuttonValue implements ActionListener {
            int choose;
            public setRadiobuttonValue(int x) {
                this.choose = x;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                radioChooser = this.choose;
            }
        }

        regJek = new JRadioButton(OwjekRegular.TYPE);
        regJek.addActionListener(new setRadiobuttonValue(0));
        sportJek = new JRadioButton(OwjekSporty.TYPE);
        sportJek.addActionListener(new setRadiobuttonValue(1));
        excJek = new JRadioButton(OwjekExclusive.TYPE);
        excJek.addActionListener(new setRadiobuttonValue(2));
    }

    private void createPanel(){
        radiobuttonPanel = new JPanel(new GridLayout(1,3));
        radiobuttonPanel.add(regJek);
        radiobuttonPanel.add(sportJek);
        radiobuttonPanel.add(excJek);

        userInput = new JPanel(new GridLayout(2,2));
        userInput.add(startLabel);
        userInput.add(startTextField);
        userInput.add(destLabel);
        userInput.add(destTextField);

        panelButton = new JPanel(new BorderLayout());
        panelButton.add(go);

        containerAll = new JPanel(new GridLayout(3,1));
        containerAll.add(radiobuttonPanel);
        containerAll.add(userInput);
        containerAll.add(panelButton);

        this.add(containerAll);
    }

    public static void main(String[] args){
        try {
            // Set System L&F
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (UnsupportedLookAndFeelException e) {
            // handle exception
        }
        catch (ClassNotFoundException e) {
            // handle exception
        }
        catch (InstantiationException e) {
            // handle exception
        }
        catch (IllegalAccessException e) {
            // handle exception
        }

        Ngojek x = new Ngojek();
    }

}
