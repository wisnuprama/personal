package tp.ddp2;

import tp.ddp2.geo.GPS;
import tp.ddp2.geo.Point;
import tp.ddp2.owjek.*;
import tp.ddp2.util.Transaction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
	// write your code here
        boolean FLAG = true;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String parse[];
        String line;
        ArrayList<Transaction> transactions = new ArrayList<>();

        GPS gps = new GPS();
        OwjekType owjekBang[] ={new OwjekRegular(gps),
                                    new OwjekSporty(gps),
                                    new OwjekExclusive(gps) };

        try{
            System.out.println("SIMULASI OWJEK PACIL");
            System.out.print(">> ");
            while(FLAG && (line = br.readLine()) != null)
            {

                try{
                    parse = line.split(" ");
                    if (parse.length == 2 && "show".equalsIgnoreCase(parse[0])
                                            && "map".equalsIgnoreCase(parse[1])) {
                        System.out.println(gps.print());
                    }
                    else if (parse.length == 8
                            && "go".equalsIgnoreCase(parse[0]) && "from".equalsIgnoreCase(parse[1])
                            && "to".equalsIgnoreCase(parse[3]) && "with".equalsIgnoreCase(parse[5])
                            && AbstractOwjek.TYPE.equalsIgnoreCase(parse[6]))
                    {
                        boolean FLAG_OWRIDE = true;

                        Point start = convert(parse[2]);
                        Point dest = convert(parse[4]);

                        // IF NULL STOP
                        if(start == null || dest == null){
                            FLAG_OWRIDE = false;
                            LOGGER.log(Level.WARNING, "");
                        }

                        boolean st = true, dt = true;
                        st = gps.isStreet(start);
                        dt = gps.isStreet(dest);

                        if(!st) {
                            System.out.println("input tidak valid: " + parse[2] + " bukan jalan");
                            continue;
                        }

                        if(!dt) {
                            System.out.println("input tidak valid: " + parse[4] + " bukan jalan");
                            continue;
                        }

                        if(start != null && dest != null && start.equals(dest)){
                            LOGGER.log(Level.INFO, "your position and destination is at the same coordinate.");
                            continue;
                        }

                        OwjekType owjekType = null;

                        if(FLAG_OWRIDE){
                            switch (parse[7].toUpperCase()){
                                case "REGULAR":
                                    owjekType = owjekBang[0];
                                    break;
                                case "SPORTY":
                                    owjekType = owjekBang[1];
                                    break;
                                case "EXCLUSIVE":
                                    owjekType = owjekBang[2];
                                    break;
                                default:
                                    LOGGER.log(Level.INFO, "tidak ditemukan tipe owjek");
                            }

                            if(owjekType != null){
                                owjekType.owRide(start, dest);
                                System.out.println(owjekType.summary());
                            } else {
                                LOGGER.log(Level.WARNING, "tipe Owjek is " + owjekType);
                            }
                        }

                    } else if("EXIT".equalsIgnoreCase(parse[0])){
                        FLAG = false;
                        return;

                    } else {
                        LOGGER.log(Level.INFO, "unknown input for \"" + line + "\"");
                    }

                    System.out.print(">> ");

                } catch (ArrayIndexOutOfBoundsException ee){
                    LOGGER.log(Level.WARNING, "Error input at index "+ ee.getMessage());
                }
            }

        } catch (IOException e){ LOGGER.log(Level.WARNING, "I/O: "+br.toString(), e); }

        return;
    }

    public static Point convert(String str){
        str = str.toUpperCase();
        boolean rxcy;
        int cPOS = 0;
        List<Character> colChars = new ArrayList<>(Arrays.asList(GPS.COL));

        for(int i=1; i < str.length(); ++i){
            char at = str.charAt(i);
            if(colChars.contains(at)){
                cPOS = i;
                break;
            }
        }

        char r=str.charAt(0),c=str.charAt(cPOS);
        int x=Integer.MAX_VALUE, y=Integer.MAX_VALUE;

        if(Character.isAlphabetic(r) && Character.isAlphabetic(c)) rxcy = true;
        else rxcy = false;

        if(rxcy){
            try{
                // parsing numeric
                x = Integer.parseInt(str.substring(1, cPOS));
                y = Integer.parseInt(str.substring(cPOS+1));

                return GPS.geoLoc(r,x,c,y);

            } catch (NumberFormatException e){
                LOGGER.log(Level.WARNING, "unknown location format! [X:" + x + ",Y:"+y+']');
            }
        } else{
            LOGGER.log(Level.WARNING, "unknown location format! [R:"+r+",C:"+c+']');
        }

        return null;
    }

    public static String convertHarga(double x){
        final String lastComma = ",00";

        int c = (int) x;
        String str = Integer.toString(c);

        if(str.length() <= 3)
            return str + lastComma;

        for(int i=str.length(), j = 0; i >= 0; --i){
            if(j % 3 == 0 && j != 0){
                str = str.substring(0, i) + '.' + str.substring(i);
            }
            j++;
        }

        return str+lastComma;
    }
}
