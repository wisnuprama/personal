package tp.ddp2.owjek;

import tp.ddp2.geo.GPS;
import tp.ddp2.geo.Point;
import tp.ddp2.transportation.Motor;

/**
 * Created by wisnuprama on 4/20/2017.
 * Merupakan class abstrak yang mengadakan salah satu implementasi dari interface OwjekType dan yang menjadi
 * dasar owjek bergerak. Untuk implementasi, programmer hanya harus mengextend class ini ke dalam class baru.
 * Class ini membutuhkan GPS untuk bekerja yang berguna untuk membantu owjek untuk mencari jalan.
 */
public abstract class AbstractOwjek
extends Motor {
    public static final String TYPE = "OW-JEK";
    protected GPS gps;

    public AbstractOwjek(GPS gps) {
        this.gps = gps;
    }

    /**
     * Mereset jarak tempuh owjek untuk digunakan di saat trip baru.
     */
    protected void reset(){
        this.setJarakTempuh(0);
    }

    /**
     * Ketika owjek membutuhkan jalan untuk dua lokasi, owjek memanggil method ini yang akan
     * mengembalikan nilai jarak dari dua lokasi sekaligus menset jalan terdekat.
     * @param start lokasi awal
     * @param to lokasi tujuan
     * @return mengembalikan jarak dua lokasi
     */
    private double cariJalan(Point start, Point to) {
        gps.findPath(start, to);
        return gps.getShorthestLength();
    }

    /**
     * Method overload super.move() dari class Motor. Method ini akan menggerakan owjek dari lokasi satu, ke lokasi
     * tujuan.
     * @param start lokasi awal
     * @param to lokasi tujuan
     */
    public void move(Point start, Point to) {
        double jarak = cariJalan(start, to);
        super.move(jarak);
    }

    /**
     * Method untuk trip owjek.
     * @param start lokasi awal
     * @param to lokasi tujuan
     */
    public void owRide(Point start, Point to) {
        this.reset();
        this.move(start, to);
    }
}
