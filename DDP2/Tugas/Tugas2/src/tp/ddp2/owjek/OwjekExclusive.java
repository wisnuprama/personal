package tp.ddp2.owjek;

import tp.ddp2.Main;
import tp.ddp2.geo.GPS;
import tp.ddp2.geo.Point;

/**
 * Created by wisnuprama on 4/20/2017.
 */
public class OwjekExclusive extends AbstractOwjek implements OwjekTypeInsurance {
    public static final String TYPE = "Exclusive";
    public static final int MIN_YEAR = 2016;
    public static final int MIN_TOPSPEED = 0;
    public static final int MIN_CC = 500;
    public static final double COST_PER_KM = 5000;
    public static final double MIN_KM = 0;
    public static final double FIXED_COST = 10000;
    public static final double PROMO = 0.5;
    public static final double MIN_PROMO_KM = 0;
    public static final double PROTECTION_COST = 0.05;

    public OwjekExclusive(GPS gps) {
        super(gps);
    }

    /**
     * Mengembalikan harga bersih dari sebuah trip
     * @param jarak jarak dua lokasi
     * @return harga bersih
     */
    @Override
    public double getTotalCost(double jarak) {
        if(jarak <= 0)
            return 0;

        return this.getCost(jarak) - this.getPromo(jarak) + this.getProtectionCost(jarak);
    }

    /**
     * Mengembalikan harga kotor dari sebuah trip
     * @param jarak jarak dua lokasi
     * @return harga kotor
     */
    @Override
    public double getCost(double jarak) {
        if(jarak > MIN_KM)
            jarak-= MIN_KM;
        return jarak * COST_PER_KM + FIXED_COST;
    }

    /**
     * Mengembalikan nilai protection cost yang didapat dari sebuah trip
     * @param jarak jarak dua lokasi
     * @return protection cost
     */
    @Override
    public double getProtectionCost(double jarak){
        return (this.getCost(jarak) - this.getPromo(jarak)) * PROTECTION_COST;
    }

    /**
     * Mengembalikan nilai promo yang didapat
     * @param jarak jarak dua lokasi
     * @return promo
     */
    @Override
    public double getPromo(double jarak) {
        return PROMO * this.getCost(jarak);
    }

    /**
     * Mengembalikan String summary dari sebuah trip
     * @return trip's summary
     */
    @Override
    public String summary() {
        StringBuilder str = new StringBuilder();
        double jarak = this.getJarakTempuh();

        str.append(gps.showPath());
        str.append("Terimakasih telah melakukan perjalanan dengan OW-JEK.\n");
        str.append(String.format("[Jarak] %.1f KM\n", jarak).replace('.',','));
        str.append("[TipeO] " + TYPE + "\n");
        str.append(String.format("[Fixed] %s (+)\n", Main.convertHarga(FIXED_COST)));
        str.append(String.format("[KMSel] %s (+)\n", Main.convertHarga(this.getCost(jarak))));
        str.append(String.format("[Promo] %s (-)\n", Main.convertHarga(this.getPromo(jarak))));
        str.append(String.format("[Prtks] %s (+)\n", Main.convertHarga(this.getProtectionCost(jarak))));
        str.append(String.format("[Total] %s \n", Main.convertHarga(this.getTotalCost(jarak))));

        return str.toString();
    }
}
