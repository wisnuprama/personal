package tp.ddp2.tugas3.gui.fx.pane;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import tp.ddp2.tugas3.gui.Initializable;
import tp.ddp2.tugas3.gui.fx.OwjekApplication;
import tp.ddp2.tugas3.util.Transaction;
import java.util.List;

/**
 * Created by Wisnu Pramadhitya Ramadhan
 * on 5/17/2017.
 * File: TransactionPane.java
 *
 * Kelas pane yang digunakan untuk GUI penampung transaction card. Kelas ini
 * mengextend VBox Pane FX. Kelas dapat dihidupkan dan dapat dimasukan langsung ke dalam Pane utama.
 *
 * Kelas ini tidak didesign untuk diubah ketika runtime. Tetapi masih dapat ditambahkan
 * Node yang bisa disesuaikan.
 *
 * Kelas ini final dan tidak di design untuk extendable atau reuse.
 */
public final class TransactionPane
extends VBox
implements Initializable {

    // parent app
    private OwjekApplication app;

    // vbox dalam scrollpane
    private VBox vBoxCards;

    // Label title
    private Label transactionPaneTitle;

    // scroll pane yang berisi card2
    private ScrollPane scrollPane;

    // list dari transaksi
    private List<Transaction> transactionList;

    /**
     * Membuat aset dan nde
     * @param app
     * @param transactionList
     */
    public TransactionPane(OwjekApplication app, List<Transaction> transactionList) {
        this.app = app;
        this.transactionList = transactionList;
        initialize();
    }

    @Override
    public void initialize() {
        /*
            TransactionPane -|
                         ScrollPane -|
                                    VBox -|
                                         Card
         */
        setPrefWidth(210);
        setSpacing(10);
        setMargin(this, new Insets(10,10,10,10));
        setPadding(new Insets(10,10,10,10));

        // membuat node-node
        transactionPaneTitle = new Label("Your last transaction");
        transactionPaneTitle.setPadding(new Insets(0,20,10,20));
        scrollPane = new ScrollPane();
        scrollPane.setEffect(new InnerShadow(5  , Color.DARKGRAY));
        vBoxCards = new VBox();

        setAlignment(Pos.TOP_CENTER);
        getChildren().add(transactionPaneTitle);

        scrollPane.setContent(vBoxCards);
        scrollPane.setPrefHeight(320);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVvalue(1.0);

        getChildren().add(scrollPane);
    }

    /**
     * menambahkan transaksi perjalanan
     * @param tr transaksi
     */
    public void addTransaction(Transaction tr){
        // membuat kartu transaksi
        TransactionCard trCard = new TransactionCard(tr);

        // set margin sehingga rapih
        setMargin(trCard, new Insets(10,10,10,10));

        // add
        transactionList.add(tr);
        vBoxCards.getChildren().add(trCard);
    }
}
