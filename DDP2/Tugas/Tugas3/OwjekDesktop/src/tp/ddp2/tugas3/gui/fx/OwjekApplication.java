package tp.ddp2.tugas3.gui.fx;

/**
 * Created by Wisnu Pramadhitya R
 * on 5/14/2017.
 * File: OwjekApplication.java
 *
 * Aplikasi utama untuk menjalankan program (MAIN)
 */

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import tp.ddp2.tugas3.geo.GPS;
import tp.ddp2.tugas3.gui.fx.pane.*;
import tp.ddp2.tugas3.owjek.OwjekExclusive;
import tp.ddp2.tugas3.owjek.OwjekRegular;
import tp.ddp2.tugas3.owjek.OwjekSporty;
import tp.ddp2.tugas3.util.Transaction;
import java.util.ArrayList;
import java.util.List;

public final class OwjekApplication extends Application {

    // PANE
    private Stage primaryStage;
    private BorderPane mainPane;
    private Scene scene;

    private TitlePane titlePane;
    private OwjekPane owjekPane;
    private TransactionPane transactionPane;
    private MapPane mapPane;
    private TabPane tabPane;

    // APP
    private final List<Transaction> transactionList = new ArrayList<>();
    private final GPS gps = new GPS();
    private final OwjekRegular owReg = new OwjekRegular(gps);
    private final OwjekSporty owSpo = new OwjekSporty(gps);
    private final OwjekExclusive owExc = new OwjekExclusive(gps);

    public static final int WINDOW_WIDTH = 1000;
    public static final int WINDOW_HEIGHT = 500;
    public static final Font DEFAULT_TITLE_FONT = Font.font("", FontWeight.EXTRA_BOLD, 30);
    public static final Font DEFAULT_BIG_BUTTON_FONT = Font.font("", FontWeight.BOLD, 15);

    public static final String IMG_LOGO = "file:///../assets/logo.png";
    public static final String IMG_OJEG_REG = "file:///../assets/ojeg_reg.png";
    public static final String IMG_OJEG_SPO = "file:///../assets/ojeg_spo.png";
    public static final String IMG_OJEG_EXC = "file:///../assets/ojeg_exc.png";
    public static final String IMG_PIN_END = "file:///../assets/redpin.png";
    public static final String IMG_PIN_START = "file:///../assets/bluepin.png";

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        primaryStage.setTitle("Owjek");
        //@TODO CSS FOR STYLE
        mainPane = new BorderPane();
        titlePane = new TitlePane(this, "OWJEK");
        owjekPane = new OwjekPane(this, gps, owReg, owSpo, owExc);
        transactionPane = new TransactionPane(this, transactionList);
        mapPane = new MapPane(this, gps);
        tabPane = new TabPane();

        create();

        scene = new Scene(mainPane, WINDOW_WIDTH, WINDOW_HEIGHT);

        WelcomeScreen welcome = new WelcomeScreen(this);
        Scene scene2 = new Scene(welcome, WINDOW_WIDTH, WINDOW_HEIGHT);

        primaryStage.setMinWidth(WINDOW_WIDTH);
        primaryStage.setMinHeight(WINDOW_HEIGHT);
        primaryStage.getIcons().add(new Image(IMG_LOGO));
        primaryStage.setResizable(true);
        primaryStage.setScene(scene2);
        primaryStage.show();
    }

    public void setToPrimaryScene(){
        primaryStage.setScene(scene);
    }

    private void create() {


        mainPane.setTop(titlePane);
//        mainPane.setRight(owjekPane);
        //      mainPane.setLeft(transactionPane);
        mainPane.setCenter(mapPane);


        tabPane.setPrefWidth(250);
        tabPane.setEffect(new DropShadow(2,0,0, Color.DARKGRAY));
        tabPane.setBorder(new Border(new BorderStroke(Color.LIGHTGRAY,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

        // set tab policy: PERMANENT TAB
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

        Tab tab = new Tab();
        tab.setText("OwRide");
        tab.setContent(owjekPane);
        Tab tab2 = new Tab();
        tab2.setText("Transaction");
        tab2.setContent(transactionPane);
        tabPane.getTabs().addAll(tab, tab2);


        mainPane.setLeft(tabPane);
    }

    public GPS getGps() {
        return gps;
    }

    public OwjekRegular getOwReg() {
        return owReg;
    }

    public OwjekSporty getOwSpo() {
        return owSpo;
    }

    public OwjekExclusive getOwExc() {
        return owExc;
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public TitlePane getTitlePane() {
        return titlePane;
    }

    public OwjekPane getOwjekPane() {
        return owjekPane;
    }

    public TransactionPane getTransactionPane() {
        return transactionPane;
    }

    public MapPane getMapPane() {
        return mapPane;
    }

    public TabPane getTabPane() {
        return tabPane;
    }

    public BorderPane getMainPane() {
        return mainPane;
    }
}
