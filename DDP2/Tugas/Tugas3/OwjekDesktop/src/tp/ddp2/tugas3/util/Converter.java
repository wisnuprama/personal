package tp.ddp2.tugas3.util;

import tp.ddp2.tugas3.geo.Point;
import tp.ddp2.tugas3.geo.GPS;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Wisnu Pramadhitya R
 * on 5/10/2017.
 * File: Converter.java
 *
 * Kelas utilitas yang digunakan untuk membatu conversi beberapa hal seperti
 *
 * String place ke point
 * Point ke place
 * Standariasi harga ke dalam bentuk String
 *
 * Kelas ini final dan tidak di design untuk extendable atau reuse.
 */
public final class Converter {

    /**
     * Mengkonversi point ke string place format RYCX
     * @param p point yang ingin dikonseversi
     * @return String place
     */
    public static String toPlace(Point p) {
        int puluhanR = p.getY() / 10;
        int puluhanC = p.getX() / 10;
        int satuanR = p.getY() % 10;
        int satuanC = p.getX() % 10;

        return GPS.ROW[puluhanR] + "" + satuanR + "" + GPS.COL[puluhanC] + "" + satuanC;
    }

    /**
     * Mengkonversi String place menjadi Point
     * @param str place
     * @return Point point hasil konversi
     * @throws NumberFormatException
     */
    public static Point toPoint(String str) throws NumberFormatException {
        str = str.toUpperCase();
        boolean rycx;
        int cPOS = 0;
        List<Character> colChars = new ArrayList<>(Arrays.asList(GPS.COL));

        for(int i=1; i < str.length(); ++i){
            char at = str.charAt(i);
            if(colChars.contains(at)){
                cPOS = i;
                break;
            }
        }

        char r=str.charAt(0),c=str.charAt(cPOS);
        int x=Integer.MAX_VALUE, y=Integer.MAX_VALUE;

        if(Character.isAlphabetic(r) && Character.isAlphabetic(c) && cPOS >= 1) rycx = true;
        else rycx = false;

        if(rycx) {
            // parsing numeric
            y = Integer.parseInt(str.substring(1, cPOS));
            x = Integer.parseInt(str.substring(cPOS + 1));

            return GPS.geoLoc(r, y, c, x);
        }

        throw new NumberFormatException(str);

    }

    /**
     * Mengkonversi harga dari format double menjadi format rupiah
     * @param x nilai uang
     * @return String yang telah berisi nilai yang telah di format
     */
    public static String toStandardCurrencyFormat(double x){
        final String lastComma = ",00";

        int c = (int) x;
        String str = Integer.toString(c);

        if(str.length() <= 3)
            return str + lastComma;

        for(int i=str.length(), j = 0; i >= 0; --i){
            if(j % 3 == 0 && j != 0){
                str = str.substring(0, i) + '.' + str.substring(i);
            }
            j++;
        }

        return str+lastComma;
    }
}
