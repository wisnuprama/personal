package tp.ddp2.tugas3.gui.fx;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.scene.text.Font;
import tp.ddp2.tugas3.gui.Initializable;


/**
 * Created by wisnuprama on 5/21/2017.
 */
public class WelcomeScreen
extends VBox
implements Initializable {

    private OwjekApplication app;
    private TextFlow textFlow;
    private Button button1;
    private Label footer;
    private static final Font FONT = Font.font("Segoe", FontWeight.BOLD, 45);
    private static final Font FONT_BUTTON = Font.font("Segoe", FontWeight.BOLD, 20);
    private static final Font FONT2 = Font.font("Segoe", FontWeight.BOLD, 65);
    private static final Font FONT3 = Font.font("Segoe", FontWeight.NORMAL, 12);
    private static final Image BACKGROUND = new Image("file:///../assets/background.png");

    public WelcomeScreen(OwjekApplication app) {
        this.app = app;
        initialize();
    }

    @Override
    public void initialize() {
        setPadding(new Insets(50,50,50,50));

        Text welcome = new Text("Welcome to\n");
        Text text = new Text("your beloved ojek\n");
        Text owjek = new Text("OWJEK");

        welcome.setFont(FONT);
        welcome.setFill(Color.WHITE);
        text.setFont(FONT);
        text.setFill(Color.WHITE);
        owjek.setFont(FONT2);
        owjek.setFill(Color.LIGHTGREEN);

        button1 = new Button("Let's go");
        button1.setFont(FONT_BUTTON);
        button1.setPrefSize(150,50);
        button1.setOpacity(0.5);
        button1.setOpaqueInsets(new Insets(5,5,5,5));
        button1.setBorder(new Border(new BorderStroke(Color.DARKGRAY,
                BorderStrokeStyle.SOLID, new CornerRadii(5), BorderWidths.DEFAULT)));
        button1.setEffect(new DropShadow(3, 0, 0, Color.DARKGRAY));
        setMargin(button1, new Insets(100,0,0,0));
        button1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                app.setToPrimaryScene();
            }
        });

        textFlow = new TextFlow(welcome, text, owjek);
        textFlow.setEffect(new DropShadow(5, 1,1, Color.BLACK));

        setAlignment(Pos.CENTER);
        setBackground(new Background(
                new BackgroundImage(BACKGROUND, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                        BackgroundPosition.CENTER, BackgroundSize.DEFAULT)
        ));

        footer = new Label("Wisnu Pramadhitya Ramadhan / 1606918055 / wisnu.pramadhitya@ui.ac.id");
        footer.setFont(FONT3);
        footer.setTextFill(Color.WHITE);
        setMargin(footer, new Insets(100,0,0,0));
        getChildren().addAll(textFlow, button1, footer);
    }
}
