package tp.ddp2.tugas3.gui.fx.pane;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import tp.ddp2.tugas3.geo.GPS;
import tp.ddp2.tugas3.geo.Point;
import tp.ddp2.tugas3.gui.Initializable;
import tp.ddp2.tugas3.gui.fx.OwjekApplication;
import tp.ddp2.tugas3.gui.fx.PopupAlert;
import tp.ddp2.tugas3.owjek.OwjekExclusive;
import tp.ddp2.tugas3.owjek.OwjekRegular;
import tp.ddp2.tugas3.owjek.OwjekSporty;
import tp.ddp2.tugas3.util.Converter;
import tp.ddp2.tugas3.util.Transaction;
import tp.ddp2.tugas3.util.Trip;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Created by Wisnu Pramadhitya R
 * on 5/15/2017.
 * File: OwjekPane.java
 *
 * Kelas pane GUI yang di dalamnya terdapat node-node fungsionalitas untuk
 * aplikasi OWJEK.
 *
 * Kelas ini tidak didesign untuk diubah ketika runtime. Tetapi masih dapat ditambahkan
 * Node yang bisa disesuaikan.
 *
 * Kelas ini final dan tidak di design untuk extendable atau reuse.
 *
 */
public final class OwjekPane
extends VBox
implements Initializable {
    // OWJEK PANE
    private OwjekApplication app;
    private Label owjekLabel;
    private ComboBox<String> owjekComboBox;
    private Label startLocLabel;
    private Label endLocLabel;
    private TextField startLocInput;
    private TextField endLocInput;
    private Button goButton;

    //
    private GPS gps;
    private OwjekRegular owReg;
    private OwjekSporty owSpo;
    private OwjekExclusive owExc;

    // LABEL untuk COMBOBOX
    private static final ObservableList<String> OPTIONS = FXCollections.observableArrayList(
            OwjekRegular.TYPE,
            OwjekSporty.TYPE,
            OwjekExclusive.TYPE
    );

    /**
     * Membuat pane untuk fungsionalitas owjek application
     * @param app aplikasi utama
     * @param gps GPS
     * @param owReg owjek regular
     * @param owSpo owjek sporty
     * @param owExc owjek exclusive
     */
    public OwjekPane(OwjekApplication app, GPS gps, OwjekRegular owReg,
                     OwjekSporty owSpo, OwjekExclusive owExc) {
        this.app = app;
        this.gps = gps;
        this.owReg = owReg;
        this.owSpo = owSpo;
        this.owExc = owExc;
        initialize();
    }

    // set default option untuk combobox
    private static final int DEFAULT_OPTION = 0;

    @Override
    public void initialize() {
        final int SPACING = 10;
        setMargin(this, new Insets(10,10,10,10));
        setSpacing(SPACING);
        setPadding(new Insets(SPACING, SPACING, SPACING, SPACING));

        owjekLabel = new Label("Choose Owjek:");

        owjekComboBox = new ComboBox<String>(OPTIONS);
        owjekComboBox.setPrefWidth(250);
        owjekComboBox.setValue(OPTIONS.get(DEFAULT_OPTION));

        // Textfield
        startLocInput = new TextField();
        endLocInput = new TextField();

        // Label
        startLocLabel = new Label("From:");
        startLocLabel.setLabelFor(startLocInput);
        endLocLabel = new Label("Destination:");
        endLocLabel.setLabelFor(endLocInput);

        // Button
        goButton = new Button("Order Now");
        setMargin(goButton, new Insets(90, 0,0,0));
        goButton.setPrefSize(250, 80);
        goButton.setFont(OwjekApplication.DEFAULT_BIG_BUTTON_FONT);

        getChildren().addAll(
                owjekLabel,
                owjekComboBox,
                startLocLabel,
                startLocInput,
                endLocLabel,
                endLocInput,
                goButton);

        // BUTTON's ACTION
        goButton.setOnAction(new EventHandler<ActionEvent>() {

            String typeOjek;
            String startLoc;
            String endLoc;

            @Override
            public void handle(ActionEvent event) {
                boolean granted = false;

                Optional<ButtonType> result = PopupAlert.
                                                showConfirmationAlert("Continue the order?");
                if(result.isPresent() && result.get() == ButtonType.CANCEL){
                    PopupAlert.showInformationAlert("Order canceled.");
                    return;
                }

                // get data
                typeOjek = owjekComboBox.getSelectionModel().getSelectedItem();
                startLoc = startLocInput.getText();
                endLoc = endLocInput.getText();

                if(startLoc.isEmpty()){
                    PopupAlert.showWarningAlert("Your location is empty.\n" +
                                                "Please, provide your location.");
                    return;
                }

                if(endLoc.isEmpty()){
                    PopupAlert.showWarningAlert("Your destination is empty.\n" +
                                                "Please provide your destination.");
                    return;
                }

                if(startLoc.length() > 4 || endLoc.length() > 4){
                    PopupAlert.showWarningAlert("Location is unknown.\n" +
                                                "Please, try again!");
                    return;
                }

                Point startLocPoint, endLocPoint;

                try {
                    startLocPoint = Converter.toPoint(startLoc);
                    endLocPoint = Converter.toPoint(endLoc);
                } catch (NumberFormatException e){
                    PopupAlert.showWarningAlert("Location is unknown.\n" +
                            "Please, try again!");
                    return;
                }

                if(!gps.isStreet(startLocPoint) || !gps.isStreet(endLocPoint)){
                    PopupAlert.showWarningAlert("The location can not be visited.\n" +
                                                "Please, try again.");
                    return;
                }

                if(startLocPoint.equals(endLocPoint)){
                    PopupAlert.showWarningAlert("Your location and destination are the same.\n" +
                            "Sorry for the inconvenience, we can not make your trip.\n"+
                            "Thank you.");
                    return;
                }

                processOrder(startLocPoint, endLocPoint);
            }

            void processOrder(Point start, Point end){
                StringBuilder message = new StringBuilder();
                double jarak;
                Transaction tr;

                Trip<String, String> trip = new Trip<>(
                        startLoc.toUpperCase(),
                        endLoc.toUpperCase());

                if(OwjekRegular.TYPE.equals(typeOjek)){
                    owReg.owRide(start, end);
                    message.append(owReg.summary());

                    jarak = owReg.getJarakTempuh();
                    tr = new Transaction(LocalDateTime.now(),
                            OwjekRegular.TYPE,
                            trip,
                            jarak,
                            owReg.getTotalCost(jarak));

                } else if(OwjekSporty.TYPE.equals(typeOjek)){
                    owSpo.owRide(start, end);
                    message.append(owSpo.summary());

                    jarak = owSpo.getJarakTempuh();
                    tr = new Transaction(LocalDateTime.now(),
                            OwjekSporty.TYPE,
                            trip,
                            jarak,
                            owSpo.getTotalCost(jarak));

                } else {
                    owExc.owRide(start, end);
                    message.append(owExc.summary());

                    jarak = owExc.getJarakTempuh();
                    tr = new Transaction(LocalDateTime.now(),
                            OwjekExclusive.TYPE,
                            trip,
                            jarak,
                            owSpo.getTotalCost(jarak));
                }

                /*
                    alert user jika perjalanan sudah selesai
                    menambahkan ke transaction
                 */


                Optional<ButtonType> result = PopupAlert.showConfirmationAlert("Owjek telah ditemukan.\n" +
                                                "Tekan OK untuk melanjutkan.");

                if(result.get().equals(ButtonType.OK)){
                    app.getMapPane().moveOwjek(typeOjek, gps.getShorthestPath());
                    app.getTransactionPane().addTransaction(tr);
                    result = PopupAlert.showInformationAlert(message.toString());
                    resetAll();
                }

            }
        });
    }

    /**
     * Reset textfield dan combobox
     */
    public void resetAll(){
        resetTextField();
        owjekComboBox.setValue(OPTIONS.get(DEFAULT_OPTION));
    }

    public void resetTextField(){
        startLocInput.setText("");
        endLocInput.setText("");
    }

    public TextField getEndLocInput() {
        return endLocInput;
    }

    public TextField getStartLocInput() {
        return startLocInput;
    }
}
