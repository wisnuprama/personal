package tp.ddp2.tugas3.gui.fx.pane;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.effect.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import tp.ddp2.tugas3.gui.Initializable;
import tp.ddp2.tugas3.gui.fx.OwjekApplication;

/**
 * Created by Wisnu Pramadhitya R
 * on 5/15/2017.
 * File: TitlePane.java
 *
 * Pane yang berisi header aplikasi termasuk title dan logo. Merupakan
 * extend dari HBox FX. Ketika dihidupkan langsung terinisiasi.
 *
 * Kelas ini tidak didesign untuk diubah ketika runtime. Kelas ini final
 * dan tidak di design untuk extendable atau reuse.
 */
public final class TitlePane
extends HBox
implements Initializable {

    private OwjekApplication app;
    private Label titleApp;
    private ImageView image;
    private static final String imageLogo = "";

    public String title;

    /**
     * Membuat title sesuai title yang diberikan
     * @param app aplikasi utama
     * @param title string title
     */
    public TitlePane(OwjekApplication app, String title) {
        this.title = title;
        this.app = app;
        initialize();
    }

    @Override
    public void initialize() {
        setPadding(new Insets(10,0,10,50));

        // set background menjadi hijau muda
        setBackground(new Background(
                new BackgroundFill(Color.LIGHTGREEN, CornerRadii.EMPTY, Insets.EMPTY)
            )
        );

        // set alignment
        setAlignment(Pos.CENTER);
        // set effect
        setTitlePaneEffect();

        titleApp = new Label(title);
        setMargin(titleApp, new Insets(5,50,5,5));
        titleApp.setFont(OwjekApplication.DEFAULT_TITLE_FONT);
        image = new ImageView(new Image(OwjekApplication.IMG_LOGO));
        image.setCache(true);
        image.setFitHeight(50);
        image.setFitWidth(50);
        getChildren().addAll(image, titleApp);
    }

    public void setTitlePaneEffect(){
        setEffect(new DropShadow(10,5,0,Color.DARKGRAY));
    }


}
