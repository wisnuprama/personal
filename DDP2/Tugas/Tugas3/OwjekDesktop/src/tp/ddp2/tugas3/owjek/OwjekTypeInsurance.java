package tp.ddp2.tugas3.owjek;


/**
 * Created by wisnuprama on 4/30/2017.
 * Interface yang extensi dari OwjekType dimana interface ini menambahkan
 * method untuk protection cost dimana di Owjek Regular tidak ada.
 */
public interface OwjekTypeInsurance extends OwjekType {

    /**
     * Mengembalikan nilai promo yang didapat
     * @param jarak jarak dua lokasi
     * @return promo
     */
    @Override
    double getPromo(double jarak);

    /**
     * Mengembalikan harga kotor
     * @param jarak jarak dua lokasi
     * @return harga kotor
     */
    @Override
    double getCost(double jarak);

    /**
     * Mengembalikan harga bersih
     * @param jarak jarak dua lokasi
     * @return harga bersih
     */
    @Override
    double getTotalCost(double jarak);

    /**
     * Mengembalikan String summary dari sebuah trip
     * @return trip's summary
     */
    @Override
    String summary();

    /**
     * Mengembalikan nilai protection cost yang didapat dari sebuah trip
     * @param jarak jarak dua lokasi
     * @return protection cost
     */
    double getProtectionCost(double jarak);
}
