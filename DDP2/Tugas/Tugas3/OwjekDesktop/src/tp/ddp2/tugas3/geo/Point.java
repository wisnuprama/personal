package tp.ddp2.tugas3.geo;

/**
 * Created by wisnuprama on 4/24/2017.
 * Class yang mempresentasikan nilai sebuah koordinat sebuah lokasi.
 */
public class Point
implements Comparable<Point> {
    private int y;
    private int x;

    public Point(int y, int x){
        this.y = y;
        this.x = x;
    }

    /**
     * Mengembalikan nilai koordinat y
     * @return koordinat y
     */
    public int getY() {
        return y;
    }

    /**
     * Mengembalikan nilai koordinat x
     * @return koordinat x
     */
    public int getX() {
        return x;
    }

    /**
     * Mengembalikan String representasi sebuah Point
     * @return
     */
    @Override
    public String toString() {
        return "Point[" +
                "y=" + y +
                ", x=" + x +
                ']';
    }

    /**
     * Mengecek kesamaan dua koordinat
     * @param o Point lain
     * @return true jika koordinat dua Point sama, false sebaliknya
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        if (y != point.y) return false;
        return x == point.x;
    }

    @Override
    public int compareTo(Point o) {
        return 0;
    }
}
