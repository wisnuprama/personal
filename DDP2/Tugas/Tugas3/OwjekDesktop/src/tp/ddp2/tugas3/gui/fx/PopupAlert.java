package tp.ddp2.tugas3.gui.fx;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * Created by Wisnu Pramadhitya
 * on 5/20/2017.
 *
 * Membuat popup alert sesuai kebutuhan dan message
 */
public class PopupAlert {

    /**
     * Membuat popup alert: WARNING
     * @param message message yang ingin ditampilkan di popup
     * @return tombol yang dipilih user
     */
    public static Optional<ButtonType> showWarningAlert(String message){
        return new Alert(Alert.AlertType.WARNING, message,
                ButtonType.OK)
                .showAndWait();
    }

    /**
     * Membuat popup alert: INFORMATION
     * @param message message yang ingin ditampilkan di popup
     * @return tombol yang dipilih user
     */
    public static Optional<ButtonType> showInformationAlert(String message) {
        return new Alert(Alert.AlertType.INFORMATION, message,
                ButtonType.OK)
                .showAndWait();
    }

    /**
     * Membuat popup alert: CONFIRMATION
     * @param message message yang ingin ditampilkan di popup
     * @return tombol yang dipilih user
     */
    public static Optional<ButtonType> showConfirmationAlert(String message) {
        return new Alert(Alert.AlertType.CONFIRMATION, message,
                ButtonType.OK, ButtonType.CANCEL)
                .showAndWait();
    }


}
