package tp.ddp2.tugas3.gui;


/**
 * Created by Wisnu Pramadhitya R
 * on 5/10/2017.
 * File: Initializable.java
 *
 * Interface untuk setiap Node sehingga wajib untuk
 * menginisiasi spesifikasi Node.
 */
public interface Initializable {

    void initialize();

}
