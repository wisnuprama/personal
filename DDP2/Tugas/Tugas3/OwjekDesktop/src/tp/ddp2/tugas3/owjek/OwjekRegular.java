package tp.ddp2.tugas3.owjek;

import tp.ddp2.tugas3.geo.GPS;
import tp.ddp2.tugas3.util.Converter;

/**
 * Created by wisnuprama on 4/20/2017.
 */
public final class OwjekRegular
        extends AbstractOwjek
        implements OwjekType{

    public static final String TYPE = "Regular";
    public static final int MIN_YEAR = 2012;
    public static final int MIN_TOPSPEED = 0;
    public static final double COST_PER_KM = 1000;
    public static final double MIN_KM = 2.;
    public static final double FIRST_TWO_KM = 3000;
    public static final double PROMO = 0.4;
    public static final double MIN_PROMO_KM = 6.;

    public OwjekRegular(GPS gps) {
        super(gps);
    }

    /**
     * Mereset jarak tempuh owjek untuk digunakan di saat trip baru.
     */
    @Override
    protected void reset() {
        super.reset();
    }

    /**
     * Mengembalikan harga bersih dari sebuah trip
     * @param jarak jarak dua lokasi
     * @return harga bersih
     */
    @Override
    public double getTotalCost(double jarak) {
        if(jarak <= 0)
            return 0;

        return this.getCost(jarak) + OwjekRegular.FIRST_TWO_KM - this.getPromo(jarak);
    }

    /**
     * Mengembalikan harga kotor dari sebuah trip
     * @param jarak jarak dua lokasi
     * @return harga kotor
     */
    @Override
    public double getCost(double jarak) {
        if(jarak > MIN_KM)
            jarak-=OwjekRegular.MIN_KM;

        return jarak * OwjekRegular.COST_PER_KM;
    }

    /**
     * Method yang mengembalikan nilai harga promo dari jarak
     * @param jarak jarak dua lokasi
     * @return promo
     */
    @Override
    public double getPromo(double jarak) {
        if(jarak < MIN_PROMO_KM)
            return 0;
        return PROMO * (FIRST_TWO_KM + (MIN_PROMO_KM - MIN_KM) * COST_PER_KM);
    }

    /**
     * Mengembalikan String summary dari sebuah trip
     * @return trip's summary
     */
    @Override
    public String summary() {
        StringBuilder str = new StringBuilder();
        double jarak = this.getJarakTempuh();

        str.append("Terimakasih telah melakukan perjalanan dengan OW-JEK.\n");
        str.append(String.format("[Jarak] %.1f KM\n", jarak).replace('.',','));
        str.append("[TipeO] " + TYPE + "\n");
        str.append(String.format("[2KMPe] %s (+)\n", Converter.toStandardCurrencyFormat(FIRST_TWO_KM)));
        str.append(String.format("[KMSel] %s (+)\n", Converter.toStandardCurrencyFormat(this.getCost(jarak))));
        str.append(String.format("[Promo] %s (-)\n", Converter.toStandardCurrencyFormat(this.getPromo(jarak))));
        str.append(String.format("[Total] %s \n", Converter.toStandardCurrencyFormat(this.getTotalCost(jarak))));

        return str.toString();
    }
}
