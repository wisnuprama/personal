package tp.ddp2.tugas3.owjek;

import tp.ddp2.tugas3.geo.GPS;
import tp.ddp2.tugas3.util.Converter;

/**
 * Created by wisnuprama on 4/20/2017.
 * Class yang memrepresentasikan object Owjek yang memiliki motor sport.
 */
public final class OwjekSporty extends AbstractOwjek implements OwjekTypeInsurance{
    public static final String TYPE = "Sporty";
    public static final int MIN_YEAR = 2015;
    public static final int MIN_TOPSPEED = 140;
    public static final double COST_PER_KM = 3000;
    public static final double MIN_KM = 5.;
    public static final double FIRST_FIVE_KM = 20000;
    public static final double PROMO = 0.6;
    public static final double MIN_PROMO_KM = 8.;
    public static final double PROTECTION_COST = 0.1;

    public OwjekSporty(GPS gps) {
        super(gps);
    }

    /**
     * Mengembalikan harga bersih setelah dihitung dengan promo, protection cost, dan tarif dasar
     * @param jarak jarak dua lokasi
     * @return harga bersih
     */
    @Override
    public double getTotalCost(double jarak) {
        if(jarak <= 0)
            return 0;
        return this.getCost(jarak) + OwjekSporty.FIRST_FIVE_KM - this.getPromo(jarak) + this.getProtectionCost(jarak);
    }

    /**
     * Mengembalikan tarif kotor dari dua lokasi
     * @param jarak jarak dua lokasi
     * @return harga kotor
     */
    @Override
    public double getCost(double jarak) {
        if(jarak > MIN_KM)
            jarak-= MIN_KM;
        return jarak * COST_PER_KM;
    }

    /**
     * Mengembalikan harga protection cost yang sudah dihitung dengan promo dan harga dasar
     * @param jarak jarak dua lokasi
     * @return protection cost
     */
    public double getProtectionCost(double jarak){
        return (this.getCost(jarak) + FIRST_FIVE_KM - this.getPromo(jarak)) * PROTECTION_COST;
    }

    /**
     * Mengembalikan harga promo yang didapat
     * @param jarak jarak dua lokasi
     * @return promo
     */
    @Override
    public double getPromo(double jarak) {
        if(jarak < MIN_PROMO_KM)
            return 0;
        return PROMO * (FIRST_FIVE_KM + (MIN_PROMO_KM - MIN_KM) * COST_PER_KM);
    }

    /**
     * Mengembalikan String summary dari sebuah trip
     * @return trip's summary
     */
    @Override
    public String summary() {
        StringBuilder str = new StringBuilder();
        double jarak = this.getJarakTempuh();

        str.append("Terimakasih telah melakukan perjalanan dengan OW-JEK.\n");
        str.append(String.format("[Jarak] %.1f KM\n", jarak).replace('.',','));
        str.append("[TipeO] " + TYPE + "\n");
        str.append(String.format("[5KMPe] %s (+)\n", Converter.toStandardCurrencyFormat(FIRST_FIVE_KM)));
        str.append(String.format("[KMSel] %s (+)\n", Converter.toStandardCurrencyFormat(this.getCost(jarak))));
        str.append(String.format("[Promo] %s (-)\n", Converter.toStandardCurrencyFormat(this.getPromo(jarak))));
        str.append(String.format("[Prtks] %s (+)\n", Converter.toStandardCurrencyFormat(this.getProtectionCost(jarak))));
        str.append(String.format("[Total] %s \n", Converter.toStandardCurrencyFormat(this.getTotalCost(jarak))));

        return str.toString();
    }
}
