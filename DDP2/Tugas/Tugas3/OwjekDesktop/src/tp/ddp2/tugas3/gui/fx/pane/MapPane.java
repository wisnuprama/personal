package tp.ddp2.tugas3.gui.fx.pane;

import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import tp.ddp2.tugas3.geo.*;
import tp.ddp2.tugas3.gui.Initializable;
import tp.ddp2.tugas3.gui.fx.OwjekApplication;
import tp.ddp2.tugas3.owjek.OwjekRegular;
import tp.ddp2.tugas3.owjek.OwjekSporty;
import tp.ddp2.tugas3.util.Converter;

import java.util.*;

/**
 * Created by Wisnu Pramadhitya R
 * 5/17/2017
 * File: MapPane.java
 *
 * Kelas pane yang digunakan untuk GUI Map Owjek yang mengextend BorderPane FX.
 * Berukuran 720 * 400 (width * heigth) dan jika dihidupkan akan menampilkan map.
 * Kelas dihidupkan dan dapat dimasukan langsung dan dapat dimasukan ke Pane lainnya.
 *
 * Kelas ini tidak didesign untuk diubah ketika runtime. Tetapi masih dapat ditambahkan
 * Node yang bisa disesuaikan.
 *
 * Kelas ini final dan tidak di design untuk extendable atau reuse.
 *
 */
public final class MapPane
extends BorderPane
implements Initializable {

    private Canvas canvasMap;
    private Label footer;
    private OwjekApplication app;
    private GPS gps;

    /*
        UKURAN CANVAS
     */
    private static final int CANVAS_HEIGHT = 400;
    private static final int CANVAS_WIDTH = 720;

    /*
        IMAGE ASSET FOR OWJEK
     */
    private static final Image IMG_OWJEK_REGULAR = new Image(
            OwjekApplication.IMG_OJEG_REG, 20,20,
            true, true, false);
    private static final Image IMG_OWJEK_SPORTY = new Image(
            OwjekApplication.IMG_OJEG_SPO, 20,20,
            true, true, false);

    private static final Image IMG_OWJEK_EXC = new Image(
            OwjekApplication.IMG_OJEG_EXC, 20,20,
            true, true, false);

    private static final Image IMG_START_PIN = new Image(OwjekApplication.IMG_PIN_START, 20, 20,
            true, true, false);

    private static final Image IMG_END_PIN = new Image(OwjekApplication.IMG_PIN_END, 20, 20,
            true, true, false);

    /**
     * Membuat canvas and map
     * @param app parent Application
     * @param gps GPS digunakan untuk membentuk map
     */
    public MapPane(OwjekApplication app, GPS gps) {
        this.app = app;
        this.gps = gps;
        initialize();
    }

    /*
        RATIO di map
        PANJANG kotak untuk tiap titik di map
     */
    private static final double RATIO = 7;
    private static final double LENGTH = 6;

    /**
     * Menginisiasi node-node yang ada di pane termasuk canvas
     */
    @Override
    public void initialize() {
        canvasMap = new Canvas(CANVAS_WIDTH, CANVAS_HEIGHT);
        canvasMap.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                setPos(event);
            }
        });
        footer = new Label("wisnu.pramadhitya@ui.ac.id/1606918055");

        createMap();
        setMinSize(CANVAS_WIDTH, CANVAS_HEIGHT);
        setPadding(new Insets(10,10,10,10));
        setCenter(canvasMap);
        //setBottom(footer);
    }

    /*
        Stack untuk fitur click di map yang menentukan tempat awal dan akhir
     */
    private final Stack<String> stackPointStartEnd = new Stack<>();

    /**
     * Menentukan tempat yang ada di map menggunakan event dari mouse canvas
     * @param event Mouse event canvas
     */
    private void setPos(MouseEvent event){
        /*
            mengambil tipe button
            jika selain primary, return
         */
        MouseButton mb = event.getButton();
        if(mb.equals(MouseButton.MIDDLE)) return;

        if(mb.equals(MouseButton.SECONDARY)) {
            stackPointStartEnd.clear();
            app.getOwjekPane().resetTextField();
            return;
        }

        /*
            Normalisasi ke text yang menggunakan format RYCX (D9Q2)
            dan memasukannya ke textfield
         */

        int x = (int) (event.getX() / RATIO -3);
        int y = (int) (event.getY() / RATIO -3);

        String place = "";
        try {
            place = Converter.toPlace(new Point(y, x));
        } catch (ArrayIndexOutOfBoundsException e) {
            return;
        }

        // mekanisme click untuk start dan end point
        if(stackPointStartEnd.isEmpty()) {
            stackPointStartEnd.push(place);
            app.getOwjekPane().getStartLocInput().setText(place);

        } else if (stackPointStartEnd.size() > 2) {
            stackPointStartEnd.pop();
            stackPointStartEnd.push(place);
            app.getOwjekPane().getEndLocInput().setText(place);

        } else {
            stackPointStartEnd.push(place);
            app.getOwjekPane().getEndLocInput().setText(place);
        }

    }

    /**
     * Membuat map berdasarkan data dari GPS
     */
    private void createMap(){
        GraphicsContext gc = canvasMap.getGraphicsContext2D();

        // set font untuk tulisan
        gc.setFont(Font.font(8.5));
        gc.setFill(Color.BLACK);

        int jjPosAlphabet = 81;
        int jjPosNumber = 0;

        // menuliskan petunjuk map
        for(int j=0; j < GPS.WIDTH; ++j){
            if(j % 10 == 0) {
                char tmp = (char) jjPosAlphabet++;
                gc.fillText(tmp+"",
                    j* RATIO + NORMALIZER*2,NORMALIZER);
            }
        }

        // menuliskan petunjuk map
        for (int j = 0; j < GPS.WIDTH; j++) {
            gc.fillText(jjPosNumber+"", j* RATIO + NORMALIZER *2,NORMALIZER+12);
            jjPosNumber++;
            if (jjPosNumber >= 10) jjPosNumber = 0;
        }

        // menuliskan petunjuk map
        int iiPosAlphabet = 65;
        int iiPosNumber = 0;

        // membuat map
        for(int i=0; i<GPS.HEIGHT; ++i){

            // menuliskan petunjuk map
            gc.setFont(Font.font(8.5));
            gc.setFill(Color.BLACK);
            if (i % 10 == 0) {
                char tmp = (char) iiPosAlphabet++;
                gc.fillText(tmp+"", 0, i* RATIO + NORMALIZER*3+1);
            }

            gc.fillText(Integer.toString(iiPosNumber),NORMALIZER, i* RATIO + NORMALIZER*3+1);
            iiPosNumber++;
            if (iiPosNumber >= 10) iiPosNumber = 0;

            for(int j=0; j <GPS.WIDTH; ++j){
                if (gps.get(i, j) == '#') {
                    gc.setFill(Color.LIGHTGREEN);
                    gc.fillRect(j* RATIO + NORMALIZER*2,i* RATIO + NORMALIZER*2+5, LENGTH,LENGTH);

                } else{
                    gc.setFill(Color.GREY);
                    gc.fillRect(j*RATIO + NORMALIZER*2,i*RATIO + NORMALIZER*2+5, LENGTH,LENGTH);
                }
            }
        }
    }

    /**
     * Menghapus isi canvas
     */
    private void clearCanvas(){
        GraphicsContext gc = canvasMap.getGraphicsContext2D();
        gc.clearRect(0,0, CANVAS_WIDTH, CANVAS_HEIGHT);
    }

    /*
        mereset map
     */
    public void resetMap() {
        clearCanvas();
        createMap();
    }

    /*
        Konstanta untuk menormalisasi posisi benda di canvas
     */
    private static final int NORMALIZER = 10;

    /**
     * Animasi menggerakan owjek
     * @param gc
     * @param p
     */
    private final void move(GraphicsContext gc, Image img_owjek, Point p, Point startP, Point endP){
        // REFRESH CANVAS
        resetMap();

        /*
            menggambar owjek, pin point
         */
        gc.drawImage(IMG_END_PIN, endP.getX() * RATIO + NORMALIZER+7, endP.getY() * RATIO + NORMALIZER);
        gc.drawImage(IMG_START_PIN, startP.getX() * RATIO + NORMALIZER + 7, startP.getY() * RATIO + NORMALIZER);
        gc.drawImage(img_owjek, p.getX() * RATIO + NORMALIZER + 2, p.getY() * RATIO + NORMALIZER);
    }

    /**
     * Method yang dipanggil untuk meggerakan owjek
     * @param type tipe owjek untuk menentukan gambar mana yang digunakan untuk menampilkan owjek
     * @param path
     */
    public void moveOwjek(String type, Point[] path) {
        // GRAPHIC UNTUK MENGGAMBAR
        GraphicsContext gc = canvasMap.getGraphicsContext2D();

        // resetAll stack
        stackPointStartEnd.clear();

        app.getTabPane().setEffect(new GaussianBlur(5));
        app.getTitlePane().setEffect(new GaussianBlur(5));
        // queue untuk path
        Deque<Point> qu = new ArrayDeque<>();
        qu.addAll(Arrays.asList(path));

        if(qu.isEmpty()) return; // check apakah queue empty

        // draw end point
        final Point endP = qu.peekLast();
        final Point startP = qu.peekFirst();

        // menentukan gambar owjek apa yang ditampilkan
        final Image img_owjek;
        if(OwjekRegular.TYPE.equals(type)){
            img_owjek = IMG_OWJEK_REGULAR;
        } else if(OwjekSporty.TYPE.equals(type)){
            img_owjek = IMG_OWJEK_SPORTY;
        } else {
            img_owjek = IMG_OWJEK_EXC;
        }

        // timer untuk refresh rate animasi
        AnimationTimer timer = new AnimationTimer() {
            // save the last timestamp
            private long prev = 0;

            @Override
            public void handle(long now) {
                if(qu.isEmpty()) {
                    app.getTabPane().setEffect(new DropShadow(2,0,0, Color.DARKGRAY));
                    app.getTitlePane().setTitlePaneEffect();
                    return;
                }

                // move if the timestamp is greater than 16.000.000 nano seconds
                if((now - prev) >= 16_000_000){
                    Point p = qu.pop();
                    move(gc, img_owjek, p, startP, endP);
                }

                // update timestamp
                prev = now;
            }
        };

        // start animasi
        timer.start();
    }
}
