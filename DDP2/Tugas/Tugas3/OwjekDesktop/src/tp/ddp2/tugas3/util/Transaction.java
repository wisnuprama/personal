package tp.ddp2.tugas3.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

/**
 * Created by wisnuprama on 4/24/2017.
 */
public class Transaction {
    private double biaya;
    private Trip<String, String> trip;
    private double jarak;
    private String type;

    private LocalDateTime dateTime;
    public static final DateTimeFormatter formatter =
                    DateTimeFormatter.ofLocalizedDateTime( FormatStyle.SHORT )
                                     .withLocale( Locale.UK )
                                     .withZone( ZoneId.systemDefault() );

    public Transaction(LocalDateTime dateTime, String type, Trip<String, String> trip,double jarak, double biaya) {
        this.dateTime = dateTime; //@TODO formatting date time
        this.trip = trip;
        this.biaya = biaya;
        this.jarak = jarak;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public Trip<String, String> getTrip() {
        return trip;
    }

    public double getBiaya() {
        return biaya;
    }

    public double getJarak() {
        return jarak;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public String getDateTimeFormatted(){
        return this.dateTime.format(formatter);
    }

    @Override
    public String toString() {
        return dateTime.toString()
                + "\nDistance: " + jarak
                + "\nCost: " + biaya;
    }
}
