package tp.ddp2.tugas3.owjek;


import tp.ddp2.tugas3.geo.Point;

/**
 * Created by wisnuprama on 4/20/2017.
 * Object yang berupa Owjek. Sebuah interface yang memberikan abstraksi yang akan di implementasikan
 * pada jenis-jenis Owjek nantinya.
 */
public interface OwjekType {

    /**
     * Method trip untuk owjek dari lokasi awal ke lokasi tujuan
     * @param start lokasi awal
     * @param to lokasi tujuan
     */
    void owRide(Point start, Point to);

    /**
     * Method yang mengembalikan nilai harga promo dari jarak
     * @param jarak jarak dua lokasi
     * @return promo
     */
    double getPromo(double jarak);

    /**
     * Mengembalikan harga kotor dari sebuah trip
     * @param jarak jarak dua lokasi
     * @return harga kotor
     */
    double getCost(double jarak);

    /**
     * Mengembalikan harga bersih dari sebuah trip
     * @param jarak jarak dua lokasi
     * @return harga bersih
     */
    double getTotalCost(double jarak);

    /**
     * Mengembalikan String summary dari sebuah trip
     * @return trip's summary
     */
    String summary();

}
