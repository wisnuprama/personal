package tp.ddp2.tugas3.gui.fx.pane;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import tp.ddp2.tugas3.gui.Initializable;
import tp.ddp2.tugas3.util.Converter;
import tp.ddp2.tugas3.util.Transaction;

/**
 * Created by Wisnu Pramadhitya R
 * on 5/17/2017.
 *
 * Kelas yang menggambarkan sebuah kartu berisi satu transaksi.
 * Merupakan extend dari VBox FX. Kelas dihidupkan dan dapat dimasukan
 * langsung dan dapat dimasukan ke Pane lainnya.
 *
 * Kelas ini tidak didesign untuk diubah ketika runtime. Tetapi masih dapat ditambahkan
 * Node yang bisa disesuaikan.
 *
 * Kelas ini final dan tidak di design untuk extendable atau reuse.
 */
public final class TransactionCard
extends VBox
implements Initializable {

    private HBox headerCard;
    private Label datetimeLabel;
    private Label tripLabel;
    private Label typeLabel;
    private Label jarakLabel;
    private Label biayaLabel;

    private Transaction tr;

    /**
     * Membuat kartu transaksi berdasarkan data transaksi yang diberikan
     * @param tr transaksi
     */
    public TransactionCard(Transaction tr){
        this.tr = tr;
        initialize();
    }

    @Override
    public void initialize() {

        // header
        datetimeLabel = new Label(tr.getDateTimeFormatted());
        datetimeLabel.setFont(javafx.scene.text.Font.font("", FontWeight.BOLD, 14));
        headerCard = new HBox(datetimeLabel);
        headerCard.autosize();
        headerCard.setAlignment(Pos.CENTER);
        headerCard.setBackground(
                new Background(
                    new BackgroundFill(Color.LIGHTGREEN, CornerRadii.EMPTY, Insets.EMPTY)
                )
        );

        tripLabel = new Label(tr.getTrip().toString());
        typeLabel = new Label(tr.getType());
        jarakLabel = new Label(String.format("%.2f", tr.getJarak()) + "km");
        biayaLabel = new Label("Rp"
                + Converter.toStandardCurrencyFormat(tr.getBiaya()));

        /*
            SETTING UP PANE
         */
        setBorder(new Border(new BorderStroke(Color.CORAL,
                BorderStrokeStyle.SOLID, new CornerRadii(5), BorderWidths.DEFAULT)));
        setPrefWidth(200);

        setBackground(new Background(new
                BackgroundFill(Color.LIGHTYELLOW, new CornerRadii(5), Insets.EMPTY))
        );

        setEffect(new DropShadow(5, Color.DARKGRAY));

        setPadding(new Insets(10, 10, 10, 10));

        setSpacing(10);

        getChildren().addAll(
                headerCard,
                typeLabel,
                tripLabel,
                jarakLabel,
                biayaLabel
        );
    }
}
