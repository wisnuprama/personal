package tp.ddp2.tugas3.transportation;

import tp.ddp2.tugas3.geo.Point;

/**
 * Created by wisnuprama on 4/20/2017.
 * Sebuah class yang mengimplementasikan interface transportasi. Class ini adalah class yang
 * memprogram sebuah Motor.
 */
public class Motor implements Transportation {
    private double jarakTempuh;

    public Motor() {
        this.jarakTempuh = 0;
    }

    /**
     * Menggerakan motor.
     * @param jarak jarak dua lokasi
     */
    @Override
    public void move(double jarak) {
        this.jarakTempuh += jarak;
    }

    /**
     * Mengembalikan nilai jarak yang ditempuh motor.
     * @return jarak tempuh dua lokasi
     */
    public double getJarakTempuh() {
        return jarakTempuh;
    }

    /**
     * Menset jarak tempuh sebuah motor
     * @param jarakTempuh jarak tempuh dua lokasi
     */
    public void setJarakTempuh(double jarakTempuh) {
        this.jarakTempuh = jarakTempuh;
    }
}
