package tp.ddp2.tugas3.util;

/**
 * Created by wisnuprama on 5/20/2017.
 */
public class Trip <S, E> {
    private S start;
    private E end;

    public Trip(S start, E end) {
        this.start = start;
        this.end = end;
    }

    public S getStart() {
        return start;
    }

    public E getEnd() {
        return end;
    }

    public void setStart(S start) {
        this.start = start;
    }

    public void setEnd(E end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "Trip: " + start + " - " + end;
    }
}
