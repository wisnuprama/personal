package ddp.tpsatu.games;

import ddp.tpsatu.liga.Liga;
import ddp.tpsatu.liga.Match;
import ddp.tpsatu.liga.Pemain;
import ddp.tpsatu.liga.Tim;
import ddp.tpsatu.liga.exception.InitOutOfRangeException;
import ddp.tpsatu.util.Generator;

import java.util.List;
import java.util.Set;

/**
 * Created by wisnuprama
 * Project  : Tugas1
 * Package  : ddp.tpsatu.games
 * Date     : 3/15/2017
 * Time     : 8:02 AM
 *
 * Class yang digunakan untuk bermain liga.
 */

public class Games {
    private boolean isFinished;
    private boolean isStarted;
    private Liga liga;

    private static final String[] COMMANDS = {"g", "kk", "km", "p"};

    // CONST AUTO
    private static final int MAX_GOL = 5;
    private static final int MAX_PELANGGARAN = 5;
    private static final int MAX_KK = 3;
    private static final int MAX_KM = 2;

    public Games() {
        this.isFinished = false;
        this.isStarted = false;
    }

    /**
     * Method yang digunakan untuk memulai sebuah game.
     * Bersifat wajib dipanggil dengan argument int 4-10 (inclusive).
     *
     * Jika argument salah akan throw exception: InitOutOfRangeException.
     *
     * @param x banyaknya tim dalam liga. Range: 4-10 (inclusive)
     * @throws InitOutOfRangeException jika x keluar dari range 4-10 (inclusive)
     */
    public void init(int x) throws InitOutOfRangeException {
        if (x >= Liga.MIN_TIM && x <= Liga.MAX_TIM) {
            this.liga = new Liga("FASILKOM", Games.registrasiLiga(x));
            this.liga.createSchedule();
            this.isStarted = true;
        }
        else
            throw new InitOutOfRangeException(x);
    }


    /**
     * Membuat array tim sesuai size berdasarkan data dummy nama Tim secara random.
     * @param size banyak tim yang ingin dibuat
     * @return array daftar pemain
     */
    private static Tim[] registrasiLiga(int size){
        String namaTim;

        // n capacity, digunakan untuk peringkat awal sebuah tim, dan indexing
        int n=size;

        // array tim
        Tim[] daftarTim = new Tim[size];

        // generate a set of random integer
        Set<Integer> randSet = Generator.generateInts(size, Tim.DUMMY_NAMA_TIM.length);

        for(int i:randSet){
            // create new tim and assign to array from last index
            // set peringkat dari n
            namaTim = Tim.DUMMY_NAMA_TIM[i];
            Tim tmpTim = new Tim(n, namaTim, Games.registrasiPemain(namaTim, Tim.MAX_PLAYER));

            // normalisasi pada array n-1
            daftarTim[--n] = tmpTim;
        }

        return daftarTim;
    }

    /**
     * Membuat array pemain sesuai size yang dimasukan berdasarkan data dummy nama Pemain secara random.
     * @param namaTim nama tim
     * @param size besarnya tim
     * @return array daftar pemain
     */
    private static Pemain[] registrasiPemain(String namaTim, int size){
        // n capacity and b bound
        int n = size;

        // array pemain
        Pemain[] daftarPemain = new Pemain[size];

        // generate random int
        Set<Integer> randSet = Generator.generateInts(size, Pemain.DUMMY_NAMA_PEMAIN.length);

        for(int i:randSet){
            // create player
            // nomor permain di random dari index nama
            // akses index dari belakang
            String nama = Pemain.DUMMY_NAMA_PEMAIN[i];
            daftarPemain[--n] = new Pemain(namaTim, nama, i+1);
        }

        return daftarPemain;
    }

    /**
     * Mengembalikan nilai true jika permainan sudah selesai, false sebaliknya.
     * @return true jika permaian sudah selesai, false belum selesai.
     */
    public boolean isFinished() {
        return isFinished;
    }

    /**
     * Mengembalikan nilai object liga.
     * @return Liga
     */
    public Liga getLiga() {
        return liga;
    }

    /**
     * Mengembalikan nilai true jika permainan sudah dimulai dan sedang berlangsung.
     * @return boolean
     */
    public boolean isStarted() {
        return isStarted;
    }

    /**
     * Mengecek apakah argumen user merupakan command.
     * @param cmd command user
     * @return true jika cmd terdapat dalam daftar commands, false sebalinya.
     */
    private boolean isCommand(String cmd){
        for(String i:Games.COMMANDS){
            if(i.equalsIgnoreCase(cmd))
                return true;
        }
        return false;
    }

    /**
     * Validating user input untuk manual game.
     * @param args array dari user input
     * @return true jika valid, false sebaliknya
     */
    public boolean isValid(String[] args){
        String[] arg;
        String namaTim;
        Tim tim;
        int nomorPemain;

        String errMessage = " tidak ditemukan";

        try {
            for (String str : args) {
                arg = str.split(" ");

                // check command
                if (!this.isCommand(arg[0])) {
                    System.err.println("Command " + arg[0] + errMessage);
                    return false;
                }

                // check tim
                namaTim = arg[1];
                if (!liga.contains(namaTim)) {
                    System.err.println("Tim " + namaTim + errMessage);
                    return false;
                } else {
                    tim = liga.get(namaTim);
                    try {
                        nomorPemain = Integer.parseInt(arg[2]);
                        if (!tim.contains(nomorPemain)) {
                            System.err.println("Nomor Pemain " + nomorPemain + errMessage);
                            return false;
                        }
                    } catch (NumberFormatException e) {
                        System.err.println(e.getMessage() + " bukan nomor");
                        return false;
                    }
                }
            }
        } catch (IndexOutOfBoundsException e){
            System.err.println("Tidak ditemukan input pada index: " + e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * Method bermain secara otomatis hingga liga selesai, menggunakan nilai random dalam permainannya.
     */
    public void nextGameAll(){

        while(!liga.scheduleIsEmpty()){
            this.nextGame();
        }

    }

    /**
     * Method bermain secara otomatis, menggunakan nilai random dalam permainannya.
     */
    public void nextGame(){
        Match today;
        Tim tim1;
        Tim tim2;
        Pemain randPemain;
        int iteration;
        int index;
        boolean flag = false;

        // grab today's match
        // deque
        today = liga.today();

        tim1 = today.getT1();
        tim2 = today.getT2();

        // maksimal iterasi sebanyak 2
        // karena tim yang bermain hanya ada 2
        final int PLAY_TIM = 2;
        for(int i=0; i<PLAY_TIM; ++i){
            //gol

            // generate random integer untuk banyaknya iterasi
            iteration = Generator.randInt(MAX_GOL);
            for(int j=0; j<iteration; ++j){

                // generate random integer for index
                index = Generator.randInt(Tim.MAX_PLAYER);
                if(flag){
                    // tim 1
                    tim1.get(index, 'i').tambahJumlahGol();
                    tim1.tambahJumlahGol();
                    tim2.tambahJumlahKebobolan();
                }
                else{
                    // tim 2
                    tim2.get(index, 'i').tambahJumlahGol();
                    tim2.tambahJumlahGol();
                    tim1.tambahJumlahKebobolan();
                }
            }

            // pelanggaran

            // generate random integer untuk banyaknya iterasi
            iteration = Generator.randInt(MAX_PELANGGARAN);
            for(int j=0; j<iteration; ++j){

                // generate random integer for index
                index = Generator.randInt(Tim.MAX_PLAYER);
                if(flag){
                    // tim 1
                    randPemain = tim1.get(index, 'i');
                    randPemain.tambahPelanggaran();
                }
                else{
                    // tim 2
                    randPemain = tim2.get(index, 'i');
                    randPemain.tambahPelanggaran();
                }

                // mengumpulkan pemain untuk reset counter
                today.addPlayer(randPemain);
            }

            // kartu kuning

            // generate random integer untuk banyaknya iterasi
            iteration = Generator.randInt(MAX_KK);
            for(int j=0; j<iteration; ++j){
                // generate random integer for index
                index = Generator.randInt(Tim.MAX_PLAYER);
                if(flag){
                    // tim 1
                    randPemain = tim1.get(index, 'i');
                }
                else{
                    // tim 2
                    randPemain = tim2.get(index, 'i');
                }

                // check apakah pemain sudah suspended di pertandingan ini
                if(!randPemain.isSuspended())
                    randPemain.tambahKartuKuning();

                // mengumpulkan pemain untuk reset counter
                today.addPlayer(randPemain);
            }

            // kartu merah

            // generate random integer untuk banyaknya iterasi
            iteration = Generator.randInt(MAX_KM);
            for(int j=0; j<iteration; ++j){
                // generate random integer for index
                index = Generator.randInt(Tim.MAX_PLAYER);
                if(flag){
                    // tim 1
                    randPemain = tim1.get(index, 'i');
                }
                else{
                    // tim 2
                    randPemain = tim2.get(index, 'i');
                }

                // check apakah pemain sudah suspended di pertandingan ini
                if(!randPemain.isSuspended())
                    randPemain.tambahKartuMerah();

                // mengumpulkan pemain untuk reset counter
                today.addPlayer(randPemain);
            }

            // ganti flag, ganti giliran tim
            flag = true;
        }

        // set the winning
        // flag true == sukses
        if(flag){
            today.winning();
            liga.sort();
            System.out.println(this.showHighlight(today));
        }

        if(liga.scheduleIsEmpty()){
            this.isFinished = true;
            this.isStarted = false;
        }
    }

    /**
     * Method untuk bermain secara manual.
     * @param args array perintah user
     */
    public void nextGame(String[] args){
        Match today;
        int nomorPemain;
        String cmd;
        String namaTim;
        boolean flag = true;

        // grab today's match
        // no deque
        today = liga.preview();
        for(String in:args){
            String[] arg = in.split(" ");
            cmd = arg[0];
            namaTim = arg[1];

            // check apakah sesuai dengan jadwal pertandingan hari ini
            if(today != null && today.contains(namaTim)) {
                try {
                    nomorPemain = Integer.parseInt(arg[2]);
                    this.play(today, cmd, namaTim, nomorPemain);
                } catch (NumberFormatException e){
                    flag = false;
                }
            }
        }

        // deque
        if(flag){
            today = liga.today();
            today.winning();
            liga.sort();
            System.out.println(this.showHighlight(today));
        }

        if(liga.scheduleIsEmpty()){
            this.isFinished = true;
            this.isStarted = false;
        }

    }

    /**
     * Method bermain dari nextgame.
     * @param today pertandingan hari ini
     * @param arg perintah user
     * @param namaTim nama tim
     * @param nomorPemain nomor pemain
     */
    private void play(Match today, String arg, String namaTim, int nomorPemain){
        Tim tm1;
        Tim tm2;
        Pemain pm;

        /*
         * TM1 SEBAGAI TIM UTAMA SESUAI DENGAN MASUKAN ARGUMEN USER
         * TM2 SEBAGAI LAWAN DARI ARGUMEN USER
         */
        tm1 = today.getT1();
        tm2 = today.getT2();

        // jika tidak sesuai, maka ditukar
        if(!tm1.getNamaTim().equalsIgnoreCase(namaTim)){
            tm1 = tm2;
            tm2 = today.getT1();
        }

        // grab pemain
        pm = tm1.get(nomorPemain, 'n');
        // check apakah sudah terkena kartu merah
        if(pm.isSuspended()) {
            System.err.println(pm.getNamaPemain() + " sudah terkena kartu merah!");
            return;
        }
        // args
        switch (arg.toLowerCase()) {
            // gol
            case "g":
                // tambah gol // harusnya sync antara pemain dan tim
                // tapi ada baiknya total gol tim adalah total gol pemain-pemain
                tm1.tambahJumlahGol(); // gol tim
                pm.tambahJumlahGol(); // gol pemain

                // tambah jumlah kebobolan tim lawan
                tm2.tambahJumlahKebobolan();
                break;

            // kartu kuning
            case "kk":
                // tambah kartu kuning
                pm.tambahKartuKuning();
                // tambahkan ke daftar pemain yang harus di reset countnya
                today.addPlayer(pm);
                break;

            // kartu merah
            case "km":
                // tambah kartu merah dan suspended
                pm.tambahKartuMerah();

                // tambahkan ke daftar pemain yang harus di reset countnnya
                today.addPlayer(pm);
                break;

            // pelanggaran
            case "p":
                // tambah jumlah pelanggaran
                pm.tambahPelanggaran();
                break;
        }
    }

    // METHOD SHOW

    /**
     * Mengembalikan nilai String berupa informasi klasemen liga.
     * @return informasi klasemen
     */
    public String showKlasemen(){
        StringBuilder str = new StringBuilder();

        str.append("Peringkat |  Nama Tim  | Jumlah Gol | Jumlah Kebobolan | Menang | Kalah | Seri | Poin\n");
        str.append("----------+------------+------------+------------------+--------+-------+------+-----\n");

        String tmp;
        Tim tim;

        // sort liga
        liga.sort();

        for(int i=1; i<=liga.size(); ++i){
            tim = liga.get(i);
            tmp = String.format("%5d\t  |   %-8s |\t  %-3d\t|\t\t %-2d \t   |   %-2d   |  %-2d   |  %-2d  |  %-2d\n",
                    tim.getPeringkat(), tim.getNamaTim(), tim.getJumlahGol(), tim.getJumlahKebobolan(),
                    tim.getJumlahMenang(), tim.getJumlahKalah(), tim.getJumlahSeri(), tim.getJumlahPoin());
            str.append(tmp);
        }

        return str.toString();
    }

    /**
     * Mengembalikan nilai String berupa informasi para top-score liga
     *
     * Catatan:
     * Tidak efisien jika dipanggil berkali-kali
     * @return informasi top score
     */
    public String showPencetakGol(){
        String tmp;
        Pemain pmn;
        StringBuilder str = new StringBuilder();
        // refresh
        List<Pemain> golPemain = liga.getGolStats();

        // membuat string
        str.append("Peringkat | Nama Pemain | Nama Tim | Jumlah Gol\n");
        str.append("----------+-------------+----------+-----------\n");

        for(int i=0; i<golPemain.size(); ++i){
            pmn = golPemain.get(i);
            tmp = String.format("%5d\t  |  %-10s |   %-8s |\t  %-3d\t\n", i+1, pmn.getNamaPemain(),
                    pmn.getNamaTim(), pmn.getJumlahGol());
            str.append(tmp);
        }

        return str.toString();
    }

    /**
     * Mengembalikan nilai String berupa informasi sebuah tim.
     * @param namaTim nama tim
     * @return informasi status tim
     */
    public String showTimStats(String namaTim){
        String tmp;
        Pemain pmn;
        StringBuilder str = new StringBuilder();

        Tim tm = liga.get(namaTim);

        // formatting
        str.append(namaTim + "\n");
        str.append("Nomor |    Nama    | Gol | Pelanggaran | Kartu Kuning | Kartu Merah\n");
        str.append("------+------------+-------------------+--------------+------------\n");

        for(int i=0; i<tm.size(); ++i){
            pmn = tm.get(i, 'i');
            tmp = String.format("  %-2d  |  %-10s|  %-2d |      %-2d     |       %-2d     |     %-2d      \n",
                                pmn.getNomorPemain(), pmn.getNamaPemain(), pmn.getJumlahGol(),
                                pmn.getJumlahPelanggaran(), pmn.getJumlahKartuKuning(), pmn.getJumlahKartuMerah());
            str.append(tmp);
        }

        return str.toString();
    }

    /**
     * Mengembalikan nilai String berisi informasi semua tim yang ada di liga.
     * @return informasi semua status tim di liga
     */
    public String showTimStatsAll(){
        StringBuilder str = new StringBuilder();
        Tim tim;
        String namaTim;

        // sort liga
        liga.sort();

        for(int i=1; i<liga.size(); ++i){
            tim = liga.get(i);
            namaTim = tim.getNamaTim();
            str.append(this.showTimStats(namaTim));
        }

        return str.toString();
    }

    /**
     * Mengembalikan nilai String berupa informasi sebuah pemain berdasarakan parameter nama tim dan nama pemain.
     * Jika tidak ditemukan pemain tersebut di sebuah tim, maka mengembalikan informasi
     * bahwa tidak ditemukan pemain tersebut
     * @param namaTim nama tim
     * @param namaPemain nama pemain
     * @return informasi status pemain
     */
    public String showPemainStats(String namaTim, String namaPemain){
        Tim tm = liga.get(namaTim);
        // check if tim contains this pemain
        if(!tm.contains(namaPemain))
            return "Tidak ditemukan pemain " + namaPemain + " dalam tim " + namaTim;
        return tm.get(namaPemain).toString();
    }

    /**
     * Mengembalikan nilai String berupa informasi sebuah pemain berdasarkan parameter nama tim dan nama pemain.
     * Jika tidak ditemukan pemain tersebut di sebuah tim, maka mengembalikan informasi bahwa
     * tidak ditemukan pemain tersebut
     * @param namaTim nama tim
     * @param nomorPemain nomor pemain
     * @return informasi status pemain
     */
    public String showPemainStats(String namaTim, int nomorPemain){
        Tim tm = liga.get(namaTim);
        // check if tim contains this pemain
        if(!tm.contains(nomorPemain))
            return "Tidak ditemukan pemain bernomor punggung " + nomorPemain + " dalam tim " + namaTim;
        return tm.get(nomorPemain, 'n').toString();
    }

    /**
     * Mengembalikan nilai String berupa informasi pertandingan selanjutnya
     * @return informasi game selanjutnya
     */
    public String showNextGame(){
        // ambil matchday dalam queue liga dan tostring
        Match pre = liga.preview();
        if(pre == null)
            return "Belum ada jadwal pertandingan lagi, terimakasih";
        return pre.toString();
    }

    /**
     * Mengembalikan nilai String berupa informasi highlight sebuah pertandingan.
     * @param match Match yang ingin di highlight
     * @return informasi highlight sebuah pertandingan
     */
    private String showHighlight(Match match){
        StringBuilder str = new StringBuilder();
        Tim t1;
        Tim t2;

        t1 = match.getT1();
        t2 = match.getT2();

        str.append("Statistik Pertandingan Tim " + t1.getNamaTim() + " vs Tim " + t2.getNamaTim() + "\n\n");

        // tim 1
        str.append("Tim : " + t1.getNamaTim()+"\n");
        str.append("Gol : " + match.getGol1() + "\n");
        str.append("Pelanggaran : " + match.getPelanggaran1() + "\n");
        str.append("Kartu Kuning : " + match.getKk1() + "\n");
        str.append("Kartu Merah : " + match.getKm1() + "\n\n");

        // tim 2
        str.append("Tim : " + t2.getNamaTim()+"\n");
        str.append("Gol : " + match.getGol2() + "\n");
        str.append("Pelanggaran : " + match.getPelanggaran2() + "\n");
        str.append("Kartu Kuning : " + match.getKk2() + "\n");
        str.append("Kartu Merah : " + match.getKm2() + "\n\n");

        return str.toString();
    }

    /**
     * Mengembalikan nilai String berisi message penutup game.
     * @return informasi end message jika game telah usai
     */
    public String showEndMessage(){
        StringBuilder str = new StringBuilder();
        str.append("LIGA " + liga.getNamaLiga() + " MUSIM INI TELAH USAI\n\n");
        str.append("CHAMPION: " + liga.get(1).getNamaTim() + "\n\n\n");
        str.append("KLASEMEN\n" + this.showKlasemen() + "\n\n");
        str.append("TOP SCORE\n" + this.showPencetakGol() + "\n\n");
        str.append("GOODBYE...");

        return str.toString();
    }

    @Override
    public String toString(){
        return "Game Liga " + liga.getNamaLiga();
    }


}