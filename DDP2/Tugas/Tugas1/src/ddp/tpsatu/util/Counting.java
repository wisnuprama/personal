package ddp.tpsatu.util;

/**
 * Created by wisnuprama
 * Project  : Tugas1
 * Package  : ddp.tpsatu.util
 * Date     : 3/18/2017
 * Time     : 11:34 PM
 */
public class Counting {

    // turn off constructor
    public static void main(String args[]){
        System.out.println(Counting.kombinasi(10, 2));

        System.out.println(faktorial(10));
    }
    private Counting() {}

    public static long faktorial(long n){
        if(n <= 1)
            return n;
        return n * faktorial(n-1);
    }

    public static long kombinasi(int n, int r){
        long fakN = Counting.faktorial(n);
        long fakR = Counting.faktorial(r);
        long fakNR = Counting.faktorial(n-r);

        return fakN / (fakR * fakNR);
    }
}
