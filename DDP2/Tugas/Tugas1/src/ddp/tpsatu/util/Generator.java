package ddp.tpsatu.util;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by wisnuprama
 * Project  : Tugas1
 * Package  : ddp.tpsatu.util
 * Date     : 3/10/2017
 * Time     : 8:02 AM
 *
 * Generator random.
 */

public class Generator {

    /**
     * Mengembalikan sebuah set 1-bound (exclusive) dengan int capacity
     * yang nilai paling besarnya tergantung bound
     * @param capacity := banyaknya nilai random yang akan di generate
     * @param bound := batas nilai randon
     * @return set of random integer
     */
    public static Set<Integer> generateInts(int capacity, int bound){
        // menjamin tidak ada int yang sama dalam satu set
        Set<Integer> randSet = new HashSet(capacity);

        // init random
        for(int i = 0, n = capacity; i < n; ++i){
            // generate pseurandom int
            int r = Generator.randInt(bound);

            // menjamin kardinalitas set adalah n
            if(randSet.contains(r)) ++n;
            else
                randSet.add(r);
        }

        return randSet;
    }

    /**
     * Meng-generate integer yang bernilai random
     * @param bound batas nilai random (exclusive)
     * @return random positive integer
     */
    public static int randInt(int bound){
        // static
        return ThreadLocalRandom.current().nextInt(bound);
    }

    /**
     * Memilih elemen dalam array secara acak.
     * @param arr array yang berisi pilihan yang akan dipilih secara acak
     * @return Elemen sebuah array
     */
    public <T> T randChooser(T[] arr){
        int i = Generator.randInt(arr.length);
        return arr[i];
    }
}
