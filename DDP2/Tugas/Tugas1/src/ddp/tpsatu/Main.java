package ddp.tpsatu;

import ddp.tpsatu.games.Games;
import ddp.tpsatu.liga.Liga;
import ddp.tpsatu.liga.Tim;
import ddp.tpsatu.liga.exception.InitOutOfRangeException;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        // declaring
        String userIn;
        String namaPemain;
        String namaTim;
        String[] parsing;
        Liga liga;
        Tim tim;
        int initX;
        int nomorPemain;

        Scanner input = new Scanner(System.in);
        Games game = new Games();

        // START GAME
        System.out.println("WELCOME..");
        while(!game.isFinished()){
            System.out.print("[LIGAF12] >> ");

            // ask user input
            userIn = input.nextLine();

            // inputParsing input
            parsing = Main.inputParsing(userIn);
            try{
                switch (parsing[0].toLowerCase().trim()){
                    case "init":
                        if(game.isStarted()){
                            System.out.println("Liga " + game.getLiga().getNamaLiga() + " sudah dimulai!\n");
                            System.out.println("Apakah ingin mengulang liga? (y/n)");
                            userIn = input.nextLine();

                            // decision
                            if("y".equalsIgnoreCase(userIn))
                                System.out.println("Restart..");

                            // tidak mengulangi liga dengan argumen n atau preventif argumen yang salah
                            else if("n".equalsIgnoreCase(userIn))
                                break;
                            else
                                break;
                        }

                        // catch error from user
                        try{
                            initX = Integer.parseInt(parsing[1]);
                            game.init(initX);
                        } catch(InitOutOfRangeException e){
                            System.err.println(e.getMessage());
                        } catch (NumberFormatException e){
                            System.err.println("Input Error " + e.getMessage() + " is not integers [0-9]+");
                        }

                        // break switch
                        break;

                    // NEXTGAME
                    case "nextgame":
                        // check apakah game sudah dimulai
                        if(!game.isStarted()){
                            System.err.println("Error: game belum di init");
                            // break switch
                            break;
                        }

                        // check apakah manual atau otomatis
                        if(parsing.length == 1){
                            game.nextGame();
                            break;
                        }

                        if("all".equalsIgnoreCase(parsing[1])){
                            game.nextGameAll();
                            break;
                        }

                        // game manual
                        parsing = Arrays.copyOfRange(parsing, 1, parsing.length);
                        if(game.isValid(parsing))
                            game.nextGame(parsing);

                        // break switch
                        break;

                    // SHOW
                    case "show":
                        // check apakah game sudah dimulai
                        if(!game.isStarted()){
                            System.err.println("Error: game belum di init");
                            // break switch
                            break;
                        }

                        if("klasemen".equalsIgnoreCase(parsing[1])){
                            System.out.println(game.showKlasemen());
                        }

                        else if("pencetakgol".equalsIgnoreCase(parsing[1])){
                            System.out.println(game.showPencetakGol());
                        }

                        else if("tim".equalsIgnoreCase(parsing[1])) {
                            liga = game.getLiga();
                            namaTim = parsing[2];

                            // mengeluarkan semua status tim di liga
                            if("-all".equalsIgnoreCase(parsing[2])){
                                System.out.println(game.showTimStatsAll());

                                // break switch
                                break;
                            }


                            if(liga.contains(namaTim))
                                System.out.println(game.showTimStats(namaTim));
                            else
                                System.err.println("Tidak ditemukan tim " + namaTim + " dalam liga "
                                        + liga.getNamaLiga());
                        }

                        else if("pemain".equalsIgnoreCase(parsing[1])){
                            liga = game.getLiga();
                            namaTim = parsing[2];
                            userIn = parsing[3]; // bisa berupa nama atau nomor pemain

                            // mengambil tim
                            if(!liga.contains(namaTim)){
                                System.err.println("Tidak ditemukan tim " + namaTim + " dalam liga "
                                                    + liga.getNamaLiga());
                                // break switch
                                break;
                            }

                            tim = game.getLiga().get(namaTim);

                            if(userIn.matches("[0-9]+")){
                                nomorPemain = Integer.parseInt(userIn);
                                if(tim.contains(nomorPemain))
                                    System.out.println(game.showPemainStats(namaTim, nomorPemain));
                                else
                                    System.err.println("Tidak ditemukan pemain nomor " + nomorPemain + " di tim "
                                                        + namaTim);
                            }

                            else {
                                namaPemain = userIn;
                                if(tim.contains(namaPemain))
                                    System.out.println(game.showPemainStats(namaTim, namaPemain));
                                else
                                    System.err.println("Tidak ditemukan pemain " + namaPemain + " di tim " + namaTim);
                            }
                        }

                        else if("nextgame".equalsIgnoreCase(parsing[1])){
                            System.out.println(game.showNextGame());
                        }

                        break;
                    default:
                        System.err.println("Input tidak dikenal");

                }
            } catch (IndexOutOfBoundsException e){
                System.err.println("Tidak ditemukan input pada index: " + e.getMessage());
            }

        }

        // show end message
        System.out.println(game.showEndMessage());
    }

    private static String[] inputParsing(String input){
        String[] parse;

        // base case untuk input
        parse = input.split(" ");

        if("nextgame".equalsIgnoreCase(parse[0]))
            return input.split(" -");

        // inputParsing normal menggunakan regex space
        // untuk init dan show
        return parse;
    }
}
