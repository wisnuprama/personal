package ddp.tpsatu.liga.exception;
import ddp.tpsatu.liga.Tim;

/**
 * Created by wisnuprama
 * Project  : Tugas1
 * Package  : ddp.tpsatu.liga.exception
 * Date     : 3/23/2017
 * Time     : 10:22 PM
 *
 * Class Error yang di throw jika pemain yang didaftarkan pada sebuah tim melebihi batas MAX_PLAYER tim.
 */
public class IllegalNumberOfPemain
extends Error {

    public IllegalNumberOfPemain(int x) {
        super("Error jumlah pemain :" + x + "\nPemain yang didaftarkan pada tim melebihi batas" +
                " maksimum pemain dalam satu tim, yaitu :" + Tim.MAX_PLAYER);
    }

    public IllegalNumberOfPemain(String message) {
        super(message);
    }

    public IllegalNumberOfPemain(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalNumberOfPemain(Throwable cause) {
        super(cause);
    }

    protected IllegalNumberOfPemain(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
