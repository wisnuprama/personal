package ddp.tpsatu.liga.exception;

/**
 * Created by wisnuprama
 * Project  : Tugas1
 * Package  : ddp.tpsatu.liga.exception
 * Date     : 3/19/2017
 * Time     : 10:20 PM
 *
 * Class exception yang akan di throw jika init games tidak sesuai dengan range.
 */
public class InitOutOfRangeException
extends Exception {

    public InitOutOfRangeException(int e) {
        super("INIT ERROR: " + e + " tidak dalam range 4 <= x <= 10!");
    }

    public InitOutOfRangeException(String message) {
        super(message);
    }

    public InitOutOfRangeException(String message, Throwable cause) {
        super(message, cause);
    }

    public InitOutOfRangeException(Throwable cause) {
        super(cause);
    }
}
