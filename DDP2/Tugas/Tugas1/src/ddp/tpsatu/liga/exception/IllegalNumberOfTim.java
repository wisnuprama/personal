package ddp.tpsatu.liga.exception;

/**
 * Created by wisnuprama
 * Project  : Tugas1
 * Package  : ddp.tpsatu.liga.exception
 * Date     : 3/23/2017
 * Time     : 10:38 PM
 */
public class IllegalNumberOfTim
extends Error {

    public IllegalNumberOfTim(int n) {
        super("Error jumlah tim : " + n + "\nJumlah tim yang didaftarkan pada liga tidak dalam range 4-10 (inclusive)");
    }

    public IllegalNumberOfTim(String message) {
        super(message);
    }

    public IllegalNumberOfTim(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalNumberOfTim(Throwable cause) {
        super(cause);
    }

    protected IllegalNumberOfTim(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
