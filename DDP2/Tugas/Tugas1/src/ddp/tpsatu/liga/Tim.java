package ddp.tpsatu.liga;

import ddp.tpsatu.liga.exception.IllegalNumberOfPemain;

/**
 * Created by wisnuprama
 * Project  : Tugas1
 * Package  : ddp.tpsatu.liga.tim
 * Date     : 3/10/2017
 * Time     : 8:02 AM
 *
 * Class Tim gunakan untuk container Pemain. Berisi informasi tim dan berisi pemain-pemain yang di simpan dalam
 * sebuah array. Maksimal sebuah pemain yang dibentuk adalah sebanyak 5. Kelas ini menyimpan nama tim, peringkat,
 * jumlah menang, jumlah kalah, jumlah seri, jumlah poin, jumlah gol, dan jumlah kebobolan, dan besarnya jumlah pemain.
 *
 */

public class Tim {
    private String namaTim;
    private int peringkat;
    private int jumlahMenang;
    private int jumlahKalah;
    private int jumlahSeri;
    private int jumlahPoin;
    private int jumlahGol;
    private int jumlahKebobolan;
    private int size;

    // isi pemain
    private Pemain[] daftarPemain;

    // count gol dalam sebuah pertandingan
    private int counterGol;

    // STATIC
    public static final String[] DUMMY_NAMA_TIM = {"Gajah", "Rusa", "Belalang", "Kodok", "Kucing", "Tupai", "Rajawali", "Siput",
            "Kumbang", "Semut", "Ular", "Harimau", "Kuda", "Serigala", "Panda", "Kadal",
            "Ayam", "Bebek"};

    // CONST
    public static final int BOUND_PLAYER_NUMBER = 99; // batas tertinggi nomor pemain
    public static final int MAX_PLAYER = 5; // banyaknya pemain dalam satu tim

    public Tim(int peringkat, String namaTim, Pemain[] daftarPemain) throws IllegalNumberOfPemain {

        // tidak sebaiknya di recoverable
        if(daftarPemain.length > MAX_PLAYER)
            throw new IllegalNumberOfPemain(daftarPemain.length);

        this.namaTim = namaTim;

        // init
        this.peringkat = peringkat;
        this.jumlahMenang = 0;
        this.jumlahKalah = 0;
        this.jumlahSeri = 0;
        this.jumlahPoin = 0;
        this.jumlahGol = 0;
        this.jumlahKebobolan = 0;

        // counter
        this.counterGol = 0;

        // jumlah pemain sebanyak 5
        this.daftarPemain = daftarPemain;
        this.size = daftarPemain.length;

        this.sort();
    }

    /**
     * Linear search, mencari pemain berdasarkan String
     * @param namaPemain String nama pemain
     * @return index jika ada, -1 jika tidak ditemukan
     */
    private int search(String namaPemain){
        Pemain pmn;

        // start search
        for(int i=0; i<this.size; ++i){
            pmn = this.daftarPemain[i];
            if(namaPemain.equalsIgnoreCase(pmn.getNamaPemain()))
                return i;
        }
        return -1;
    }

    /**
     * Linear search, mencari pemain berdasarkan int
     * @param nomorPemain int nomor pemain
     * @return index jika ada, -1 jika tidak ditemukan
     */
    private int search(int nomorPemain){
        Pemain pmn;

        // start search
        for(int i=0; i<this.size; ++i){
            pmn = this.daftarPemain[i];
            if(nomorPemain == pmn.getNomorPemain())
                return i;
        }
        return -1;
    }

    /**
     * Check apakah terdapat nomor pemain tersebut di Tim.
     * @param nomorPemain int nomor pemain
     * @return true jika ada, false sebaliknya.
     */
    public boolean contains(int nomorPemain){
        int i = this.search(nomorPemain);
        if(i == -1)
            return false;
        return true;
    }

    /**
     * heck apakah terdapat nama pemain tersebut di Tim.
     * @param namaPemain String nama pemain
     * @return true jika ada, false sebaliknya.
     */
    public boolean contains(String namaPemain){
        int i = this.search(namaPemain);
        if(i == -1)
            return false;
        return true;
    }

    /**
     * Sorting nama pemain berdasarkan nomor pemain
     */
    public void sort(){
        int n=this.size;

        boolean swapped;
        Pemain left;
        Pemain right;

        while(--n>=0)
        {
            swapped = false;
            for(int i=0; i<n; ++i)
            {
                left = this.daftarPemain[i];
                right = this.daftarPemain[i+1];
                if(left.getNomorPemain() > right.getNomorPemain()){
                    // SWAP
                    this.daftarPemain[i] = right;
                    this.daftarPemain[i+1] = left;

                    swapped = true;
                }
            }
            if(!swapped)
                break;
        }
    }

    @Override
    public String toString(){
        return this.namaTim + ":= " + this.peringkat;
    }

    /**
     * Reset counter gol agar pemain dapat digunakan dipertandingan selanjutnya.
     */
    public void resetCounterGol(){
        this.counterGol = 0;
    }

    /**
     * Mengembalikan nilai counter gol.
     * @return banyaknya counter gol
     */
    public int getCounterGol(){
        return this.counterGol;
    }

    // setter-getter

    /**
     * Mengganti nilai peringkat Tim.
     * @param peringkat int peringkat
     */
    public void setPeringkat(int peringkat) {
        this.peringkat = peringkat;
    }

    /**
     * Menambah jumlah gol.
     */
    public void tambahJumlahMenang() {
        ++this.jumlahMenang;
    }

    /**
     * Menambah jumlah kalah.
     */
    public void tambahJumlahKalah() {
        ++this.jumlahKalah;
    }

    /**
     * Menambah jumlah seri.
     */
    public void tambahJumlahSeri() {
        ++this.jumlahSeri;
    }

    /**
     * Menambah jumlah poin.
     * @param poin int poin
     */
    public void tambahJumlahPoin(int poin) {
        this.jumlahPoin += poin;
    }

    /**
     * Menambah jumlah gol dan counter gol.
     */
    public void tambahJumlahGol() {
        ++this.counterGol;
        ++this.jumlahGol;
    }

    /**
     * Menambah jumlah kebobolan.
     */
    public void tambahJumlahKebobolan() {
        ++this.jumlahKebobolan;
    }

    /**
     * Mengembalikan nama tim.
     * @return nama tim
     */
    public String getNamaTim() {
        return namaTim;
    }

    /**
     * Mengembalikan nilai peringkat.
     * @return peringkat
     */
    public int getPeringkat() {
        return peringkat;
    }

    /**
     * Mengembalikan jumlah menang.
     * @return jumlah menang
     */
    public int getJumlahMenang() {
        return jumlahMenang;
    }

    /**
     * Mengembalikan jumlah kalah.
     * @return jumlah kalah
     */
    public int getJumlahKalah() {
        return jumlahKalah;
    }

    /**
     * Mengembalikan jumlah seri.
     * @return jumlah seri.
     */
    public int getJumlahSeri() {
        return jumlahSeri;
    }

    /**
     * Mengembalikan jumlah poin.
     * @return jumlah poin
     */
    public int getJumlahPoin() {
        return jumlahPoin;
    }

    /**
     * Mengembalikan jumlah gol.
     * @return jumlah gol
     */
    public int getJumlahGol() {
        return jumlahGol;
    }

    /**
     * Mengembalikan jumlah kebobolan.
     * @return jumlah kebobolan
     */
    public int getJumlahKebobolan() {
        return jumlahKebobolan;
    }

    /**
     * Mengembalikan nilai selisih gol (jumlah gol - jumlah kebobolan).
     * @return jumlah selisih gol
     */
    public int getSelisihGol(){
        return jumlahGol - jumlahKebobolan;
    }

    /**
     * Mengambil pemain berdasarkan nomor pemain atau index pemain.
     *
     * @param nomor int nomor/index pemain
     * @param tipe i := index, n := nomor pemain
     * @return Pemain, jika tidak ditemukan mengembalikan nilai null.
     */
    public Pemain get(int nomor, char tipe){
        if(tipe == 'i')
            return this.daftarPemain[nomor];
        else if(tipe == 'n'){
            int i = this.search(nomor);
            if(i != -1)
                return this.daftarPemain[i];
        }
        return null;
    }

    /**
     * Mengambil pemain berdasarkan nama pemain.
     * @param namaPemain String nama pemain
     * @return Pemaim, jika tidak ditemukan mengembalikan nilai null
     */
    public Pemain get(String namaPemain){
        int i = this.search(namaPemain);

        // worst case
        if(i == -1)
            return null;

        return this.daftarPemain[i];
    }

    /**
     * Mengembalikan nilai banyaknya pemain dalam sebuah tim.
     * @return size
     */
    public int size(){
        return this.size;
    }
}
