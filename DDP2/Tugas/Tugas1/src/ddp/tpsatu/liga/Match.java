package ddp.tpsatu.liga;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by wisnuprama
 * Project  : Tugas1
 * Package  : ddp.tpsatu.liga
 * Date     : 3/15/2017
 * Time     : 8:02 AM
 *
 * Class pertandingan yang digunakan untuk membantu kelas liga dalam membuat schedule (queue) pertandingan.
 * Kelas ini menyimpan tim apa saja yang akan bertanding dalam satu pertandingan (nextxgame). Kelas ini juga
 * menghitung gol, pelanggaran, kartu kuning, kartu merah selama satu pertandingan berlangsung, dan juga menentukan
 * tim apa yang memenangkan pertandingan.
 */

public class Match {
    private Tim t1;
    private Tim t2;
    private Set<Pemain> pemainToday;

    private int gol1;
    private int gol2;
    private int pelanggaran1;
    private int pelanggaran2;
    private int kk1;
    private int kk2;
    private int km1;
    private int km2;

    public Match(Tim t1, Tim t2) {
        this.t1 = t1;
        this.t2 = t2;
        this.pemainToday = new HashSet();
    }

    /**
     * Mengembalikan tim 1.
     * @return tim 1
     */
    public Tim getT1() {
        return t1;
    }

    /**
     * Mengembalikan tim 2.
     * @return tim 2
     */
    public Tim getT2() {
        return t2;
    }

    /**
     * Mengecek apakah terdapat tim dengan nama tersebut dalam Match.
     * @param t nama tim
     * @return true jika ada, false sebaliknya
     */
    public boolean contains(String t){
        return this.t1.getNamaTim().equalsIgnoreCase(t) || this.t2.getNamaTim().equalsIgnoreCase(t);
    }

    /**
     * Hitung kartu kuning dalam sebuah tim dalam satu pertandingan dari akumulasi pemain
     * @param tim tim
     * @return jumlah kartu kuning
     */
    private int hitungKartuKuning(Tim tim){
        int n = 0;

        for(int i=0; i<tim.size(); ++i){
            n += tim.get(i, 'i').getCountKartuKuning();
        }
        return n;
    }

    /**
     * Hitung kartu merah dalam sebuah tim dalam satu pertandingan dari akumulasi pemain
     * @param tim tim
     * @return jumlah kartu merah
     */
    private int hitungKartuMerah(Tim tim){
        int n = 0;

        for(int i=0; i<tim.size(); ++i)
            n += tim.get(i, 'i').getCountKartuMerah();

        return n;
    }

    /**
     * Hitung pelanggaran dalam sebuah tim dalam satu pertandingan dari akumulasi pemain
     * @param tim tim
     * @return jumlah pelanggaran
     */
    private int hitungPelanggaran(Tim tim){
        int n = 0;

        for(int i=0; i<tim.size(); ++i){
            n += tim.get(i, 'i').getCountPelanggaran();
        }
        return n;
    }

    /**
     * Penentuan siapa pemenang dalam match.
     * Dalam prosesnya mereset counter pemain.
     */
    public void winning(){
        // gather info from tim
        this.gol1 = t1.getCounterGol();
        this.pelanggaran1 = this.hitungPelanggaran(t1);
        this.kk1 = this.hitungKartuKuning(t1);
        this.km1 = this.hitungKartuMerah(t1);

        this.gol2 = t2.getCounterGol();
        this.pelanggaran2 = this.hitungPelanggaran(t2);
        this.kk2 = this.hitungKartuKuning(t2);
        this.km2 = this.hitungKartuMerah(t2);

        // get pelanggaran

        if(gol1 > gol2){
            t1.tambahJumlahMenang();
            t1.tambahJumlahPoin(Liga.POIN_MENANG);
            t2.tambahJumlahKalah();
        }
        else if(gol1 < gol2){
            t2.tambahJumlahMenang();
            t2.tambahJumlahPoin(Liga.POIN_MENANG);
            t1.tambahJumlahKalah();
        }
        else{
            t1.tambahJumlahPoin(Liga.POIN_SERI);
            t1.tambahJumlahSeri();
            t2.tambahJumlahPoin(Liga.POIN_SERI);
            t2.tambahJumlahSeri();
        }

        // reset para pemain dan tim
        this.reset();
    }

    /**
     * Menambakan pemain ke daftar pemain yang harus di reset setelah pertandingan.
     * @param pemain pemain yang ingin ditambahkan
     */
    public void addPlayer(Pemain pemain){
        this.pemainToday.add(pemain);
    }

    /**
     * Mereset counter pemain dan tim.
     */
    private void reset(){
        for(Pemain pm:this.pemainToday){
            pm.resetCount();
        }
        t1.resetCounterGol();
        t2.resetCounterGol();
    }

    /**
     * Mengembalikan jumlah gol tim 1.
     * @return jumlah gol tim 1
     */
    public int getGol1() {
        return gol1;
    }

    /**
     * Mengembalikan jumlah gol tim 2.
     * @return jumlah gol tim 2
     */
    public int getGol2() {
        return gol2;
    }

    /**
     * Mengembalikan jumlah pelanggaran tim 1.
     * @return jumlah pelanggaran tim 1
     */
    public int getPelanggaran1() {
        return pelanggaran1;
    }

    /**
     * Mengembalikan jumlah pelanggaran tim 2.
     * @return jumlah pelanggaran tim 2
     */
    public int getPelanggaran2() {
        return pelanggaran2;
    }

    /**
     * Mengembalikan jumlah kartu kuning tim 1.
     * @return jumlah kartu kuning tim 1
     */
    public int getKk1() {
        return kk1;
    }

    /**
     * Mengembalikan jumlah kartu kuning tim 2.
     * @return jumlah kartu kuning tim 2
     */
    public int getKk2() {
        return kk2;
    }

    /**
     * Mengembalikan jumlah kartu merah tim 1.
     * @return jumlah kartu merah tim 1
     */
    public int getKm1() {
        return km1;
    }

    /**
     * Mengembalikan jumlah kartu merah tim 1.
     * @return jumlah kartu merah tim 1
     */
    public int getKm2() {
        return km2;
    }

    public String toString(){
        return t1.getNamaTim() + " vs " + t2.getNamaTim();
    }
}
