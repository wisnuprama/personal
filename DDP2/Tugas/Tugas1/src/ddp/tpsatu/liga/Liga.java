package ddp.tpsatu.liga;

import ddp.tpsatu.liga.exception.IllegalNumberOfTim;

import java.util.*;

/**
 * Created by wisnuprama
 * Project  : Tugas1
 * Package  : ddp.tpsatu.liga
 * Date     : 3/10/2017
 * Time     : 8:02 AM
 *
 * Class container Tim. Kelas ini menyimpan data klasemen liga yaitu array Tim. Kelas ini juga membuat schedule
 * Match antar tim selama liga. Dalam sebuah liga terdapat dua container, array klasemen dan juga queue pertandingan.
 * Keduanya hanya bisa di akses dengan getter. klasemen dapat diakses dengan get dan contains, sedangkan
 * queue schedule dapat diakses dengan preview dan today.
 */

public class Liga {
    private Tim[] klasemenLiga;
    private int size;
    private String namaLiga;
    // queue
    private Deque<Match> schedule;

    // CONST
    public static final int POIN_MENANG = 3;
    public static final int POIN_SERI = 1;
    public static final int POIN_KALAH = 0;

    public static final int MAX_TIM = 10;
    public static final int MIN_TIM = 4;

    public Liga(String namaLiga, Tim[] daftarTim) {
        if(daftarTim.length < MIN_TIM || daftarTim.length > MAX_TIM)
            throw new IllegalNumberOfTim(daftarTim.length);

        this.namaLiga = namaLiga;
        this.size = daftarTim.length;
        this.klasemenLiga = daftarTim;
    }

    /**
     * Membuat schedule pertandingan-pertandingan selama liga.
     */
    public void createSchedule(){
        List<Match> forShuffle = new ArrayList<>();

        Match tmp;
        // KOMBINASI
        for(int i=0; i<this.size; ++i){
            for(int j=i+1; j<this.size; ++j){
                tmp = new Match(this.klasemenLiga[i], this.klasemenLiga[j]);
                forShuffle.add(tmp);
            }
        }
        Collections.shuffle(forShuffle);
        this.schedule = new ArrayDeque<>(forShuffle);
    }

    /**
     * Mengurutkan tim-tim berdasarkan banyaknya poin. Urutan dari terbesar ke terkecil.
     * Sorting menggunakan bubble sort algorithm.
     */
    public void sort(){
        int n=this.size;
        boolean swapped;
        Tim left;
        Tim right;

        while(--n>=0){
            // flag false
            swapped = false;
            for(int i=0; i<n; ++i){
                // compare jumlah poin
                // get tim, komparasi index kiri == kanan
                left = this.klasemenLiga[i];
                right = this.klasemenLiga[i+1];

                if(left.getJumlahPoin() == right.getJumlahPoin())
                {
                    if(left.getSelisihGol() < right.getSelisihGol())
                    {
                        // SWAP BERDASARKAN SELISIH GOL AKIBAT POIN SAMA
                        this.klasemenLiga[i] = right;
                        this.klasemenLiga[i+1] = left;

                        // set peringkat baru tim
                        right.setPeringkat(i+1);
                        left.setPeringkat(i+2);

                        // flag true
                        // masih terdapat swap
                        swapped = true;
                    }

                    else if(left.getSelisihGol() == right.getSelisihGol())
                    {
                        if(left.getJumlahGol() < right.getJumlahGol())
                        {
                            // SWAP BERDASARKAN SELISIH GOL AKIBAT POIN SAMA
                            this.klasemenLiga[i] = right;
                            this.klasemenLiga[i+1] = left;

                            // set peringkat baru tim
                            right.setPeringkat(i+1);
                            left.setPeringkat(i+2);

                            // flag true
                            // masih terdapat swap
                            swapped = true;
                        }

                        else if(left.getJumlahGol() == right.getJumlahGol())
                        {
                            // jika kebobolan kiri lebih banyak dari kanan, swap
                            if(left.getJumlahKebobolan() > right.getJumlahKebobolan())
                            {
                                // SWAP BERDASARKAN SELISIH GOL AKIBAT POIN SAMA
                                this.klasemenLiga[i] = right;
                                this.klasemenLiga[i+1] = left;

                                // set peringkat baru tim
                                right.setPeringkat(i+1);
                                left.setPeringkat(i+2);

                                // flag true
                                // masih terdapat swap
                                swapped = true;
                            }
                        }
                    }
                }

                else if (left.getJumlahPoin() < right.getJumlahPoin())
                {
                    // SWAP BERDASARKAN POIN
                    this.klasemenLiga[i] = right;
                    this.klasemenLiga[i+1] = left;

                    // set peringkat baru tim
                    right.setPeringkat(i+1);
                    left.setPeringkat(i+2);

                    // flag true
                    // masih terdapat swap
                    swapped = true;
                }
            }
            // sorted
            if(!swapped)
                break;
        }
    }

    /**
     * Mencari tim berdasarkan nama tim
     * @param namaTim String nama tim
     * @return mengembalikan index tim, jika tidak ditemukan -1.
     */
    private int search(String namaTim){
        Tim tm;
        for(int i=0; i<this.klasemenLiga.length; ++i){
            tm = this.klasemenLiga[i];
            if(namaTim.equalsIgnoreCase(tm.getNamaTim()))
                return i;
        }
        return -1;
    }

    /**
     * Mengecek apakah Liga mengandung tim yang sesuai argumen.
     * @param namaTim String nama tim
     * @return true jika argumen (namaTim) terdapat dalam sebuah liga.
     */
    public boolean contains(String namaTim){
        int i = this.search(namaTim);

        if(i == -1)
            return false;
        return true;
    }

    /**
     * Membuat list pencetak gol dan sorted dari pemain dengan gol terbanyak
     * @return list pemain
     */
    private List<Pemain> createListPencetakGol(){
        Pemain pmn;
        List<Pemain> result = new ArrayList<>();

        // DIJAMIN TIDAK ADA NULL
        // DIJAMIN TIDAK EFISIEN, SEHARUSNYA BUAT CLASS CONTAINER BARU KWKW
        for(Tim tm:this.klasemenLiga)
        {
            for(int i=0; i<tm.size(); ++i){
                // ambil pemain berdasarkan index
                pmn = tm.get(i, 'i');
                if(pmn.getJumlahGol() > 0){
                    result.add(pmn);
                }
            }
        }

        // belum ada yang mencetak gol atau kurang
        if(result.size() < 10)
            return result;

        // sort dengan method arraylist
        result.sort(Pemain::compareGolTo);
        return result.subList(0, 10);
    }

    /**
     * Mengembalikan nilai true jika schedule tidak berisi Match
     * @return true jika schedule liga sudah habis, false jika masih ada
     */
    public boolean scheduleIsEmpty(){
        return schedule.isEmpty();
    }

    /**
     * Mengembalikan pertandingan hari ini dalam queue (head of queue)
     * Retrieves, but does not remove, the head of the queue represented by this deque,
     * or returns null if this deque is empty.
     * @return pertandingan hari ini
     */
    public Match preview(){
        return this.schedule.peek();
    }

    /**
     * Mengembalikan pertandingan hari ini dalam queue (head of queue)
     * dan menghapusnya dalam queue
     * Retrieves and removes the head of the queue represented by this deque
     * (in other words, the first element of this deque),
     * or returns null if this deque is empty.
     * @return pertandingan hari ini
     */
    public Match today(){
        return this.schedule.poll();
    }

    /**
     * Mengembalikan list top score.
     * @return list pemain-pemain.
     */
    public List<Pemain> getGolStats(){
        return this.createListPencetakGol();
    }

    /**
     * Mengembalikan sebuah tim.
     * @param peringkat integer peringkat sebuah tim
     * @return tim
     */
    public Tim get(int peringkat){
        return this.klasemenLiga[peringkat-1];
    }

    /**
     * Mengembalikan sebuah tim berdasarkan nama tim.
     * @param namaTim String nama sebuah tim
     * @return tim, null jika tidak ditemukan
     */
    public Tim get(String namaTim){
        int i = this.search(namaTim);

        // jika tidak ada
        if(i == -1)
            return null;

        return this.klasemenLiga[i];
    }

    /**
     * Mengembalikan nama liga.
     * @return nama liga
     */
    public String getNamaLiga() {
        return namaLiga;
    }

    /**
     * Mengembalikan nilai besaran sebuah liga (banyak sebuah tim dalam liga)
     * @return size
     */
    public int size() {
        return size;
    }

    @Override
    public String toString(){
        return Arrays.toString(this.klasemenLiga);
    }
}