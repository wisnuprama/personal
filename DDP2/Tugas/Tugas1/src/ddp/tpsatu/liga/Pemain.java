package ddp.tpsatu.liga;

/**
 * Created by wisnuprama
 * Project  : Tugas1
 * Package  : ddp.tpsatu.liga.pemain
 * Date     : 3/10/2017
 * Time     : 8:02 AM
 *
 * Class untuk membuat sebuah object pemain. Kelas ini menyimpan informasi tentang pemain, seperti nomor pemain
 * nama pemain. Selain itu juga menyimpan jumlah gol, jumlah pelanggaran, jumlah kartu kuning, jumlah kartu merah
 * selama pemain bermain dalam liga.
 */

public class Pemain {
    private String namaTim;
    private String namaPemain;
    private int nomorPemain;
    private int jumlahGol;
    private int jumlahPelanggaran;
    private int jumlahKartuKuning;
    private int jumlahKartuMerah;

    // count and flag untuk satu pertandingan
    // kembali ke 0 jika selesai pertandingan
    private int countPelanggaran;
    private int countKartuKuning;
    private int countKartuMerah;

    // status pemain dalam sebuah pertandingan
    private boolean suspended;

    /**
     * Nama-nama pemain yang dapat digunakan untuk nama pemain
     */
    public static final String[] DUMMY_NAMA_PEMAIN = {"Arnold", "Kaidou", "Chopper", "Pica", "Enel", "Zoro", "Pedro", "Beckman", "Ace",
            "Shiryu", "Sakazuki", "Marco", "Garp", "Dadan", "Sengoku", "Sanji", "Magellan", "Dragon",
            "Sabo", "Smoker", "Luffy", "Franky", "Borsalino", "Buggy", "Crocodile", "Shanks", "Yasopp",
            "Coby", "Burgess", "Usopp", "Law", "Kid","Bege", "Yonji", "Doffy", "Edward", "Mihawk", "Shanks",
            "Jinbei", "Killer", "Robin", "Roger", "Shiki", "Rayleigh", "Robb", "Kuma", "Moriah", "Teach",
            "Pagaya", "Conis", "Hachi", "Brook", "Kinemon", "Vergo", "Caesar", "Momo", "Mohji", "Cabaji",
            "Jozu", "Vista", "Doma", "Augur", "Drake", "Ivankov", "Charlotte", "Bellamy", "Demaro", "Dorry",
            "Brogy", "Kuro", "Zeff", "Gin", "Pearl", "Alvide", "Apoo", "Kuzan", "Nami", "Brook",
            "Hancock", "Koala"};

    public Pemain(String namaTim, String namaPemain, int nomorPemain) {
        this.namaTim = namaTim;
        this.namaPemain = namaPemain;
        this.nomorPemain = nomorPemain;

        this.jumlahGol = 0;
        this.jumlahPelanggaran = 0;
        this.jumlahKartuKuning = 0;
        this.jumlahKartuMerah = 0;

        //count per-pertandingan
        this.countKartuKuning = 0;
        this.countKartuMerah = 0;

        // status pemain
        this.suspended = false;
    }

    /**
     * Mengembalikan nilai String berisi informasi sebuah pemain.
     * @return Stirng
     */
    public String toString(){
        return "Nomor : " + this.nomorPemain + "\nNama :  " + this.namaPemain + "\nGol : " + this.jumlahGol +
                "\nPelanggaran : " + this.jumlahPelanggaran + "\nKartu Kuning : " + this.jumlahKartuKuning +
                "\nKartu Merah : " + this.jumlahKartuMerah;
    }

    /**
     * Compare dua pemain berdasarkan banyaknya gol.
     * Digunakan untuk sorting ArrayList().
     * @param pemain pemain yang ingin dibandingkan
     * @return 1 jika jumlah lebih kecil dari pemain b. -1 jika lebih besar dari pemain b. 0 jika jumlah gol sama.
     */
    public int compareGolTo(Pemain pemain){
        int pmn = pemain.getJumlahGol();

        // descending
        if(this.jumlahGol < pmn)
            return 1;
        else if(this.jumlahGol > pmn)
            return -1;
        return 0;
    }

    /**
     * Reset count pemain dan suspended menjadi nilai awal.
     *
     * Method wajib dipanggil setiap usai pertandingan.
     */
    public void resetCount(){
        this.countKartuKuning = 0;
        this.countKartuMerah = 0;
        this.countPelanggaran = 0;
        this.suspended = false;
    }

    /**
     * Mengembalikan nilai suspended
     * @return true jika pemain suspended, false sebalinya
     */
    public boolean isSuspended(){
        return this.suspended;
    }

    // setter-getter

    /**
     * Menambah jumlah gol.
     */
    public void tambahJumlahGol() {
        ++this.jumlahGol;
    }

    /**
     * Menambah jumlah kartu kuning dan count kartu kuning.
     *
     * Jika count kartu kuning sudah berjumlah dua dalam sebuah pertandingan
     * akan secara otomatis menambah kartu merah.
     * Kartu kuning juga otomatis menambah jumlah pelanggaran.
     */
    public void tambahKartuKuning(){
        ++this.countKartuKuning;
        ++this.jumlahKartuKuning;


        if(this.countKartuKuning == 2){
            this.tambahKartuMerah();
        }
        else {
            this.tambahPelanggaran();
        }
    }

    /**
     * Menambah jumlah kartu merah dan count kartu merah.
     *
     * Kartu merah dapat ditambah jika dalam sebuah pertandingan count kartu merah
     * masih berjumlah 0 (nol). Method ini juga mengganti nilai suspended menjadi true
     * jika syarat terpenuhi.
     * Kartu merah juga otomatis menambah jumlah pelanggaran.
     */
    public void tambahKartuMerah() {
        if(this.countKartuMerah < 1){
            ++this.countKartuMerah;
            ++this.jumlahKartuMerah;
            ++this.jumlahPelanggaran;
            ++this.countPelanggaran;
            this.suspended = true;
        }
    }

    /**
     * Menambah jumlah pelanggaran.
     */
    public void tambahPelanggaran(){
        ++this.countPelanggaran;
        ++this.jumlahPelanggaran;
    }

    /**
     * Mengembalikan count pelanggaran.
     * @return banyaknya count pelanggaran
     */
    public int getCountPelanggaran() {
        return countPelanggaran;
    }

    /**
     * Mengembalikan count kartu kuning.
     * @return banyaknya count kartu kuning
     */
    public int getCountKartuKuning() {
        return countKartuKuning;
    }

    /**
     * Mengembalikan count kartu merah.
     * @return banyaknya count kartu merah
     */
    public int getCountKartuMerah() {
        return countKartuMerah;
    }

    /**
     * Mengembalikan nama tim.
     * @return nama tim
     */
    public String getNamaTim() {
        return namaTim;
    }

    /**
     * Mengembalikan nama pemain.
     * @return nama pemain
     */
    public String getNamaPemain() {
        return namaPemain;
    }

    /**
     * Mengembalikan nomor pemain.
     * @return nomor pemain
     */
    public int getNomorPemain() {
        return nomorPemain;
    }

    /**
     * Mengembalikan jumlah gol.
     * @return banyaknya jumlah gol
     */
    public int getJumlahGol() {
        return jumlahGol;
    }

    /**
     * Mengembalikan jumlah pelanggaran.
     * @return banyaknya jumlah pelanggaran
     */
    public int getJumlahPelanggaran() {
        return jumlahPelanggaran;
    }

    /**
     * Mengembalikan jumlah kartu kuning.
     * @return banyaknya jumlah kartu kuning
     */
    public int getJumlahKartuKuning() {
        return jumlahKartuKuning;
    }

    /**
     * Mengembalikan jumlah kartu merah.
     * @return banyaknya jumlah kartu merah
     */
    public int getJumlahKartuMerah() {
        return jumlahKartuMerah;
    }
}
