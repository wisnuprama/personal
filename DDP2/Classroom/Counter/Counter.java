/**
 * class Counter
 */
public class Counter
{   
    // instance variable
    private int value;

    // constructor
    public Counter()
    {
        this.value = 0;
    }

    /**
     * constructor
     * @param value
     */
    public Counter(int value)  
    {
        this.value = value;
    }

    // method
    public void count()
    {
        this.value += 1;
    }
    // method
    public int getValue()
    {
        return this.value;
    }

    /**
     * method
     * @param value
     */
    public void setValue(int value)
    {
        this.value = value;
    }
}

/**
 * b. getValue() and setValue()
 * c. agar tidak dapat diakses dan mengikuti konsep encapsulasi, membuat programmer yang mengimplementasikan tidak mengetahui
 *    / tidak diributkan mengenai proses dan implementasi yang ada didalamnya.
 * d. 
 * g. 3
 *    3
 *    3
 *    5
 *
 */