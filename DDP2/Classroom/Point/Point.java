/**
 */
 
public class Point
{
    private int coordX, coordY;
    public Point(int x, int y)
    {
        this.coordX = x;
        this.coordY = y;
    }
    public int getX()
    {
        return this.coordX;
    }
    public int getY()
    {
        return this.coordY;
    }
    public void setX(int x)
    {
        this.coordX = x;      
    }
    public void setY(int y)
    {
        this.coordY = y;
    }
    public void movePoint(int x, int y)
    {
        this.coordX += x;
        this.coordY += y;
    }

}