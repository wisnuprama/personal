/**
 * 
*/

import java.lang.Math;

public class Lingkaran
{   
    private double radius;
    private static double PHI = Math.PI;

    public Lingkaran(double radius)
    {
        this.radius = radius;
    }
    public double getLuas()
    {
        return PHI * this.radius * this.radius;
    }
}
