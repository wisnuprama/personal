package class01;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        try {
            o1();
        } catch (Exception e){
            System.out.println("LOL");
        }
    }

    public static void o1() throws FileNotFoundException {
        FileReader file = new FileReader("input.txt");
        Scanner in = new Scanner(file);

        Map<String, Set<String>> user = new HashMap<>();

        while(in.hasNext()){

            String line = in.nextLine();
            String parse[] = line.split(" ");

            if(user.containsKey(parse[1])){
                user.get(parse[1]).add(parse[0]);
            } else {
                Set<String> follower = new HashSet<>();
                follower.add(parse[0]);
                user.put(parse[1], follower);
            }
        }

        for(String nm: user.keySet()){
            System.out.println(nm + ": " + user.get(nm));
        }
    }

    public static void o2() throws FileNotFoundException {
        FileReader file = new FileReader("input.txt");
        Scanner in = new Scanner(file);

        Map<String, String> user = new HashMap<>();
    }

    public static boolean o3() {
        Scanner in = new Scanner(System.in);

        String str = in.nextLine();

        Stack<Character> cc = new Stack<>();

        for(int i=0; i < str.length(); ++i){
            char c = str.charAt(i);

            if(c == '('){
                cc.push(c);
            } else {
                if(cc.isEmpty()) return false;

                cc.pop();
            }
        }

        if(cc.isEmpty())
            return true;
        else return false;
    }
}
