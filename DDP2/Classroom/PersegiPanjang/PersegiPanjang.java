/**
 * PersegiPanjang
 */

public class PersegiPanjang
{
    // instanse variable-data
    private int panjang;
    private int lebar;

    // constructor
    public PersegiPanjang() 
    {
        this.panjang = 0;
        this.lebar = 0;        
    }

    public PersegiPanjang(int panjang, int lebar) 
    {
        this.panjang = panjang;
        this.lebar = lebar;        
    }

    public int hitungLuas() 
    {
        return this.panjang * this.lebar;
    }

}