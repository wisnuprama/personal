package lab10;

/**
 * Created by wisnuprama on 5/8/2017.
 */
public class Physics {

    public static final double GRAVITY = 9.81;

    // parabola
    private Physics(){}

    public static double findGLBB(double v0, double s, double t){
        return v0 * Math.sin(Math.toRadians(s)) - (GRAVITY * t);
    }

    public static double findTinggiY(double v0, double s, double t){
        return v0 * Math.sin(Math.toRadians(s)) * t - (0.5 * GRAVITY * t * t);
    }

    public static double findJarakX(double v0, double s, double t){
        return v0 * Math.cos(Math.toRadians(s)) * t;
    }

    public static double findMaxJarakX(double v0, double s){
        return v0 * v0 * Math.sin(Math.toRadians(s+s)) / GRAVITY ;
    }

    public static double findMaxTinggiY(double v0, double s){
        return 0.5 * v0 * v0 * Math.pow(Math.sin(Math.toRadians(s)), 2) / GRAVITY;
    }

    public static double findWaktuPuncak(double v0, double s){
        return (v0 * Math.sin(Math.toRadians(s))) / GRAVITY;
    }

    public static double findWaktuTotal(double v0, double s){
        return 2 * findWaktuPuncak(v0, s);
    }
}
