package lab10;

import lab10.object.Boom;
import lab10.object.Player;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by wisnuprama on 5/9/2017.
 */
public class Application
extends JFrame {
    private static final int WIDTH = 1366;
    private static final int HEIGHT = 768;
    private JPanel playerPanel;
    private JButton startButton;
    public static final int PY = 400;
    private int pos1=0, pos2=0, v1=0, s1=0;
    private GameBoom gb;

    public Application() throws HeadlessException {
        super("ANGRY BOOM");
        this.gb = null;
        create();
        setVisible(true);
    }

    private void create() {
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        Player p1 = new Player(pos1, PY, new Boom(pos1, PY, v1, s1), Color.RED);
        Player p2 = new Player(pos2, PY, new Boom(pos2, PY, 0, 0), Color.BLUE);
        gb = new GameBoom(p1, p2);
        playerPanel = new PlayerPanel();
        startButton = new JButton("Start");

        class StartAction implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                askUser();
            }
        }

        startButton.addActionListener(new StartAction());
        add(playerPanel, BorderLayout.NORTH);
        add(gb, BorderLayout.CENTER);
        add(startButton, BorderLayout.SOUTH);

    }

    private void askUser(){
        final BooleanReference FLAG = new BooleanReference(false);
        boolean READY_TO_RUN = false;

        do {
            pos1 = askUser(FLAG, "Masukan posisi player1 (dalam meter [0-1000])");

            if(FLAG.isBool() && (0 <= pos1 && pos1 <= 1280)){
                FLAG.setBool(false);
                pos2 = askUser(FLAG, "Masukan posisi player2 (dalam meter [0-1000])");

                if(FLAG.isBool() && (0 <= pos2 && pos2 <= 1000)){
                    FLAG.setBool(false);
                    v1 = askUser(FLAG, "Masukan kecepatan tembakan player1 (dalam meter/sekon [0-200])");

                    if(FLAG.isBool() && (0 <= v1 && v1 <= 200)){
                        FLAG.setBool(false);
                        s1 = askUser(FLAG, "Masukan sudut evalasi tembakan player1 (dalam derajat [0-90])");

                        if(FLAG.isBool() && (0 <= s1 && s1 <= 90)) READY_TO_RUN = true;
                        else {
                            JOptionPane.showMessageDialog(this, "Masukan untuk input tidak sesuai");
                        }

                    } else {
                        FLAG.setBool(true);
                        JOptionPane.showMessageDialog(this, "Masukan untuk input tidak sesuai");
                    }

                } else {
                    FLAG.setBool(true);
                    JOptionPane.showMessageDialog(this, "Masukan untuk input tidak sesuai");
                }

            } else {
                FLAG.setBool(true);
                JOptionPane.showMessageDialog(this, "Masukan untuk input tidak sesuai");
            }


        } while (!FLAG.isBool());

        // run game
        if(READY_TO_RUN) {
            run();
        }
    }

    private int askUser(BooleanReference b, String message){
        final int INIT = 0;
        String in = JOptionPane.showInputDialog(this, message, INIT);

        try {
            int n = Integer.parseInt(in);
            b.setBool(true);
            return n;
        } catch (NumberFormatException e){
            b.setBool(false);
        }

        return -1;
    }

    private void run(){
        Player p1 = this.gb.getPlayer1();
        Player p2 = this.gb.getPlayer2();
        Boom bom = p1.getpBoom();

        // set numbers
        p1.setX(pos1);
        bom.setX(pos1);
        bom.setY(PY);
        bom.setDegree(s1);
        bom.setSpeed(v1);

        p2.setX(pos2);
        gb.repaint();

        // FIRE!!!
        gb.fire();
    }
}
