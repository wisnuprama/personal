package lab10;

import lab10.object.Boom;
import lab10.object.Player;
import lab10.object.Renderable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created by wisnuprama on 5/9/2017.
 */
public class GameBoom
extends JPanel {

    private Player player1;
    private Player player2;
    private boolean daBoom;
    private static final Color BG = Color.white;
    public static final double INTERVAL_TIME = 0.1;

    public GameBoom(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.daBoom = false;
        setBackground(BG);

    }

    private static final double RADIUS_BOOM = 5.0;
    public void fire() {
        final double vo1 = this.player1.getpBoom().getSpeed();
        final double s1 = this.player1.getpBoom().getDegree();
        final double tMAX = Physics.findWaktuTotal(vo1, s1);

        double s;
        double boomX;
        double boomY;

        for(s=0; s < tMAX; s+=INTERVAL_TIME){
            boomX = Physics.findJarakX(vo1, s1, s);
            boomY = Physics.findTinggiY(vo1, s1, s);
            System.out.printf("t:   %5.2f detik x:   %5.2f m y:   %5.2f m\n", s, boomX, boomY);
        }

        boomX = Physics.findJarakX(vo1, s1, tMAX);
        boomY = Physics.findTinggiY(vo1, s1, tMAX);
        System.out.printf("t: %.2f detik x:  %.2f m y:  %.2f m\n", tMAX, boomX, boomY);
        /*
            Jarak dalam meter, maka asumsi jarak yang ada di mantissa adalah cm
            maka dengan jarak yang presisi di cm tetap akan mengenai player 2
            walau tidak tepat 100%
         */
        JPanel panel = this;
        Timer timer = new Timer(10, null);
        timer.addActionListener(new AbstractAction() {
            Boom bom = player1.getpBoom();
            double speedX = bom.getSpeed() * Math.cos(Math.toRadians(bom.getDegree()));
            double speedY = bom.getSpeed() * Math.sin(Math.toRadians(bom.getDegree()));

            @Override
            public void actionPerformed(ActionEvent e) {
                speedY = speedY - (Physics.GRAVITY * INTERVAL_TIME);

                double x = bom.getX();
                double y = bom.getY();

                double newX = x + speedX * INTERVAL_TIME;
                double newY = y - speedY * INTERVAL_TIME;

                bom.setX(newX);
                bom.setY(newY);

                repaint();

                if(bom.getY() >= player1.getY()){
                    timer.stop();
                    System.out.println(Math.abs(bom.getX()-player2.getX()));
                    if(Math.abs(bom.getX() - player2.getX()) <= RADIUS_BOOM){
                        daBoom = true;
                    }

                    bom.setY(Application.PY+10);
                    repaint();

                    System.out.println(getResult());
                    JOptionPane.showMessageDialog(panel, getResult());
                }
            }
        });

        timer.start();
        daBoom = false;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.player1.render(g);
        this.player2.render(g);

        // hanya player 1
        this.player1.getpBoom().render(g);
    }

    public String getResult(){
        if(daBoom)
            return "Tembabakan berhasil mengenai player 2";
        return"Tembakan gagal mengenai player 2";
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer1(Player p1){
        this.player1 = p1;
    }

    public void setPlayer2(Player p2){
        this.player2 = p2;
    }
}
