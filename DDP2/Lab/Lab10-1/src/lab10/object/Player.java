package lab10.object;

import java.awt.*;

/**
 * Created by wisnuprama on 5/9/2017.
 */
public class Player
extends AbstractPoint
implements Renderable {

    private final Color color;
    private Boom pBoom;

    private static final int HEIGHT = 30;
    private static final int WIDTH = 30;

    public Player(double x, double y, Boom boom, Color color) {
        super(x, y);
        this.color = color;
        this.pBoom = boom;
    }

    @Override
    public void render(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g2d.setPaint(color);
        g2d.fillRect((int) this.getX(), (int) this.getY(), HEIGHT, HEIGHT);

    }

    public Boom getpBoom() {
        return pBoom;
    }

    public void setpBoom(Boom pBoom) {
        this.pBoom = pBoom;
    }
}
