package lab10.object;

import java.awt.*;

/**
 * Created by wisnuprama on 5/9/2017.
 */
public class Boom
extends AbstractPoint
implements Renderable {
    private static final int RADIUS = 20;
    private static final Color BALL_COLOR = Color.RED;

    private double speed;
    private double degree;

    public Boom(double x, double y, double speed, double degree) {
        super(x, y);
        this.speed = speed;
        this.degree = degree;
    }

    @Override
    public void render(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g2d.setPaint(BALL_COLOR);
        g2d.fillOval((int) this.getX(), (int) this.getY(), RADIUS, RADIUS);
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getDegree() {
        return degree;
    }

    public void setDegree(double degree) {
        this.degree = degree;
    }
}
