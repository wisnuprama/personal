package lab10.object;

import java.awt.*;

/**
 * Created by wisnuprama on 5/9/2017.
 */
public interface Renderable {

    void render(Graphics g);
}
