public class Task {
	private String name;
	private int bobot;
	private boolean selesai;
	
	public Task(String name, int bobot) {
		this.name = name;
		this.bobot = bobot;
		selesai = false;
	}

	public String getName() {
		return name;
	}

	public int getBobot() {
		return bobot;
	}

	public boolean isSelesai() {
		return selesai;
	}

	public void setSelesai(boolean selesai) {
		this.selesai = selesai;
	}

	public String toString(){
		/*
		 
		*/
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Task: " + this.name + ", bobot: " + this.bobot + ", status: ");

		// true jika sudah selesai
		if (this.selesai){
			stringBuilder.append("Selesai");
			return stringBuilder.toString();
		}
		// belum selesai false
		stringBuilder.append("Belum Selesai");
		return stringBuilder.toString();
	}
}
