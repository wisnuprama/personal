/***
 * Wisnu Pramadhitya Ramadhan/1606918055
 * DDP2-A
 * DDP2Tutorial4
 */

public class DDP2Tutorial4 {
	public static void main(String[] args) {
        
		Employee ann = new Employee("Ann", "1SE40", 3, 0);
		System.out.println(ann);
		Employee bob = new Employee("Bob", "1SE25", 2, 1);
		System.out.println(Employee.temanTerbaru);
		Employee charlie = new Employee("Charlie", "1PM21", 1, 2);
		System.out.println(charlie);
		
		Task taskOne = new Task("Implement first feature", 10);
		System.out.println(taskOne);
		Task taskTwo = new Task("Implement second feature", 15);
		System.out.println(taskTwo);
		Task taskThree = new Task("Supervise development", 20);
		System.out.println(taskThree);
		
		System.out.println(ann.workOnTask(taskOne));
		System.out.println(bob.workOnTask(taskTwo));
		System.out.println(charlie.workOnTask(taskThree));
		
		System.out.println(taskOne);
		System.out.println(taskTwo);
		System.out.println(taskThree);

		System.out.println(Gaji.dispenseWages(ann));
        System.out.println(Gaji.dispenseWages(bob));
        System.out.println(Gaji.dispenseWages(charlie));
	}
}
