/**
 * Created by wisnuprama on 3/5/2017.
 * class util
 **************************************/
public class Gaji {
    private static final int GAJI_DASAR = 5000000;

    public static long hitungGaji(int pangkat){
        // hitung gaji sesuai pangkat
        return pangkat * GAJI_DASAR;

    }

    public static long calculateBonus(int pangkat, int totalBobot) {
        // @TODO
        // hitung bonus sesuai dengan instruksi di soal
		
        int tmpBonus;
        if (pangkat == 1)
            tmpBonus = totalBobot - 5;

        else if (pangkat == 2)
            tmpBonus = totalBobot - 10;

        else if (pangkat == 3)
            tmpBonus = totalBobot - 15;
        // pangkat unknown
        else return 0;

        if (tmpBonus <= 0)
            return 0;
        return tmpBonus*1000000;
    }

    public static String dispenseWages(Employee employee) {
        // @TODO
        // Cetak: "[nama karyawan] telah menerima gaji sebesar [gaji] dengan bonus sebesar [jumlah bonus]".
        int pangkat = employee.getPangkat();

        // return value
        return employee.getName() + " telah menerima gaji sebesar " + Gaji.hitungGaji(pangkat) + " dengan bonus sebesar "
                + Gaji.calculateBonus(pangkat, employee.getTotalBobot());
    }
}
