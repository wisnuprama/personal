/**
 * class actor
 */

public class Employee {
	private String name;
	private String employeeId;
	private int pangkat;
	private int divisi;
	private int totalBobot;

	// hmmm
	public static String temanTerbaru;
	
	public Employee(String name, String employee_id, int pangkat, int division) {
		this.name = name;
		this.employeeId = employee_id;
		this.pangkat = pangkat;
		this.divisi = division;
		totalBobot = 0;
		temanTerbaru = name;
	}
	
	public String workOnTask(Task assigned) {
		// @TODO implementasi fungsi ini
		StringBuilder stringBuilder = new StringBuilder();

		int tmpBobot = assigned.getBobot();
		if(!assigned.isSelesai()) {
			// menandai tugas selesai
			assigned.setSelesai(true);
			// menambahkan total bobot dengan bobot tugas
			this.totalBobot += tmpBobot;
			stringBuilder.append(this.name + " telah mengerjakan tugas " + assigned.getName() +  " dengan bobot " + tmpBobot);
			return stringBuilder.toString();
		}
        
		stringBuilder.append(this.name + " gagal mengerjakan tugas " + assigned.getName() + " karena status sudah selesai");
		return stringBuilder.toString();
	}

	@Override
    public String toString(){
	    
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Karyawan #" + this.employeeId + ": " + this.name + ", ");

        // menambahkan pangkat
        switch(this.pangkat){
            case 1:
                stringBuilder.append("Newbie ");
                break;
            case 2:
                stringBuilder.append("Junior ");
                break;
            case 3:
                stringBuilder.append("Senior ");
                break;
            default:
                stringBuilder.append("Unknown ");
        }

        // menambahkan divisi
        switch(this.divisi){
            case 0:
            stringBuilder.append("Engineering");
            break;
            case 1:
            stringBuilder.append("Data");
            break;
            case 2:
            stringBuilder.append("Marketing");
            break;
            default:
            stringBuilder.append("Unknown");
        }

        return stringBuilder.toString();
	}

    public String getName() {
        return name;
    }

    public int getPangkat() {
        return pangkat;
    }

    public int getTotalBobot() {
        return totalBobot;
    }
}

