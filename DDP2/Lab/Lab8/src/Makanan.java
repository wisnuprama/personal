/**
 * Class yang merepresentasikan barang
 * dengan kategori Makanan.
 */
public class Makanan
extends Item {
    public static final int DISKON_MAKANAN = 5;
    public Makanan(String name, int price) {
        super(name, price);
    }

    public void added() {
        System.out.println("Menambahkan " + this.getName() + " seharga " + this.getPrice() + ". Enak!");
    }

    @Override
    public void detail() {
        System.out.println(this.getName() + " bisa dimakan. Diskon " + DISKON_MAKANAN + "%.");
    }

    @Override
    public double price() {
        return super.price(DISKON_MAKANAN);
    }
}
