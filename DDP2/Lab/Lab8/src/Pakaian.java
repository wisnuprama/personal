/**
 * Class yang merepresentasikan barang
 * dengan kategori Pakaian.
 */
public class Pakaian extends Item {
    public static final int DISKON_PAKAIAN = 10;

    public Pakaian(String name, int price) {
        super(name, price);
    }
    public void added() {
        System.out.println("Menambahkan " + this.getName() + " seharga " + this.getPrice() + " agar tampil lebih modis!");
    }

    @Override
    public void detail() {
        System.out.println("Pakaian " + this.getName() + " dengan diskon " + DISKON_PAKAIAN + "%.");
    }

    @Override
    public double price() {
        return super.price(DISKON_PAKAIAN);
    }
}
