/**
 * Class yang merepresentasikan barang
 * yang tersedia di Kiospedia.
 *
 * Anda diperbolehkan mengubah isi class ini.
 */
public class Item implements Comparable {
    protected String name;
    protected double price;

    public Item(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public void added() {
        System.out.println("Menambah barang.");
    }

    public double price() {
        return price;
    }

    public double price(int diskon){
        return price - price * diskon * 0.01;
    }

    public void checkout() {
        System.out.println(name + " berhasil dibeli. Harga: " + this.price() + ".");
    }

    public void checkout(int diskon){
        System.out.println(name + " berhasil dibeli. Harga: " + this.price(diskon) + ".");
    }

    public void detail() {
        System.out.println("Detail barang.");
    }

    @Override
    public int compareTo(Object o) {
        if(!(o instanceof Item)) return 0;

        Item item = ((Item) o);
        return this.name.compareTo(item.name);
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}
