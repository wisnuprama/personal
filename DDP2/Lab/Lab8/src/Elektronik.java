/**
 * Class yang merepresentasikan barang
 * dengan kategori Elektronik.
 */
public class Elektronik extends Item {
    public static final int DISKON_ELEKTRONIK = 15;

    public Elektronik(String name, int price) {
        super(name, price);
    }

    public void added() {
        System.out.println("Menambahkan " + this.getName() + " seharga " + this.getPrice() + " biar canggih!");
    }

    @Override
    public void detail() {
        System.out.println(this.getName() + " adalah peralatan elektronik. Mendapat diskon " + DISKON_ELEKTRONIK + "%.");
    }

    @Override
    public double price() {
        return super.price(DISKON_ELEKTRONIK);
    }
}
