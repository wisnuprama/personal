package tp.dpp;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.*;

public class Swing extends JFrame {
    private static final int WIDTH = 300;
    private static final int HEIGHT = 150;
    private static final int WIDTH_TEXT = 20;

    private JButton check;
    private JTextField nama1, nama2;
    private JLabel labelNama1, labelNama2;
    private JPanel panel1, panel2;

    public Swing() {
        setSize(WIDTH, HEIGHT);
        setTitle("Love Meter");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        createLabel();
        createButton();
        createTextField();
        createPanel();

        this.setVisible(true);
    }

    public void createButton(){
        check = new JButton("Check !");

        class MyButtonListener implements ActionListener {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                String namaOrang1 = nama1.getText();
                String namaOrang2 = nama1.getText();

                int hasil = (ThreadLocalRandom.current().nextInt() * 1000 )% 100;

                JOptionPane.showMessageDialog(null, "Love Meter = " + hasil + "%");
            }

        }

        // add action
        check.addActionListener(new MyButtonListener());
    }

    public void createTextField(){
        nama1 = new JTextField(WIDTH_TEXT);
        nama2 = new JTextField(WIDTH_TEXT);
    }

    public void createLabel(){
        labelNama1 = new JLabel("Nama Pertama");
        labelNama2 = new JLabel("Nama Kedua");
    }

    private static final int WIDTH_LAYOUT = 2;
    private static final int HEIGHT_LAYOUT = 2;

    public void createPanel(){
        panel1 = new JPanel();
        panel2 = new JPanel();

        panel1.setLayout(new GridLayout(WIDTH_LAYOUT, HEIGHT_LAYOUT));
        panel2.setLayout(new BorderLayout());

        panel1.add(labelNama1, 0);
        panel1.add(nama1, 1);

        panel1.add(labelNama2, 2);
        panel1.add(nama2, 3);

        panel2.add(panel1, BorderLayout.NORTH);
        panel2.add(check, BorderLayout.SOUTH);

        this.add(panel2);
    }
}
