package soal.tutorial.satu.model.mapper;

import soal.tutorial.satu.model.PenggunaMahasiswaUI;

/**
 * Created by agungwb on 11/02/2017.
 */
public interface UserMapper {

    public PenggunaMahasiswaUI getPenggunaByUsername(String username);
    public PenggunaMahasiswaUI getPenggunaByUsernameAndHashedPassword(String username, String hashedPassword);

}
