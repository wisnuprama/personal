/**
 * Created by wisnuprama on 13/02/17.
 */
public class Mahasiswa {
    private String name, npm;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNpm(String npm) {
        this.npm = npm;
    }
}
