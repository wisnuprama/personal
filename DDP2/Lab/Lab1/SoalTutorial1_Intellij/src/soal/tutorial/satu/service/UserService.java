package soal.tutorial.satu.service;

import soal.tutorial.satu.model.PenggunaMahasiswaUI;
import soal.tutorial.satu.model.mapper.UserMapper;

/**
 * Created by agungwb on 11/02/2017.
 */
public class UserService {
    private UserMapper penggunaMapper;


    public PenggunaMahasiswaUI getPenggunaByUsernameAndPassword(String username, String hashedPassword){

        PenggunaMahasiswaUI pengguna = penggunaMapper.getUserByUsernameAndHashedPassword(username, hashedPassword);
        return pengguna;

    }

    public Boolean validateUser(String username, String password){

        PenggunaMahasiswaUI pengguna = getUserByUsername(username);

        if (pengguna == null) return false;

        Boolean validation = false;
        try {
            validation = Hasher.validatePassword(password, pengguna.getPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return validation;

    }

    public PenggunaMahasiswaUI getUserByUsername(String username){
        PenggunaMahasiswaUI pengguna = penggunaMapper.getUserByUsername(username);
        return pengguna;
    }

}
