package soal.tutorial.satu.model.mapper;

import soal.tutorial.satu.model.PenggunaMahasiswaUI;

/**
 * Created by agungwb on 11/02/2017.
 */
public interface UserMapper {

    public PenggunaMahasiswaUI getUserByUsername(String username);
    public PenggunaMahasiswaUI getUserByUsernameAndHashedPassword(String username, String hashedPassword);

}
