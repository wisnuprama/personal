package lab10.window;

import lab10.BooleanReference;
import lab10.game.BoomGame;
import sun.plugin2.message.Message;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by wisnuprama on 5/7/2017.
 */

public class MainFrame
extends JFrame {
    private static final int WIDTH = 1366;
    private static final int HEIGHT = 768;
    private JPanel playerPanel;
    private JButton startButton;

    public MainFrame(String title) throws HeadlessException {
        super(title);
        create();
        setVisible(true);
    }

    private void create() {
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        playerPanel = new PlayerPanel();
        startButton = new JButton("Start");
        BoomGame boom = new BoomGame(0,0,0,0);

        class StartAction implements ActionListener {
            private JFrame fr;
            private StartAction(JFrame fr) {
                this.fr = fr;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                final BooleanReference FLAG = new BooleanReference(false);
                int pos1=0, pos2=0, v1=0, s1=0;
                boolean READY_TO_RUN = false;

                do {

                    pos1 = askUser(FLAG, "Masukan posisi player1 (dalam meter)");

                    if(FLAG.isBool()){
                        FLAG.setBool(false);
                        pos2 = askUser(FLAG, "Masukan posisi player2 (dalam meter)");

                        if(FLAG.isBool()){
                            FLAG.setBool(false);
                            v1 = askUser(FLAG, "Masukan kecepatan tembakan player1 (dalam meter/sekon)");

                            if(FLAG.isBool()){
                                FLAG.setBool(false);
                                s1 = askUser(FLAG, "Masukan sudut evalasi tembakan player1 (dalam derajat)");

                                if(FLAG.isBool()) READY_TO_RUN = true;

                            } else {
                                FLAG.setBool(true);
                            }

                        } else {
                            FLAG.setBool(true);
                        }

                    } else {
                        FLAG.setBool(true);
                    }


                } while (!FLAG.isBool());

                if(READY_TO_RUN){
                    boom.setPos1(pos1);
                    boom.setPos2(pos2);
                    boom.setS1(s1);
                    boom.setVo1(v1);


                        boom.fire();

                    JOptionPane.showMessageDialog(this.fr, boom.getResult());
                }

            }
        }

        startButton.addActionListener(new StartAction(this));
        add(playerPanel, BorderLayout.NORTH);
        add(boom, BorderLayout.CENTER);
        add(startButton, BorderLayout.SOUTH);

    }

    private int askUser(BooleanReference b, String message){
        final int INIT = 0;
        String in = JOptionPane.showInputDialog(this, message, INIT);

        try {
            int n = Integer.parseInt(in);
            b.setBool(true);
            return n;
        } catch (NumberFormatException e){
            b.setBool(false);
        }

        return -1;
    }
}
