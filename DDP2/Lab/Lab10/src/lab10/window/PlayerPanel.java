package lab10.window;

import javax.swing.*;
import java.awt.*;

/**
 * Created by wisnuprama on 5/7/2017.
 */
public class PlayerPanel
extends JPanel {
    private JTextField player1;
    private JTextField player2;
    private JLabel labelPlayer1;
    private JLabel labelPlayer2;

    private static final int ROW = 2;
    private static final int COLUMN = 2;

    public PlayerPanel() {
        setLayout(new GridLayout(ROW, COLUMN));
        createComponent();
        create();
    }

    public static final String P1 = "player1";
    public static final String P2 = "player2";
    private static final int FIELD_LEN = 10;
    private void createComponent(){
        // label
        labelPlayer1 = new JLabel(P1);
        labelPlayer2 = new JLabel(P2);

        // text field
        player1 = new JTextField(FIELD_LEN);
        player2 = new JTextField(FIELD_LEN);

    }

    private void create(){
        add(labelPlayer1);
        add(labelPlayer2);
        add(player1);
        add(player2);
    }

    public String getPlayerName(int i){

        if(i == 1)
            return player1.getText();
        else if(i == 2)
            return player2.getText();

        throw new IndexOutOfBoundsException(Integer.toString(i));
    }
}
