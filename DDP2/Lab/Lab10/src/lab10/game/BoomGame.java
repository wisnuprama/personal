package lab10.game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

/**
 * Created by wisnuprama on 5/7/2017.
 */
public class BoomGame
extends JPanel {
    private int pos1;
    private int pos2;
    private int vo1;
    private int s1;
    private boolean daBoom;

    // bom
    private double boomX;
    private double boomY;

    /*
        jika interval semakin kecil gerakan bola semakin smooth
     */
    public static final double INTERVAL_TIME = 0.001;

    public BoomGame(int pos1, int pos2, int vo1, int s1) {
        super();
        this.pos1 = pos1;
        this.pos2 = pos2;
        this.vo1 = vo1;
        this.s1 = s1;
        this.daBoom = false;
        this.boomX = pos1;
        this.boomY = 0;

        setBackground(Color.white);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_ON);

        final int Y_DISPLAY = 300;
        Rectangle2D p1 = new Rectangle2D.Double(pos1, 0 + Y_DISPLAY, 50,50);
        Rectangle2D p2 = new Rectangle2D.Double(pos2, 0 + Y_DISPLAY, 50,50);

        g2d.setPaint(Color.RED);
        g2d.fillOval((int) boomX, (int) boomY+Y_DISPLAY, 20, 20);

        g2d.setPaint(Color.RED);
        g2d.fill(p1);

        g2d.setPaint(Color.BLUE);
        g2d.fill(p2);
    }

    public void fire() {

        final double tMAX = Physics.findWaktuTotal(this.vo1, this.s1);

        Timer timer = new Timer(10, null);
        timer.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double s;
                for(s=0; s < tMAX; s+=INTERVAL_TIME){
                    boomX = Physics.findJarakX(vo1, s1, s);
                    boomY = Physics.findTinggiY(vo1, s1, s);
                    System.out.printf("t:   %5.2f detik x:   %5.2f m y:   %5.2f m\n", s, boomX, boomY);
                    repaint();
                }

                boomX = Physics.findJarakX(vo1, s1, tMAX);
                boomY = Physics.findTinggiY(vo1, s1, tMAX);
                System.out.printf("t: %.2f detik x:  %.2f m y:  %.2f m\n", tMAX, boomX, boomY);
                boomY +=30;
                repaint();
        /*
            Jarak dalam meter, maka asumsi jarak yang ada di mantissa adalah cm
            maka dengan jarak yang presisi di cm tetap akan mengenai player 2
            walau tidak tepat 100%
         */
                System.out.println(boomY);
                if(boomY >= 0){
                    timer.stop();
                    if(Math.abs(boomX - pos2) <= 5.)
                        daBoom = true;

                    System.out.println(getResult());
                }
            }
        });

        timer.start();

    }

    public String getResult(){
        if(this.isDaBoom())
            return "Tembabakan berhasil mengenai player 2";
        return"Tembakan gagal mengenai player 2";
    }

    public boolean isDaBoom() {
        return daBoom;
    }

    public void setPos1(int pos1) {
        this.pos1 = pos1;
    }

    public void setPos2(int pos2) {
        this.pos2 = pos2;
    }

    public void setVo1(int vo1) {
        this.vo1 = vo1;
    }

    public void setS1(int s1) {
        this.s1 = s1;
    }
}
