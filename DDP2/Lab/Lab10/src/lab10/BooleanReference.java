package lab10;

/**
 * Created by wisnuprama on 5/8/2017.
 */

public class BooleanReference {
    private boolean bool;

    public BooleanReference(boolean bool) {
        this.bool = bool;
    }

    public void setBool(boolean bool){
        this.bool = bool;
    }

    public boolean isBool() {
        return bool;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BooleanReference that = (BooleanReference) o;

        return bool == that.bool;
    }
}
