package lab9;

/**
 * Created by wisnuprama on 5/6/2017.
 */
public class NumberFormatAtIndexException extends NumberFormatException {

    private int index;

    public NumberFormatAtIndexException() {
    }

    public NumberFormatAtIndexException(int i) {
        super(i+"");
        this.index = i;
    }

    public int getIndex(){
        return this.index;
    }
}
