package lab9;

/**
 * Created by wisnuprama on 5/4/2017.
 */
public class Statprob {

    /**
     * Mencari nilai minimal dari index X hingga Y inclusive
     * @param num
     * @param x
     * @param y
     * @return
     */
    public static int min(int num[], int x, int y){
        if(num.length == 0)
            throw new ArrayIndexOutOfBoundsException(num.length);
        if(x < 0)
            throw new ArrayIndexOutOfBoundsException(x);
        if(y >= num.length)
            throw new ArrayIndexOutOfBoundsException(y);
        if(x > y)
            throw new ArrayIndexOutOfBoundsException(x > y ? x : y);
        if(x == y)
            return num[x];

        return min(num, x, y, Integer.MAX_VALUE);
    }

    private static int min(int num[], int x, int y, int min){
        if(x > y)
            return min;

        // detect abnormal number
        if(num[x] != Integer.MIN_VALUE && num[x] < min) {
            // change min with new number
            min = num[x];
        }

        return min(num, ++x, y, min);
    }

    public static double mean(int num[], int x, int y){
        if(num.length == 0)
            throw new ArrayIndexOutOfBoundsException(num.length);
        if(x < 0)
            throw new ArrayIndexOutOfBoundsException(x);
        if(y >= num.length)
            throw new ArrayIndexOutOfBoundsException(y);
        if(x > y)
            throw new ArrayIndexOutOfBoundsException(x > y ? x : y);
        if(x == y)
            return num[x];

        int sum = 0;
        double j =.0;
        while(x <= y){
            if(num[x] != Integer.MIN_VALUE) {
                sum += num[x];
                j++;
            }
            x++;
        }

        //@TODO using try catch or preventing
        // sebenernya tidak mungkin selama array tidak kosong
        if(j <= 0) return 0;

        return sum / j;
    }

    public static double median(int num[], int x, int y){
        if(num.length == 0)
            throw new ArrayIndexOutOfBoundsException(num.length);
        if(x < 0)
            throw new ArrayIndexOutOfBoundsException(x);
        if(y > num.length)
            throw new ArrayIndexOutOfBoundsException(y);
        if(x > y)
            throw new ArrayIndexOutOfBoundsException(x > y ? x : y);
        if(x == y)
            return num[x-1];

        int len = y;
        int mid;

        /*
            asumsi bahwa item yang berada di index mid adalah
            bukan integer
         */

        if(len % 2 == 0){
            mid = len / 2;
            if(num[mid-1] == Integer.MIN_VALUE)
                throw new NumberFormatAtIndexException(mid);

            if(num[mid] == Integer.MIN_VALUE)
                throw new NumberFormatAtIndexException(mid+1);

            return 0.5 * (num[mid-1] + num[mid]);
        }

        mid = (len-1) / 2;
        if(num[mid] == Integer.MIN_VALUE)
            throw new NumberFormatAtIndexException(mid);

        return num[mid];
    }

    public static double varian(int num[], int x, int y){
        if(num.length == 0)
            throw new ArrayIndexOutOfBoundsException(num.length);
        if(x < 0)
            throw new ArrayIndexOutOfBoundsException(x);
        if(y >= num.length)
            throw new ArrayIndexOutOfBoundsException(y);
        if(x > y)
            throw new ArrayIndexOutOfBoundsException(x > y ? x : y);
        if(x == y)
            throw new ArithmeticException();

        double len = y - x;
        double sumSigma = 0;
        double mean = Statprob.mean(num, x, y);

        while(x <= y){
            sumSigma += Math.pow(num[x++]-mean, 2);
        }

        return sumSigma / (len - 1);
    }

}
