package lab9;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Lab9 {
    private static final Logger LOGGER = Logger.getLogger(Lab9.class.getName());
    private static final String ERROR_MESSAGE = "[Error] = ";
    private static final String ERROR_NUMBER_MESSAGE = ERROR_MESSAGE + "Invalid Number Format at ";
    private static String TEMP_ARRAY_OF_NUMBERS[] = new String[1];

    public static void main(String[] args) {
	// write your code here
        final String inputFile = "input.in";
        final String outputFile = "output.out";
        /*
         * N = panjang array
         * Q = banyak operasi
         * line = input line
         * parsing = array of input
         */
        int N, Q, num[];
        String line, inputArr[];

        /*
         * automatically closing file
         * behaviornya syntax sama seperti with open file milik python
         * Java 1.7++
         *
         * writing file
         */

        try(BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile))){
            /*
            * reading file
            * and put to string builder and write to output file
            */
            StringBuilder writing = new StringBuilder();

            try (BufferedReader br = new BufferedReader(new FileReader(inputFile))){

                line = br.readLine();
                // get panjang array
                N = Integer.parseInt(line);

                // parsing numbers
                // get array of number which is still in string
                TEMP_ARRAY_OF_NUMBERS = br.readLine().trim().split(" ");
                num = new int[N];
                Lab9.convertArrayOfNumbers(num, TEMP_ARRAY_OF_NUMBERS);

                // get numbers of operations
                line = br.readLine().trim();
                Q = Integer.parseInt(line);

                while (Q-- > 0 && br.ready() && (line = br.readLine()) != null) {
                    // reuse inputArr karena sudah di transfer ke num
                    inputArr = line.trim().split(" ");
                    writing.append(Lab9.processFile(num, inputArr));
                }

            } catch (FileNotFoundException e){
                LOGGER.log(Level.WARNING, e.getMessage());
                bw.write(ERROR_MESSAGE + "File not found Exception for " + '\"' + inputFile + "\"\n");
                bw.flush();

            } catch (IOException e){
                LOGGER.log(Level.WARNING, e.getMessage());
            } catch (NumberFormatException e){
                LOGGER.log(Level.WARNING, e.getMessage());
            } catch (Exception e){
                LOGGER.log(Level.WARNING, "Exception " + e.getMessage());
            } finally {

                // writing
                bw.write(writing.toString());
                bw.flush();
            }

        } catch (IOException e){
            LOGGER.log(Level.WARNING, e.getMessage());
        }
    }

    private static void convertArrayOfNumbers(int num[], String numStr[]){
        /*
            turn array of string to array of numbers
         */
        for(int i=0; i < num.length; ++i){
            try{
                num[i] = Integer.parseInt(numStr[i]);
            } catch (NumberFormatException e){
                LOGGER.log(Level.INFO, e.getMessage());
                /*
                fill this index with big number so we can detect some bad item in index i
                 */
                num[i] = Integer.MIN_VALUE;
            }
        }
    }

    private static String processFile(int num[], String input[]) {
        /* CONSTRAINT
            N, Q, X, Y dijamin bilangan
            X <= Y
            Untuk hal lain yang tidak dijelaskan, silahkan buat asumsi sendiri
         */
        int len = input.length;
        StringBuilder str = new StringBuilder();
        Integer x=null,y=null;

        try {
            x = Integer.parseInt(input[1]);
            y = Integer.parseInt(input[2]);

            /*
             * asumsi bahwa
             * jika index bawah yang error maka x + 1
             * jika index atas yang error maka y - 1
             * jika di dalam range ada error, maka yang ditulis terlebih dahulu adalah error
             * dan diakhiri dengan hasil
             */
            while(num[x-1] == Integer.MIN_VALUE) {
                str.append(ERROR_NUMBER_MESSAGE + x++ + "\n");
            }

            while(num[y-1] == Integer.MIN_VALUE) {
                str.append(ERROR_NUMBER_MESSAGE + y-- + "\n");
            }

            /*
             * hanya periksa exclusive dalam range
             */
            for(int i=x; i < y-1; ++i){
                if(num[i] == Integer.MIN_VALUE)
                    str.append(ERROR_NUMBER_MESSAGE + (i+1) + "\n");
            }

            if(len == 3 && "MIN".equalsIgnoreCase(input[0])){
                str.append("Nilai minimum dari index "+ x + " - " + y + " adalah "
                            + Statprob.min(num, x-1, y-1) + "\n");

            } else if(len == 3 && "MEDIAN".equalsIgnoreCase(input[0])){
                double med;
                try {
                    med = Statprob.median(num, x, y);

                    str.append("Nilai minimal dari index " + x + " - " + y + " adalah "
                            + med + "\n");
                } catch (NumberFormatAtIndexException e){
                    LOGGER.log(Level.INFO, "Median: " + e.getMessage());
                    /*
                        karena semua error yang ada dalam range sudah ditambahkan
                        ke dalam string builder maka error ini hanya perlu di finally
                        dan tidak melakukan apa-apa jika error
                     */
                }


            } else if(len == 3 && "MEAN".equalsIgnoreCase(input[0])){
                str.append("Nilai rata-rata dari index ke " + x + " - " + y + " adalah "
                            + Statprob.mean(num, x-1, y-1) + "\n");

            } else if(len == 3 && "VARIAN".equalsIgnoreCase(input[0])){
                str.append("Nilai varian dari index ke " + x + " - " + y + " adalah "
                            + Statprob.varian(num, x-1, y-1) + "\n");

            } else{
                str.append(ERROR_MESSAGE + "cannot found operation \"" + input[0] + "\"\n");
            }

        } catch (ArrayIndexOutOfBoundsException e){
            LOGGER.log(Level.WARNING, "Index out of bounds at " + e.getMessage());
            str.append(ERROR_MESSAGE + "Index out of bounds at " + e.getMessage());

        } catch (NumberFormatException e){
            LOGGER.log(Level.WARNING, "Number format exception " + e.getMessage());
            if(x == null) {
                str.append(ERROR_NUMBER_MESSAGE + input[1] + "\n");
            } else if (y == null) {
                str.append(ERROR_NUMBER_MESSAGE + input[2] + "\n");
            }
        } catch (Exception e){
            LOGGER.log(Level.WARNING, "IN EXCEPTION");
        }

        return str.toString();
    }
}
