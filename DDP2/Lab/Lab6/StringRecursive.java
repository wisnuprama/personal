import java.util.*;

public class StringRecursive {
    public static void main(String args[]) {
        String line;
        Scanner input = new Scanner(System.in);

        line = input.nextLine();
        System.out.println(checkDuplicate(line));
        
    }

    private static String reverse(String str, String result) {
            // base case
            if(str.length() == 0){
                return result;
            }

            char head = str.charAt(0);
            return reverse(str.substring(1), head+result);
    }

    public static String reverse(String str) {
        return reverse(str, "");     
    }

    private static boolean checkDuplicate(String str, int p1, int p2) {
        if(p1 == str.length()-1) return false;

        if(Character.toLowerCase(str.charAt(p1)) == Character.toLowerCase(str.charAt(p2)))
            return true;
        
        return checkDuplicate(str, p1+1, p2+1);
    }

    public static boolean checkDuplicate(String str) {
        if(str.isEmpty() || str.length() <= 1) return false;
        return checkDuplicate(str, 0, 1);
    }
}