package lab.ddp2.resources;

/**
 * Created by wisnuprama on 15/04/17.
 */
public class Security extends Karyawan {
    private static final long GAJI_SECURITY = 3500000;
    public static final String JABATAN_SECURITY = "Security";

    public Security(String namaKaryawan, String jabatan, String gender) {
        super(namaKaryawan, jabatan, gender);
    }

    @Override
    public String gajian() {
        super.tambahGaji(Security.GAJI_SECURITY);
        return super.gajian();
    }

    public String jaga(){
        return this.getNamaKaryawan() + " sedang menjaga perusahaan";
    }

    @Override
    public String toString() {
        return JABATAN_SECURITY + ": " + this.getNamaKaryawan();
    }
}
