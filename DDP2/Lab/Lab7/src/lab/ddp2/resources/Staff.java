package lab.ddp2.resources;

/**
 * Created by wisnuprama on 15/04/17.
 */
public abstract class Staff extends Karyawan{
    private static final long GAJI_STAFF = 5000000;

    public Staff(String namaKaryawan, String jabatan, String gender) {
        super(namaKaryawan, jabatan, gender);
    }

    @Override
    public String gajian() {
        super.tambahGaji(Staff.GAJI_STAFF);
        return super.gajian();
    }
}
