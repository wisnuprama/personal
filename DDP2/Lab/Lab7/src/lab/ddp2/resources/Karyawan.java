package lab.ddp2.resources;

/**
 * Created by wisnuprama on 15/04/17.
 */
public abstract class Karyawan {
    private String namaKaryawan;
    private String jabatan;
    private String gender;
    private long gaji;

    public final static String GENDER_PRIA = "Pria";
    public final static String GENDER_WANITA = "Wanita";

    public Karyawan(String namaKaryawan, String jabatan, String gender) {
        this.namaKaryawan = namaKaryawan;
        this.jabatan = jabatan;
        this.gender = gender;
        this.gaji = 0;
    }

    public void lembur(long bonus){
        this.tambahGaji(bonus);
    }

    public String sapa(Karyawan other){
        String sapaan = "";
        if(other instanceof Staff)
            sapaan = "Kak";
        else {
            switch (other.getGender()) {
                // PRIA
                case Karyawan.GENDER_PRIA:
                    if (other instanceof Janitor) sapaan = "Mas";
                    else if (other instanceof Security) sapaan = "Pak";
                    break;

                // WANITA
                case Karyawan.GENDER_WANITA:
                    if (other instanceof Janitor) sapaan = "Mbak";
                    else if (other instanceof Security) sapaan = "Bu";
                    break;
            }
        }

        return this.namaKaryawan + " menyapa " + sapaan + " " + other.getNamaKaryawan() + " dengan penuh kasih sayang";
    }

    public String gajian(){
        return this.getJabatan() + " bernama " + this.getNamaKaryawan() + " memiliki gaji " + this.getGaji();
    }

    protected void tambahGaji(long gaji){
        this.gaji += gaji;
    }

    public String getNamaKaryawan() {
        return namaKaryawan;
    }

    public String getJabatan() {
        return jabatan;
    }

    public String getGender() {
        return gender;
    }

    public long getGaji() {
        return gaji;
    }
}
