package lab.ddp2.resources;

/**
 * Created by wisnuprama on 15/04/17.
 */
public class StaffHRD extends Staff {
    public static final String JABATAN_STAFF_HRD = "Staf HRD";

    public StaffHRD(String namaKaryawan, String jabatan, String gender) {
        super(namaKaryawan, jabatan, gender);
    }

    public String kelola(){
        return this.getNamaKaryawan() + " sedang mengelola perusahaan";
    }

    @Override
    public String toString() {
        return JABATAN_STAFF_HRD + ": " + this.getNamaKaryawan();
    }
}
