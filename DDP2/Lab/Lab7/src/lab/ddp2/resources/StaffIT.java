package lab.ddp2.resources;

/**
 * Created by wisnuprama on 15/04/17.
 */
public class StaffIT extends Staff{
    public static final String JABATAN_STAFF_IT = "Staf IT";

    public StaffIT(String namaKaryawan, String jabatan, String gender) {
        super(namaKaryawan, jabatan, gender);
    }

    public String ngoding(){
        return this.getNamaKaryawan() + " sedang ndoging gembira";
    }

    @Override
    public String toString() {
        return JABATAN_STAFF_IT + ": " + this.getNamaKaryawan();
    }
}
