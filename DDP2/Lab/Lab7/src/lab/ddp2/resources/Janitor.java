package lab.ddp2.resources;

/**
 * Created by wisnuprama on 15/04/17.
 */
public class Janitor extends Karyawan {
    private static final long GAJI_JANITOR = 4000000;
    public static final String JABATAN_JANITOR = "Janitor";

    public Janitor(String namaKaryawan, String jabatan, String gender) {
        super(namaKaryawan, jabatan, gender);
    }

    @Override
    public String gajian() {
        super.tambahGaji(Janitor.GAJI_JANITOR);
        return super.gajian();
    }

    public String sapu(){
        return this.getNamaKaryawan() + " sedang menyapu bersih";
    }

    @Override
    public String toString() {
        return JABATAN_JANITOR + ": " + this.getNamaKaryawan();
    }
}
