package lab.ddp2;

import lab.ddp2.resources.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by wisnuprama on 15/04/17.
 */
public class Perusahaan {
    private List<Karyawan> listKaryawan;
    private static final double PERSEN_BONUS_LEMBUR = 0.15;
    private static final Logger LOGGER = Logger.getLogger(Perusahaan.class.getName());

    public static void main(String[] args) {
        // write your code here
        String[] parsing;
        String nama;
        String gender;
        Karyawan karyawan;
        Perusahaan perusahaan = new Perusahaan();
        Scanner input = new Scanner(System.in);

        while(input.hasNextLine()){
            parsing = input.nextLine().split(" ");
            try {
                switch (parsing[0].toUpperCase()) {
                    case "REKRUT":
                        nama = parsing[1];
                        if (perusahaan.isKaryawan(nama)) {
                            System.out.println("Sudah ada karyawan bernama " + nama);
                            break;
                        }

                        if ("staf".equalsIgnoreCase(parsing[2]) && "IT".equalsIgnoreCase(parsing[3])) {
                            gender = parsing[4];
                            StaffIT newStafIT = new StaffIT(nama, StaffIT.JABATAN_STAFF_IT, gender);
                            System.out.println(perusahaan.recruit(newStafIT));

                        } else if ("staf".equalsIgnoreCase(parsing[2]) && "HRD".equalsIgnoreCase(parsing[3])) {
                            gender = parsing[4];
                            StaffHRD newStafHRD = new StaffHRD(nama, StaffHRD.JABATAN_STAFF_HRD, gender);
                            System.out.println(perusahaan.recruit(newStafHRD));

                        } else if ("security".equalsIgnoreCase(parsing[2])) {
                            gender = parsing[3];
                            Security newSecurity = new Security(nama, Security.JABATAN_SECURITY, gender);
                            System.out.println(perusahaan.recruit(newSecurity));

                        } else if ("janitor".equalsIgnoreCase(parsing[2])) {
                            gender = parsing[3];
                            Janitor newJanitor = new Janitor(nama, Janitor.JABATAN_JANITOR, gender);
                            System.out.println(perusahaan.recruit(newJanitor));
                        }

                        break;

                    case "GAJIAN":
                        System.out.println(perusahaan.perintahGajian());
                        break;

                    case "LEMBUR":
                        // cut array to get names
                        System.out.println(perusahaan.perintahLembur(Arrays.copyOfRange(parsing, 1, parsing.length)));
                        break;

                    case "SAPA":
                        nama = parsing[1];
                        String other = parsing[2];
                        boolean check1;
                        boolean check2;

                        if ((check1 = perusahaan.isKaryawan(nama)) & (check2 = perusahaan.isKaryawan(other))) {
                            karyawan = perusahaan.get(nama);
                            Karyawan otherKaryawan = perusahaan.get(other);
                            karyawan.sapa(otherKaryawan);
                        } else {
                            if (!check1) {
                                System.out.println("Tidak ada karyawan bernama " + nama);
                            }
                            if (!check2) {
                                System.out.println("Tidak ada karyawan bernama " + other);
                            }
                        }
                        break;

                    case "NGODING":
                        nama = parsing[1];
                        if (perusahaan.isKaryawan(nama)) {
                            karyawan = perusahaan.get(nama);
                            if (karyawan instanceof StaffIT) {
                                StaffIT stafIT = (StaffIT) karyawan;
                                System.out.println(stafIT.ngoding());
                            } else {
                                System.out.println(nama + " tidak bisa mengambil pekerjaan lain");
                            }
                        } else {
                            System.out.println("Tidak karyawan bernama " + nama);
                        }

                        break;

                    case "KELOLA":
                        nama = parsing[1];
                        if (perusahaan.isKaryawan(nama)) {
                            karyawan = perusahaan.get(nama);
                            if (karyawan instanceof StaffHRD) {
                                StaffHRD hrd = (StaffHRD) karyawan;
                                System.out.println(hrd.kelola());
                            } else {
                                System.out.println(nama + " tidak bisa mengambil pekerjaan lain");
                            }
                        } else {
                            System.out.println("Tidak karyawan bernama " + nama);
                        }

                        break;

                    case "SAPU":
                        nama = parsing[1];
                        if (perusahaan.isKaryawan(nama)) {
                            karyawan = perusahaan.get(nama);
                            if (karyawan instanceof Janitor) {
                                Janitor janitor = (Janitor) karyawan;
                                System.out.println(janitor.sapu());
                            } else {
                                System.out.println(nama + " tidak bisa mengambil pekerjaan lain");
                            }
                        } else {
                            System.out.println("Tidak karyawan bernama " + nama);
                        }

                        break;

                    case "JAGA":
                        nama = parsing[1];
                        if (perusahaan.isKaryawan(nama)) {
                            karyawan = perusahaan.get(nama);
                            if (karyawan instanceof StaffIT) {
                                StaffIT bpkSatpam = (StaffIT) karyawan;
                                System.out.println(bpkSatpam.ngoding());
                            } else {
                                System.out.println(nama + " tidak bisa mengambil pekerjaan lain");
                            }
                        } else {
                            System.out.println("Tidak karyawan bernama " + nama);
                        }

                        break;

                    default:
                        LOGGER.log(Level.INFO, "Command not found");

                }
            } catch (IndexOutOfBoundsException e) {
                LOGGER.log(Level.WARNING, "Parsing Error (" + parsing.length + "):" + e.getMessage());
            }

        }
    }

    public Perusahaan(){
        this.listKaryawan = new ArrayList<>();
    }

    public boolean isKaryawan(String namaKaryawan){
        for(Karyawan kk:this.listKaryawan){
            if(kk.getNamaKaryawan()
                    .equalsIgnoreCase(namaKaryawan))
                return true;
        }

        return false;
    }

    public Karyawan get(String namaKaryawan){
        for(Karyawan kk:listKaryawan){
            if(kk.getNamaKaryawan()
                    .equalsIgnoreCase(namaKaryawan))
                return kk;
        }

        return null;
    }

    public Karyawan get(int index){
        return this.listKaryawan.get(index);
    }

    public String recruit(Karyawan newKaryawan){
        String str;

        if(!this.isKaryawan(newKaryawan.getNamaKaryawan())){
            this.listKaryawan.add(newKaryawan);
            str = newKaryawan.getNamaKaryawan() + " diterima bekerja sebagai " + newKaryawan.getJabatan();
        } else
            str = "Sudah ada karyawan bernama " + newKaryawan.getNamaKaryawan() + " yang diterima";

        return str;
    }

    public String perintahLembur(String[] daftarKaryawanLembur){
        Karyawan kk;
        Long bonus;
        StringBuilder str = new StringBuilder();
        String tmp;
        for(int i=0; i < daftarKaryawanLembur.length; ++i){
            tmp = daftarKaryawanLembur[i];
            if(this.isKaryawan(tmp)){
                kk = this.get(tmp);

                double tmpBonus = kk.getGaji() * PERSEN_BONUS_LEMBUR;
                long bonus = (long) tmpBonus;
                kk.lembur(bonus);
                str.append("Gaji " + kk.getNamaKaryawan() + " telah ditambahkan sebanyak " + bonus);
            } else {
                // karyawan kk tidak ditemukan
                str.append("Tidak ada karyawan bernama " + tmp);
            }
        }

        return str.toString();
    }

    public String perintahGajian(){
        if (this.listKaryawan.isEmpty())
            return "Tidak ada karyawan yang terdaftar";

        Karyawan kk;
        StringBuilder str = new StringBuilder();
        for(int i=0; i < this.listKaryawan.size(); ++i){
            kk = this.listKaryawan.get(i);

            if(kk instanceof Staff){
                Staff staf = (Staff) kk;
                str.append(staf.gajian());
            } else if (kk instanceof Janitor){
                Janitor jan = (Janitor) kk;
                str.append(jan.gajian());
            } else if (kk instanceof Security){
                Security sec = (Security) kk;
                str.append(sec.gajian());
            } else {
                LOGGER.log(Level.INFO, "Karyawan not found: ", kk);
            }

            if(i < this.listKaryawan.size())
                str.append("\n");
        }
        return str.toString();
    }
}
