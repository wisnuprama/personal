public class Manusia {
    // initiate
    private String nama;
    private int umur;
    private int berat;
    private int tinggi;
	private char jenisKelamin;
    private double totalKalori;

    public Manusia(String nama, int umur, int berat, int tinggi, char jenisKelamin) {
        // @TODO constructor
        this.nama = nama;
        this.umur = umur;
        this.berat = berat;
        this.tinggi = tinggi;
        this.jenisKelamin = jenisKelamin;
        this.totalKalori = 0;

    }

    private double hitungBMR() {
        // @TODO kalkulasi BMR
        // https://en.wikipedia.org/wiki/Harris%E2%80%93Benedict_equation
        /**
         Men	BMR = (10 × weight in kg) + (6.25 × height in cm) - (5 × age in years) + 5
         Women	BMR = (10 × weight in kg) + (6.25 × height in cm) - (5 × age in years) - 161
         */
        if (this.jenisKelamin == 'L' || this.jenisKelamin == 'l')
            return (10 * this.berat) + (6.25 * this.tinggi) - (5 * this.umur) + 5;

        else if (this.jenisKelamin == 'P' || this.jenisKelamin == 'p')
            return (10 * this.berat) + (6.25 * this.tinggi) - (5 * this.umur) - 161;

        return 0;
    }

    private double hitungKaloriHarian() {
        // @TODO kalkulasi kebutuhan kalori harian
        // (lihat perhitungan Harris-Benedict, asumsikan manusia melakukan light exercise)
        return hitungBMR() * 1.375;
    }

    public void makan(Makanan makanan) {
        // @TODO
        // tambah totalKalori sebanyak kalori dari makanan
        // tolak jika kalori tambahan akan melebihi kebutuhan kalori harian
        // Cetak "[nama manusia] memakan [nama makanan] yang rasanya [rasa]"
        // [nama manusia] tidak bisa memakan [nama makanan] karena akan melebihi batas kebutuhan kalori hariannya.
        double temp2;
        temp2 = makanan.getKalori();
        if (hitungKaloriHarian() < (this.totalKalori + temp2)){
            System.out.printf("%s tidak bisa memakan %s karena akan melebihi batas kebutuhan kalori hariannya.\n",
                                this.nama, makanan.getNama(), makanan.getRasa());
        }
        else{
            this.totalKalori += temp2;
            System.out.printf("%s memakan %s yang rasanya %s.\n",
                                this.nama, makanan.getNama(), makanan.getRasa());
        }
    }

    public void cetakInformasi() {
        // @TODO
        // keluarkan nama, umur, berat, tinggi, totalKalori, BMR, dan kebutuhan kalori harian
        // -------
        // Contoh:
        // -------
        // John Smith
        // Umur    : 25 Tahun
        // Berat   : 60 KG
        // Tinggi  : 180 CM
        // Kalori  : 0
        // BMR     : 1618
        // KKH     : 2207 Kalori/hari
        System.out.println("\n"+this.nama);
        System.out.printf("Umur\t: %d Tahun\n", this.umur);
        System.out.printf("Berat\t: %d KG\n",this.berat);
        System.out.printf("Tinggi\t: %d CM\n", this.tinggi);
        System.out.printf("Kalori\t: %d\n", (int)this.totalKalori);
        System.out.printf("BMR\t\t: %d\n", (int)hitungBMR());
        System.out.printf("KKH\t\t: %d Kalori/hari\n\n", (int)hitungKaloriHarian());
    }

    // @TODO setter-getter seperlunya

    public String getNama() {
        return nama;
    }

    public double getTotalKalori() {
        return totalKalori;
    }

    public void isMenambahKalori(double kalori) {
        this.totalKalori += kalori;
    }

    public void isGunakanKalori(double kaloriPakai) {
        this.totalKalori -= kaloriPakai;
    }

}