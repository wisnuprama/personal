public class Makanan {
    private String nama;
    private String rasa;
    private int kalori;

    public Makanan(String nama){
            // @TODO constructor makanan dengan 0 kalori dan rasa tidak enak
        this.nama = nama;
        this.rasa = "tidak enak";
        this.kalori = 0;
    }

    public Makanan(String nama, String rasa, int kalori) {
        // @TODO constructor
        this.nama = nama;
        this.rasa = rasa;
        this.kalori = kalori;
    }

    // @TODO setter-getter seperlunya


    public String getNama() {
        return nama;
    }

    public int getKalori() {
        return kalori;
    }

    public void setKalori(int kalori) {
        this.kalori = kalori;
    }

    public String getRasa() {
        return rasa;
    }

    public void setRasa(String rasa) {
        this.rasa = rasa;
    }
}