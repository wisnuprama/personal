public class DDP2Tutorial2 {
    public static void main(String[] args) {
        // @TODO instansiasi obyek manusia
        Manusia manusia = new Manusia("John Smith", 25, 60, 180, 'L');

        manusia.cetakInformasi(); 

        // @TODO instansiasi beberapa makanan:
        // apel: 52 kalori
        // nasi goreng: 163 kalori
        // nasi padang: 306 kalori
        // indomie: 342 kalori
        // soto betawi: 363 kalori
        // pizza: 266 kalori
        Makanan apel, nasiGoreng, nasiPadang, indomie, sotoBetawi, pizza;

        apel = new Makanan("Apel", "enak", 52);
        nasiGoreng = new Makanan("Nasi Goreng", "enak", 163);
        nasiPadang = new Makanan("Nasi Padang", "enak", 306);
        indomie = new Makanan("Indomie", "enak", 342);
        sotoBetawi = new Makanan("Soto Betawi", "tidak enak", 363);
        pizza = new Makanan("Pizza", "enak", 266);

        // ------------------------
        manusia.makan(nasiPadang);
        manusia.makan(apel);
        manusia.cetakInformasi();
        // ----------------
        // Contoh keluaran:
        // ----------------
        // John Smith
        // Umur    : 25 Tahun
        // Berat   : 60 KG
        // Tinggi  : 180 CM
        // Kalori  : 358
        // BMR     : 1618
        // KKH     : 2207 Kalori/hari

        // @TODO instansiasi gym dengan koefisien 1.25
        Gym gym = new Gym("Celebrity Fitness", 1.25);

        gym.gunakanTreadmill(manusia, 10, 10);
        gym.gunakanTreadmill(manusia, 20, 10);
        gym.gunakanTreadmill(manusia, 500, 10);
        manusia.cetakInformasi();
        // ----------------
        // Contoh keluaran:
        // ----------------
        // John Smith
        // Umur    : 25 Tahun
        // Berat   : 60 KG
        // Tinggi  : 180 CM
        // Kalori  : 208
        // BMR     : 1618
        // KKH     : 2207 Kalori/hari

        manusia.makan(indomie);
        manusia.makan(indomie);
        manusia.makan(indomie);
        manusia.makan(indomie);
        manusia.makan(indomie);
        manusia.makan(indomie); // kelebihan makan
        manusia.cetakInformasi();
        // ----------------
        // Contoh keluaran:
        // ----------------
        // John Smith
        // Umur    : 25 Tahun
        // Berat   : 60 KG
        // Tinggi  : 180 CM
        // Kalori  : 1928
        // BMR     : 1618
        // KKH     : 2207 Kalori/hari

        gym.gunakanStationaryBike(manusia, 30);
        gym.gunakanTreadmill(manusia, 30, 10);
        manusia.cetakInformasi();
        // ----------------
        // Contoh keluaran:
        // ----------------
        // John Smith
        // Umur    : 25 Tahun
        // Berat   : 60 KG
        // Tinggi  : 180 CM
        // Kalori  : 1103
        // BMR     : 1618
        // KKH     : 2207 Kalori/hari
    }
}