public class Gym {
    private String nama;
    private double koefisienEfektivitas;

    public Gym(String nama, double koefisien) {
        // @TODO constructor
        this.nama = nama;
        this.koefisienEfektivitas = koefisien;
    }

    public void gunakanTreadmill(Manusia manusia, int waktu, int kecepatan) {
        // @TODO 
        // kurangi totalKalori manusia sebanyak kalkulasi kalori
        // tolak jika totalKalori yang ada kurang dari kalori yang akan dibakar dan cetak
        // [nama manusia] tidak dapat menggunakan treadmill karena kekurangan energi."
        // apabila berhasil cetak "[nama manusia] menggunakan treadmill selama [waktu] dengan kecepatan [kecepatan]"

        double tempKaloriManusia, tempKaloriTreadmill;
        tempKaloriManusia = manusia.getTotalKalori();
        tempKaloriTreadmill = hitungKaloriTreadmill(waktu, kecepatan);

        if (tempKaloriManusia < tempKaloriTreadmill)
            System.out.printf("%s tidak dapat menggunakan treadmill karena kekurangan energi.\n",
                            manusia.getNama());

        else{
            manusia.isGunakanKalori(tempKaloriTreadmill);
            System.out.printf("%s menggunakan treadmill selama %d menit dengan kecepatan %d km/j \n",
                            manusia.getNama(), waktu, kecepatan);
        }

    }

    private double hitungKaloriTreadmill(int waktu, int kecepatan) {
        // @TODO kalkulasi kalori terbakar:
        //      koefisienEfektivitas * 1.2 * kecepatan (km/h) * waktu (menit)
        return this.koefisienEfektivitas * 1.2 * kecepatan * waktu;
    }

    public void gunakanStationaryBike(Manusia manusia, int waktu) {
        // @TODO kurangi totalKalori manusia sebanyak kalkulasi kalori
        // tolak jika totalKalori yang ada kurang dari kalori yang akan dibakar dan cetak
        // "[nama manusia] tidak dapat menggunakan stationary bike karena kekurangan energi."
        // apabila berhasil cetak "[nama manusia] menggunakan stationary bike selama [waktu]"
        double tempKaloriManusia, tempKaloriBike;
        tempKaloriManusia = manusia.getTotalKalori();
        tempKaloriBike = hitungKaloriStationaryBike(waktu);

        if (tempKaloriManusia < tempKaloriBike)
            System.out.printf("%s tidak dapat menggunakan stationary bike karena kekurangan energi.\n",
                                manusia.getNama());
        else{
            manusia.isGunakanKalori(tempKaloriBike);
            System.out.printf("%s menggunakan stationary bike selama %d menit\n",
                                manusia.getNama(), waktu);
        }

    }

    private double hitungKaloriStationaryBike(int waktu) {
        // @TODO kalkulasi kalori terbakar:
        //      koefisienEfektivitas * 10 * waktu
        return this.koefisienEfektivitas * 10 * waktu;
    }
}