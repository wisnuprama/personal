//*
// Creator: Wisnu Pramadhitya Ramadhan
// Menghitung umur dari tgl lahir hingga - hari ini menggunakan package java.time
// https://docs.oracle.com/javase/8/docs/api/java/time/LocalTime.html
// https://docs.oracle.com/javase/8/docs/api/java/time/Period.html

import java.time.LocalDate;
import java.time.Period;

public class Internship{
    // initiate
    private String nama;
    private String asalKota;
    private String tanggalLahir;
    private String golDarah;
    private String pendidikan;
    private String pengalaman;
    private int tahunLulus;
    private int umur;
    private double berat;
    private double tinggi;
    private double bmi;
    private long noNIK;
    private boolean jomblo;


    public Internship(String nama, String ttl, long nik, double berat, double tinggi, String golDarah) {
        // @TODO constructor
        this.nama = nama;
        this.noNIK = nik;

        // ttl
        String[] tmpArr = ttl.split("/");
        this.asalKota = tmpArr[0];
        this.tanggalLahir = tmpArr[1];
        this.umur = Calculator.hitungUmur(this.tanggalLahir);

        // fisik
        this.berat = berat;
        this.tinggi = tinggi;
        this.golDarah = golDarah;
        this.bmi = Calculator.hitungBMI(this.tinggi, berat);
    }

    public String getData(){
        // @TODO format cetak
        /** print data yang tersimpan
         */

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("\nDATA TERSIMPAN\n");
        stringBuilder.append("-----------------------------------\n");
        stringBuilder.append(String.format("%s - %d tahun - BMI : %.4f\n", this.nama, this.umur, this.bmi));
        stringBuilder.append(String.format("Asal %s Bergolongan darah %s\n", this.asalKota, this. golDarah));
        stringBuilder.append(String.format("Lulus dalam %d tahun dari %s\n",
                                Calculator.hitungLulus(this.tahunLulus), this.pendidikan));
        stringBuilder.append(String.format("Berpengalaman %s\n", this.pengalaman));

        if (this.jomblo)
            stringBuilder.append("Belum mempunyai pasangan hidup :(");
        else
            stringBuilder.append("Sudah mempunyai pasangan hidup :)");

        return stringBuilder.toString();
    }

    public String getNama() {
        return nama;
    }

    public String getGolDarah() {
        return golDarah;
    }

    public int getUmur() {
        return umur;
    }

    public double getBerat() {
        return berat;
    }

    public double getTinggi() {
        return tinggi;
    }

    public void setPendidikan(String pendidikan) {
        this.pendidikan = pendidikan;
    }

    public void setPengalaman(String pengalaman) {
        this.pengalaman = pengalaman;
    }

    public int getTahunLulus() {
        return tahunLulus;
    }

    public void setTahunLulus(int tahunLulus) {
        this.tahunLulus = tahunLulus;
    }

    public boolean isJomblo() {
        return jomblo;
    }

    public void setJomblo(boolean jomblo) {
        this.jomblo = jomblo;
    }

    public double getBMI() {
        return bmi;
    }

}

class Calculator {

    private Calculator(){}

    public static int hitungUmur(String tanggal){
        // @TODO create another method to calculate manually, hmm
        LocalDate tanggalSkrg;
        LocalDate tanggalArg;
        int umur;
        int tahunLhr;
        int bulanLhr;
        int hariLhr;

        // hari ini
        tanggalSkrg = LocalDate.now();

        // tanggal lahir
        String[] arr = tanggal.split("-");
        tahunLhr = Integer.parseInt(arr[2]);
        bulanLhr = Integer.parseInt(arr[1]);
        hariLhr = Integer.parseInt(arr[0]);
        tanggalArg = LocalDate.of(tahunLhr, bulanLhr, hariLhr);

        // umur
        umur = Period.between(tanggalArg, tanggalSkrg).getYears();
        if (umur < 0)
            return -1; // belum lahir

        // sudah lahir
        return umur;
    }

    public static double hitungBMI(double tinggi, double berat) {
        /**
         * @param
         * */
        // Rumus menghitung BMI = berat badan kg / tinggi badan*tinggi badan m^2
        // normalisasi tinggi cm ke meter

        return berat * 10e3 / (tinggi * tinggi);
    }

    public static int hitungLulus(int tahunLulus){
        // tahun sekarang
        int tahunSkrg = LocalDate.now().getYear();

        // sudah lulus
        if (tahunLulus <= tahunSkrg){
            return 0;
        }
        // belum lulus
        return tahunLulus - tahunSkrg;
    }

}
