/**
 * Created by wisnuprama on 2/26/2017.
 */

import java.util.Scanner;
public class Result {
    // constructor
    public static void main(String[] args) {
        // declaration
        boolean FLAG;
        String namaPerusahaan;
        String nama;
        String ttl;
        String golDarah = " ";
        double berat = 0;
        double tinggi = 0;
        long nik;
        Scanner input = new Scanner(System.in);

        // perusahaan
        namaPerusahaan = "iSUS";
        Main prediksi = new Main(namaPerusahaan);
        //----------------

        System.out.printf("Program Wawancara Perusahaan %s\n", namaPerusahaan);
        System.out.println("---------------------------------");
        // nama
        System.out.print("Nama : ");
        nama = input.nextLine();

        // ttl
        // @TODO check input is in correct format
        System.out.print("Tempat/Tanggal Lahir : ");
        ttl = input.nextLine();

        // NIK
        System.out.print("NIK : ");
        nik = Long.parseLong(input.nextLine());

        // tinggi
        // @TODO check input is between 150-200
        FLAG = true;
        while (FLAG) {
            System.out.print("Tinggi : ");
            tinggi = Double.parseDouble(input.nextLine());

            if (tinggi >= 150 && tinggi <= 200)
                FLAG = false;
            else {
                System.out.println("Tinggi antara 150-200 cm");
            }
        }

        // berat
        // @TODO check input is between 30-80
        FLAG = true;
        while (FLAG) {
            System.out.print("Berat Badan : ");
            berat = Double.parseDouble(input.nextLine());

            if (berat >= 30 && berat <= 80)
                FLAG = false;
            else {
                System.out.println("Berat antara 30-80 kg");
            }
        }

        // golongan darah
        // @TODO check input is only A, AB, B, O
        // use switch?
        FLAG = true;
        while (FLAG) {
            System.out.print("Golongan Darah : ");
            golDarah = input.nextLine().toUpperCase();

            if (golDarah.equals("A") || golDarah.equals("AB")
                    || golDarah.equals("B") || golDarah.equals("O"))
                FLAG = false;
        }

        // create object interviewee
        Internship interviewee = new Internship(nama, ttl, nik, berat, tinggi, golDarah);

        // ask: pengalaman interviewee
        System.out.print("Pengalaman : ");
        interviewee.setPengalaman(input.nextLine());

        // ask: pendidikan interviewee
        System.out.print("Pendidikan saat ini : ");
        interviewee.setPendidikan(input.nextLine());

        // ask: perkiraan lulus
        System.out.print("Perkiraan lulus : ");
        interviewee.setTahunLulus(Integer.parseInt(input.nextLine()));

        // ask: jomblo interviewee
        System.out.print("Jomblo : ");
        interviewee.setJomblo(Boolean.parseBoolean(input.nextLine()));

        // print data interviewee dan hasil interview
        System.out.println(interviewee.getData());
        System.out.println(prediksi.result(interviewee));
    }

}

class Main {
    // init
    private String namaPerusahaan;
    private final boolean JOMBLO = false;
    private final String GOL_AB = "AB";
    private final int MINIMUM_MAGANG = 1; // tahun terakhir

    public Main(String namaPerusahaan) {
        this.namaPerusahaan = namaPerusahaan;
    }

    public String result(Internship interviewee){
        /**
         * @TODO prediksi
         PREDIKSI
         -----------------------------------
         Nilai iSUS [Nilai iSUS]
         Memenuhi [Jumlah Terpenuhi] dari 5 syarat
         [Nama] [diterima/ditolak]
         */
        String namaInterviewee;
        long nilaiIsus;
        int syarat = 0;
        StringBuilder stringBuilder = new StringBuilder();

        namaInterviewee = interviewee.getNama();
        nilaiIsus = hitungIsus(interviewee.getUmur(),
                                interviewee.getTinggi(),
                                interviewee.getBerat());

        // prediksi
        if (prediksiNama(namaInterviewee)) syarat++;
        if (prediksiBMI(interviewee.getBMI())) syarat++;
        if (prediksiGolDarah(interviewee.getGolDarah())) syarat++;
        if (prediksiLulus(interviewee.getTahunLulus())) syarat++;
        if (prediksiJomblo(interviewee.isJomblo())) syarat++;

        String penerimaan = "ditolak";
        if (syarat >= 3)
            penerimaan = "diterima";

        // print
        stringBuilder.append(String.format("\nPREDIKSI\n-----------------------------------"));
        stringBuilder.append(String.format("Nilai %s %d\nMemenuhi %d dari 5 syarat\n", this.namaPerusahaan, nilaiIsus, syarat));
        stringBuilder.append(String.format("%s %s", namaInterviewee, penerimaan));

        return stringBuilder.toString();
    }

    private boolean prediksiNama(String nama){
        int jumlahChar = 0;
        String[] kata = nama.split(" ");

        // hitung char perkata
        for (String i:kata){
            jumlahChar += i.length();
        }

        return jumlahChar % 2 == 1;
    }

    private boolean prediksiBMI(double bmi){
        // true jika bmi normal 18-20.~ sekian
        return bmi >= 18.0 && bmi <= 20.0;
    }

    private boolean prediksiGolDarah(String golDarah){
        // return true jika golongan darah AB
        return golDarah.equals(this.GOL_AB);
    }

    private boolean prediksiLulus(int tahunLulus){
        // return true jika kelulusan tinggal 0-1 tahun lagi
        return Calculator.hitungLulus(tahunLulus) <= this.MINIMUM_MAGANG;
    }

    private boolean prediksiJomblo(boolean jomblo){
        // return TRUE jika interviewee tidak jomblo
        return jomblo == this.JOMBLO;
    }

    private long hitungIsus(int umur, double tinggi, double berat){
        long nilai = umur * umur * (int) berat * (int) tinggi * (int) tinggi;
        return nilai;
    }
}
