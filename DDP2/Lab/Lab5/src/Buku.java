/**
 * Created by wisnuprama on 3/12/2017.
 */
public class Buku {
    private String idBuku;
    private String genre;
    private String judulBuku;
    public final static int MINIMAL_ID_BUKU = 5;

    public Buku(String idBuku, String genre, String judulBuku) {
        this.idBuku = idBuku;
        this.genre = genre;
        this.judulBuku = judulBuku;
    }

    public String toString(){
        return this.judulBuku + "," + this.idBuku + "," + this.genre;
    }

    public String getIdBuku() {
        return idBuku;
    }

    public String getGenre() {
        return genre;
    }

    public String getJudulBuku() {
        return judulBuku;
    }
}
