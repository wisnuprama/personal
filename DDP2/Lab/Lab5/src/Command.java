import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Created by wisnuprama on 3/13/2017.
 */
public class Command {
    private String[] cmd;

    public Command(String cmd) {
        this.cmd = cmd.split(" ");
    }

    public String[] get(){
        return cmd;
    }

    public String get(int index){
        try{
            return cmd[index];
        } catch (IndexOutOfBoundsException e){
            return " ";
        }

    }

    /**
     *
     * @param start inclusive
     * @return String of subCommand
     */
    public String subToLast(int start){
        try{
            StringBuilder stringBuilder = new StringBuilder();

            for(int i=start; i < this.cmd.length; ++i){
                stringBuilder.append(this.cmd[i] + " ");
            }
            return stringBuilder.substring(0,stringBuilder.length()-1);
        } catch (IndexOutOfBoundsException e){
            return " ";
        }

    }
}
