/**
 * Created by wisnuprama on 3/12/2017.
 */
public class Lemari {
    public final static int MIN_BUKU = 1;
    public final static int MAX_BUKU = 30;

    private Buku[] koleksiBuku;
    public static int totalBuku = 0;
    private int nomorLemari;
    private int valueSize;

    public Lemari(int nomorLemari, int jumlahBuku) {
        this.nomorLemari = nomorLemari;
        this.koleksiBuku = new Buku[jumlahBuku];
        this.valueSize = 0;
    }

    public void add(Buku buku) {

        this.koleksiBuku[this.valueSize] = buku;
        ++this.valueSize;
        // static jumlah lemari, bertambah setiap kali lemari dibuat
        ++Lemari.totalBuku;
    }

    public Buku get(int index){
        return this.koleksiBuku[index];
    }

    public boolean isEmpty(){
        //
        for(Buku obj:this.koleksiBuku){
            if(obj != null)
                return false;
        }
        return true;
    }

    public boolean isFull(){
        return this.valueSize == this.koleksiBuku.length;
    }

    public boolean contains(String idBuku){
        if(this.isEmpty())
            return false;

        for(Buku bk:this.koleksiBuku){
            // if null
            if(bk == null)
                continue;

            String bkTmp = bk.getIdBuku();
            if(bkTmp.equals(idBuku))
                return true;
        }
        // tidak ada buku yang sama
        return false;
    }

    @Override
    public String toString(){
        if(this.isEmpty())
            return "Tidak ada buku di lemari " + this.nomorLemari + "\n";

        StringBuilder stringBuilder = new StringBuilder();
        for(Buku bk:koleksiBuku){
            if(bk != null)
                stringBuilder.append(bk.toString()+"\n");
        }
        return stringBuilder.toString();
        //return stringBuilder.substring(0, stringBuilder.length()-1);
    }

    public Buku[] getKoleksiBuku() {
        return koleksiBuku;
    }

    public int getNomorLemari() {
        return nomorLemari;
    }

    public int size(){
        return this.valueSize;
    }
}
