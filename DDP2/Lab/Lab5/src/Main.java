
import java.util.Scanner;

/**
 * Created by wisnuprama on 3/12/2017.
 */
public class Main {
    private final static int MAX_CMD = 100;
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);
        Command[] arrCmd = new Command[MAX_CMD];
        int valueSize = 0;

        while(input.hasNextLine()){
            // parsing input
            Command line = new Command(input.nextLine());
            arrCmd[valueSize++] = line;
        }
        // print the output
        System.out.println(output(arrCmd));
        input.close();
    }

    public static String output(Command[] arrCmd){
        // declare
        String idBuku;
        String genre;
        String judul;
        int nomorLemari;
        Perpustakaan perpusPacil;

        // init string builder
        StringBuilder stringBuilder = new StringBuilder();

        // membuat perpustakaan pertama kali
        Command createPerpus = arrCmd[0];
        int nLemari = Integer.parseInt(createPerpus.get(0));
        int nBuku = Integer.parseInt(createPerpus.get(1));

        // check jumlah lemari dan jumlah buku yang ingin diinisiasi
        if(nLemari >= Perpustakaan.MIN_LEMARI && nLemari <= Perpustakaan.MAX_LEMARI
                            && nBuku >= Lemari.MIN_BUKU && nBuku <= Lemari.MAX_BUKU){
            perpusPacil = new Perpustakaan(nLemari, nBuku);
        }
        else {
            // default full
            perpusPacil = new Perpustakaan(10, 30);
        }
        // run command
        for(int i=1; i < MAX_CMD; ++i){
            // get object command from array
            Command cmd = arrCmd[i];

            // if null break
            if(cmd == null)
                break;

            // get first word
            String cmdType = cmd.get(0).toUpperCase();
            switch (cmdType){
                case "TAMBAH":
                    idBuku = cmd.get(1);
                    genre = cmd.get(2);
                    nomorLemari = Integer.parseInt(cmd.get(3));
                    judul = cmd.subToLast(4);

                    // get lemari
                    Lemari lm = perpusPacil.get(nomorLemari);

                    // check judul buku dan apakah lemari n sudah ada buku tersebut
                    if(judul.equals(" ") || lm.contains(idBuku)){
                        stringBuilder.append("Judul Buku tidak ditemukan atau ditemukan buku yang sama (ID UNIK)!\n");
                        break;
                    }

                    // check jika id tedapat letter dan panjang id kurang dari minimal id buku
                    if(idBuku.matches("[0-9]+") == false || idBuku.length() < Buku.MINIMAL_ID_BUKU){
                        stringBuilder.append("ID Buku terdapat alphabet atau jumlah ID kurang dari MINIMAL\n");
                        break;
                    }

                    // buat buku baru dan tambahkan
                    Buku newBuku = new Buku(idBuku, genre, judul);
                    stringBuilder.append(perpusPacil.addBuku(nomorLemari, newBuku));
                    break;

                case "CARI":
                    // ambil genre
                    genre = cmd.get(1);
                    // search genre
                    stringBuilder.append(perpusPacil.search(genre));
                    break;

                case "LIST":
                    //
                    nomorLemari = Integer.parseInt(cmd.get(1));
                    String tmp = perpusPacil.get(nomorLemari).toString();
                    stringBuilder.append(tmp);
                    break;
                case "INFO":
                    // tambah info perpus pacil
                    stringBuilder.append(perpusPacil.toString());
                    break;
                case "+LEMARI":
                    perpusPacil.addLemari();
                    stringBuilder.append("Lemari berhasil ditambah\n");
                    break;
                default:
                    System.err.println("COMMAND TIDAK DITEMUKAN");

            }
        }

        return stringBuilder.toString();
    }
}
