/**
 * Created by wisnuprama on 3/12/2017.
 */

public class Perpustakaan {
    public final static int MIN_LEMARI = 1;
    public final static int MAX_LEMARI = 10;
    private Lemari[] arrLemari;
    private int jumlahBuku;
    private int valueSize;

    public Perpustakaan(int nLemari, int mBuku) {
        this.arrLemari = new Lemari[MAX_LEMARI];
        this.valueSize = nLemari;
        this.jumlahBuku = mBuku;
        this.create(mBuku);

    }

    private void create(int mBuku){
        for(int i=0; i<this.valueSize; ++i){
            this.arrLemari[i] = new Lemari(i+1, mBuku);
        }
    }

    public Lemari get(int nomor){
        return this.arrLemari[nomor-1];
    }

    public Lemari[] getArrLemari(){
        return this.arrLemari;
    }

    public String search(String genre){
        // linear search by GENRE
        StringBuilder stringBuilder = new StringBuilder();
        
        for(int i=0; i<this.valueSize; ++i){
            Lemari lm = this.arrLemari[i];
            for(int ii=0; ii<lm.size(); ++ii){
                Buku bk = lm.getKoleksiBuku()[ii];
                String tmp = bk.getGenre().toLowerCase();

                if(tmp.equals(genre.toLowerCase())){
                        stringBuilder.append(bk.getJudulBuku() + ", " + bk.getIdBuku() + ", terdapat di lemari "
                                + lm.getNomorLemari() + "\n");
                }
            }
        }
        
        /**
        for(Lemari lm:this.arrLemari)
            if(lm != null){
                for(Buku bk:lm.getKoleksiBuku()){
                    if(bk == null)
                        break;
                    

                    // jika genre sama ditambahkan
                    if(tmp.equals(genre.toLowerCase())){
                        stringBuilder.append(bk.getJudulBuku() + ", " + bk.getIdBuku() + ", terdapat di lemari "
                                + lm.getNomorLemari() + "\n");
                    }
                }
                 */
            }

        if(stringBuilder.toString().equals(""))
            return "Tidak ada buku dengan genre tersebut\n";

        return stringBuilder.toString();
        //return stringBuilder.substring(0, stringBuilder.length()-1);
    }

    public String addBuku(int nomorLemari, Buku buku){
        Lemari tmpLm = this.get(nomorLemari);
        if(tmpLm.isFull()){
            return "Lemari " + tmpLm.getNomorLemari() + " sudah penuh!\n";
        }
        // tambah buku
        tmpLm.add(buku);
        return "Buku " + buku.getJudulBuku() + " ditambakan pada lemari " + tmpLm.getNomorLemari() + "\n";
    }

    public void addLemari(){
        try{
            // add, then ++
            this.arrLemari[this.valueSize++] = new Lemari(this.valueSize, this.jumlahBuku);
        } catch(IndexOutOfBoundsException e){
            // untuk debugging
            System.err.println(e.getMessage());
        }

    }

    public String toString(){
        return "Perpusatakaan terdiri dari " + this.valueSize + " lemari dengan jumlah buku yang " +
                "terdapat di lemari sebanyak " + Lemari.totalBuku + " Buku\n";
    }
}
