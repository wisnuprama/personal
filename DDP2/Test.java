import java.util.Arrays;

public class Test
{
   public static void main(String[] args){
        // code here
        int[] ar = {2,4,6,3,10,3,19,3,5,6,6,44,72,29};
        System.out.print(Test.isDuplicate(ar));
    }
    public static boolean isDuplicate(int[] arr){
            for(int i = 0; i < arr.length; ++i){
                for(int j = i+1; j < arr.length; ++j){
                    if(arr[i] == arr[j]){
                        return true;
                    }
                }
            }
            return false;
        }
    
    public static boolean isDuplicate2(int[] arr){
        Arrays.sort(arr);
        for(int i = 0; i < arr.length; ++i){
            System.out.println(arr[i]+" "+arr[i+1]);
            if(arr[i] == arr[i+1])
                return true;
        }
        return false; 
    }
}