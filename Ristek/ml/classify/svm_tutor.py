from sklearn import datasets
from sklearn import svm
import matplotlib.pyplot as plt

# LOAD DATA
## iris datasets
iris_data = datasets.load_iris()
## mnist digit dataset
digits = datasets.load_digits()

# LEARNING
## untuk parameter gamma ada baiknya kita mencari
## menggunakan grid search atau cross validation
## default kernelnya RBF
clf = svm.SVC(gamma=0.001, C=100, degree=2) # classifier

## classifier di fit ke model, dan learn dari model yang ada
## training set di fit ke classifier 
## menggunakan semua data, kecuali data terakhir
### menggunakan data iris
x, y = iris_data.data, iris_data.target_names[iris_data.target]
clf.fit(x, y)

# PREDICTING
print(clf.predict(x[-1:]))