"""
http://mnemstudio.org/clustering-k-means-introduction.htm
https://en.wikipedia.org/wiki/K-means_clustering


This outline of the algorithm assumes two clusters, and each individual's 
scores include two variables (as in the example above).
Adding more clusters is as easy as adding another step like Step 4 or Step 5.  
Adding another variable for each individual is as easy as adding 
calculations within the type of step like Step 4 or Step 5.

Step 1:
Choose the number of clusters.

Step 2:
Set the initial partition, and the initial mean vectors for each cluster.

Step 3:
For each remaining individual...

Step 4:
Get averages for comparison to the Cluster 1:
Add individual's A value to the sum of A values of the individuals in Cluster 1, 
then divide by the total number of scores that were summed.

Add individual's B value to the sum of B values of the individuals in Cluster 1, 
then divide by the total number of scores that were summed.

Step 5:
Get averages for comparison to the Cluster 2:
Add individual's A value to the sum of A values of the individuals in Cluster 2, 
then divide by the total number of scores that were summed.

Add individual's B value to the sum of B values of the individuals in Cluster 2, 
then divide by the total number of scores that were summed.

Step 6:
If the averages found in Step 4 are closer to the mean values of Cluster 1, 
then this individual belongs to Cluster 1, and the averages found now become the new mean vectors for Cluster 1.

If closer to Cluster 2, then it goes to Cluster 2, along with the averages as new mean vectors.

Step 7:
If there are more individual's to process, continue again with Step 4.  Otherwise go to Step 8.

Step 8:
Now compare each individual’s distance to its own cluster's mean vector, 
and to that of the opposite cluster.  The distance to its cluster's mean vector should be smaller 
than it distance to the other vector.  If not, relocate the individual to the opposite cluster.

Step 9:
If any relocations occurred in Step 8, the algorithm must continue again with Step 3, 
using all individuals and the new mean vectors.
If no relocations occurred, stop.  Clustering is complete.

Again, in case the algorithm never settles on a final solution, 
it may be a good idea to implement a maximum number of iterations check.

"""
import random
import math
import matplotlib.pyplot as plt
import csv

class Data:
    global INFINITE_CLUSTER
    INFINITE_CLUSTER = 2 ** 64 - 1

    def __init__(self, X=0, y=0):
        # list of coords
        self.x = X
        self.y = y
        self.num_cluster = INFINITE_CLUSTER

    def __repr__(self) -> str:
        return "Data=(X:{}, y:{})".format(self.x, self.y)

    def get_X(self):
        return self.x

    def get_y(self):
        return self.y

    def set_cluster(self, num):
        self.num_cluster = num

    def get_cluster(self):
        return self.num_cluster

class Centroid:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def set_X(self, x):
        self.x = x

    def get_X(self):
        return self.x

    def set_y(self, y):
        self.y = y

    def get_y(self):
        return self.y

    def __repr__(self):
        return ("Centroid=(x:{}, y:{})".format(self.x, self.y))

class K_Means:
    "K-Means algorithm for only two-dimensional data"
    def __init__(self, k=2):
        self.is_moving = False
        self.number_of_cluster = k
        self.data = []
        self.centroid = []
        self.init_group = {}
        self.total_data = 2 ** 64 -1

    def _init_centroids_(self):
        set_rand = []
        for i in range(self.number_of_cluster):
            self.init_group[i] = i
            rand_i =  random.randint(0, len(self.data))

            if rand_i not in set_rand:
                d = self.data[rand_i]
                c = Centroid(d.get_X(), d.get_y())
                self.centroid.append(c)
                set_rand.append(rand_i)

        print(self.centroid)
        print(self.number_of_cluster)

    def _init_data_(self):
        for i in range(len(self.data)):
            d = self.data[i]

            key = self.init_group.keys()
            if i in key:
                cls = self.init_group[i]
                d.set_cluster(cls)
            else:
                d.set_cluster(None)

    def _distance_(self, data_i=Data(), centroid=Centroid()):
        "EUCLID: https://en.wikipedia.org/wiki/Euclidean_distance"
        centroid_x, centroid_y = centroid.get_X(), centroid.get_y()
        data_x, data_y = data_i.get_X(), data_i.get_y()
        return math.sqrt((centroid_x - data_x)**2 + (centroid_y - data_y)**2)

    def _calculate_centroids(self):
        total_x, total_y, total_in_cluster = 0,0,0

        for i in range(self.number_of_cluster):
            for j in range(len(self.data)):
                if(self.data[j].get_cluster()):
                    total_x+=self.data[j].get_X()
                    total_y+=self.data[j].get_y()
                    total_in_cluster+= 1

                if(total_in_cluster > 0):
                    self.centroid[i].set_X(total_x/total_in_cluster)
                    self.centroid[i].set_y(total_y/total_in_cluster)

    def _update_cluster(self):
        self.is_moving = False

        for i in range(len(self.data)):
            best_min = 2**64 -1
            current_cluster = 0
            data_i = self.data[i]

            for j in range(self.number_of_cluster):
                dist = self._distance_(data_i, self.centroid[j])

                if(dist < best_min):
                    best_min = dist
                    current_cluster = j

            data_i.set_cluster(current_cluster)

            if(data_i.get_cluster() is None or data_i.get_cluster() != current_cluster):
                data_i.set_cluster(current_cluster)
                self.is_moving = True

    def fit(self, data_x=list(), data_y=list()):
        if len(data_x) != len(data_y):
            raise ValueError()

        for i in range(len(data_x)):
            data_i = Data(data_x[i], data_y[i])
            self.data.append(data_i)

        self.is_moving = True
        self._init_centroids_()
        self._init_data_()

        while self.is_moving:
            self._calculate_centroids()
            self._update_cluster()

    def show(self):
        import matplotlib.pyplot as plt

        for i in range(self.number_of_cluster):
            print("Cluster ", i)
            for j in range(len(self.data)):
                data_j = self.data[j]
                if(data_j.get_cluster() == i):
                    print(data_j)
            print()


if __name__ == "__main__":
    km = K_Means(3)

    data_x, data_y = [],[]

    with open("Iris.csv", 'r') as file:
        reader = csv.reader(file)
        next(reader, None)
        for row in reader:
            # gather data
            x = float(row[1]) * float(row[2])
            y = float(row[3]) * float(row[4])

            # add data
            data_x.append(x)
            data_y.append(y)

    km.fit(data_x, data_y)
    km.show()
